all: frontend/gumble.js frontend/stockfish.js

frontend/stockfish.js:
	git -C stockfish.js checkout -- src/Makefile
	sed -i -e 's/^CXXFLAGS += -DANTI/#/' -e 's/^CXXFLAGS += -DSKILL/#/' stockfish.js/src/Makefile
	make -C stockfish.js/src clean
	make -C stockfish.js/src build COMP=emscripten ARCH=js
	cat stockfish.js/preamble.js stockfish.js/src/stockfish.js > $@
	make -C stockfish.js/src clean
	make -C stockfish.js/src build COMP=emscripten ARCH=wasm
	git -C stockfish.js checkout -- src/Makefile
	cp stockfish.js/src/stockfish.wasm frontend
	cat stockfish.js/preamble.js stockfish.js/src/stockfish.js > frontend/stockfish.wasm.js

frontend/gumble.js:
	cd gumble && make clean test/PerftSuite.cmake
	mkdir -p gumble/build gumble/build-js
	cd gumble/build && cmake .. && make enginecore gumble
	cd gumble/build-js && CFLAGS="-Oz -DNDEBUG" emcmake cmake .. && emmake make enginecore
	emcc -Oz --memory-init-file 0 -s EXTRA_EXPORTED_RUNTIME_METHODS="['getValue']" -s EXPORTED_FUNCTIONS="['_cch_init_board', '_cch_load_fen', '_cch_save_fen', '_cch_play_legal_move', '_cch_parse_lan_move', '_cch_format_lan_move', '_cch_format_san_move', '_cch_generate_moves', '_cch_is_move_legal', '_cch_is_square_checked']" gumble/build-js/src/libenginecore.a -o $@

clean:
	make -C stockfish.js/src clean
	make -C gumble clean
	rm -f frontend/gumble.js frontend/stockfish.js

.PHONY: all clean
