A00	Amar Gambit		rn1qkbnr/ppp2ppp/8/3p4/5p2/6PB/PPPPP2P/RNBQK2R w
A00	Amar Opening: Paris Gambit		rnbqkbnr/ppp2ppp/8/3pp3/5P2/6PN/PPPPP2P/RNBQKB1R b
A00	Anderssen Opening		rnbqkbnr/pppppppp/8/8/8/P7/1PPPPPPP/RNBQKBNR b
A00	Anderssen Opening: Polish Gambit		rnbqkbnr/1ppppppp/8/p7/1P6/P7/2PPPPPP/RNBQKBNR b
A00	Barnes Opening: Fool's Mate		rnb1kbnr/pppp1ppp/8/4p3/6Pq/5P2/PPPPP2P/RNBQKBNR w
A00	Barnes Opening: Gedult Gambit #2		rnbqkbnr/ppppp1pp/8/8/4p3/2N2P2/PPPP2PP/R1BQKBNR b
A00	Barnes Opening: Walkerling		rnbqkb1r/pppp1ppp/5n2/4p3/2B1P3/5P2/PPPP2PP/RNBQK1NR b
A00	Clemenz Opening		rnbqkbnr/pppppppp/8/8/8/7P/PPPPPPP1/RNBQKBNR b
A00	Clemenz Opening: Spike Lee Gambit		rnbqkbnr/ppppppp1/8/7p/6P1/7P/PPPPPP2/RNBQKBNR b
A00	Crab Opening		rnbqkbnr/pppp1ppp/8/4p3/P6P/8/1PPPPPP1/RNBQKBNR b
A00	Creepy Crawly Formation: Classical Defense		rnbqkbnr/ppp2ppp/8/3pp3/8/P6P/1PPPPPP1/RNBQKBNR w
A00	Formation: Hippopotamus Attack		r1bq1rk1/ppp2ppp/2nb1n2/3pp3/8/PPPPPPP1/7P/RNBQKBNR b
A00	Gedult's Opening		rnbqkbnr/pppppppp/8/8/8/5P2/PPPPP1PP/RNBQKBNR b
A00	Global Opening		rnbqkbnr/pppp1ppp/8/4p3/8/P6P/1PPPPPP1/RNBQKBNR b
A00	Grob Opening		rnbqkbnr/pppppppp/8/8/6P1/8/PPPPPP1P/RNBQKBNR b
A00	Grob Opening: Alessi Gambit		rnbqkbnr/ppppp1pp/8/5p2/6P1/8/PPPPPP1P/RNBQKBNR w
A00	Grob Opening: Double Grob		rnbqkbnr/pppppp1p/8/6p1/6P1/8/PPPPPP1P/RNBQKBNR w
A00	Grob Opening: Double Grob, Coca-Cola Gambit		rnbqkbnr/pppppp1p/8/6p1/5PP1/8/PPPPP2P/RNBQKBNR b
A00	Grob Opening: Grob Gambit		rnbqkbnr/ppp1pppp/8/3p4/6P1/8/PPPPPPBP/RNBQK1NR b
A00	Grob Opening: Grob Gambit Declined		rnbqkbnr/pp2pppp/2p5/3p4/6P1/8/PPPPPPBP/RNBQK1NR w
A00	Grob Opening: Grob Gambit, Basman Gambit		rnbqkbnr/ppp1ppp1/8/3p3P/8/8/PPPPPPBP/RNBQK1NR b
A00	Grob Opening: Grob Gambit, Fritz Gambit		rn1qkbnr/ppp1pppp/8/3p4/2P3b1/8/PP1PPPBP/RNBQK1NR b
A00	Grob Opening: Grob Gambit, Richter-Grob Gambit		rnbqkbnr/pp2pppp/2p5/8/2p3P1/1P6/P2PPPBP/RNBQK1NR b
A00	Grob Opening: Keene Defense		rnbqkbnr/pp3ppp/2p5/3pp3/6P1/7P/PPPPPPB1/RNBQK1NR w
A00	Grob Opening: Keene Defense, Main Line		rnbqk2r/pp2nppp/2pb4/3p4/2PPp1P1/2N4P/PP2PPB1/R1BQK1NR w
A00	Grob Opening: London Defense		r1bqkbnr/pppp1ppp/2n5/4p3/6P1/7P/PPPPPP2/RNBQKBNR w
A00	Grob Opening: Spike Attack		rnbqkbnr/pp2pppp/2p5/3p2P1/8/8/PPPPPPBP/RNBQK1NR b
A00	Grob Opening: Spike, Hurst Attack		rnbqkbnr/ppp2ppp/8/3pp3/2P3P1/8/PP1PPPBP/RNBQK1NR b
A00	Grob Opening: Zilbermints Gambit		rnbqkbnr/ppp1pppp/8/8/4p1P1/2N5/PPPP1P1P/R1BQKBNR b
A00	Hungarian Opening		rnbqkbnr/pppppppp/8/8/8/6P1/PPPPPP1P/RNBQKBNR b
A00	Hungarian Opening: Buecker Gambit		rnbqkbnr/ppp2ppp/8/3pp3/1P6/6P1/P1PPPPBP/RNBQK1NR b
A00	Hungarian Opening: Catalan Formation		rnbqkbnr/ppp2ppp/4p3/3p4/8/6P1/PPPPPPBP/RNBQK1NR w
A00	Hungarian Opening: Dutch Defense		rnbqkbnr/ppppp1pp/8/5p2/8/6P1/PPPPPP1P/RNBQKBNR w
A00	Hungarian Opening: Indian Defense		rnbqkb1r/pppppppp/5n2/8/8/6P1/PPPPPP1P/RNBQKBNR w
A00	Hungarian Opening: Myers Defense		rnbqkbnr/pppppp1p/8/6p1/8/6P1/PPPPPP1P/RNBQKBNR w
A00	Hungarian Opening: Pachman Gambit		rqbk2r1/pp5p/3N4/6p1/2QR4/4P1P1/1P3P1P/R1B3K1 b
A00	Hungarian Opening: Paris Gambit		rn1qkbnr/ppp2ppp/8/3p4/8/6pB/PPPPP2P/RNBQ1RK1 w
A00	Hungarian Opening: Reversed Alekhine		rnbqkbnr/pppp1ppp/8/4p3/8/5NP1/PPPPPP1P/RNBQKB1R b
A00	Hungarian Opening: Reversed Modern Defense		rnbqkbnr/pp2pppp/8/2pp4/8/6P1/PPPPPPBP/RNBQK1NR w
A00	Hungarian Opening: Reversed Norwegian Defense		rnbqkbnr/pppp1ppp/8/8/4p2N/6P1/PPPPPP1P/RNBQKB1R b
A00	Hungarian Opening: Sicilian Invitation		rnbqkbnr/pp1ppppp/8/2p5/8/6P1/PPPPPP1P/RNBQKBNR w
A00	Hungarian Opening: Slav Formation		rnbqkbnr/pp2pppp/2p5/3p4/8/6P1/PPPPPPBP/RNBQK1NR w
A00	Hungarian Opening: Symmetrical Variation		rnbqkbnr/pppppp1p/6p1/8/8/6P1/PPPPPP1P/RNBQKBNR w
A00	Hungarian Opening: Van Kuijk Gambit		rnbqkbnr/ppppppp1/8/8/7p/5NP1/PPPPPP1P/RNBQKB1R w
A00	Hungarian Opening: Wiedenhagen-Beta Gambit		rnbqkbnr/ppp1pp1p/8/3p2p1/8/5NP1/PPPPPP1P/RNBQKB1R w
A00	Hungarian Opening: Winterberg Gambit		rnbqkbnr/ppp2ppp/8/4p3/2p5/1P4P1/P2PPPBP/RNBQK1NR b
A00	Kadas Opening		rnbqkbnr/pppppppp/8/8/7P/8/PPPPPPP1/RNBQKBNR b
A00	Kadas Opening: Kadas Gambit		rnbqkbnr/pp1ppppp/8/2p5/1P5P/8/P1PPPPP1/RNBQKBNR b
A00	Kadas Opening: Kadas Gambit #3		rnbqkbnr/pppp1ppp/8/8/3p3P/2P5/PP2PPP1/RNBQKBNR b
A00	Kadas Opening: Schneider Gambit		rnbqkbnr/pppppp1p/8/6p1/7P/8/PPPPPPP1/RNBQKBNR w
A00	Lasker Simul Special		rnbqkbnr/ppppppp1/8/7p/8/6P1/PPPPPP1P/RNBQKBNR w
A00	Mieses Opening		rnbqkbnr/pppppppp/8/8/8/3P4/PPP1PPPP/RNBQKBNR b
A00	Mieses Opening: Myers Spike Attack		rnbqkbnr/pppppp1p/6p1/8/6P1/3P4/PPP1PP1P/RNBQKBNR b
A00	Mieses Opening: Reversed Rat		rnbqkbnr/pppp1ppp/8/4p3/8/3P4/PPP1PPPP/RNBQKBNR w
A00	Nimzo-Larsen Attack: Graz Attack		rnbqkbnr/ppp1pppp/8/3p4/8/BP6/P1PPPPPP/RN1QKBNR b
A00	Polish Opening		rnbqkbnr/pppppppp/8/8/1P6/8/P1PPPPPP/RNBQKBNR b
A00	Polish Opening: Baltic Defense		rn1qkbnr/ppp1pppp/8/3p1b2/1P6/8/PBPPPPPP/RN1QKBNR w
A00	Polish Opening: Birmingham Gambit		rnbqkbnr/pp1ppppp/8/2p5/1P6/8/P1PPPPPP/RNBQKBNR w
A00	Polish Opening: Bugayev Advance Variation		rnbqkbnr/pppp2pp/5p2/1P2p3/8/8/PBPPPPPP/RN1QKBNR b
A00	Polish Opening: Bugayev Attack		rnbqkbnr/pppp1ppp/8/4p3/1P6/P7/2PPPPPP/RNBQKBNR b
A00	Polish Opening: Czech Defense		rnbqkbnr/ppp2ppp/3p4/4p3/1P6/8/PBPPPPPP/RN1QKBNR w
A00	Polish Opening: Dutch Defense		rnbqkbnr/ppppp1pp/8/5p2/1P6/8/P1PPPPPP/RNBQKBNR w
A00	Polish Opening: German Defense		rnb1kbnr/ppp1pppp/3q4/3p4/1P6/8/PBPPPPPP/RN1QKBNR w
A00	Polish Opening: Grigorian Variation		r1bqkbnr/pppppppp/2n5/8/1P6/8/P1PPPPPP/RNBQKBNR w
A00	Polish Opening: Karniewski Variation		rnbqkb1r/pppppppp/7n/8/1P6/8/P1PPPPPP/RNBQKBNR w
A00	Polish Opening: King's Indian Variation		rnbqkb1r/pppppp1p/5np1/8/1P6/8/PBPPPPPP/RN1QKBNR w
A00	Polish Opening: King's Indian Variation, Schiffler Attack		rnbqkb1r/pppppp1p/5np1/8/1P2P3/8/PBPP1PPP/RN1QKBNR b
A00	Polish Opening: King's Indian Variation, Sokolsky Attack		rnbq1rk1/ppp1ppbp/3p1np1/8/1PPP4/4PN2/PB3PPP/RN1QKB1R b
A00	Polish Opening: Myers Variation		rnbqkbnr/pp2pppp/2p5/3p4/PP6/8/1BPPPPPP/RN1QKBNR b
A00	Polish Opening: Outflank Variation		rnbqkbnr/pp1ppppp/2p5/8/1P6/8/P1PPPPPP/RNBQKBNR w
A00	Polish Opening: Queen's Indian Variation		rnbqkb1r/p1pp1ppp/1p2pn2/1P6/8/8/PBPPPPPP/RN1QKBNR w
A00	Polish Opening: Queenside Defense		rnbqkb1r/1ppp1ppp/p3pn2/1P6/8/8/PBPPPPPP/RN1QKBNR w
A00	Polish Opening: Schiffler-Sokolsky Variation		rnbqkb1r/ppp2ppp/4pn2/1P1p4/8/4P3/PBPP1PPP/RN1QKBNR b
A00	Polish Opening: Schuehler Gambit		rnbqkbnr/1p1ppppp/8/pp6/4P3/8/PBPP1PPP/RN1QKBNR b
A00	Polish Opening: Symmetrical Variation		rnbqkbnr/p1pppppp/8/1p6/1P6/8/P1PPPPPP/RNBQKBNR w
A00	Polish Opening: Tartakower Gambit		rnbqkbnr/pppp2pp/5p2/4p3/1P2P3/8/PBPP1PPP/RN1QKBNR b
A00	Polish Opening: Wolferts Gambit		rnbqkbnr/pp1p1ppp/8/2p1p3/1P6/8/PBPPPPPP/RN1QKBNR w
A00	Saragossa Opening		rnbqkbnr/pppppppp/8/8/8/2P5/PP1PPPPP/RNBQKBNR b
A00	Sodium Attack		rnbqkbnr/pppppppp/8/8/8/N7/PPPPPPPP/R1BQKBNR b
A00	Sodium Attack: Durkin Gambit		rr6/ppQnkpbp/2p4n/8/8/2PP4/PP2B2P/R1B1K3 w
A00	Valencia Opening		rnbqkbnr/pppp1ppp/8/4p3/8/3P4/PPPNPPPP/R1BQKBNR b
A00	Van Geet Opening		rnbqkbnr/pppppppp/8/8/8/2N5/PPPPPPPP/R1BQKBNR b
A00	Van Geet Opening: Battambang Variation		rnbqkbnr/pppp1ppp/8/4p3/8/P1N5/1PPPPPPP/R1BQKBNR b
A00	Van Geet Opening: Berlin Gambit		r1bqkbnr/ppp1pppp/2n5/3P4/4p3/2N5/PPP2PPP/R1BQKBNR b
A00	Van Geet Opening: Billockus-Johansen Gambit		rnbqk1nr/pppp1ppp/8/2b1p3/8/2N2N2/PPPPPPPP/R1BQKB1R w
A00	Van Geet Opening: Damhaug Gambit		rnbqkbnr/ppp2ppp/8/3pp3/5P2/2N5/PPPPP1PP/R1BQKBNR w
A00	Van Geet Opening: Dougherty Gambit		rnbqkbnr/ppp1pppp/8/8/4p3/2N2P2/PPPP2PP/R1BQKBNR b
A00	Van Geet Opening: Duesseldorf Gambit		rnbqkbnr/pp1ppppp/8/2p5/1P6/2N5/P1PPPPPP/R1BQKBNR b
A00	Van Geet Opening: Dunst-Perrenet Gambit		rnbqkbnr/ppp1pppp/8/8/4p3/2NP4/PPP2PPP/R1BQKBNR b
A00	Van Geet Opening: Gladbacher Gambit		rnbqkbnr/ppp2ppp/8/4p3/8/1PNp4/P1P2PPP/R1BQKBNR w
A00	Van Geet Opening: Grünfeld Defense		rnbqkbnr/ppp2ppp/8/4p3/4N3/8/PPPP1PPP/R1BQKBNR w
A00	Van Geet Opening: Grünfeld Defense, Steiner Gambit		rnbqkbnr/ppp2ppp/8/4p3/4NP2/8/PPPP2PP/R1BQKBNR b
A00	Van Geet Opening: Hector Gambit		rnbqkbnr/ppp1pppp/8/8/2B1p3/2N5/PPPP1PPP/R1BQK1NR b
A00	Van Geet Opening: Hergert Gambit		r1bqkbnr/ppp2ppp/2np4/4P3/8/2N5/PPPPP1PP/R1BQKBNR w
A00	Van Geet Opening: Hulsemann Gambit		rn1qkbnr/ppp2ppp/4b3/3pQ3/8/2N1P3/PPPP1PPP/R1B1KBNR b
A00	Van Geet Opening: Kluever Gambit		rnbqkbnr/ppppp1pp/8/8/4p3/2NP4/PPP2PPP/R1BQKBNR b
A00	Van Geet Opening: Laroche Gambit		rnbqkbnr/p1pppppp/8/1p6/8/2N5/PPPPPPPP/R1BQKBNR w
A00	Van Geet Opening: Liebig Gambit		rnbqkb1r/ppp2ppp/5n2/3pp2Q/8/2N1P3/PPPP1PPP/R1B1KBNR w
A00	Van Geet Opening: Melleby Gambit		rnbqkbnr/pp2pppp/8/2p5/3pNP2/8/PPPPP1PP/R1BQKBNR w
A00	Van Geet Opening: Myers Attack		rnbqkbnr/pppppp1p/6p1/8/7P/2N5/PPPPPPP1/R1BQKBNR b
A00	Van Geet Opening: Napoleon Attack		r1bqkbnr/pppp1ppp/2n5/4p3/3P4/2N2N2/PPP1PPPP/R1BQKB1R b
A00	Van Geet Opening: Novosibirsk Variation		r1bqkbnr/pp1ppppp/2n5/8/7Q/2N5/PPP1PPPP/R1B1KBNR b
A00	Van Geet Opening: Pfeiffer Gambit		rnbqkbnr/ppp2ppp/8/4p3/3p1P2/6N1/PPPPP1PP/R1BQKBNR b
A00	Van Geet Opening: Reversed Nimzowitsch		rnbqkbnr/pppp1ppp/8/4p3/8/2N5/PPPPPPPP/R1BQKBNR w
A00	Van Geet Opening: Reversed Scandinavian		r1bqkbnr/pppp1ppp/2n5/8/Q7/2N5/PPP1PPPP/R1B1KBNR b
A00	Van Geet Opening: Sicilian Two Knights		r1bqkbnr/pp1ppppp/2n5/8/3N4/2N5/PPP1PPPP/R1BQKB1R b
A00	Van Geet Opening: Sleipnir Gambit		rnbqk1nr/ppp2ppp/8/3pp3/1b1P4/2N1P3/PPP2PPP/R1BQKBNR w
A00	Van Geet Opening: Tuebingen Gambit		rnbqkb1r/pppppppp/5n2/8/6P1/2N5/PPPPPP1P/R1BQKBNR b
A00	Van Geet Opening: Venezolana Variation		rnbqkb1r/ppp1pppp/5n2/3p4/8/2NP2P1/PPP1PP1P/R1BQKBNR b
A00	Van Geet Opening: Warsteiner Gambit		8/1pp1R2p/p2k4/4p3/4bbP1/3B3P/PPP5/1K6 w
A00	Van't Kruijs Opening		rnbqkbnr/pppppppp/8/8/8/4P3/PPPP1PPP/RNBQKBNR b
A00	Van't Kruijs Opening: Bouncing Bishop Variation		rnbqkbnr/p2p1ppp/2p5/1p2p3/8/1B2P3/PPPP1PPP/RNBQK1NR w
A00	Van't Kruijs Opening: Keoni-Hiva Gambit, Alua Variation		3r2k1/r4pp1/2pP3p/p1Bn4/R7/P7/1P4PP/2K5 w
A00	Venezolana Opening		r1bqkbnr/pp1ppppp/2n5/2p5/8/2NP2P1/PPP1PP1P/R1BQKBNR b
A00	Ware Opening		rnbqkbnr/pppppppp/8/8/P7/8/1PPPPPPP/RNBQKBNR b
A00	Ware Opening: Wing Gambit		1n6/p1p2p1k/5qpp/1P1Q4/P4N2/4P3/3RK2P/8 w
A01	Nimzo-Larsen Attack		rnbqkbnr/pppppppp/8/8/8/1P6/P1PPPPPP/RNBQKBNR b
A01	Nimzo-Larsen Attack: Classical Variation		rnbqkbnr/ppp1pppp/8/3p4/8/1P6/P1PPPPPP/RNBQKBNR w
A01	Nimzo-Larsen Attack: Dutch Variation		rnbqkbnr/ppppp1pp/8/5p2/8/1P6/P1PPPPPP/RNBQKBNR w
A01	Nimzo-Larsen Attack: English Variation		rnbqkbnr/pp1ppppp/8/2p5/8/1P6/P1PPPPPP/RNBQKBNR w
A01	Nimzo-Larsen Attack: Indian Variation		rnbqkb1r/pppppppp/5n2/8/8/1P6/P1PPPPPP/RNBQKBNR w
A01	Nimzo-Larsen Attack: Modern Variation		rnbqkbnr/pppp1ppp/8/4p3/8/1P6/P1PPPPPP/RNBQKBNR w
A01	Nimzo-Larsen Attack: Modern Variation #2		rnbqkbnr/pppp1ppp/8/4p3/8/1P6/PBPPPPPP/RN1QKBNR b
A01	Nimzo-Larsen Attack: Modern Variation #3		r1bqkbnr/pppp1ppp/2n5/4p3/8/1P6/PBPPPPPP/RN1QKBNR w
A01	Nimzo-Larsen Attack: Modern Variation #4		r1bqkbnr/pppp1ppp/2n5/4p3/8/1P2P3/PBPP1PPP/RN1QKBNR b
A01	Nimzo-Larsen Attack: Norfolk Gambit		rnbqkb1r/pp2pppp/5n2/2pp4/4P3/1P3N2/PBPP1PPP/RN1QKB1R b
A01	Nimzo-Larsen Attack: Norfolk Gambit #2		rnbqkbnr/pp2pppp/8/2pp4/4P3/1P3N2/P1PP1PPP/RNBQKB1R b
A01	Nimzo-Larsen Attack: Pachman Gambit		r1bqkbnr/pppp1ppp/2n5/4p3/5P2/1P6/PBPPP1PP/RN1QKBNR b
A01	Nimzo-Larsen Attack: Ringelbach Gambit		rnbqkbnr/pppp2pp/4p3/5p2/4P3/1P6/PBPP1PPP/RN1QKBNR b
A01	Nimzo-Larsen Attack: Spike Variation		rnbqkb1r/pppppp1p/5np1/8/6P1/1P6/PBPPPP1P/RN1QKBNR b
A01	Nimzo-Larsen Attack: Symmetrical Variation		rnbqkbnr/p1pppppp/1p6/8/8/1P6/P1PPPPPP/RNBQKBNR w
A01	Nimzowitsch-Larsen Attack: Polish Variation		rnbqkbnr/p1pppppp/8/1p6/8/1P6/P1PPPPPP/RNBQKBNR w
A02	Bird Opening		rnbqkbnr/pppppppp/8/8/5P2/8/PPPPP1PP/RNBQKBNR b
A02	Bird Opening: Batavo-Polish Attack		rnbqkb1r/pppppp1p/5np1/8/1P3P2/5N2/P1PPP1PP/RNBQKB1R b
A02	Bird Opening: Dutch Variation, Batavo Gambit		rnbqkbnr/pp2pppp/8/2p5/4pP2/5N2/PPPP2PP/RNBQKB1R w
A02	Bird Opening: Dutch Variation, Dudweiler Gambit		rnbqkbnr/ppp1pppp/8/3p4/5PP1/8/PPPPP2P/RNBQKBNR b
A02	Bird Opening: From's Gambit		rnbqkbnr/pppp1ppp/8/4p3/5P2/8/PPPPP1PP/RNBQKBNR w
A02	Bird Opening: From's Gambit, Bahr Gambit		rnbqkbnr/pppp1ppp/8/4p3/5P2/2N5/PPPPP1PP/R1BQKBNR b
A02	Bird Opening: From's Gambit, Langheld Gambit		rnbqkb1r/ppp2ppp/3P1n2/8/8/8/PPPPP1PP/RNBQKBNR w
A02	Bird Opening: From's Gambit, Lasker Variation		rnbqk1nr/ppp2p1p/3b4/6p1/8/5N2/PPPPP1PP/RNBQKB1R w
A02	Bird Opening: From's Gambit, Lipke Variation		rnbqk2r/ppp2ppp/3b3n/8/3P4/5N2/PPP1P1PP/RNBQKB1R b
A02	Bird Opening: Hobbs Gambit		rnbqkbnr/pppppp1p/8/6p1/5P2/8/PPPPP1PP/RNBQKBNR w
A02	Bird Opening: Hobbs-Zilbermints Gambit		rnbqkbnr/pppppp2/7p/6p1/5P2/5N2/PPPPP1PP/RNBQKB1R w
A02	Bird Opening: Lasker Gambit		rnbqkbnr/pppp2pp/5p2/4P3/8/8/PPPPP1PP/RNBQKBNR w
A02	Bird Opening: Myers Defense		rnbqkbnr/p1pppppp/8/1p6/5P2/8/PPPPP1PP/RNBQKBNR w
A02	Bird Opening: Platz Gambit		rnbqkb1r/ppppnppp/8/4P3/8/8/PPPPP1PP/RNBQKBNR w
A02	Bird Opening: Schlechter Gambit		r1bqkbnr/pppp1ppp/2n5/4P3/8/8/PPPPP1PP/RNBQKBNR w
A02	Bird Opening: Siegener Gambit		rnbqkbnr/pp1p1ppp/8/2p5/3p1P2/2P2N2/PP2P1PP/RNBQKB1R b
A02	Bird Opening: Sturm Gambit		rnbqkbnr/ppp1pppp/8/3p4/2P2P2/8/PP1PP1PP/RNBQKBNR b
A02	Bird Opening: Swiss Gambit		rnbqkb1r/ppppp1pp/5n2/8/4pPP1/2N5/PPPP3P/R1BQKBNR b
A02	Bird Opening: Thomas Gambit		rnbqkb1r/pp2pppp/5n2/2p5/3p1P2/1P2PN2/PBPP2PP/RN1QKB1R b
A02	Bird Opening: Wagner-Zwitersch Gambit		rnbqkbnr/ppppp1pp/8/5p2/4PP2/8/PPPP2PP/RNBQKBNR b
A02	Bird Opening: Williams Gambit		rnbqkb1r/ppp1pppp/5n2/8/4pP2/2N5/PPPPQ1PP/R1B1KBNR b
A02	Bird Opening: Williams-Zilbermints Gambit		rnbqkb1r/ppp1pppp/5n2/8/4pP2/2N5/PPPPN1PP/R1BQKB1R b
A02	System: Double Duck Formation		rnbqkbnr/ppp1p1pp/8/3p1p2/3P1P2/8/PPP1P1PP/RNBQKBNR w
A03	Bird Opening		rnbqkb1r/pppppppp/5n2/8/5P2/8/PPPPP1PP/RNBQKBNR w
A03	Bird Opening: Dutch Variation		rnbqkbnr/ppp1pppp/8/3p4/5P2/8/PPPPP1PP/RNBQKBNR w
A03	Bird Opening: Horsefly Defense		rnbqkb1r/pppppppp/7n/8/5P2/8/PPPPP1PP/RNBQKBNR w
A03	Bird Opening: Lasker Variation		rnbqkb1r/pp2pppp/5n2/2pp4/5P2/4PN2/PPPP2PP/RNBQKB1R w
A03	Bird Opening: Mujannah		rnbqkb1r/pppppppp/5n2/8/2P2P2/8/PP1PP1PP/RNBQKBNR b
A03	Bird Opening: Williams Gambit		rnbqkbnr/ppp1pppp/8/3p4/4PP2/8/PPPP2PP/RNBQKBNR b
A04	Polish Opening: Zukertort System		rnbqkb1r/pppppppp/5n2/8/1P6/5N2/P1PPPPPP/RNBQKB1R b
A04	Réti Opening #2		rnbqkbnr/pppppppp/8/8/8/5N2/PPPPPPPP/RNBQKB1R b
A04	Zukertort Defense: Drunken Cavalry Variation		r1bqkb1r/pppppppp/n6n/8/4P3/5N2/PPPP1PPP/RNBQKB1R w
A04	Zukertort Defense: Kingside Variation		rnbqkb1r/pppppp1p/6pn/8/3P4/5N2/PPP1PPPP/RNBQKB1R w
A04	Zukertort Defense: Sicilian Knight Variation		r1bqkbnr/pp1ppppp/n7/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
A04	Zukertort Opening		r1bqkb1r/pppppppp/2n2n2/8/8/2N2N2/PPPPPPPP/R1BQKB1R w
A04	Zukertort Opening: Arctic Defense		rnbqkbnr/ppppp1pp/5p2/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Arctic Defense, Drunken Knight Variation		rnbqkb1r/pppppnpp/5p2/8/3PP3/5N2/PPP2PPP/RNBQKB1R w
A04	Zukertort Opening: Basman Defense		rnbqkbnr/ppppppp1/7p/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Black Mustang Defense		r1bqkbnr/pppppppp/2n5/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Dutch Variation		rnbqkbnr/ppppp1pp/8/5p2/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Herrstrom Gambit		rnbqkbnr/pppppp1p/8/6p1/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Kingside Fianchetto		rnbqkbnr/pppppp1p/6p1/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Lisitsyn Gambit		rnbqkbnr/ppppp1pp/8/5p2/4P3/5N2/PPPP1PPP/RNBQKB1R b
A04	Zukertort Opening: Lisitsyn Gambit Deferred		rnbqkb1r/ppppp1pp/5n2/5p2/4P3/3P1N2/PPP2PPP/RNBQKB1R b
A04	Zukertort Opening: Myers Polish Attack		rnbqk2r/ppppppbp/5np1/8/PP6/5N2/2PPPPPP/RNBQKB1R w
A04	Zukertort Opening: Nimzo-Larsen Variation		rnbqkb1r/pppppppp/5n2/8/8/1P3N2/P1PPPPPP/RNBQKB1R b
A04	Zukertort Opening: Pachman Gambit		5rk1/p4ppp/4p3/8/N7/5Pq1/5b2/4KR2 w
A04	Zukertort Opening: Pirc Invitation		rnbqkbnr/ppp1pppp/3p4/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Polish Defense		rnbqkbnr/p1pppppp/8/1p6/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Queen's Gambit Invitation		rnbqkbnr/pppp1ppp/4p3/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Queenside Fianchetto Variation		rnbqkbnr/p1pppppp/1p6/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Quiet System		rnbqkb1r/pppppppp/5n2/8/8/4PN2/PPPP1PPP/RNBQKB1R b
A04	Zukertort Opening: Ross Gambit		rnbqkbnr/pppp1ppp/8/4p3/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Shabalov Gambit		rnbqkbnr/3p1ppp/p3p3/1pp5/2P5/2N2NP1/PP1PPP1P/R1BQKB1R w
A04	Zukertort Opening: Sicilian Invitation		rnbqkbnr/pp1ppppp/8/2p5/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Slav Invitation		rnbqkbnr/pp1ppppp/2p5/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Speelsmet Gambit		rnbqkbnr/pp1ppppp/8/8/3p4/4PN2/PPP2PPP/RNBQKB1R b
A04	Zukertort Opening: St. George Defense		rnbqkbnr/1ppppppp/p7/8/8/5N2/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: The Walrus		r1bqkbnr/ppp2ppp/2p5/8/8/8/PPPPPPPP/RNBQKB1R w
A04	Zukertort Opening: Vos Gambit		rnbqkbnr/ppp2ppp/3p4/4p3/3P4/5N2/PPP1PPPP/RNBQKB1R w
A04	Zukertort Opening: Wade Defense		rn1qkbnr/ppp1pppp/3p4/8/4P1b1/5N2/PPPP1PPP/RNBQKB1R w
A04	Zukertort Opening: Ware Defense		rnbqkbnr/1ppppppp/8/p7/8/5N2/PPPPPPPP/RNBQKB1R w
A05	King's Indian Attack: Smyslov Variation		rnbqkb1r/pppppp1p/5np1/8/1P6/5NP1/P1PPPP1P/RNBQKB1R b
A05	King's Indian Attack: Spassky Variation		rnbqkb1r/p1pppppp/5n2/1p6/8/5NP1/PPPPPP1P/RNBQKB1R w
A05	King's Indian Attack: Symmetrical Defense		rnbqkb1r/pppppp1p/5np1/8/8/5NP1/PPPPPP1P/RNBQKB1R w
A05	King's Indian Attack: Wahls Defense		rnbq1rk1/ppp1ppbp/5np1/3p4/8/3P1NP1/PPP1PPBP/RNBQ1RK1 w
A06	Nimzowitsch-Larsen Attack		rnbqkbnr/ppp1pppp/8/3p4/8/1P3N2/P1PPPPPP/RNBQKB1R b
A06	Réti Opening		rnbqkbnr/ppp1pppp/8/3p4/8/5N2/PPPPPPPP/RNBQKB1R w
A06	Zukertort Opening: Old Indian Attack		rnbqkbnr/ppp1pppp/8/3p4/8/3P1N2/PPP1PPPP/RNBQKB1R b
A06	Zukertort Opening: Reversed Mexican Defense		rnbqkbnr/ppp1pppp/8/3p4/8/2N2N2/PPPPPPPP/R1BQKB1R b
A06	Zukertort Opening: Santasiere's Folly		rnbqkbnr/ppp1pppp/8/3p4/1P6/5N2/P1PPPPPP/RNBQKB1R b
A06	Zukertort Opening: Tennison Gambit		rnbqkbnr/ppp1pppp/8/3p4/4P3/5N2/PPPP1PPP/RNBQKB1R b
A06	Zukertort Opening: The Potato		rnbqkbnr/ppp1pppp/8/3p4/P7/5N2/1PPPPPPP/RNBQKB1R b
A07	King's Indian Attack		rnbqkbnr/ppp1pppp/8/3p4/8/5NP1/PPPPPP1P/RNBQKB1R b
A07	King's Indian Attack: Double Fianchetto		rnbqkbnr/ppp1pp1p/6p1/3p4/8/5NP1/PPPPPP1P/RNBQKB1R w
A07	King's Indian Attack: Keres Variation		r2qkbnr/pppnpppp/8/3p4/6b1/5NP1/PPPPPPBP/RNBQK2R w
A07	King's Indian Attack: Keres Variation #2		rn1qkbnr/ppp1pppp/8/3p4/6b1/5NP1/PPPPPP1P/RNBQKB1R w
A07	King's Indian Attack: Omega-Delta Gambit		rnbqkbnr/ppp2ppp/8/3pp3/8/5NP1/PPPPPP1P/RNBQKB1R w
A07	King's Indian Attack: Pachman System		rnbqk2r/ppp1npbp/6p1/3pp3/8/3P1NP1/PPP1PPBP/RNBQ1RK1 w
A07	King's Indian Attack: Sicilian Variation		rnbqkbnr/pp2pppp/8/2pp4/8/5NP1/PPPPPP1P/RNBQKB1R w
A07	King's Indian Attack: Yugoslav Variation		rn1qkb1r/pp2pppp/2p2n2/3p4/6b1/5NP1/PPPPPPBP/RNBQ1RK1 w
A08	King's Indian Attack		rnbqkb1r/ppp1pppp/5n2/3p4/8/5NP1/PPPPPP1P/RNBQKB1R w
A08	King's Indian Attack: French Variation		r1bqkbnr/pp2pppp/2n5/2pp4/8/5NP1/PPPPPPBP/RNBQK2R w
A08	King's Indian Attack: Sicilian Variation		rnbqkbnr/pp2pppp/8/2pp4/8/5NP1/PPPPPPBP/RNBQK2R b
A08	King's Indian Attack: Sicilian Variation #2		r1bq1rk1/pp2bppp/2n1pn2/2pp4/4P3/3P1NP1/PPPN1PBP/R1BQR1K1 b
A09	Réti Opening		rnbqkbnr/ppp1pppp/8/3p4/2P5/5N2/PP1PPPPP/RNBQKB1R b
A09	Réti Opening: Advance Variation		rnbqkbnr/ppp1pppp/8/8/2Pp4/5N2/PP1PPPPP/RNBQKB1R w
A09	Réti Opening: Advance Variation, Michel Gambit		rnbqkbnr/pp2pppp/8/2p5/1PPp4/5N2/P2PPPPP/RNBQKB1R w
A09	Réti Opening: Reversed Blumenfeld Gambit		rnbqkbnr/pp2pppp/8/2p5/1PPp4/4PN2/P2P1PPP/RNBQKB1R b
A09	Réti Opening: Réti Accepted		rnbqkbnr/ppp1pppp/8/8/2p5/5N2/PP1PPPPP/RNBQKB1R w
A09	Réti Opening: Réti Gambit, Keres Variation		rn1qkbnr/ppp1pppp/4b3/8/2p5/4PN2/PP1P1PPP/RNBQKB1R w
A09	Réti Opening: Zilbermints Gambit		2r3k1/1r1n1p2/p1pPp1p1/2B5/1PR1P3/6P1/5PBP/6K1 w
A10	English Opening		rnbqkbnr/pppppppp/8/8/2P5/8/PP1PPPPP/RNBQKBNR b
A10	English Opening: 2.g4		rnbqkb1r/pppppppp/5n2/8/2P3P1/8/PP1PPP1P/RNBQKBNR b
A10	English Opening: Achilles-Omega Gambit		rnbqkb1r/pppppppp/5n2/8/2P1P3/8/PP1P1PPP/RNBQKBNR b
A10	English Opening: Adorjan Defense		rnbqkbnr/pppp1p1p/6p1/4p3/2P1P3/8/PP1P1PPP/RNBQKBNR w
A10	English Opening: Anglo-Dutch Defense		rnbqkbnr/ppppp1pp/8/5p2/2P5/8/PP1PPPPP/RNBQKBNR w
A10	English Opening: Anglo-Dutch Defense, Hickmann Gambit		rnbqkbnr/ppppp1pp/8/5p2/2P1P3/8/PP1P1PPP/RNBQKBNR b
A10	English Opening: Anglo-Dutch Variation, Chabanon Gambit		rnbqkbnr/ppp1p1pp/3p4/5p2/2P1P3/5N2/PP1P1PPP/RNBQKB1R b
A10	English Opening: Anglo-Dutch Variation, Ferenc Gambit		rnbqkb1r/ppppp1pp/5n2/5p2/2P1P3/2N5/PP1P1PPP/R1BQKBNR b
A10	English Opening: Anglo-Lithuanian Variation		r1bqkbnr/pppppppp/2n5/8/2P5/8/PP1PPPPP/RNBQKBNR w
A10	English Opening: Anglo-Scandinavian Defense		rnbqkbnr/ppp1pppp/8/3p4/2P5/8/PP1PPPPP/RNBQKBNR w
A10	English Opening: Anglo-Scandinavian Defense, Loehn Gambit		rnbqkbnr/ppp2ppp/4p3/3P4/8/8/PP1PPPPP/RNBQKBNR w
A10	English Opening: Anglo-Scandinavian Defense, Malvinas Variation		rnb1kbnr/ppp1pppp/8/q7/8/2N5/PP1PPPPP/R1BQKBNR w
A10	English Opening: Anglo-Scandinavian Defense, Schulz Gambit		rnbqkb1r/ppp1pppp/5n2/3P4/8/8/PP1PPPPP/RNBQKBNR w
A10	English Opening: Great Snake Variation		rnbqkbnr/pppppp1p/6p1/8/2P5/8/PP1PPPPP/RNBQKBNR w
A10	English Opening: Jaenisch Gambit		rnbqkbnr/p1pppppp/8/1p6/2P5/8/PP1PPPPP/RNBQKBNR w
A10	English Opening: Myers Variation		rnbqk1nr/ppppppbp/8/6p1/2PP4/8/PP2PPPP/RNBQKBNR w
A10	English Opening: Porcupine Variation		rnbqkb1r/ppppp1pp/5n2/8/2P1p1P1/2N5/PP1P1P1P/R1BQKBNR b
A10	English Opening: Wade Gambit		rnbqkbnr/ppppp1pp/8/5p2/2P3P1/8/PP1PPP1P/RNBQKBNR b
A10	English Opening: Zilbermints Gambit		rnbqkbnr/pppp1p1p/8/4p1p1/2PP4/8/PP2PPPP/RNBQKBNR w
A11	English Opening: Anglo-Indian Defense, Romanishin Variation		rnbqkb1r/1ppp1ppp/p3pn2/8/2P5/5NP1/PP1PPP1P/RNBQKB1R w
A11	English Opening: Caro-Kann Defensive System		rnbqkbnr/pp1ppppp/2p5/8/2P5/8/PP1PPPPP/RNBQKBNR w
A12	Réti Opening: Anglo-Slav Variation, Bled Variation		rnbqkb1r/pp2pp1p/2p2np1/3p4/2P5/1P3N2/PB1PPPPP/RN1QKB1R w
A12	Réti Opening: Anglo-Slav Variation, Bogoljubov Variation		rnbqkb1r/pp2pppp/2p2n2/3p4/2P5/1P3N2/PB1PPPPP/RN1QKB1R b
A12	Réti Opening: Anglo-Slav Variation, Bogoljubov Variation, II		rn1qkbnr/pp2pppp/2p5/3p4/2P3b1/1P3N2/P2PPPPP/RNBQKB1R w
A12	Réti Opening: Anglo-Slav Variation, Bogoljubov Variation, III		rnbqkbnr/pp2pppp/2p5/3p4/2P5/1P3N2/P2PPPPP/RNBQKB1R b
A12	Réti Opening: Anglo-Slav Variation, Bogoljubov Variation, Stonewall Line		rnbq1rk1/pp2bppp/2p1pn2/3p4/2P5/1P3NP1/PB1PPPBP/RN1Q1RK1 b
A12	Réti Opening: Anglo-Slav Variation, Capablanca Variation		rn1qkb1r/pp2pppp/2p2n2/3p4/2P3b1/1P3N2/PB1PPPPP/RN1QKB1R w
A12	Réti Opening: Anglo-Slav Variation, London Defensive System		rn1qkb1r/pp2pppp/2p2n2/3p1b2/2P5/1P3NP1/P2PPP1P/RNBQKB1R w
A12	Réti Opening: Anglo-Slav Variation, New York System		rn1qkb1r/pp2pppp/2p2n2/3p1b2/2P5/1P3N2/PB1PPPPP/RN1QKB1R w
A12	Réti Opening: Anglo-Slav Variation, Torre System		rn1qkb1r/pp2pppp/2p2n2/3p4/2P3b1/1P3NP1/P2PPP1P/RNBQKB1R w
A13	English Opening: Agincourt Defense		rnbqkbnr/pppp1ppp/4p3/8/2P5/8/PP1PPPPP/RNBQKBNR w
A13	English Opening: Agincourt Defense #2		rnbqkbnr/ppp2ppp/4p3/3p4/2P5/5N2/PP1PPPPP/RNBQKB1R w
A13	English Opening: Agincourt Defense #3		rnbqkbnr/pppp1ppp/4p3/8/2P5/5N2/PP1PPPPP/RNBQKB1R b
A13	English Opening: Agincourt Defense, Bogoljubov Defense		rnbqk2r/ppp2ppp/3bpn2/3p4/2P5/5NP1/PP1PPPBP/RNBQK2R w
A13	English Opening: Agincourt Defense, Catalan Defense		rnbqkbnr/pp3ppp/4p3/2pp4/2P5/5NP1/PP1PPP1P/RNBQKB1R w
A13	English Opening: Agincourt Defense, Catalan Defense Accepted		rnbqkb1r/ppp2ppp/4pn2/8/2p5/5NP1/PP1PPPBP/RNBQK2R w
A13	English Opening: Agincourt Defense, Catalan Defense, Semi-Slav Defense		rnbqkb1r/pp3ppp/2p1pn2/3p4/2P5/5NP1/PP1PPPBP/RNBQK2R w
A13	English Opening: Agincourt Defense, Kurajica Defense		rnbqkbnr/pp3ppp/2p1p3/3p4/2P5/5NP1/PP1PPP1P/RNBQKB1R w
A13	English Opening: Agincourt Defense, Wimpy System		rnbqkb1r/pp3ppp/4pn2/2pp4/2P5/1P2PN2/PB1P1PPP/RN1QKB1R b
A13	English Opening: Anglo-Indian Defense, Queen's Indian Formation		rn1qkb1r/pbpp1ppp/1p2pn2/8/2P5/5NP1/PP1PPPBP/RNBQK2R w
A13	English Opening: Neo-Catalan		rnbqkb1r/ppp2ppp/4pn2/3p4/2P5/5NP1/PP1PPP1P/RNBQKB1R w
A13	English Opening: Romanishin Gambit		rnbqkb1r/2pp1ppp/p3pn2/1p6/2P5/5NP1/PP1PPPBP/RNBQK2R w
A14	English Opening: Agincourt Defense, Catalan Defense		rn1qkbnr/pbp2ppp/1p2p3/3p4/2P5/5NP1/PP1PPPBP/RNBQ1RK1 b
A14	English Opening: Agincourt Defense, Keres Defense		r1bqk2r/pp2bppp/2n1p3/2pn4/8/2N2NP1/PP1PPPBP/R1BQ1RK1 w
A14	English Opening: Agincourt Defense, Neo-Catalan Declined		rnbqk2r/ppp1bppp/4pn2/3p4/2P5/5NP1/PP1PPPBP/RNBQ1RK1 b
A14	English Opening: Agincourt Defense, Tarrasch Defense		r1bqk2r/pp2bppp/2n1pn2/2pp4/2P5/1P3NP1/P2PPPBP/RNBQ1RK1 w
A14	English Opening: Neo-Catalan Declined		rnbqk2r/ppp1bppp/4pn2/3p4/2P5/5NP1/PP1PPPBP/RNBQK2R w
A15	English Opening: Anglo-Indian Defense		rnbqkb1r/pppppppp/5n2/8/2P5/8/PP1PPPPP/RNBQKBNR w
A15	English Opening: Anglo-Indian Defense, Grünfeld Formation		rnbqkb1r/ppp1pp1p/5np1/3p4/2P5/5NP1/PP1PPP1P/RNBQKB1R w
A15	English Opening: Anglo-Indian Defense, King's Indian Formation		rnbqkb1r/pppppp1p/5np1/8/2P5/5N2/PP1PPPPP/RNBQKB1R w
A15	English Opening: Anglo-Indian Defense, King's Indian Formation, Double Fianchetto		rn1qkb1r/pbpppp1p/1p3np1/8/2P5/5NP1/PP1PPPBP/RNBQK2R w
A15	English Opening: Anglo-Indian Defense, King's Knight Variation		rnbqkb1r/pppppppp/5n2/8/2P5/5N2/PP1PPPPP/RNBQKB1R b
A15	English Opening: Anglo-Indian Defense, Old Indian Formation		rnbqkb1r/ppp1pppp/3p1n2/8/2P5/5N2/PP1PPPPP/RNBQKB1R w
A15	English Opening: Anglo-Indian Defense, Queen's Indian Formation		rnbqkb1r/p1pppppp/1p3n2/8/2P5/5N2/PP1PPPPP/RNBQKB1R w
A15	English Opening: Anglo-Indian Defense, Scandinavian Defense		rnbqkb1r/ppp1pppp/5n2/3p4/2P5/5N2/PP1PPPPP/RNBQKB1R w
A15	English Opening: Anglo-Indian Defense, Scandinavian Defense, Exchange Variation		rnbqkb1r/ppp1pppp/8/3n4/8/5N2/PP1PPPPP/RNBQKB1R w
A15	English Opening: Anglo-Indian Defense, Slav Formation		rnbqkb1r/pp1ppp1p/2p2np1/8/2P5/5NP1/PP1PPP1P/RNBQKB1R w
A15	English Orangutan		rnbqkb1r/pppppppp/5n2/8/1PP5/8/P2PPPPP/RNBQKBNR b
A16	English Opening: Anglo-Grünfeld Defense		rnbqkb1r/ppp1pppp/5n2/3p4/2P5/2N5/PP1PPPPP/R1BQKBNR w
A16	English Opening: Anglo-Grünfeld Defense: Korchnoi Variation		rnbqk2r/ppp2pbp/6p1/3np3/8/2N2NP1/PP1PPPBP/R1BQK2R w
A16	English Opening: Anglo-Indian Defense, Anglo-Grünfeld Variation		rnbqkb1r/ppp1pp1p/6p1/8/8/2n3P1/PP1PPPBP/R1BQK1NR w
A16	English Opening: Anglo-Indian Defense, Anglo-Grünfeld Variation #2		rnbqkb1r/ppp1pp1p/1n4p1/8/8/2N3P1/PP1PPPBP/R1BQK1NR w
A16	English Opening: Anglo-Indian Defense, Anglo-Grünfeld Variation #3		rnbqkb1r/ppp1pppp/8/3n4/8/2N2N2/PP1PPPPP/R1BQKB1R b
A16	English Opening: Anglo-Indian Defense, Queen's Knight Variation		rnbqkb1r/pppppppp/5n2/8/2P5/2N5/PP1PPPPP/R1BQKBNR b
A17	English Opening: Anglo-Indian Defense, Anti-Anti-Grünfeld		rnbqk2r/ppppppbp/5np1/8/2P1P3/2N2N2/PP1P1PPP/R1BQKB1R b
A17	English Opening: Anglo-Indian Defense, Hedgehog System		rnbqkb1r/pppp1ppp/4pn2/8/2P5/2N5/PP1PPPPP/R1BQKBNR w
A17	English Opening: Anglo-Indian Defense, Nimzo-English		rnbqk2r/pppp1ppp/4pn2/8/1bP5/2N2N2/PP1PPPPP/R1BQKB1R w
A17	English Opening: Anglo-Indian Defense, Queen's Indian Formation		rnbqkb1r/p1pp1ppp/1p2pn2/8/2P5/2N2N2/PP1PPPPP/R1BQKB1R w
A17	English Opening: Anglo-Indian Defense, Queen's Indian Variation		rn1qkb1r/pbpp1ppp/1p2pn2/8/2P1P3/2NB1N2/PP1P1PPP/R1BQK2R b
A18	English Opening: Anglo-Indian Defense, Zvjaginsev-Krasenkow Attack		rnbqk2r/pppp1ppp/4p3/8/1bP3n1/2N2N2/PP1PPP1P/R1BQKBR1 b
A18	English Opening: Mikenas-Carls Variation		rnbqkb1r/ppp2ppp/4pn2/3pP3/2P5/2N5/PP1P1PPP/R1BQKBNR b
A18	English Opening: Mikenas-Carls Variation #2		rnbqkb1r/pppp1ppp/4pn2/8/2P1P3/2N5/PP1P1PPP/R1BQKBNR b
A18	English Opening: Mikenas-Carls Variation #3		r1bqkb1r/pppp1ppp/2n1pn2/8/2P1P3/2N5/PP1P1PPP/R1BQKBNR w
A19	English Opening: Anglo-Indian Defense, Flohr-Mikenas-Carls Variation, Nei Gambit		rnbqkbnr/pp1p1ppp/4p3/2p1P3/2P5/2N5/PP1P1PPP/R1BQKBNR w
A19	English Opening: Mikenas-Carls, Sicilian		rnbqkb1r/pp1p1ppp/4pn2/2p5/2P1P3/2N5/PP1P1PPP/R1BQKBNR w
A20	English Opening: Drill Variation		rnbqkbnr/pppp1pp1/8/4p2p/2P5/6P1/PP1PPP1P/RNBQKBNR w
A20	English Opening: King's English Variation		rnbqkbnr/pppp1ppp/8/4p3/2P5/8/PP1PPPPP/RNBQKBNR w
A20	English Opening: King's English Variation, Nimzowitsch Variation		rnbqkbnr/pppp1ppp/8/4p3/2P5/5N2/PP1PPPPP/RNBQKB1R b
A20	English Opening: King's English Variation, Nimzowitsch-Flohr Variation		rnbqkbnr/pppp1ppp/8/8/2P1p3/5N2/PP1PPPPP/RNBQKB1R w
A20	English Openings: King's English, Kahiko-Hula Gambit		8/2p5/2kp4/8/2P4R/p3P3/PP5P/4K3 w
A21	English Opening: King's English Variation		rnbqkbnr/ppp2ppp/3p4/4p3/2P5/2N2N2/PP1PPPPP/R1BQKB1R b
A21	English Opening: King's English Variation, Keres Defense		rnbqkbnr/pp3ppp/2pp4/4p3/2P5/2N3P1/PP1PPP1P/R1BQKBNR w
A21	English Opening: King's English Variation, Kramnik-Shirov Counterattack		rnbqk1nr/pppp1ppp/8/4p3/1bP5/2N5/PP1PPPPP/R1BQKBNR w
A21	English Opening: King's English Variation, Reversed Sicilian		rnbqkbnr/pppp1ppp/8/4p3/2P5/2N5/PP1PPPPP/R1BQKBNR b
A21	English Opening: King's English Variation, Smyslov Defense		rn1qkbnr/ppp2ppp/3p4/4p3/2P3b1/2N2N2/PP1PPPPP/R1BQKB1R w
A21	English Opening: King's English Variation, Troger Defense		r2qkbnr/ppp2ppp/2npb3/4p3/2P5/2N3P1/PP1PPPBP/R1BQK1NR w
A22	English Opening: Carls-Bremen System		rnbqkb1r/pppp1ppp/5n2/4p3/2P5/2N3P1/PP1PPP1P/R1BQKBNR b
A22	English Opening: King's English Variation, Bellon Gambit		rnbqkb1r/p1pp1ppp/5n2/1p4N1/2P1p3/2N5/PP1PPPPP/R1BQKB1R w
A22	English Opening: King's English Variation, Two Knights Variation		rnbqkb1r/pppp1ppp/5n2/4p3/2P5/2N5/PP1PPPPP/R1BQKBNR w
A22	English Opening: King's English Variation, Two Knights Variation, Reversed Dragon		rnbqkb1r/ppp2ppp/5n2/3pp3/2P5/2N3P1/PP1PPP1P/R1BQKBNR w
A22	English Opening: King's English Variation, Two Knights Variation, Smyslov System		rnbqk2r/pppp1ppp/5n2/4p3/1bP5/2N3P1/PP1PPP1P/R1BQKBNR w
A22	English Opening: King's English, Erbenheimer Gambit		rnbqkb1r/pppp1ppp/8/6N1/2P1p1n1/2N5/PP1PPPPP/R1BQKB1R w
A23	English Opening: King's English Variation, Two Knights Variation, Keres Variation		rnbqkb1r/pp1p1ppp/2p2n2/4p3/2P5/2N3P1/PP1PPP1P/R1BQKBNR w
A24	English Opening: King's English Variation, Two Knights Variation, Fianchetto Line		rnbqkb1r/pppp1p1p/5np1/4p3/2P5/2N3P1/PP1PPP1P/R1BQKBNR w
A25	English Opening: Closed, Taimanov Variation		6k1/p1p1r1bp/1p2r1p1/1Pp5/P1P2P2/3RPKP1/4N3/1R6 w
A25	English Opening: King's English Variation, Bremen-Hort Variation		r2qk1nr/ppp2pbp/2npb1p1/4p3/2P5/2N1P1P1/PP1PNPBP/R1BQK2R w
A25	English Opening: King's English Variation, Closed System		r1bqk1nr/pppp1pbp/2n3p1/4p3/2P5/2NP2P1/PP2PPBP/R1BQK1NR b
A25	English Opening: King's English Variation, Reversed Closed Sicilian		r1bqkbnr/pppp1ppp/2n5/4p3/2P5/2N5/PP1PPPPP/R1BQKBNR w
A25	English Opening: King's English Variation, Taimanov Variation		r1bqk1nr/pppp1pbp/2n3p1/4p3/2P5/2N3P1/PP1PPPBP/R1BQK1NR w
A25	English Opening: King's English, Mazedonisch		rnbqkb1r/pppp1ppp/5n2/4p3/2P2P2/2N5/PP1PP1PP/R1BQKBNR b
A26	English Opening: King's English Variation, Botvinnik System		r1bqk1nr/ppp2pbp/2np2p1/4p3/2P1P3/2NP2P1/PP3PBP/R1BQK1NR b
A26	English Opening: King's English Variation, Botvinnik System, Prickly Pawn Pass System		rnbq1rk1/1p3pbp/p1pp1np1/4p3/2P1P3/2NP2P1/PP2NPBP/R1BQ1RK1 w
A26	English Opening: King's English Variation, Closed System, Full Symmetry		r1bqk1nr/ppp2pbp/2np2p1/4p3/2P5/2NP2P1/PP2PPBP/R1BQK1NR w
A27	English Opening: King's English Variation, Three Knights System		r1bqkbnr/pppp1ppp/2n5/4p3/2P5/2N2N2/PP1PPPPP/R1BQKB1R b
A28	English Opening: Four Knights System, Nimzowitsch Variation		r1bqkb1r/pppp1ppp/2n2n2/4p3/2P1P3/2N2N2/PP1P1PPP/R1BQKB1R b
A28	English Opening: King's English Variation, Four Knights Variation		5k2/5p2/p4Bpp/3pP3/1Pb5/5P2/3K2PP/8 w
A28	English Opening: King's English Variation, Four Knights Variation #2		r1bqkb1r/pppp1ppp/2n2n2/4p3/2P5/2N2N2/PP1PPPPP/R1BQKB1R w
A28	English Opening: King's English Variation, Four Knights Variation, Bradley Beach Variation		r1bqkb1r/pppp1ppp/2n2n2/8/2PPp3/2N2N2/PP2PPPP/R1BQKB1R w
A28	English Opening: King's English Variation, Four Knights Variation, Flexible Line		r1bqkb1r/pppp1ppp/2n2n2/4p3/2P5/2NP1N2/PP2PPPP/R1BQKB1R b
A28	English Opening: King's English Variation, Four Knights Variation, Korchnoi Line		r1bqkb1r/pppp1ppp/2n2n2/4p3/2P5/P1N2N2/1P1PPPPP/R1BQKB1R b
A28	English Opening: King's English Variation, Four Knights Variation, Quiet Line		r1bqk2r/pppp1ppp/2n2n2/4p3/2P5/2b1PN2/PPQP1PPP/R1B1KB1R w
A28	English Opening: King's English Variation, Four Knights Variation, Quiet Line #2		r1bqr1k1/pppp1ppp/2n2n2/3NpQ2/1bP5/4PN2/PP1P1PPP/R1B1KB1R b
A28	English Opening: King's English Variation, Four Knights Variation, Quiet Line #3		r1bqkb1r/pppp1ppp/2n2n2/4p3/2P5/2N1PN2/PP1P1PPP/R1BQKB1R b
A29	English Opening: King's English Variation, Four Knights Variation, Fianchetto Line		r1bqkb1r/pppp1ppp/2n2n2/4p3/2P5/2N2NP1/PP1PPP1P/R1BQKB1R b
A30	English Opening: Symmetrical Variation		rnbqkbnr/pp1ppppp/8/2p5/2P5/8/PP1PPPPP/RNBQKBNR w
A30	English Opening: Symmetrical Variation, Hedgehog Defense		rn1qk2r/pb1pbppp/1p2pn2/2p5/2P5/2N2NP1/PP1PPPBP/R1BQ1RK1 w
A30	English Opening: Symmetrical Variation, Napolitano Gambit		rnbqkb1r/pp1ppppp/5n2/2p5/1PP5/5N2/P2PPPPP/RNBQKB1R b
A30	English Opening: Symmetrical, Hedgehog, Flexible Formation		r2qk2r/1b1nbppp/pp1ppn2/8/2PQ4/1PN2NP1/P3PPBP/R1BR2K1 w
A30	English Opening: Wing Gambit		rnbqkbnr/pp1ppppp/8/2p5/1PP5/8/P2PPPPP/RNBQKBNR b
A31	English Opening: Symmetrical Variation, Anti-Benoni Variation		rnbqkb1r/pp1ppppp/5n2/2p5/2PP4/5N2/PP2PPPP/RNBQKB1R b
A32	English Opening: Symmetrical Variation, Anti-Benoni Variation, Spielmann Defense		rnbqkb1r/pp1p1ppp/4pn2/8/2PN4/8/PP2PPPP/RNBQKB1R w
A33	English Opening: Symmetrical Variation, Anti-Benoni Variation, Geller Variation		r1b1kb1r/pp1p1ppp/1qn1pn2/8/2PN4/2N3P1/PP2PP1P/R1BQKB1R w
A33	English Opening: Symmetrical Variation, Anti-Benoni Variation, Spielmann Defense		r1bqkb1r/pp1p1ppp/2n1pn2/8/2PN4/2N5/PP2PPPP/R1BQKB1R w
A34	English Opening: Symmetrical Variation, Fianchetto Variation		rnbqkb1r/pp1ppppp/5n2/2p5/2P5/2N3P1/PP1PPP1P/R1BQKBNR b
A34	English Opening: Symmetrical Variation, Normal Variation		rnbqkbnr/pp1ppppp/8/2p5/2P5/2N5/PP1PPPPP/R1BQKBNR b
A34	English Opening: Symmetrical Variation, Rubinstein Variation		rnbqkb1r/ppn1pppp/8/2p5/8/2N3P1/PP1PPPBP/R1BQK1NR w
A34	English Opening: Symmetrical Variation, Three Knights Variation		rnbqkb1r/pp2pppp/8/2pn4/8/2N2N2/PP1PPPPP/R1BQKB1R w
A35	English Opening: Symmetrical Variation, Four Knights Variation		r1bqkb1r/pp1ppppp/2n2n2/2p5/2P5/2N2N2/PP1PPPPP/R1BQKB1R w
A35	English Opening: Symmetrical Variation, Two Knights Variation		r1bqkbnr/pp1ppppp/2n5/2p5/2P5/2N5/PP1PPPPP/R1BQKBNR w
A36	English Opening: Symmetrical Variation, Botvinnik System		r1bqk1nr/pp1pppbp/2n3p1/2p5/2P1P3/2N3P1/PP1P1PBP/R1BQK1NR b
A36	English Opening: Symmetrical Variation, Botvinnik System Reversed		r1bqk1nr/pp1p1pbp/2n3p1/2p1p3/2P5/2N1P1P1/PP1P1PBP/R1BQK1NR w
A36	English Opening: Symmetrical Variation, Fianchetto Variation		r1bqkbnr/pp1ppppp/2n5/2p5/2P5/2N3P1/PP1PPP1P/R1BQKBNR b
A36	English Opening: Symmetrical Variation, Symmetrical Variation		r1bqk1nr/pp1pppbp/2n3p1/2p5/2P5/2N3P1/PP1PPPBP/R1BQK1NR w
A37	English Opening: Symmetrical Variation, Botvinnik System Reversed		r1bqk1nr/pp1p1pbp/2n3p1/2p1p3/2P5/2N2NP1/PP1PPPBP/R1BQK2R w
A37	English Opening: Symmetrical Variation, Two Knights Line		r1bqk1nr/pp1pppbp/2n3p1/2p5/2P5/2N2NP1/PP1PPPBP/R1BQK2R b
A38	English Opening: Symmetrical Variation, Double Fianchetto		r1bq1rk1/pp1pppbp/2n2np1/2p5/2P5/1PN2NP1/P2PPPBP/R1BQ1RK1 b
A38	English Opening: Symmetrical Variation, Duchamp Variation		r1bq1rk1/pp1pppbp/2n2np1/2p5/2P5/2NP1NP1/PP2PPBP/R1BQ1RK1 b
A38	English Opening: Symmetrical Variation, Full Symmetry Line		r1bqk2r/pp1pppbp/2n2np1/2p5/2P5/2N2NP1/PP1PPPBP/R1BQK2R w
A39	English Opening: Symmetrical Variation, Mecking Variation		r1bq1rk1/pp1pppbp/2n2np1/2p5/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 b
A40	Australian Defense		r1bqkbnr/pppppppp/n7/8/3P4/8/PPP1PPPP/RNBQKBNR w
A40	Borg Defense: Borg Gambit		rnbqkbnr/pppppp1p/8/6p1/3P4/8/PPP1PPPP/RNBQKBNR w
A40	Caro-Kann Defense: De Bruycker Defense		r1bqkbnr/pp1ppppp/n1p5/8/3PP3/8/PPP2PPP/RNBQKBNR w
A40	English Defense		rnbqkbnr/p1pp1ppp/1p2p3/8/2PP4/8/PP2PPPP/RNBQKBNR w
A40	English Defense #2		rnbqkbnr/p1pppppp/1p6/8/3P4/8/PPP1PPPP/RNBQKBNR w
A40	English Defense: Eastbourne Gambit		rn1qkbnr/pbpp1ppp/1p6/4p3/2PP4/2N5/PP2PPPP/R1BQKBNR w
A40	English Defense: Hartlaub Gambit Accepted		rn1qkb1r/pbpp2pp/1p2pn2/5P2/2PP4/2N5/PP3PPP/R1BQKBNR w
A40	English Defense: Hartlaub Gambit Declined		rn1qkbnr/pbpp2pp/1p2p3/3P1p2/2P1P3/2N5/PP3PPP/R1BQKBNR b
A40	English Defense: Perrin Variation		r2qkbnr/pbpp1ppp/1pn1p3/8/2PPP3/3B4/PP3PPP/RNBQK1NR w
A40	Englund Gambit		rnbqkbnr/pppp1ppp/8/4p3/3P4/8/PPP1PPPP/RNBQKBNR w
A40	Englund Gambit Complex Declined		rnbqkbnr/pppp1ppp/8/3Pp3/8/8/PPP1PPPP/RNBQKBNR b
A40	Englund Gambit Complex Declined, Diemer Counterattack		rnb1k1nr/pppp1ppp/8/2bPp3/4P2q/8/PPP2PPP/RNBQKBNR w
A40	Englund Gambit Complex: Englund Gambit		r1b1kbnr/ppppqppp/2n5/4P3/8/5N2/PPP1PPPP/RNBQKB1R w
A40	Englund Gambit Complex: Felbecker Gambit		r1bqk1nr/pppp1ppp/2n5/2b1P3/8/5N2/PPP1PPPP/RNBQKB1R w
A40	Englund Gambit Complex: Hartlaub-Charlick Gambit		rnbqkbnr/ppp2ppp/3p4/4P3/8/8/PPP1PPPP/RNBQKBNR w
A40	Englund Gambit Complex: Mosquito Gambit		rnb1kbnr/pppp1ppp/8/4P3/7q/8/PPP1PPPP/RNBQKBNR w
A40	Englund Gambit Complex: Soller Gambit		rnbqkbnr/pppp2pp/5p2/4P3/8/8/PPP1PPPP/RNBQKBNR w
A40	Englund Gambit Complex: Soller Gambit Deferred		r1bqkbnr/pppp2pp/2n2p2/4P3/8/5N2/PPP1PPPP/RNBQKB1R w
A40	Englund Gambit Complex: Stockholm Variation		r1b1kbnr/ppppqppp/2n5/3QP3/8/5N2/PPP1PPPP/RNB1KB1R b
A40	Englund Gambit Complex: Zilbermints Gambit		r1bqkb1r/ppppnppp/2n5/4P3/8/5N2/PPP1PPPP/RNBQKB1R w
A40	Englund Gambit Complex: Zilbermints Gambit II		r1bqkbnr/pppp1pp1/2n4p/4P3/8/5N2/PPP1PPPP/RNBQKB1R w
A40	Englund Gambit Declined, Reversed Alekhine		rnbqkbnr/pppp1ppp/8/4p3/3P4/5N2/PPP1PPPP/RNBQKB1R b
A40	Englund Gambit Declined, Reversed Brooklyn		rnbqkbnr/pppp1ppp/8/8/3Pp3/8/PPP1PPPP/RNBQKBNR b
A40	Englund Gambit Declined, Reversed French		rnbqkbnr/pppp1ppp/8/4p3/3P4/4P3/PPP2PPP/RNBQKBNR b
A40	Englund Gambit Declined, Reversed Krebs		rnbqkbnr/pppp1ppp/8/8/3Pp3/5N2/PPP1PPPP/RNBQKB1R w
A40	Englund Gambit Declined, Reversed Mokele Mbembe		rnbqkbnr/pppp1ppp/8/4N3/3Pp3/8/PPP1PPPP/RNBQKB1R b
A40	Horwitz Defense		rnbqkbnr/pppp1ppp/4p3/8/3P4/8/PPP1PPPP/RNBQKBNR w
A40	Horwitz Defense: Zilbermints Gambit		rnbqkbnr/pppp1ppp/8/4p3/2PP4/8/PP2PPPP/RNBQKBNR w
A40	Mikenas Defense		r1bqkbnr/pppppppp/2n5/8/3P4/8/PPP1PPPP/RNBQKBNR w
A40	Mikenas Defense: Cannstatter Variation		r1bqkbnr/pppp1ppp/8/3Pp3/2Pn4/8/PP2PPPP/RNBQKBNR w
A40	Mikenas Defense: Lithuanian Variation		r1bqkbnr/ppppnppp/8/3Pp3/2P5/8/PP2PPPP/RNBQKBNR w
A40	Mikenas Defense: Pozarek Gambit		r1bqkbnr/pppp1ppp/8/8/2n5/2N5/PP2PPPP/R1BQKBNR w
A40	Modern Defense		rnbqkbnr/pppppp1p/6p1/8/3P4/8/PPP1PPPP/RNBQKBNR w
A40	Modern Defense: Beefeater Variation		rnbqk1nr/pp1pp2p/6p1/2pP1p2/2P5/2P5/P3PPPP/R1BQKBNR w
A40	Modern Defense: Dunworthy Variation		8/pp3pkp/2n3p1/2P5/4R3/P1r4P/5PPK/8 w
A40	Modern Defense: Lizard Defense, Pirc-Diemer Gambit		rnbqkb1r/pppppp1p/5np1/7P/3P4/8/PPP1PPP1/RNBQKBNR b
A40	Modern Defense: Semi-Averbakh Variation, Pterodactyl Variation		rnb1k1nr/pp1pppbp/6p1/q1p5/2PPP3/5N2/PP3PPP/RNBQKB1R w
A40	Modern Defense: Semi-Averbakh Variation, Pterodactyl Variation Accepted		rnb1k1nr/pp2ppbp/3p2p1/q1P5/2P1P3/5N2/PP3PPP/RNBQKB1R w
A40	Montevideo Defense		rnbqkbnr/pppppppp/8/3P4/8/8/PPP1PPPP/RNBQKBNR w
A40	Polish Defense		rnbqkbnr/p1pppppp/8/1p6/3P4/8/PPP1PPPP/RNBQKBNR w
A40	Polish Defense: Spassky Gambit Accepted		rn1qkbnr/pbpppppp/8/1B6/3PP3/8/PPP2PPP/RNBQK1NR b
A40	Pterodactyl Defense: Fianchetto, Queen Benoni Pterodactyl		rnb1k1nr/pp1pppbp/6p1/q1pP4/2P5/2N5/PP2PPPP/R1BQKBNR w
A40	Pterodactyl Defense: Fianchetto, Queen Pteranodon		rnb1k1nr/pp1ppp1p/6p1/q1pP4/2P5/2P5/P3PPPP/R1BQKBNR w
A40	Pterodactyl Defense: Fianchetto, Queen Pterodactyl		rnb1k1nr/pp1pppbp/6p1/q1p5/3P4/5NP1/PPP1PPBP/RNBQK2R w
A40	Pterodactyl Defense: Queen Pterodactyl, Quiet Line		rnbqk1nr/pp1pppbp/6p1/2p5/2PP4/2N1P3/PP3PPP/R1BQKBNR b
A40	Queen's Pawn		rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR b
A40	St. George Defense: St. George Gambit		rnbqkbnr/2pp1ppp/4p3/1p6/3PP3/8/PP3PPP/RNBQKBNR w
A40	Ware Defense: Snagglepuss Defense		r1bqkbnr/1ppppppp/2n5/p7/3PP3/8/PPP2PPP/RNBQKBNR w
A41	English Rat: Pounds Gambit		rn1qkbnr/ppp2ppp/3pb3/4P3/2P5/8/PP2PPPP/RNBQKBNR w
A41	Modern Defense		rnbqk1nr/ppp1ppbp/3p2p1/8/2PP4/2N5/PP2PPPP/R1BQKBNR w
A41	Modern Defense: Neo-Modern Defense		rnbqk1nr/pppp1pbp/6p1/4p3/2PPP3/8/PP3PPP/RNBQKBNR w
A41	Old Indian Defense		rnbqkbnr/ppp1pppp/3p4/8/2PP4/8/PP2PPPP/RNBQKBNR b
A41	Queen's Pawn		rnbqkbnr/ppp1pppp/3p4/8/3P4/8/PPP1PPPP/RNBQKBNR w
A41	Queen's Pawn Game: Anglo-Slav Opening		rnbqkbnr/pp2pppp/2pp4/8/2PP4/8/PP2PPPP/RNBQKBNR w
A41	Rat Defense: English Rat		rnbqkbnr/ppp2ppp/3p4/4p3/2PP4/8/PP2PPPP/RNBQKBNR w
A41	Rat Defense: English Rat, Lisbon Gambit		r1bqkbnr/ppp2ppp/2np4/4P3/2P5/8/PP2PPPP/RNBQKBNR w
A41	Robatsch Defense		rn1qk1nr/ppp1ppbp/3p2p1/8/2PPP1b1/5N2/PP3PPP/RNBQKB1R w
A41	Wade Defense		rn1qkbnr/ppp1pppp/3p4/8/3P2b1/5N2/PPP1PPPP/RNBQKB1R w
A42	Modern Defense: Averbakh System, Kotov Variation		r1bqk1nr/ppp1ppbp/2np2p1/8/2PPP3/2N5/PP3PPP/R1BQKBNR w
A42	Modern Defense: Averbakh System, Randspringer Variation		rnbqk1nr/ppp1p1bp/3p2p1/5p2/2PPP3/2N5/PP3PPP/R1BQKBNR w
A42	Modern Defense: Averbakh Variation		rnbqk1nr/ppp1ppbp/3p2p1/8/2PPP3/2N5/PP3PPP/R1BQKBNR b
A42	Modern Defense: Averbakh Variation, Pseudo-Sämisch		rnbqk2r/ppp1ppbp/3p1np1/8/2PPP3/4BP2/PP4PP/RN1QKBNR b
A42	Modern Defense: Pterodactyl Variation		rnb1k1nr/pp2ppbp/3p2p1/q1p5/2PPP3/2N2N2/PP3PPP/R1BQKB1R w
A42	Sicilian Defense: Hyperaccelerated Fianchetto		rnbqkbnr/pp1ppp1p/6p1/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R b
A43	Benoni Defense: Benoni Gambit Accepted		rnbqkbnr/pp1ppppp/8/2P5/8/8/PPP1PPPP/RNBQKBNR b
A43	Benoni Defense: Benoni Gambit, Schlenker Defense		r1bqkbnr/pp1ppppp/n7/2P5/8/8/PPP1PPPP/RNBQKBNR w
A43	Benoni Defense: Benoni-Indian Defense		rnbqkb1r/pp1ppppp/5n2/2pP4/8/8/PPP1PPPP/RNBQKBNR w
A43	Benoni Defense: Benoni-Indian Defense, Kingside Move Order		rnbqkb1r/pp1ppppp/5n2/2pP4/8/5N2/PPP1PPPP/RNBQKB1R b
A43	Benoni Defense: Benoni-Staunton Gambit		rnbqkbnr/pp1pp1pp/8/2pP1p2/4P3/8/PPP2PPP/RNBQKBNR b
A43	Benoni Defense: Cormorant Gambit		rnbqkbnr/p2ppppp/1p6/2P5/8/8/PPP1PPPP/RNBQKBNR w
A43	Benoni Defense: Franco-Sicilian Hybrid		rnbqk2r/pp2npbp/3p2p1/2pP4/4P3/2N2N2/PP3PPP/R1BQKB1R w
A43	Benoni Defense: Hawk Variation		rnbqkb1r/pp1ppppp/5n2/3P4/2p5/5N2/PPP1PPPP/RNBQKB1R w
A43	Benoni Defense: Old Benoni		rnbqkbnr/pp2pppp/3p4/2pP4/8/8/PPP1PPPP/RNBQKBNR w
A43	Benoni Defense: Old Benoni, Schmid Variation		rnbqkbnr/pp2pp1p/3p2p1/2pP4/8/2N5/PPP1PPPP/R1BQKBNR w
A43	Benoni Defense: Snail Variation		r1bqkbnr/pp1ppppp/n7/2pP4/8/8/PPP1PPPP/RNBQKBNR w
A43	Benoni Defense: Woozle		rnb1kb1r/pp1ppppp/5n2/q1pP4/8/2N5/PPP1PPPP/R1BQKBNR w
A43	Benoni Defense: Zilbermints-Benoni Gambit		rnbqkbnr/pp1ppppp/8/2p5/1P1P4/8/P1P1PPPP/RNBQKBNR b
A43	Franco-Benoni Defense		rnbqkbnr/pp1p1ppp/4p3/2pP4/4P3/8/PPP2PPP/RNBQKBNR b
A43	Old Benoni Defense		rnbqkbnr/pp1ppppp/8/2p5/3P4/8/PPP1PPPP/RNBQKBNR w
A43	Old Benoni Defense #2		rnbqkbnr/pp1ppppp/8/2pP4/8/8/PPP1PPPP/RNBQKBNR b
A43	Old Benoni Defense: Mujannah Formation		rnbqkbnr/pp1pp1pp/8/2pP1p2/8/8/PPP1PPPP/RNBQKBNR w
A43	Queen's Pawn Game: Liedmann Gambit		rnbqkbnr/pp1ppppp/8/8/2Pp4/4P3/PP3PPP/RNBQKBNR b
A44	Old Benoni Defense		rnbqkbnr/pp1p1ppp/8/2pPp3/8/8/PPP1PPPP/RNBQKBNR w
A44	Semi-Benoni		rnbqkbnr/pp3ppp/3p4/2pPp3/4P3/8/PPP2PPP/RNBQKBNR w
A45	Bronstein Gambit		rnbqkb1r/pppppppp/5n2/8/3P2P1/8/PPP1PP1P/RNBQKBNR b
A45	Canard Opening		rnbqkb1r/pppppppp/5n2/8/3P1P2/8/PPP1P1PP/RNBQKBNR b
A45	Indian Game		rnbqkb1r/pppppppp/5n2/8/3P4/8/PPP1PPPP/RNBQKBNR w
A45	Indian Game: Gedult Attack, Gedult Attack		rnbqkb1r/ppp1pppp/5n2/3p4/3P2P1/5P2/PPP1P2P/RNBQKBNR b
A45	Indian Game: Gibbins-Wiedenhagen Gambit Accepted		rnbqkb1r/pppppppp/8/8/3P2n1/8/PPP1PP1P/RNBQKBNR w
A45	Indian Game: Gibbins-Wiedenhagen Gambit, Maltese Falcon		rnbqkb1r/pppppppp/5n2/8/3PP3/5P2/PPP4P/RNBQKBNR b
A45	Indian Game: Gibbins-Wiedenhagen Gambit, Oshima Defense		rnbqkb1r/pppp1ppp/5n2/4p3/3P2P1/8/PPP1PP1P/RNBQKBNR w
A45	Indian Game: Gibbins-Wiedenhagen Gambit, Stummer Gambit		rnbqkb1r/ppp1pppp/3p1n2/8/3PP3/2N5/PPP1BP1P/R1BQK1NR b
A45	Indian Game: Lazard Gambit		rnbqkb1r/pppp1ppp/5n2/4p3/3P4/8/PPPNPPPP/R1BQKBNR w
A45	Indian Game: Maddigan Gambit		rnbqkb1r/pppp1ppp/5n2/4p3/3P4/2N5/PPP1PPPP/R1BQKBNR w
A45	Indian Game: Omega Gambit		rnbqkb1r/pppppppp/5n2/8/3PP3/8/PPP2PPP/RNBQKBNR b
A45	Indian Game: Omega Gambit, Arafat Gambit		rnbqkb1r/pppppppp/5n2/6B1/3P4/3B4/PPP2PPP/RN1QK1NR b
A45	Indian Game: Paleface Attack, Blackmar-Diemer Gambit Deferred		rnbqkb1r/ppp1pppp/5n2/3p4/3PP3/5P2/PPP3PP/RNBQKBNR b
A45	Indian Game: Pawn Push Variation		rnbqkb1r/pppppppp/5n2/3P4/8/8/PPP1PPPP/RNBQKBNR b
A45	Indian Game: Reversed Chigorin Defense		rnbqkb1r/pp1ppppp/5n2/2p5/3P4/2N5/PPP1PPPP/R1BQKBNR w
A45	Indian Game: Tartakower Attack		rnbqkb1r/pppppppp/5n2/8/3P4/6P1/PPP1PP1P/RNBQKBNR b
A45	Paleface Attack		rnbqkb1r/pppppppp/5n2/8/3P4/5P2/PPP1P1PP/RNBQKBNR b
A45	Trompowsky Attack		rnbqkb1r/pppppppp/5n2/6B1/3P4/8/PPP1PPPP/RN1QKBNR b
A45	Trompowsky Attack: Borg Variation		rnbqkb1r/pppppp1p/8/6p1/3PnB2/8/PPP1PPPP/RN1QKBNR w
A45	Trompowsky Attack: Classical Defense		rnbqkb1r/pppp1ppp/4pn2/6B1/3P4/8/PPP1PPPP/RN1QKBNR w
A45	Trompowsky Attack: Classical Defense, Big Center Variation		rnbqkb1r/pppp1ppp/4pn2/6B1/3PP3/8/PPP2PPP/RN1QKBNR b
A45	Trompowsky Attack: Edge Variation		rnbqkb1r/pppppppp/8/8/3Pn2B/8/PPP1PPPP/RN1QKBNR b
A45	Trompowsky Attack: Edge Variation, Hergert Gambit		rn1qkb1r/ppp1pppp/5n2/5b2/3Pp2B/2N2P2/PPP3PP/R2QKBNR w
A45	Trompowsky Attack: Poisoned Pawn Variation		rnb1kb1r/pp1ppppp/1q3n2/2pP2B1/8/2N5/PPP1PPPP/R2QKBNR b
A45	Trompowsky Attack: Raptor Variation		rnbqkb1r/pppppppp/8/6B1/3Pn3/8/PPP1PPPP/RN1QKBNR w
A45	Trompowsky Attack: Raptor Variation, Hergert Gambit		rnbqkb1r/pppp1ppp/8/4p1P1/3P4/8/PPP1PPP1/RN1QKBNR w
A46	Doery Defense		rnbqkb1r/pppppppp/8/8/3Pn3/5N2/PPP1PPPP/RNBQKB1R w
A46	Indian Game: Czech-Indian		rnbqkb1r/pp1ppppp/2p2n2/8/3P4/5N2/PPP1PPPP/RNBQKB1R w
A46	Indian Game: Knights Variation, Alburt-Miles Variation		rnbqkb1r/1ppppppp/p4n2/8/3P4/5N2/PPP1PPPP/RNBQKB1R w
A46	Indian Game: London System		rnbqkb1r/pppp1ppp/4pn2/8/3P1B2/5N2/PPP1PPPP/RN1QKB1R b
A46	Indian Game: Polish Variation		rnbqkb1r/p1pppppp/5n2/1p6/3P4/5N2/PPP1PPPP/RNBQKB1R w
A46	Indian Game: Pseudo-Benko		rnbqkb1r/p2ppppp/5n2/1ppP4/8/5N2/PPP1PPPP/RNBQKB1R w
A46	Indian Game: Spielmann-Indian		rnbqkb1r/pp1ppppp/5n2/2p5/3P4/5N2/PPP1PPPP/RNBQKB1R w
A46	Indian Game: Wade-Tartakower Defense		rnbqkb1r/ppp1pppp/3p1n2/8/3P4/5N2/PPP1PPPP/RNBQKB1R w
A46	Torre Attack		rnbqkb1r/pppp1ppp/4pn2/6B1/3P4/5N2/PPP1PPPP/RN1QKB1R b
A46	Torre Attack #2		rnbqkb1r/pppppppp/5n2/8/3P4/5N2/PPP1PPPP/RNBQKB1R b
A46	Torre Attack: Classical Defense, Nimzowitsch Variation		rnbqkb1r/pppp1pp1/4pn1p/6B1/3P4/5N2/PPP1PPPP/RN1QKB1R w
A46	Torre Attack: Classical Defense, Petrosian Gambit		rnbqkb1r/p2p1ppp/1p2pn2/2pP2B1/8/4PN2/PPP2PPP/RN1QKB1R b
A46	Torre Attack: Wagner Gambit		rnbqkb1r/pp1p1ppp/4pn2/2p3B1/3PP3/5N2/PPP2PPP/RN1QKB1R b
A46	Yusupov-Rubinstein System		rnbqkb1r/pppp1ppp/4pn2/8/3P4/4PN2/PPP2PPP/RNBQKB1R b
A47	Marienbad System		rn1qkb1r/pb1ppppp/1p3n2/2p5/3P4/5NP1/PPP1PPBP/RNBQK2R w
A47	Queen's Indian Defense		rnbqkb1r/p1pppppp/1p3n2/8/3P4/5N2/PPP1PPPP/RNBQKB1R w
A47	Queen's Indian Defense: Marienbad System, Berg Variation		rn1qkb1r/pb1ppppp/1p3n2/8/2PQ4/5NP1/PP2PPBP/RNB1K2R b
A48	Indian Game: Colle System, King's Indian Variation		rnbqk2r/ppp1ppbp/3p1np1/8/3P4/3BPN2/PPP2PPP/RNBQK2R w
A48	London System		rnbqkb1r/pppppp1p/5np1/8/3P1B2/5N2/PPP1PPPP/RN1QKB1R b
A48	Queen's Pawn Game: Colle System, Grünfeld Formation		rnbqk2r/ppp1ppbp/5np1/3p4/3P4/3BPN2/PPP2PPP/RNBQK2R w
A48	Torre Attack		rnbqkb1r/pppppp1p/5np1/6B1/3P4/5N2/PPP1PPPP/RN1QKB1R b
A48	Torre Attack: Fianchetto Defense, Euwe Variation		rnbqk2r/pp1pppbp/5np1/2p3B1/3P4/5N2/PPPNPPPP/R2QKB1R w
A49	East Indian Defense		rnbqkb1r/pppppp1p/5np1/8/3P4/5N2/PPP1PPPP/RNBQKB1R w
A49	Indian Game: King's Indian Variation, Fianchetto Variation		rnbqk2r/ppppppbp/5np1/8/2PP4/6P1/PP2PPBP/RNBQK1NR b
A49	Indian Game: Przepiorka Variation		rnbqkb1r/pppppp1p/5np1/8/3P4/5NP1/PPP1PP1P/RNBQKB1R b
A49	Zukertort Opening: Double Fianchetto Attack		rnbq1rk1/ppp1ppbp/3p1np1/8/8/1P3NP1/PBPPPPBP/RN1Q1RK1 b
A49	Zukertort Opening: Grünfeld Reversed		r1bqkbnr/pp3ppp/2n1p3/2pp4/3P4/5NP1/PPP1PPBP/RNBQ1RK1 b
A50	Indian Game: Medusa Gambit		rnbqkb1r/pppppp1p/5n2/6p1/2PP4/8/PP2PPPP/RNBQKBNR w
A50	Indian Game: Normal Variation		rnbqkb1r/pppppppp/5n2/8/2PP4/8/PP2PPPP/RNBQKBNR b
A50	Indian Game: Pyrenees Gambit		rnbqkb1r/p1pppppp/5n2/1p6/2PP4/8/PP2PPPP/RNBQKBNR w
A50	Mexican Defense		r1bqkb1r/pppppppp/2n2n2/8/2PP4/8/PP2PPPP/RNBQKBNR w
A50	Mexican Defense: Horsefly Gambit		r1bqkb1r/pppppppp/5n2/3Pn3/2P2P2/8/PP2P1PP/RNBQKBNR b
A50	Queen's Indian Accelerated		rnbqkb1r/p1pppppp/1p3n2/8/2PP4/8/PP2PPPP/RNBQKBNR w
A50	Slav Indian		rnbqkb1r/pp1ppppp/2p2n2/8/2PP4/8/PP2PPPP/RNBQKBNR w
A50	Slav Indian: Kudischewitsch Gambit		rnbqkb1r/p2ppppp/2p2n2/1p6/2PP4/5N2/PP2PPPP/RNBQKB1R w
A51	Budapest Defense: Fajarowicz Defense, Bonsdorf Variation		rnbqkb1r/p1pp1ppp/1p6/4P3/2P1n3/P7/1P2PPPP/RNBQKBNR w
A51	Budapest Defense: Fajarowicz Variation		rnbqkb1r/pppp1ppp/8/4P3/2P1n3/8/PP2PPPP/RNBQKBNR w
A51	Budapest Defense: Fajarowicz-Steiner Variation		rnbqkb1r/pppp1ppp/8/4P3/2P1n3/8/PPQ1PPPP/RNB1KBNR b
A51	Indian Game: Budapest Defense		rnbqkb1r/pppp1ppp/5n2/4p3/2PP4/8/PP2PPPP/RNBQKBNR w
A52	Budapest Defense		rnbqkb1r/pppp1ppp/8/4P3/2P3n1/8/PP2PPPP/RNBQKBNR w
A52	Budapest Defense: Adler Variation		rnbqkb1r/pppp1ppp/8/4P3/2P3n1/5N2/PP2PPPP/RNBQKB1R b
A52	Budapest Defense: Alekhine Variation		rnbqkb1r/pppp1ppp/8/4P3/2P1P1n1/8/PP3PPP/RNBQKBNR b
A52	Budapest Defense: Alekhine Variation, Abonyi Variation		rnbqkb1r/pppp1ppp/2n5/8/2P1PP2/8/PP4PP/RNBQKBNR w
A52	Budapest Defense: Alekhine Variation, Tartakower Defense		rnbqkb1r/ppp2ppp/3p4/4P3/2P1P1n1/8/PP3PPP/RNBQKBNR w
A52	Budapest Defense: Rubinstein Variation		rnbqkb1r/pppp1ppp/8/4P3/2P2Bn1/8/PP2PPPP/RN1QKBNR b
A53	Old Indian Defense		rnbqkb1r/ppp1pppp/3p1n2/8/2PP4/8/PP2PPPP/RNBQKBNR w
A53	Old Indian Defense: Janowski Variation		rn1qkb1r/ppp1pppp/3p1n2/5b2/2PP4/2N5/PP2PPPP/R1BQKBNR w
A53	Old Indian Defense: Janowski Variation, Fianchetto Variation		rn1qkb1r/ppp1pppp/3p1n2/5b2/2PP4/2N3P1/PP2PP1P/R1BQKBNR b
A53	Old Indian Defense: Janowski Variation, Fianchetto Variation #2		rn1qkb1r/ppp1pppp/3p1n2/5b2/2PP4/2N2N2/PP2PPPP/R1BQKB1R b
A53	Old Indian Defense: Janowski Variation, Grinberg Gambit		rn1qkb1r/ppp1pppp/3p1n2/5b2/2PPP3/2N5/PP3PPP/R1BQKBNR b
A53	Old Indian Defense: Janowski Variation, Main Line		rn1qkb1r/ppp1pppp/3p1n2/5b2/2PP4/2N2P2/PP2P1PP/R1BQKBNR b
A53	Old Indian: Aged Gibbon Gambit		rnbqkb1r/ppp1pppp/3p1n2/8/2PP2P1/8/PP2PP1P/RNBQKBNR b
A53	Old Indian: Czech Variation		rnbqkb1r/pp2pppp/2pp1n2/8/2PP4/2N2N2/PP2PPPP/R1BQKB1R b
A53	Old Indian: Czech Variation, with Nc3		rnbqkb1r/pp2pppp/2pp1n2/8/2PP4/2N5/PP2PPPP/R1BQKBNR w
A53	Old Indian: Czech Variation, with Nf3		rnbqkb1r/pp2pppp/2pp1n2/8/2PP4/5N2/PP2PPPP/RNBQKB1R w
A54	Old Indian Defense: Dus-Khotimirsky Variation		r1bqkb1r/pppn1ppp/3p1n2/4p3/2PP4/2NBP3/PP3PPP/R1BQK1NR b
A54	Old Indian Defense: Tartakower-Indian		rn1qkb1r/ppp1pppp/3p1n2/8/2PP2b1/5N2/PP2PPPP/RNBQKB1R w
A54	Old Indian Defense: Two Knights Variation		rnbqkb1r/ppp2ppp/3p1n2/4p3/2PP4/2N2N2/PP2PPPP/R1BQKB1R b
A54	Old Indian Defense: Ukrainian Variation		rnbqkb1r/ppp2ppp/3p1n2/4p3/2PP4/2N5/PP2PPPP/R1BQKBNR w
A55	Old Indian Defense: Normal Variation		r1bqkb1r/pppn1ppp/3p1n2/4p3/2PPP3/2N2N2/PP3PPP/R1BQKB1R b
A56	Benoni Defense		rnbqkb1r/pp1ppppp/5n2/2p5/2PP4/8/PP2PPPP/RNBQKBNR w
A56	Benoni Defense: Czech Benoni Defense		rnbqkb1r/pp1p1ppp/5n2/2pPp3/2P5/8/PP2PPPP/RNBQKBNR w
A56	Benoni Defense: King's Indian System		rnbqkb1r/pp3p1p/3p1np1/2pPp3/2P1P3/2N5/PP3PPP/R1BQKBNR w
A56	Benoni Defense: Weenink Variation		rnbqkb1r/pp1p1ppp/4pn2/2P5/2P5/8/PP2PPPP/RNBQKBNR w
A56	Benoni Defense: Zilbermints-Benoni Gambit		rnbqkbnr/pp1ppppp/8/8/1P1p4/5N2/P1P1PPPP/RNBQKB1R b
A56	Vulture Defense		rnbqkb1r/pp1ppppp/8/2pP4/2P1n3/8/PP2PPPP/RNBQKBNR w
A57	Benko Gambit		rnbqkb1r/p2ppppp/5n2/1ppP4/2P5/8/PP2PPPP/RNBQKBNR w
A57	Benko Gambit Accepted		rnbqkb1r/3ppppp/p4n2/1PpP4/8/8/PP2PPPP/RNBQKBNR w
A57	Benko Gambit Accepted, Central Storming Variation		rn1qkb1r/3ppp1p/b4np1/2pP4/5P2/2N5/PP2P1PP/R1BQKBNR b
A57	Benko Gambit Accepted, Dlugy Variation		rnbqkb1r/3ppppp/p4n2/1PpP4/8/5P2/PP2P1PP/RNBQKBNR b
A57	Benko Gambit Accepted, Modern Variation		rnbqkb1r/3ppppp/p4n2/1PpP4/8/4P3/PP3PPP/RNBQKBNR b
A57	Benko Gambit Accepted, Pawn Return Variation		rnbqkb1r/3ppppp/pP3n2/2pP4/8/8/PP2PPPP/RNBQKBNR b
A57	Benko Gambit Declined, Bishop Attack		rnbqkb1r/p2ppppp/5n2/1ppP2B1/2P5/8/PP2PPPP/RN1QKBNR b
A57	Benko Gambit Declined, Hjoerring Countergambit		rnbqkb1r/p2ppppp/5n2/1ppP4/2P1P3/8/PP3PPP/RNBQKBNR b
A57	Benko Gambit Declined, Main Line		rnbqkb1r/p2ppppp/5n2/1ppP4/2P5/5N2/PP2PPPP/RNBQKB1R b
A57	Benko Gambit Declined, Pseudo-Sämisch		rnbqkb1r/p2ppppp/5n2/1ppP4/2P5/5P2/PP2P1PP/RNBQKBNR b
A57	Benko Gambit Declined, Quiet Line		rnbqkb1r/p2ppppp/5n2/1ppP4/2P5/8/PP1NPPPP/R1BQKBNR b
A57	Benko Gambit Declined, Sosonko Variation		rnbqkb1r/p2ppppp/5n2/1ppP4/P1P5/8/1P2PPPP/RNBQKBNR b
A57	Benko Gambit: Mutkin Countergambit		1r2k3/4pr2/3p2QR/p1pP1pP1/5q2/5N2/1P3K2/1R6 w
A57	Benko Gambit: Nescafe Frappe Attack		rnbqkb1r/4pppp/3p1n2/1NpP4/1pB1P3/8/PP3PPP/R1BQK1NR b
A57	Benko Gambit: Zaitsev Variation, Nescafe Frappe Attack		rnbqkb1r/3ppppp/5n2/1NpP4/1p2P3/8/PP3PPP/R1BQKBNR b
A57	Benoni Defense: Hromádka System		rnbqkb1r/pp2pppp/3p1n2/2pP4/2P5/8/PP2PPPP/RNBQKBNR w
A58	Benko Gambit Accepted, Fianchetto Variation		rn1qk2r/4ppbp/b2p1np1/2pP4/8/2N2NP1/PP2PPBP/R1BQK2R b
A58	Benko Gambit Accepted, Fully Accepted Variation		rnbqkb1r/3ppppp/P4n2/2pP4/8/8/PP2PPPP/RNBQKBNR b
A58	Benko Gambit: Fianchetto Variation		rn1qkb1r/4pp1p/b2p1np1/2pP4/8/2N2NP1/PP2PP1P/R1BQKB1R b
A58	Benko Gambit: Nd2 Variation		rn1qkb1r/4pp1p/b2p1np1/2pP4/8/2N5/PP1NPPPP/R1BQKB1R b
A58	Benko Gambit: Zaitsev System		rnbqkb1r/3ppppp/p4n2/1PpP4/8/2N5/PP2PPPP/R1BQKBNR b
A59	Benko Gambit Accepted, Yugoslav, without 7...Bxf1		rn1qkb1r/4pppp/b2p1n2/2pP4/4P3/2N5/PP3PPP/R1BQKBNR b
A60	Benoni Defense: Modern Variation		rnbqkb1r/pp1p1ppp/4pn2/2pP4/2P5/8/PP2PPPP/RNBQKBNR w
A60	Benoni Defense: Modern Variation, Snake Variation		rnbqk2r/pp1p1ppp/3b1n2/2pP4/8/2N5/PP2PPPP/R1BQKBNR w
A61	Benoni Defense		rnbqkb1r/pp3p1p/3p1np1/2pP4/8/2N2N2/PP2PPPP/R1BQKB1R w
A61	Benoni Defense: Fianchetto Variation		rnbqkb1r/pp3p1p/3p1np1/2pP4/8/2N2NP1/PP2PP1P/R1BQKB1R b
A61	Benoni Defense: Knight's Tour Variation		rnbqkb1r/pp3p1p/3p1np1/2pP4/8/2N5/PP1NPPPP/R1BQKB1R b
A61	Benoni Defense: Uhlmann Variation		rnbqkb1r/pp3p1p/3p1np1/2pP2B1/8/2N2N2/PP2PPPP/R2QKB1R b
A62	Benoni Defense: Fianchetto Variation		rnbq1rk1/pp3pbp/3p1np1/2pP4/8/2N2NP1/PP2PPBP/R1BQK2R w
A63	Benoni Defense: Fianchetto Variation, Hastings Defense		r1bq1rk1/pp1n1pbp/3p1np1/2pP4/8/2N2NP1/PP2PPBP/R1BQ1RK1 w
A64	Benoni Defense: Fianchetto Variation, Hastings Defense, Main Line		r1bqr1k1/1p1n1pbp/p2p1np1/2pP4/P7/2N3P1/1P1NPPBP/R1BQ1RK1 w
A65	Benoni Defense: King's Pawn Line		rnbqkb1r/pp3ppp/3p1n2/2pP4/4P3/2N5/PP3PPP/R1BQKBNR b
A66	Benoni Defense: Mikenas Variation		rnbqk2r/pp3pbp/3p1np1/2pPP3/5P2/2N5/PP4PP/R1BQKBNR b
A66	Benoni Defense: Pawn Storm Variation		rnbqkb1r/pp3p1p/3p1np1/2pP4/4PP2/2N5/PP4PP/R1BQKBNR b
A67	Benoni Defense: Taimanov Variation		rnbqk2r/pp3pbp/3p1np1/1BpP4/4PP2/2N5/PP4PP/R1BQK1NR b
A68	Benoni Defense: Four Pawns Attack		rnbq1rk1/pp3pbp/3p1np1/2pP4/4PP2/2N2N2/PP4PP/R1BQKB1R w
A69	Benoni Defense: Four Pawns Attack, Main Line		rnbqr1k1/pp3pbp/3p1np1/2pP4/4PP2/2N2N2/PP2B1PP/R1BQK2R w
A70	Benoni Defense: Classical Variation		rnbqkb1r/pp3p1p/3p1np1/2pP4/4P3/2N2N2/PP3PPP/R1BQKB1R b
A70	Benoni Defense: Classical Variation, New York Variation		rnbqk2r/pp3pbp/3p1np1/2pP4/4P3/2N2N1P/PP3PP1/R1BQKB1R b
A71	Benoni Defense: Classical Variation, Averbakh-Grivas Attack		rnbqk2r/pp3pbp/3p1np1/2pP2B1/4P3/2N2N2/PP3PPP/R2QKB1R b
A72	Benoni Defense: Classical Variation, Traditional Variation		rnbqk2r/pp3pbp/3p1np1/2pP4/4P3/2N2N2/PP2BPPP/R1BQK2R b
A72	Benoni Defense: Classical, without 9.O-O		rnbq1rk1/pp3pbp/3p1np1/2pP4/4P3/2N2N2/PP2BPPP/R1BQK2R w
A73	Benoni Defense: Classical Variation, Main Line		rnbq1rk1/pp3pbp/3p1np1/2pP4/4P3/2N2N2/PP2BPPP/R1BQ1RK1 b
A74	Benoni Defense: Classical Variation, Full Line		rnbq1rk1/1p3pbp/p2p1np1/2pP4/P3P3/2N2N2/1P2BPPP/R1BQ1RK1 b
A75	Benoni Defense: Classical Variation, Argentine Counterattack		rn1q1rk1/1p3pbp/p2p1np1/2pP4/P3P1b1/2N2N2/1P2BPPP/R1BQ1RK1 w
A76	Benoni Defense: Classical Variation, Czerniak Defense		rnbqr1k1/pp3pbp/3p1np1/2pP4/4P3/2N2N2/PP2BPPP/R1BQ1RK1 w
A77	Benoni Defense: Classical Variation, Czerniak Defense, Tal Line		rnbqr1k1/pp3pbp/3p1np1/2pP4/4P3/2N5/PP1NBPPP/R1BQ1RK1 b
A78	Benoni Defense: Classical Variation, Czerniak Defense		r1bqr1k1/pp3pbp/n2p1np1/2pP4/4P3/2N5/PP1NBPPP/R1BQ1RK1 w
A79	Benoni Defense: Classical Variation, Czerniak Defense		r1bqr1k1/pp3pbp/n2p1np1/2pP4/4P3/2N2P2/PP1NB1PP/R1BQ1RK1 b
A80	Dutch Defense		rnbqkbnr/ppppp1pp/8/5p2/3P4/8/PPP1PPPP/RNBQKBNR w
A80	Dutch Defense: Alapin Variation		rnbqkbnr/ppppp1pp/8/5p2/3P4/3Q4/PPP1PPPP/RNB1KBNR b
A80	Dutch Defense: Blackmar's Second Gambit		rnbqkb1r/ppppp1pp/5n2/8/3Pp3/2N2P2/PPP3PP/R1BQKBNR b
A80	Dutch Defense: Hevendehl Gambit		rnbqkbnr/pppp2pp/8/4pp2/3P2P1/8/PPP1PP1P/RNBQKBNR w
A80	Dutch Defense: Hopton Attack		rnbqkbnr/ppppp1pp/8/5pB1/3P4/8/PPP1PPPP/RN1QKBNR b
A80	Dutch Defense: Janzen-Korchnoi Gambit		rnbqkb1r/ppppp1pp/5n2/5p2/3P2P1/7P/PPP1PP2/RNBQKBNR b
A80	Dutch Defense: Kingfisher Gambit		rnbqkbnr/ppp1p1pp/8/3p1p2/3PP3/2N5/PPP2PPP/R1BQKBNR b
A80	Dutch Defense: Korchnoi Attack		rnbqkbnr/ppppp1pp/8/5p2/3P4/7P/PPP1PPP1/RNBQKBNR b
A80	Dutch Defense: Krejcik Gambit		rnbqkbnr/ppppp1pp/8/5p2/3P2P1/8/PPP1PP1P/RNBQKBNR b
A80	Dutch Defense: Krejcik Gambit, Tate Gambit		rnbqkbnr/ppp1p1pp/8/8/3PN1p1/8/PPP2P1P/R1BQKBNR b
A80	Dutch Defense: Manhattan Gambit, Anti-Classical Line		rnbqkbnr/pppp2pp/4p3/5p2/3P2P1/3Q4/PPP1PP1P/RNB1KBNR b
A80	Dutch Defense: Manhattan Gambit, Anti-Modern		rnbqkbnr/ppp1p1pp/3p4/5p2/3P2P1/3Q4/PPP1PP1P/RNB1KBNR b
A80	Dutch Defense: Manhattan Gambit, Anti-Stonewall		rnbqkbnr/ppp1p1pp/8/3p1p2/3P2P1/3Q4/PPP1PP1P/RNB1KBNR b
A80	Dutch Defense: Omega-Isis Gambit		rnbqkbnr/pppp2pp/8/4pp2/3P4/5N2/PPP1PPPP/RNBQKB1R w
A80	Dutch Defense: Raphael Variation		rnbqkbnr/ppppp1pp/8/5p2/3P4/2N5/PPP1PPPP/R1BQKBNR b
A80	Dutch Defense: Senechaud Gambit		rnbqkbnr/pppp2pp/4p3/5p2/3P1BP1/8/PPP1PP1P/RN1QKBNR b
A80	Queen's Pawn Game: Veresov Attack, Dutch System		rnbqkbnr/ppp1p1pp/8/3p1p2/3P4/2N5/PPP1PPPP/R1BQKBNR w
A81	Dutch Defense: Blackburne Variation		rnbqkb1r/pppp2pp/4pn2/5p2/3P4/6PN/PPP1PPBP/RNBQK2R b
A81	Dutch Defense: Fianchetto Attack		rnbqkbnr/ppppp1pp/8/5p2/3P4/6P1/PPP1PP1P/RNBQKBNR b
A81	Dutch Defense: Leningrad Variation, Karlsbad Variation		rnbqk1nr/ppppp1bp/6p1/5p2/3P4/6PN/PPP1PPBP/RNBQK2R b
A81	Dutch Defense: Semi-Leningrad Variation		rnbqkb1r/ppppp2p/5np1/5p2/3P4/6P1/PPP1PPBP/RNBQK1NR w
A82	Dutch Defense: Staunton Gambit		rnbqkbnr/ppppp1pp/8/5p2/3PP3/8/PPP2PPP/RNBQKBNR b
A82	Dutch Defense: Staunton Gambit Accepted		rnbqkbnr/ppppp1pp/8/8/3Pp3/8/PPP2PPP/RNBQKBNR w
A82	Dutch Defense: Staunton Gambit, American Attack		rnbqkbnr/ppppp1pp/8/8/3Pp3/8/PPPN1PPP/R1BQKBNR b
A82	Dutch Defense: Staunton Gambit, Tartakower Variation		rnbqkb1r/ppppp1pp/5n2/8/3Pp1P1/2N5/PPP2P1P/R1BQKBNR b
A83	Dutch Defense: Staunton Gambit		rnbqkb1r/ppppp1pp/5n2/6B1/3Pp3/2N5/PPP2PPP/R2QKBNR b
A83	Dutch Defense: Staunton Gambit, Alekhine Variation		rnbqkb1r/ppppp2p/5np1/6B1/3Pp2P/2N5/PPP2PP1/R2QKBNR b
A83	Dutch Defense: Staunton Gambit, Chigorin Variation		rnbqkb1r/pp1pp1pp/2p2n2/6B1/3Pp3/2N5/PPP2PPP/R2QKBNR w
A83	Dutch Defense: Staunton Gambit, Lasker Variation		rnbqkb1r/ppppp2p/5np1/6B1/3Pp3/2N2P2/PPP3PP/R2QKBNR b
A83	Dutch Defense: Staunton Gambit, Nimzowitsch Variation		rnbqkb1r/p1ppp1pp/1p3n2/6B1/3Pp3/2N5/PPP2PPP/R2QKBNR w
A84	Dutch Defense		rnbqkbnr/ppppp1pp/8/5p2/2PP4/8/PP2PPPP/RNBQKBNR b
A84	Dutch Defense: Bladel Variation		rnbqkb1r/ppppp2p/6pn/5p2/2PP4/2N5/PP2PPPP/R1BQKBNR w
A84	Dutch Defense: Classical Variation		rnbqkbnr/pppp2pp/4p3/5p2/2PP4/8/PP2PPPP/RNBQKBNR w
A84	Dutch Defense: Krause Variation		r1bqkb1r/ppp1p1pp/2np1n2/5p2/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
A84	Dutch Defense: Normal Variation		rnbqkb1r/ppppp1pp/5n2/5p2/2PP4/8/PP2PPPP/RNBQKBNR w
A84	Dutch Defense: Rubinstein Variation		rnbqkbnr/pppp2pp/4p3/5p2/2PP4/2N5/PP2PPPP/R1BQKBNR b
A84	Horwitz Defense: Dutch Defense, Bellon Gambit		rnbqkbnr/pppp2pp/4p3/5p2/2PPP3/8/PP3PPP/RNBQKBNR b
A85	Dutch Defense: Queen's Knight Variation		rnbqkb1r/ppppp1pp/5n2/5p2/2PP4/2N5/PP2PPPP/R1BQKBNR b
A86	Dutch Defense: Fianchetto Variation		rnbqkb1r/ppppp1pp/5n2/5p2/2PP4/6P1/PP2PP1P/RNBQKBNR b
A86	Dutch Defense: Hort-Antoshin System		rnb1kb1r/ppq1p1pp/2pp1n2/5p2/2PP4/2N3P1/PP2PPBP/R1BQK1NR w
A86	Dutch Defense: Leningrad Variation		rnbqkb1r/ppppp2p/5np1/5p2/2PP4/6P1/PP2PP1P/RNBQKBNR w
A87	Dutch Defense: Leningrad Variation		rnbqk2r/ppppp1bp/5np1/5p2/2PP4/5NP1/PP2PPBP/RNBQK2R b
A88	Dutch Defense: Leningrad Variation, Warsaw Variation		rnbq1rk1/pp2p1bp/2pp1np1/5p2/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
A89	Dutch Defense: Leningrad Variation, Matulovic Variation		r1bq1rk1/ppp1p1bp/2np1np1/5p2/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
A90	Dutch Defense: Classical Variation		rnbqkb1r/pppp2pp/4pn2/5p2/2PP4/6P1/PP2PPBP/RNBQK1NR b
A90	Dutch Defense: Nimzo-Dutch Variation		rnbqk2r/pppp2pp/4pn2/5p2/1bPP4/6P1/PP2PPBP/RNBQK1NR w
A90	Dutch Defense: Nimzo-Dutch Variation, Alekhine Variation		rnbqk2r/ppppb1pp/4pn2/5p2/2PP4/6P1/PP1BPPBP/RN1QK1NR w
A90	Dutch Defense: Stonewall Variation, Modern Variation		rnbqk2r/pp4pp/2pbpn2/3p1p2/2PP4/5NP1/PP2PPBP/RNBQ1RK1 w
A91	Dutch Defense: Classical Variation		rnbqk2r/ppppb1pp/4pn2/5p2/2PP4/6P1/PP2PPBP/RNBQK1NR w
A91	Dutch Defense: Classical Variation, Blackburne Attack		rnbqk2r/ppppb1pp/4pn2/5p2/2PP4/6PN/PP2PPBP/RNBQK2R b
A92	Dutch Defense: Alekhine Variation		rnbq1rk1/ppppb1pp/4p3/5p2/2PPn3/5NP1/PP2PPBP/RNBQ1RK1 w
A92	Dutch Defense: Classical Variation		rnbq1rk1/ppppb1pp/4pn2/5p2/2PP4/5NP1/PP2PPBP/RNBQK2R w
A92	Dutch Defense: Stonewall Variation		rnbq1rk1/ppp1b1pp/4pn2/3p1p2/2PP4/5NP1/PP2PPBP/RNBQ1RK1 w
A92	Dutch Defense: Stonewall, with Nc3		rnbq1rk1/ppp1b1pp/4pn2/3p1p2/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 b
A93	Dutch Defense: Classical Variation, Stonewall Variation, Botvinnik Variation		rnbq1rk1/ppp1b1pp/4pn2/3p1p2/2PP4/1P3NP1/P3PPBP/RNBQ1RK1 b
A94	Dutch Defense: Classical Variation, Stonewall Variation		rnbq1rk1/pp2b1pp/2p1pn2/3p1p2/2PP4/BP3NP1/P3PPBP/RN1Q1RK1 b
A95	Dutch Defense: Classical Variation, Stonewall Variation		rnbq1rk1/pp2b1pp/2p1pn2/3p1p2/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
A95	Dutch Defense: Stonewall: Chekhover Variation		4r1k1/pp4pp/8/3P4/2N1B3/1P3RPb/P6P/6K1 w
A96	Dutch Defense: Classical Variation		rnbq1rk1/ppp1b1pp/3ppn2/5p2/2PP4/5NP1/PP2PPBP/RNBQ1RK1 w
A96	Dutch Defense: Classical Variation, Buenos Aires Variation		rnbq1rk1/1pp1b1pp/3ppn2/p4p2/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
A96	Dutch Defense: Classical Variation, Huisl Variation		rnbq1rk1/ppp1b1pp/3pp3/5p2/2PPn3/2N2NP1/PP2PPBP/R1BQ1RK1 w
A97	Dutch Defense: Classical Variation, Ilyin-Zhenevsky Variation		rnb1qrk1/ppp1b1pp/3ppn2/5p2/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
A97	Dutch Defense: Ilyin-Zhenevsky, Winter Variation		rnb1qrk1/ppp1b1pp/3ppn2/5p2/2PP4/2N2NP1/PP2PPBP/R1BQR1K1 b
A98	Dutch Defense: Classical Variation, Ilyin-Zhenevsky Variation, Alatortsev-Lisitsyn Line		rnb1qrk1/ppp1b1pp/3ppn2/5p2/2PP4/2N2NP1/PPQ1PPBP/R1B2RK1 b
A99	Dutch Defense: Classical Variation, Ilyin-Zhenevsky Variation, Modern Main Line		rnb1qrk1/ppp1b1pp/3ppn2/5p2/2PP4/1PN2NP1/P3PPBP/R1BQ1RK1 b
B00	Barnes Defense		rnbqkbnr/ppppp1pp/5p2/8/4P3/8/PPPP1PPP/RNBQKBNR w
B00	Borg Defense		rnbqkbnr/pppppp1p/8/6p1/4P3/8/PPPP1PPP/RNBQKBNR w
B00	Borg Defense: Borg Gambit		rnbqk1nr/ppppppbp/8/6p1/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Borg Defense: Troon Gambit		rnbqkbnr/pppppp2/7p/8/3PP1pP/8/PPP2PP1/RNBQKBNR w
B00	Borg Opening: Zilbermints Gambit		rnbqkbnr/pppp1p1p/8/4p1p1/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Caro-Kann Defense: Hillbilly Attack		rnbqkbnr/pp1ppppp/2p5/8/2B1P3/8/PPPP1PPP/RNBQK1NR b
B00	Carr Defense		rnbqkbnr/ppppppp1/7p/8/4P3/8/PPPP1PPP/RNBQKBNR w
B00	Carr Defense: Zilbermints Gambit		rnbqkbnr/pppp1pp1/7p/4p3/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Duras Gambit		rnbqkbnr/ppppp1pp/8/5p2/4P3/8/PPPP1PPP/RNBQKBNR w
B00	Goldsmith Defense		rnbqkbnr/ppppppp1/8/7p/4P3/8/PPPP1PPP/RNBQKBNR w
B00	Goldsmith Defense: Picklepuss Defense		rnbqkb1r/ppppppp1/5n2/7p/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Guatemala Defense		rn1qkbnr/p1pppppp/bp6/8/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Hippopotamus Defense		rnbqkb1r/ppppp2p/5ppn/8/2PPP3/8/PP3PPP/RNBQKBNR w
B00	Hippopotamus Defense #2		rnbqkb1r/pppppppp/7n/8/4P3/8/PPPP1PPP/RNBQKBNR w
B00	King's Pawn		rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b
B00	King's Pawn Game: Nimzowitsch Defense		r1bqkbnr/pppppppp/2n5/8/3PP3/8/PPP2PPP/RNBQKBNR b
B00	King's Pawn Game: Nimzowitsch Defense: Wheeler Gambit		r1bqkbnr/pppppppp/2n5/8/3PP3/2P5/P4PPP/RNBQKBNR b
B00	Lemming Defense		r1bqkbnr/pppppppp/n7/8/4P3/8/PPPP1PPP/RNBQKBNR w
B00	Nimzowitsch Defense		r1bqkbnr/pppppppp/2n5/8/4P3/8/PPPP1PPP/RNBQKBNR w
B00	Nimzowitsch Defense, Declined Variation		r1bqkbnr/pppppppp/2n5/8/4P3/5N2/PPPP1PPP/RNBQKB1R b
B00	Nimzowitsch Defense: Breyer Variation		r1bqkb1r/pppp1ppp/2n2n2/4p3/3PP3/2N5/PPP2PPP/R1BQKBNR w
B00	Nimzowitsch Defense: El Columpio Defense		r1bqkb1r/pppppppp/2n5/4P3/6n1/5N2/PPPP1PPP/RNBQKB1R w
B00	Nimzowitsch Defense: El Columpio Defense, El Columpio Gambit		5r2/4p2p/p1pk3p/2p5/P1Pp1rPN/7P/1P3PK1/R3R3 w
B00	Nimzowitsch Defense: El Columpio Defense, Exchange Variation		r1bqkb1r/ppp1pppp/2nP3n/8/3P4/5N1P/PPP2PP1/RNBQKB1R b
B00	Nimzowitsch Defense: El Columpio Defense, Pin Variation		r1bqkb1r/ppp1pppp/2np3n/1B2P3/3P4/5N1P/PPP2PP1/RNBQK2R b
B00	Nimzowitsch Defense: Franco-Nimzowitsch Variation		r1bqkbnr/pppp1ppp/2n1p3/8/4P3/5N2/PPPP1PPP/RNBQKB1R w
B00	Nimzowitsch Defense: French Connection		r1bqkbnr/pppp1ppp/2n1p3/8/4P3/2N5/PPPP1PPP/R1BQKBNR w
B00	Nimzowitsch Defense: Hornung Gambit		r1bqkbnr/ppp1pppp/2n5/3p4/3PP3/4B3/PPP2PPP/RN1QKBNR b
B00	Nimzowitsch Defense: Kennedy Variation		r1bqkbnr/pppp1ppp/2n5/4p3/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Kennedy Variation, Bielefelder Gambit		r1bqk1nr/pppp1ppp/2n5/2b1P3/4P3/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Kennedy Variation, Hammer Gambit		r1bqkbnr/pppp2pp/2n2p2/4P3/4P3/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Kennedy Variation, Herford Gambit		r1b1kbnr/pppp1ppp/2n5/4P3/4P2q/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Kennedy Variation, Keres Attack		r1bqkbnr/pppp1ppp/8/4n3/4P3/2N5/PPP2PPP/R1BQKBNR b
B00	Nimzowitsch Defense: Kennedy Variation, Linksspringer Variation		r1bqkbnr/pppp1ppp/2n5/3Pp3/4P3/8/PPP2PPP/RNBQKBNR b
B00	Nimzowitsch Defense: Kennedy Variation, Main Line		r1bqkbnr/pppp1ppp/6n1/8/4PP2/8/PPP3PP/RNBQKBNR w
B00	Nimzowitsch Defense: Kennedy Variation, Paulsen Attack		r1bqkbnr/pppp1ppp/8/4n3/4P3/5N2/PPP2PPP/RNBQKB1R b
B00	Nimzowitsch Defense: Kennedy Variation, Riemann Defense		r1bqkbnr/pppp1ppp/2n5/8/4PP2/8/PPP3PP/RNBQKBNR w
B00	Nimzowitsch Defense: Kennedy Variation, de Smet Gambit		r1bqkbnr/ppp2ppp/2np4/4P3/4P3/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Lean Variation		r1bqkbnr/ppppp1pp/2n5/5p2/4P3/5N2/PPPP1PPP/RNBQKB1R w
B00	Nimzowitsch Defense: Lean Variation, Colorado Counter Accepted		r1bqkbnr/ppppp1pp/2n5/5P2/8/5N2/PPPP1PPP/RNBQKB1R b
B00	Nimzowitsch Defense: Mikenas Variation		r1bqkbnr/ppp1pppp/2np4/8/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Neo-Mongoloid Defense		r1bqkbnr/ppppp1pp/2n2p2/8/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Pirc Connection		r1bqkbnr/pppppp1p/2n3p1/8/4P3/2N5/PPPP1PPP/R1BQKBNR w
B00	Nimzowitsch Defense: Pseudo-Spanish Variation		r1bqkbnr/pppppppp/2n5/1B6/4P3/8/PPPP1PPP/RNBQK1NR b
B00	Nimzowitsch Defense: Scandinavian Variation		r1bqkbnr/ppp1pppp/2n5/3p4/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Scandinavian Variation, Aachen Gambit		r1bqkbnr/ppp1pppp/8/3P4/1n1P4/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Scandinavian Variation, Advance Variation		r1bqkbnr/ppp1pppp/2n5/3pP3/3P4/8/PPP2PPP/RNBQKBNR b
B00	Nimzowitsch Defense: Scandinavian Variation, Bogoljubov Variation		r1bqkbnr/ppp1pppp/2n5/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR b
B00	Nimzowitsch Defense: Scandinavian Variation, Bogoljubov Variation, Brandics Gambit		r1bqkbnr/1pp1pppp/p1n5/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR w
B00	Nimzowitsch Defense: Scandinavian Variation, Bogoljubov Variation, Erben Gambit		r1bqkbnr/ppp1pp1p/2n3p1/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR w
B00	Nimzowitsch Defense: Scandinavian Variation, Bogoljubov Variation, Heinola-Deppe Gambit		r1bqkbnr/ppp2ppp/2n5/3pp3/3PP3/2N5/PPP2PPP/R1BQKBNR w
B00	Nimzowitsch Defense: Scandinavian Variation, Bogoljubov Variation, Nimzowitsch Gambit		r1bqkbnr/ppp1pppp/8/3Pn3/4p3/2N5/PPP2PPP/R1BQKBNR w
B00	Nimzowitsch Defense: Scandinavian Variation, Bogoljubov Variation, Richter Gambit		rnbqkbnr/ppp1pppp/8/3P4/4p3/2N2P2/PPP3PP/R1BQKBNR b
B00	Nimzowitsch Defense: Scandinavian Variation, Bogoljubov Variation, Vehre Variation		r1bqkb1r/ppp1pppp/2n2n2/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR w
B00	Nimzowitsch Defense: Scandinavian Variation, Exchange Variation		r1b1kbnr/ppp1pppp/2n5/3q4/3P4/8/PPP2PPP/RNBQKBNR w
B00	Nimzowitsch Defense: Scandinavian Variation, Exchange Variation, Marshall Gambit		r1b1kbnr/ppp1pppp/2n5/3q4/3P4/2N5/PPP2PPP/R1BQKBNR b
B00	Nimzowitsch Defense: Wheeler Gambit		r1bqkbnr/pppppppp/2n5/8/1P2P3/8/P1PP1PPP/RNBQKBNR b
B00	Nimzowitsch Defense: Williams Variation		r1bqkbnr/ppp1pppp/2np4/8/4P3/5N2/PPPP1PPP/RNBQKB1R w
B00	Owen Defense		rnbqkbnr/p1pppppp/1p6/8/4P3/8/PPPP1PPP/RNBQKBNR w
B00	Owen Defense: Hekili-Loa Gambit		r1bqkbnr/p2ppppp/1pn5/2P5/4P3/8/PPP2PPP/RNBQKBNR w
B00	Owen Defense: Matovinsky Gambit		rn1qkbnr/p1ppp2p/1p4P1/7Q/3P4/3B4/PPP2PbP/RNB1K1NR b
B00	Owen Defense: Naselwaus Gambit		rn1qkbnr/pbpppppp/1p6/6B1/3PP3/8/PPP2PPP/RN1QKBNR b
B00	Owen Defense: Smith Gambit		rn1qkbnr/pbpppppp/1p6/8/3PP3/5N2/PPP2PPP/RNBQKB1R b
B00	Owen Defense: Unicorn Variation		rn1qkbnr/pbppp1pp/1p3p2/8/2PPP3/8/PP3PPP/RNBQKBNR w
B00	Owen Defense: Wind Gambit		rn1qkbnr/pbpp1ppp/1p6/4p3/3PP3/5P2/PPP3PP/RNBQKBNR w
B00	St. George Defense		rnbqkbnr/1ppppppp/p7/8/4P3/8/PPPP1PPP/RNBQKBNR w
B00	St. George Defense: New St. George, Sanky-George Gambit		rnbqkbnr/2pp1ppp/p3p3/1p6/2PPP3/8/PP3PPP/RNBQKBNR w
B00	St. George Defense: New St. George, Three Pawn Attack		rnbqkbnr/1ppp1ppp/p3p3/8/2PPP3/8/PP3PPP/RNBQKBNR b
B00	St. George Defense: New St. George, Traditional Line		rn1qkb1r/1b1p1ppp/p3pn2/1pp5/3PP3/2PB1N2/PP3PPP/RNBQ1RK1 w
B00	St. George Defense: Polish Variation		rn1qkbnr/1bpp1ppp/p3p3/1p6/3PP3/3B1N2/PPP2PPP/RNBQK2R w
B00	St. George Defense: San Jorge Variation		rn1qk1nr/1bp1ppbp/p2p2p1/1p6/3PP3/2PB1N2/PP3PPP/RNBQ1RK1 w
B00	St. George Defense: Woodchuck Variation		r1bqkbnr/1ppppppp/p1n5/8/3PP3/8/PPP2PPP/RNBQKBNR w
B00	St. George Defense: Zilbermints Gambit		rnbqkbnr/1ppp1ppp/p7/4p3/3PP3/8/PPP2PPP/RNBQKBNR w
B00	Ware Defense		rnbqkbnr/1ppppppp/8/p7/4P3/8/PPPP1PPP/RNBQKBNR w
B01	Scandinavian Defense		rnbqkbnr/ppp1pppp/8/3p4/4P3/8/PPPP1PPP/RNBQKBNR w
B01	Scandinavian Defense: 2.b3		rnbqkbnr/ppp1pppp/8/3p4/4P3/1P6/P1PP1PPP/RNBQKBNR b
B01	Scandinavian Defense: Anderssen Counterattack		rnb1kbnr/ppp2ppp/8/q3p3/3P4/2N5/PPP2PPP/R1BQKBNR w
B01	Scandinavian Defense: Anderssen Counterattack, Collijn Variation		rn2kbnr/ppp2ppp/8/q3p3/3P2b1/2N2N2/PPP2PPP/R1BQKB1R w
B01	Scandinavian Defense: Anderssen Counterattack, Goteborg System		rnb1kbnr/ppp2ppp/8/q3p3/3P4/2N2N2/PPP2PPP/R1BQKB1R b
B01	Scandinavian Defense: Anderssen Counterattack, Orthodox Attack		r1b1k1nr/ppp2ppp/2n5/q3P3/1b6/2N2N2/PPPB1PPP/R2QKB1R b
B01	Scandinavian Defense: Blackburne Gambit		r1bqkbnr/pp2pppp/2n5/8/8/8/PPPP1PPP/RNBQKBNR w
B01	Scandinavian Defense: Blackburne-Kloosterboer Gambit		rnbqkbnr/pp2pppp/2p5/3P4/8/8/PPPP1PPP/RNBQKBNR w
B01	Scandinavian Defense: Boehnke Gambit		rn1qkbnr/ppp2ppp/4b3/8/8/8/PPPP1PPP/RNBQKBNR w
B01	Scandinavian Defense: Bronstein Variation		rnb1kb1r/1pp1pppp/p2q1n2/8/3P4/2N2N2/PPP2PPP/R1BQKB1R w
B01	Scandinavian Defense: Classical Variation		rn2kb1r/ppp1pppp/5n2/q4b2/3P4/2N2N2/PPP2PPP/R1BQKB1R w
B01	Scandinavian Defense: Grünfeld Variation		rn2kb1r/pp2pppp/2p2n2/q3Nb2/3P2P1/2N5/PPP2P1P/R1BQKB1R b
B01	Scandinavian Defense: Gubinsky-Melts Defense		rnb1kbnr/ppp1pppp/3q4/8/8/2N5/PPPP1PPP/R1BQKBNR w
B01	Scandinavian Defense: Icelandic-Palme Gambit		rnbqkb1r/ppp2ppp/4pn2/3P4/2P5/8/PP1P1PPP/RNBQKBNR w
B01	Scandinavian Defense: Kadas Gambit		rnbqkb1r/pp3ppp/2P2n2/4p3/3P4/8/PPP2PPP/RNBQKBNR w
B01	Scandinavian Defense: Kiel Variation		rnbqkb1r/ppp1pppp/8/8/1nPP4/8/PP3PPP/RNBQKBNR w
B01	Scandinavian Defense: Kloosterboer Gambit		rnbqkbnr/pp3ppp/2P5/4p3/8/8/PPPP1PPP/RNBQKBNR w
B01	Scandinavian Defense: Lasker Variation		rn2kb1r/ppp1pppp/5n2/q7/3P2b1/2N2N1P/PPP2PP1/R1BQKB1R b
B01	Scandinavian Defense: Main Line		rnb1kbnr/ppp1pppp/8/q7/8/2N5/PPPP1PPP/R1BQKBNR w
B01	Scandinavian Defense: Main Line, Leonhardt Gambit		rnb1kbnr/ppp1pppp/8/q7/1P6/2N5/P1PP1PPP/R1BQKBNR b
B01	Scandinavian Defense: Main Line, Mieses Variation		rnb1kb1r/ppp1pppp/5n2/q7/3P4/2N5/PPP2PPP/R1BQKBNR w
B01	Scandinavian Defense: Marshall Variation		rnbqkb1r/ppp1pppp/8/3n4/3P4/8/PPP2PPP/RNBQKBNR w
B01	Scandinavian Defense: Mieses-Kotroc Variation		rnb1kbnr/ppp1pppp/8/3q4/8/8/PPPP1PPP/RNBQKBNR w
B01	Scandinavian Defense: Modern Variation		rnbqkb1r/ppp1pppp/5n2/3P4/3P4/8/PPP2PPP/RNBQKBNR b
B01	Scandinavian Defense: Modern Variation #2		rnbqkb1r/ppp1pppp/5n2/3P4/8/8/PPPP1PPP/RNBQKBNR w
B01	Scandinavian Defense: Modern Variation, Gipslis Variation		rn1qkb1r/ppp1pppp/8/3n4/3P2b1/5N2/PPP2PPP/RNBQKB1R w
B01	Scandinavian Defense: Modern Variation, Wing Gambit		rnbqkb1r/p1p1pp1p/5np1/1p1P4/2PP4/8/PP3PPP/RNBQKBNR w
B01	Scandinavian Defense: Panov Transfer		rnbqkb1r/pp2pppp/2p2n2/3P4/2P5/8/PP1P1PPP/RNBQKBNR w
B01	Scandinavian Defense: Portuguese Variation		rn1qkb1r/ppp1pppp/5n2/3P4/3P2b1/8/PPP2PPP/RNBQKBNR w
B01	Scandinavian Defense: Portuguese Variation, Portuguese Gambit		r2qkb1r/pppnpppp/5n2/1B1P1b2/3P4/5P2/PPP3PP/RNBQK1NR w
B01	Scandinavian Defense: Richter Variation		rnbqkb1r/ppp1pp1p/6p1/3n4/3P4/5N2/PPP2PPP/RNBQKB1R w
B01	Scandinavian Defense: Richter Variation #2		rnbqkb1r/ppp1pp1p/5np1/3P4/3P4/8/PPP2PPP/RNBQKBNR w
B01	Scandinavian Defense: Schiller-Pytel Variation		rnb1kbnr/pp2pppp/2pq4/8/3P4/2N5/PPP2PPP/R1BQKBNR w
B01	Scandinavian Defense: Schiller-Pytel Variation, Modern Variation		rn2kb1r/pp2pppp/2p2n2/5b2/1qBP1B2/2N5/PPP1NPPP/R2QK2R w
B01	Scandinavian Defense: Zilbermints Gambit		rnbqkbnr/ppp1pppp/8/3p4/1P2P3/8/P1PP1PPP/RNBQKBNR b
B02	Alekhine Defense		rnbqkb1r/pppppppp/5n2/8/4P3/8/PPPP1PPP/RNBQKBNR w
B02	Alekhine Defense: Brooklyn Variation		rnbqkbnr/pppppppp/8/4P3/8/8/PPPP1PPP/RNBQKBNR w
B02	Alekhine Defense: Brooklyn Variation, Everglades Variation		rnbqkbnr/ppppp1pp/8/4Pp2/3P4/8/PPP2PPP/RNBQKBNR w
B02	Alekhine Defense: Hunt Variation, Lasker Simul Gambit		rnbqkb1r/pppp1ppp/4p3/2PnP3/2B5/2N5/PP1P1PPP/R1BQK1NR b
B02	Alekhine Defense: Hunt Variation, Matsukevich Gambit		rnbqkb1r/ppp1pppp/3p4/2P1P1B1/8/2P5/PP3PPP/R2QKBNR b
B02	Alekhine Defense: Hunt Variation, Mikenas Gambit		rnbqkb1r/ppp2ppp/3p4/2PBP3/8/8/PP1P1PPP/R1BQK1NR b
B02	Alekhine Defense: John Tracy Gambit		rnbqkb1r/pppppppp/5n2/8/4P3/5N2/PPPP1PPP/RNBQKB1R b
B02	Alekhine Defense: Kmoch Variation		rnbqkb1r/pp1ppppp/1n6/2p1P3/8/1B1P4/PPP2PPP/RNBQK1NR b
B02	Alekhine Defense: Krejcik Variation		rnbqkb1r/pppppppp/5n2/8/2B1P3/8/PPPP1PPP/RNBQK1NR b
B02	Alekhine Defense: Krejcik Variation, Krejcik Gambit		rnbq1b1r/pppppkpp/8/8/4n3/8/PPPP1PPP/RNBQK1NR w
B02	Alekhine Defense: Maróczy Variation		rnbqkb1r/pppppppp/5n2/8/4P3/3P4/PPP2PPP/RNBQKBNR b
B02	Alekhine Defense: Mokele Mbembe		rnbqkb1r/pppppppp/8/4P3/4n3/8/PPPP1PPP/RNBQKBNR w
B02	Alekhine Defense: Mokele Mbembe, Modern Line		rnbqkb1r/ppppp1pp/5p2/4P3/3Pn3/8/PPP2PPP/RNBQKBNR w
B02	Alekhine Defense: Mokele Mbembe, Vavra Defense		rnbqkb1r/pppp1ppp/4p3/4P3/3Pn3/8/PPP2PPP/RNBQKBNR w
B02	Alekhine Defense: Normal Variation		rnbqkb1r/pppppppp/8/3nP3/8/8/PPPP1PPP/RNBQKBNR w
B02	Alekhine Defense: Scandinavian Variation		rnbqkb1r/ppp1pppp/5n2/3p4/4P3/2N5/PPPP1PPP/R1BQKBNR w
B02	Alekhine Defense: Scandinavian Variation, Geschev Gambit		rnbqkb1r/pp2pppp/2p2n2/3P4/8/2N5/PPPP1PPP/R1BQKBNR w
B02	Alekhine Defense: Scandinavian Variation, Myers Gambit		rnbqkb1r/ppp1pppp/5n2/6B1/4p3/2NP4/PPP2PPP/R2QKBNR b
B02	Alekhine Defense: Spielmann Gambit		rnbqkb1r/pppnpppp/4P3/3p4/8/2N5/PPPP1PPP/R1BQKBNR b
B02	Alekhine Defense: Steiner Variation		rnbqkb1r/pppppppp/1n6/4P3/2P5/1P6/P2P1PPP/RNBQKBNR b
B02	Alekhine Defense: Sämisch Attack		rnbqkb1r/pppppppp/8/3nP3/8/2N5/PPPP1PPP/R1BQKBNR b
B02	Alekhine Defense: The Squirrel		rnbqkb1r/pppppppp/8/4P3/2P2n2/8/PP1P1PPP/RNBQKBNR w
B02	Alekhine Defense: Two Pawn Attack		rnbqkb1r/pppppppp/8/3nP3/2P5/8/PP1P1PPP/RNBQKBNR b
B02	Alekhine Defense: Two Pawn Attack, Lasker Variation		rnbqkb1r/pppppppp/1n6/2P1P3/8/8/PP1P1PPP/RNBQKBNR b
B02	Alekhine Defense: Two Pawns Attack, Mikenas Variation		rnbqkb1r/ppp2ppp/3pp3/2PnP3/2B5/2N5/PP1P1PPP/R1BQK1NR w
B02	Alekhine Defense: Welling Variation		rnbqkb1r/pppppppp/8/3nP3/8/1P6/P1PP1PPP/RNBQKBNR b
B02	Zaire Defense		rnbqkbnr/pppppppp/8/3PP3/8/8/PPP2PPP/RNBQKBNR w
B03	Alekhine Defense		rnbqkb1r/ppp1pppp/3p4/3nP3/3P4/8/PPP2PPP/RNBQKBNR w
B03	Alekhine Defense #2		rnbqkb1r/pppppppp/8/3nP3/3P4/8/PPP2PPP/RNBQKBNR b
B03	Alekhine Defense #3		rnbqkb1r/ppp1pppp/3p4/3nP3/2PP4/8/PP3PPP/RNBQKBNR b
B03	Alekhine Defense: Balogh Variation		rnbqkb1r/ppp1pppp/3p4/3nP3/2BP4/8/PPP2PPP/RNBQK1NR b
B03	Alekhine Defense: Buckley Attack		rnbqkb1r/pppppppp/8/3nP3/8/N7/PPPP1PPP/R1BQKBNR b
B03	Alekhine Defense: Exchange Variation		rnbqkb1r/ppp1pppp/1n1P4/8/2PP4/8/PP3PPP/RNBQKBNR b
B03	Alekhine Defense: Exchange Variation, Karpov Variation		r2q1rk1/pp2ppbp/1nnp2p1/5b2/2PP1B2/2N2N1P/PP2BPP1/R2Q1RK1 b
B03	Alekhine Defense: Four Pawns Attack		rnbqkb1r/ppp1pppp/1n1p4/4P3/2PP1P2/8/PP4PP/RNBQKBNR b
B03	Alekhine Defense: Four Pawns Attack, 6...Nc6		r1bqkb1r/ppp1pppp/1nn5/4P3/2PP4/8/PP4PP/RNBQKBNR w
B03	Alekhine Defense: Four Pawns Attack, Cambridge Gambit		rnbqkb1r/ppp1pp1p/1n1p4/4P1p1/2PP1P2/8/PP4PP/RNBQKBNR w
B03	Alekhine Defense: Four Pawns Attack, Fianchetto Variation		rnbqkb1r/ppp1pp1p/1n1p2p1/4P3/2PP1P2/8/PP4PP/RNBQKBNR w
B03	Alekhine Defense: Four Pawns Attack, Ilyin-Zhenevsky Variation		r2qkb1r/ppp1p1pp/1nn1p3/2P5/3P2b1/5N2/PP4PP/RNBQKB1R b
B03	Alekhine Defense: Four Pawns Attack, Main Line		r1bqkb1r/ppp1pppp/1nn5/4P3/2PP4/4B3/PP4PP/RN1QKBNR b
B03	Alekhine Defense: Four Pawns Attack, Tartakower Variation		2kr3r/pppqbppp/1nn1p3/4Pb2/2PP4/2N1BN2/PP2B1PP/R2Q1RK1 w
B03	Alekhine Defense: Four Pawns Attack, Trifunovic Variation		rn1qkb1r/ppp1pppp/1n1p4/4Pb2/2PP1P2/8/PP4PP/RNBQKBNR w
B03	Alekhine Defense: Hunt Variation		rnbqkb1r/ppp1pppp/1n1p4/2P1P3/3P4/8/PP3PPP/RNBQKBNR b
B03	Alekhine Defense: O'Sullivan Gambit		rnbqkb1r/p1pppppp/8/1p1nP3/3P4/8/PPP2PPP/RNBQKBNR w
B04	Alekhine Defense: Modern Variation		rnbqkb1r/ppp1pppp/3p4/3nP3/3P4/5N2/PPP2PPP/RNBQKB1R b
B04	Alekhine Defense: Modern Variation, Alburt Variation		rnbqkb1r/ppp1pp1p/3p2p1/3nP3/3P4/5N2/PPP2PPP/RNBQKB1R w
B04	Alekhine Defense: Modern Variation, Alekhine Gambit		rn1qkb1r/ppp1pppp/1n1p4/4P3/2PP2b1/5N2/PP2BPPP/RNBQK2R b
B04	Alekhine Defense: Modern Variation, Keres Variation		rnbqk2r/ppp1ppbp/1n1p2p1/4P3/P2P4/1B3N2/1PP2PPP/RNBQK2R b
B04	Alekhine Defense: Modern Variation, Larsen Variation		rnbqkb1r/ppp1pppp/8/3np3/3P4/5N2/PPP2PPP/RNBQKB1R w
B04	Alekhine Defense: Modern Variation, Larsen-Haakert Variation		r1bqkb1r/ppp1pppp/2np4/3nP3/3P4/5N2/PPP2PPP/RNBQKB1R w
B04	Alekhine Defense: Modern Variation, Schmid Variation		rnbqkb1r/ppp1pppp/1n1p4/4P3/3P4/5N2/PPP2PPP/RNBQKB1R w
B05	Alekhine Defense: Modern Variation, Alekhine Variation		rn1qkb1r/ppp1pppp/3p4/3nP3/2PP2b1/5N2/PP3PPP/RNBQKB1R b
B05	Alekhine Defense: Modern Variation, Main Line		rn1qkb1r/ppp1pppp/3p4/3nP3/3P2b1/5N2/PPP2PPP/RNBQKB1R w
B05	Alekhine Defense: Modern Variation, Panov Variation		rn1qkb1r/ppp1pppp/3p4/3nP3/3P2b1/5N1P/PPP2PP1/RNBQKB1R b
B05	Alekhine Defense: Modern, Flohr Variation		rn1qkb1r/pp2pppp/2pp4/3nP3/3P2b1/5N2/PPP1BPPP/RNBQK2R w
B05	Alekhine Defense: Modern, Vitolins Attack		rn1qkb1r/ppp1pppp/1n1p4/3PP3/2P3b1/5N2/PP3PPP/RNBQKB1R b
B06	Modern Defense		rnbqkbnr/pppppp1p/6p1/8/4P3/8/PPPP1PPP/RNBQKBNR w
B06	Modern Defense: Anti-Modern		rnbqk1nr/pp2ppbp/2pp2p1/8/2BPP3/2N5/PPP1QPPP/R1B1K1NR b
B06	Modern Defense: Bishop Attack		rnbqk1nr/ppppppbp/6p1/8/2BPP3/8/PPP2PPP/RNBQK1NR b
B06	Modern Defense: Bishop Attack, Buecker Gambit		rnbqk1nr/p1ppppbp/6p1/1p6/2BPP3/8/PPP2PPP/RNBQK1NR w
B06	Modern Defense: Bishop Attack, Monkey's Bum		rnbqk1nr/pppp1p1p/4p1p1/8/2BbP3/5Q2/PPP2PPP/RNB1K1NR w
B06	Modern Defense: Fianchetto Gambit		rnbqkbnr/ppppp2p/6p1/5p2/3PP3/8/PPP2PPP/RNBQKBNR w
B06	Modern Defense: Geller's System		rnbqk1nr/ppp1ppbp/3p2p1/8/3PP3/2P2N2/PP3PPP/RNBQKB1R b
B06	Modern Defense: Gurgenidze Defense		rnbqk1nr/pp2ppb1/2p3p1/3pP2p/3P1P2/2N5/PPP3PP/R1BQKBNR w
B06	Modern Defense: Improved Maróczy		rnb1k1nr/pp1pppbp/1q4p1/2p5/2PPP3/5N2/PP3PPP/RNBQKB1R w
B06	Modern Defense: Lizard Defense, Mittenberger Gambit		rnbqk1nr/ppp1ppbp/6p1/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR w
B06	Modern Defense: Modern Pterodactyl		rnbqk1nr/pp1pppbp/6p1/2p5/3PP3/2N5/PPP2PPP/R1BQKBNR w
B06	Modern Defense: Mongredien Defense		rnbqk1nr/p1ppppbp/1p4p1/8/3PP3/2N5/PPP2PPP/R1BQKBNR w
B06	Modern Defense: Mongredien Defense #2		rnbqk1nr/p1ppppbp/1p4p1/8/3PP3/5N2/PPP2PPP/RNBQKB1R w
B06	Modern Defense: Norwegian Defense		rnbqkb1r/pppppp1p/5np1/8/3PP3/8/PPP2PPP/RNBQKBNR w
B06	Modern Defense: Norwegian Defense, Norwegian Gambit		rnbqkb1r/ppp1pp1p/3p2p1/4P2n/3P4/8/PPP1BPPP/RNBQK1NR w
B06	Modern Defense: Pseudo-Austrian Attack		rnbqk1nr/ppp1ppbp/3p2p1/8/3PPP2/2N5/PPP3PP/R1BQKBNR b
B06	Modern Defense: Pterodactyl Variation		rnb1k1nr/pp1pppbp/6p1/q1p5/3PP3/2N2N2/PPP2PPP/R1BQKB1R w
B06	Modern Defense: Standard Defense		rnbqk1nr/ppp1ppbp/3p2p1/8/3PP3/2N5/PPP2PPP/R1BQKBNR w
B06	Modern Defense: Standard Line		rnbqk1nr/ppppppbp/6p1/8/3PP3/2N5/PPP2PPP/R1BQKBNR b
B06	Modern Defense: Three Pawns Attack		rnbqk1nr/ppppppbp/6p1/8/3PPP2/8/PPP3PP/RNBQKBNR b
B06	Modern Defense: Two Knights Variation		rnbqk1nr/ppp1ppbp/3p2p1/8/3PP3/2N2N2/PPP2PPP/R1BQKB1R b
B06	Modern Defense: Two Knights Variation, Suttles Variation		rnbqk1nr/pp2ppbp/2pp2p1/8/3PP3/2N2N2/PPP2PPP/R1BQKB1R w
B06	Modern Defense: Two Knights Variation, Suttles Variation, Tal Gambit		rnb1k1nr/pp2ppbp/2pp2p1/6B1/3PP3/q1N2N2/P1PQ1PPP/1R2KB1R w
B06	Modern Defense: Westermann Gambit		rnbqk1nr/ppppppbp/6p1/8/3PP3/8/PPPB1PPP/RN1QKBNR b
B06	Modern Defense: Wind Gambit		rnbqk1nr/ppppppbp/6p1/8/3PP3/3B4/PPP2PPP/RNBQK1NR b
B06	Norwegian Defense		rnbqkb1r/ppppppnp/6p1/4P3/3P2P1/8/PPP2P1P/RNBQKBNR w
B06	Pterodactyl Defense: Austrian, Austriadactylus Western		rnb1k1nr/pp1pppbp/6p1/q1p5/3PPP2/5N2/PPP3PP/RNBQKB1R w
B06	Pterodactyl Defense: Austrian, Grand Prix Pterodactyl		rnb1k1nr/pp1pppbp/6p1/q1p5/4PP2/2N2N2/PPPP2PP/R1BQKB1R w
B06	Pterodactyl Defense: Austrian, Pteranodon		rnb1k1nr/pp1pppbp/6p1/q1p5/3PPP2/2P5/PP4PP/RNBQKBNR w
B06	Pterodactyl Defense: Central, Benoni Beefeater Pterodactyl		rnb1k1nr/pp2pp1p/3p2p1/q1pP4/2P1P3/2P5/P4PPP/R1BQKBNR w
B06	Pterodactyl Defense: Central, Benoni Pterodactyl		rnb1k1nr/pp1pppbp/6p1/q1pP4/2P1P3/8/PP3PPP/RNBQKBNR w
B06	Pterodactyl Defense: Central, Benoni Quetzalcoatlus		rnb1k1nr/pp2ppbp/3p2p1/q1pP4/2P1P3/2N5/PP3PPP/R1BQKBNR w
B06	Pterodactyl Defense: Central, Quetzalcoatlus Gambit		rnb1k1nr/pp2ppbp/3p2p1/q1P5/2P1P3/2N5/PP3PPP/R1BQKBNR w
B06	Pterodactyl Defense: Eastern, Anhanguera		rnbqk1nr/pp1pppbp/6p1/2p5/3PP3/2N1B3/PPP2PPP/R2QKBNR b
B06	Pterodactyl Defense: Eastern, Benoni		rnbqk1nr/pp1pppbp/6p1/2pP4/4P3/2N5/PPP2PPP/R1BQKBNR b
B06	Pterodactyl Defense: Eastern, Benoni Pteranodon		rnb1k1nr/pp1ppp1p/6p1/q1pP4/4P3/2P5/P1P2PPP/R1BQKBNR w
B06	Pterodactyl Defense: Eastern, Benoni Pterodactyl		rnb1k1nr/pp1pppbp/6p1/q1pP4/4P3/2N5/PPP2PPP/R1BQKBNR w
B06	Pterodactyl Defense: Eastern, Pteranodon		rnb1k1nr/pp1ppp1p/6p1/q1P5/4P3/2P5/P1P2PPP/R1BQKBNR w
B06	Pterodactyl Defense: Eastern, Pterodactyl		rnb1k1nr/pp1pppbp/6p1/q1P5/4P3/2N5/PPP2PPP/R1BQKBNR w
B06	Pterodactyl Defense: Eastern, Rhamphorhynchus		rnbqk1nr/pp1pppbp/6p1/2P5/4P3/2N5/PPP2PPP/R1BQKBNR b
B06	Pterodactyl Defense: Fianchetto, Rhamphorhynchus		rnb1k1nr/pp1pppbp/6p1/q1P5/4P3/6P1/PPP2P1P/RNBQKBNR w
B06	Pterodactyl Defense: Sicilian, Anhanguera		rnb1k1nr/pp1pppbp/6p1/q1p5/3PP3/2N1BN2/PPP2PPP/R2QKB1R b
B06	Pterodactyl Defense: Sicilian, Benoni Gambit		rnb1k1nr/pp1pppbp/6p1/q1pP4/4P3/2N2N2/PPP2PPP/R1BQKB1R b
B06	Pterodactyl Defense: Sicilian, Pteranodon		rnb1k1nr/pp1ppp1p/6p1/q1P5/4P3/2P2N2/P1P2PPP/R1BQKB1R b
B06	Pterodactyl Defense: Sicilian, Rhamporhynchus		rnb1k1nr/pp1pppbp/6p1/q1P5/4P3/2N2N2/PPP2PPP/R1BQKB1R b
B06	Pterodactyl Defense: Sicilian, Siroccopteryx		rnb1k1nr/pp1pppbp/6p1/q1p5/2BPP3/2N2N2/PPP2PPP/R1BQK2R b
B06	Pterodactyl Defense: Sicilian, Unpin		rnb1k1nr/pp1pppbp/6p1/q1p5/3PP3/2N2N2/PPPB1PPP/R2QKB1R b
B06	Pterodactyl Defense: Western, Anhanguera		rnb1k1nr/pp1pppbp/6p1/q1p5/3PP3/4BN2/PPP2PPP/RN1QKB1R w
B06	Pterodactyl Defense: Western, Pterodactyl		rnb1k1nr/pp1pppbp/6p1/q1p5/3PP3/2P2N2/PP3PPP/RNBQKB1R w
B06	Pterodactyl Defense: Western, Rhamphorhynchus		rnb1k1nr/pp1pppbp/6p1/q1P5/4P3/5N2/PPP2PPP/RNBQKB1R w
B06	Pterodactyl Defense: Western, Siroccopteryx		rnb1k1nr/pp1pppbp/6p1/q7/2BNP3/8/PPP2PPP/RNBQK2R w
B06	Rat Defense: Spike Attack		rnbqkbnr/ppp1pppp/3p4/8/4P1P1/8/PPPP1P1P/RNBQKBNR b
B06	Robatsch (Modern) Defense		rnbqk1nr/ppppppbp/6p1/8/3PP3/8/PPP2PPP/RNBQKBNR w
B07	Czech Defense		rnbqkb1r/pp2pppp/2pp1n2/8/3PP3/2N5/PPP2PPP/R1BQKBNR w
B07	King's Pawn Game: Maróczy Defense		rnbqkbnr/ppp2ppp/3p4/4p3/3PP3/8/PPP2PPP/RNBQKBNR w
B07	King's Pawn Game: Philidor Gambit		rn1qkbnr/pppb1ppp/3p4/4P3/4P3/8/PPP2PPP/RNBQKBNR w
B07	Lion Defense: Anti-Philidor		r1bqkb1r/pppnpppp/3p1n2/8/3PPP2/2N5/PPP3PP/R1BQKBNR b
B07	Lion Defense: Anti-Philidor, Lion's Cave		r1bqkb1r/pppn1ppp/3p1n2/4p3/3PPP2/2N5/PPP3PP/R1BQKBNR w
B07	Lion Defense: Anti-Philidor, Lion's Cave, Lion Claw Gambit		r1bqk2r/pp1n1ppp/2p2n2/2bP4/2BQ1P2/2N2N2/PPP3PP/R1B1K2R w
B07	Lion Defense: Bayonet Attack		r1bqkb1r/pppnpppp/3p1n2/8/3PP1P1/2N5/PPP2P1P/R1BQKBNR b
B07	Lion Defense: Lion's Jaw		rnbqkb1r/ppp1pppp/3p1n2/8/3PP3/5P2/PPP3PP/RNBQKBNR b
B07	Pirc Defense		rnbqkb1r/ppp1pppp/3p1n2/8/3PP3/8/PPP2PPP/RNBQKBNR w
B07	Pirc Defense #2		rnbqkb1r/ppp1pppp/3p1n2/8/3PP3/2N5/PPP2PPP/R1BQKBNR b
B07	Pirc Defense #3		rnbqkb1r/ppp1pp1p/3p1np1/8/3PP3/2N5/PPP2PPP/R1BQKBNR w
B07	Pirc Defense #4		rnbqkbnr/ppp1pppp/3p4/8/4P3/8/PPPP1PPP/RNBQKBNR w
B07	Pirc Defense #5		rnbqkbnr/ppp1pppp/3p4/8/3PP3/8/PPP2PPP/RNBQKBNR b
B07	Pirc Defense: 150 Attack		rnbqkb1r/pp2pp1p/2pp1np1/8/3PP3/2N1B3/PPPQ1PPP/R3KBNR b
B07	Pirc Defense: 150 Attack, Sveshnikov-Jansa Attack		rnbqkb1r/pp2pp1p/2pp1np1/8/3PP3/2N1B2P/PPP2PP1/R2QKBNR b
B07	Pirc Defense: Bayonet Attack		rnbqk2r/ppp1ppbp/3p1np1/8/3PP2P/2N5/PPP1BPP1/R1BQK1NR b
B07	Pirc Defense: Byrne Variation		rnbqkb1r/ppp1pp1p/3p1np1/6B1/3PP3/2N5/PPP2PPP/R2QKBNR b
B07	Pirc Defense: Chinese Variation		rnbqk2r/ppp1ppbp/3p1np1/8/3PP1P1/2N5/PPP1BP1P/R1BQK1NR b
B07	Pirc Defense: Classical Variation		rnbqkb1r/ppp1pp1p/3p1np1/8/3PP3/2N5/PPP1BPPP/R1BQK1NR b
B07	Pirc Defense: Classical Variation #2		rnbqkb1r/ppp1pp1p/3p1np1/8/3PP3/2N2N2/PPP2PPP/R1BQKB1R b
B07	Pirc Defense: Kholmov System		rnbqkb1r/ppp1pp1p/3p1np1/8/2BPP3/2N5/PPP2PPP/R1BQK1NR b
B07	Pirc Defense: Roscher Gambit		rnbqkb1r/ppp1pppp/3p1n2/8/3PP3/5N2/PPP2PPP/RNBQKB1R b
B07	Pirc Defense: Sveshnikov System		rnbqkb1r/ppp1pp1p/3p1np1/8/3PP3/2N3P1/PPP2P1P/R1BQKBNR b
B07	Rat Defense: Accelerated Gurgenidze		rnbqkbnr/pp2pp1p/2pp2p1/8/3PP3/2N5/PPP2PPP/R1BQKBNR w
B07	Rat Defense: Antal Defense		r1bqkbnr/pppnpppp/3p4/8/3PP3/8/PPP2PPP/RNBQKBNR w
B07	Rat Defense: Balogh Defense		rnbqkbnr/ppp1p1pp/3p4/5p2/3PP3/8/PPP2PPP/RNBQKBNR w
B07	Rat Defense: Fuller Gambit		rnbqkb1r/ppp1pppp/5n2/3P4/5P2/8/PPPP2PP/RNBQKBNR w
B07	Rat Defense: Harmonist		rnbqkbnr/ppp1pppp/3p4/8/4PP2/8/PPPP2PP/RNBQKBNR b
B07	Rat Defense: Petruccioli Attack		rnbqkbnr/ppp1pppp/3p4/8/4P2P/8/PPPP1PP1/RNBQKBNR b
B08	Pirc Defense: Classical Variation		rnbqk2r/ppp1ppbp/3p1np1/8/3PP3/2N2N2/PPP2PPP/R1BQKB1R w
B08	Pirc Defense: Classical Variation, Quiet System		rnbqk2r/ppp1ppbp/3p1np1/8/3PP3/2N2N2/PPP1BPPP/R1BQK2R b
B08	Pirc Defense: Classical Variation, Quiet System, Chigorin Line		r1bq1rk1/ppp1ppbp/2np1np1/8/3PP3/2N2N2/PPP1BPPP/R1BQ1RK1 w
B08	Pirc Defense: Classical Variation, Quiet System, Czech Defense		rnbq1rk1/pp2ppbp/2pp1np1/8/3PP3/2N2N2/PPP1BPPP/R1BQ1RK1 w
B08	Pirc Defense: Classical Variation, Quiet System, Parma Defense		rn1q1rk1/ppp1ppbp/3p1np1/8/3PP1b1/2N2N2/PPP1BPPP/R1BQ1RK1 w
B08	Pirc Defense: Classical Variation, Schlechter Variation		rnbqk2r/ppp1ppbp/3p1np1/8/3PP3/2N2N1P/PPP2PP1/R1BQKB1R b
B09	Pirc Defense: Austrian Attack		rnbqkb1r/ppp1pp1p/3p1np1/8/3PPP2/2N5/PPP3PP/R1BQKBNR b
B09	Pirc Defense: Austrian Attack #2		rnbq1rk1/ppp1ppbp/3p1np1/8/3PPP2/2N2N2/PPP3PP/R1BQKB1R w
B09	Pirc Defense: Austrian Attack, Dragon Formation		rnbqk2r/pp2ppbp/3p1np1/2p5/3PPP2/2N2N2/PPP3PP/R1BQKB1R w
B09	Pirc Defense: Austrian Attack, Kurajica Variation		rnbq1rk1/ppp1ppbp/3p1np1/8/3PPP2/2N1BN2/PPP3PP/R2QKB1R b
B09	Pirc Defense: Austrian Attack, Ljubojevic Variation		rnbqk2r/ppp1ppbp/3p1np1/8/2BPPP2/2N5/PPP3PP/R1BQK1NR b
B09	Pirc Defense: Austrian Attack, Unzicker Attack		rnbq1rk1/ppp1ppbp/3p1np1/4P3/3P1P2/2N2N2/PPP3PP/R1BQKB1R b
B09	Pirc Defense: Austrian Attack, Unzicker Attack, Bronstein Variation		rnbq1rk1/pppnppbp/3p2p1/4P3/3P1P1P/2N2N2/PPP3P1/R1BQKB1R b
B09	Pirc Defense: Austrian Attack, Weiss Variation		rnbq1rk1/ppp1ppbp/3p1np1/8/3PPP2/2NB1N2/PPP3PP/R1BQK2R b
B10	Caro-Kann Defense		rnbqkbnr/pp1ppppp/2p5/8/4P3/8/PPPP1PPP/RNBQKBNR w
B10	Caro-Kann Defense #2		rnbqkbnr/pp1ppppp/2p5/8/4P3/2N5/PPPP1PPP/R1BQKBNR b
B10	Caro-Kann Defense: Accelerated Panov Attack		rnbqkbnr/pp1ppppp/2p5/8/2P1P3/8/PP1P1PPP/RNBQKBNR b
B10	Caro-Kann Defense: Accelerated Panov Attack #2		rnbqkbnr/pp2pppp/2p5/3p4/2P1P3/8/PP1P1PPP/RNBQKBNR w
B10	Caro-Kann Defense: Accelerated Panov Attack, Modern Variation		rnbqkb1r/pp2pppp/5n2/3P4/8/8/PP1P1PPP/RNBQKBNR w
B10	Caro-Kann Defense: Accelerated Panov Attack, Open Variation		rnbqkbnr/pp1p1ppp/2p5/4p3/2P1P3/8/PP1P1PPP/RNBQKBNR w
B10	Caro-Kann Defense: Accelerated Panov Attack, Pseudo-Scandinavian		rnb1kbnr/pp2pppp/2p5/3P4/8/8/PP1P1PPP/RNBQKBNR b
B10	Caro-Kann Defense: Accelerated Panov Attack, Van Weersel Attack		rnbqkbnr/pp2pppp/8/8/4p3/1Q6/PP1P1PPP/RNB1KBNR w
B10	Caro-Kann Defense: Breyer Variation		rnbqkbnr/pp1ppppp/2p5/8/4P3/3P4/PPP2PPP/RNBQKBNR b
B10	Caro-Kann Defense: Euwe Attack		rnbqkbnr/pp1ppppp/2p5/8/4P3/1P6/P1PP1PPP/RNBQKBNR b
B10	Caro-Kann Defense: Euwe Attack, Prins Gambit		rnbqkbnr/pp2pppp/8/2pp4/4P3/1P6/PBPP1PPP/RN1QKBNR b
B10	Caro-Kann Defense: Hillbilly Attack, Schaeffer Gambit		rnbqkbnr/pp2pppp/2p5/7Q/4p3/1B6/PPPP1PPP/RNB1K1NR b
B10	Caro-Kann Defense: Labahn Attack		rnbqkbnr/pp1ppppp/2p5/8/1P2P3/8/P1PP1PPP/RNBQKBNR b
B10	Caro-Kann Defense: Labahn Attack, Double Gambit		rnbqkbnr/pp2pppp/2p5/1P1p4/4P3/8/P1PP1PPP/RNBQKBNR b
B10	Caro-Kann Defense: Labahn Attack, Polish Variation		rnbqkbnr/pp1p1ppp/2p5/4p3/1P2P3/8/PBPP1PPP/RN1QKBNR b
B10	Caro-Kann Defense: Masi Variation		rnbqkb1r/pp1ppppp/2p2n2/8/3PP3/8/PPP2PPP/RNBQKBNR w
B10	Caro-Kann Defense: Massachusetts Defense		rnbqkbnr/pp1pp1pp/2p5/5p2/3PP3/8/PPP2PPP/RNBQKBNR w
B10	Caro-Kann Defense: Scorpion-Horus Gambit		rnbqkbnr/pp2pppp/2p5/6B1/4p3/2NP4/PPP2PPP/R2QKBNR b
B10	Caro-Kann Defense: Spike Variation		rnbqkbnr/pp1ppppp/2p5/8/4P1P1/8/PPPP1P1P/RNBQKBNR b
B10	Caro-Kann Defense: Toikkanen Gambit		rnbqkbnr/pp2pppp/2p5/3pP3/2P5/8/PP1P1PPP/RNBQKBNR b
B10	Caro-Kann Defense: Two Knights Attack		rnbqkbnr/pp2pppp/2p5/3p4/4P3/2N2N2/PPPP1PPP/R1BQKB1R b
B10	Van Geet Opening: Caro-Kann Variation, St. Patrick's Attack		rnbqkbnr/pp2pppp/2p5/3p4/4P3/2N4P/PPPP1PP1/R1BQKBNR b
B11	Caro-Kann Defense		rnbqkbnr/pp2pppp/2p5/3p4/4P3/2N5/PPPP1PPP/R1BQKBNR w
B11	Caro-Kann Defense: Two Knights Attack, Mindeno Variation		rn1qkbnr/pp2pppp/2p5/3p4/4P1b1/2N2N2/PPPP1PPP/R1BQKB1R w
B11	Caro-Kann Defense: Two Knights Attack, Mindeno Variation, Exchange Line		rn1qkbnr/pp2pppp/2p5/3p4/4P3/2N2b1P/PPPP1PP1/R1BQKB1R w
B11	Caro-Kann Defense: Two Knights Attack, Mindeno Variation, Retreat Line		rn1qkbnr/pp2pppp/2p5/3p3b/4P3/2N2N1P/PPPP1PP1/R1BQKB1R w
B12	Caro-Kann Defense		rnbqkbnr/pp1ppppp/2p5/8/3PP3/8/PPP2PPP/RNBQKBNR b
B12	Caro-Kann Defense #2		rnbqkbnr/pp2pppp/2p5/3p4/3PP3/8/PPP2PPP/RNBQKBNR w
B12	Caro-Kann Defense: Advance Variation		rnbqkbnr/pp2pppp/2p5/3pP3/3P4/8/PPP2PPP/RNBQKBNR b
B12	Caro-Kann Defense: Advance Variation, Bayonet Attack		rn1qkbnr/pp2pppp/2p5/3pPb2/3P2P1/8/PPP2P1P/RNBQKBNR b
B12	Caro-Kann Defense: Advance Variation, Botvinnik-Carls Defense		rnbqkbnr/pp2pppp/8/2ppP3/3P4/8/PPP2PPP/RNBQKBNR w
B12	Caro-Kann Defense: Advance Variation, Bronstein Variation		rn1qkbnr/pp2pppp/2p5/3pPb2/3P4/8/PPP1NPPP/RNBQKB1R b
B12	Caro-Kann Defense: Advance Variation, Prins Attack		rn1qkbnr/pp2pppp/2p5/3pPb2/1P1P4/8/P1P2PPP/RNBQKBNR b
B12	Caro-Kann Defense: Advance Variation, Short Variation		rn1qkbnr/pp2pppp/2p5/3pPb2/3P4/5N2/PPP2PPP/RNBQKB1R b
B12	Caro-Kann Defense: Advance Variation, Tal Variation		rn1qkbnr/pp2pppp/2p5/3pPb2/3P3P/8/PPP2PP1/RNBQKBNR b
B12	Caro-Kann Defense: Advance Variation, Van der Wiel Attack		rn1qkbnr/pp2pppp/2p5/3pPb2/3P4/2N5/PPP2PPP/R1BQKBNR b
B12	Caro-Kann Defense: Advance Variation, Van der Wiel Attack #2		rn1qkbnr/pp3ppp/4p1b1/2ppP3/3P2PP/2N5/PPP1NP2/R1BQKB1R b
B12	Caro-Kann Defense: Advance, Short Variation		rn1qkbnr/pp3ppp/2p1p3/3pPb2/3P4/2P5/PP2BPPP/RNBQK1NR b
B12	Caro-Kann Defense: De Bruycker Defense		r1bqkbnr/ppnppppp/2p5/8/3PP3/2N5/PPP2PPP/R1BQKBNR w
B12	Caro-Kann Defense: Edinburgh Variation		rnb1kbnr/pp2pppp/1qp5/3p4/3PP3/8/PPPN1PPP/R1BQKBNR w
B12	Caro-Kann Defense: Goldman Variation		rnbqkbnr/pp2pppp/2p5/3p4/4P3/2N2Q2/PPPP1PPP/R1B1KBNR b
B12	Caro-Kann Defense: Hector Gambit		rnbqkbnr/pp2pppp/2p5/6N1/4p3/2N5/PPPP1PPP/R1BQKB1R b
B12	Caro-Kann Defense: Maróczy Variation		rnbqkbnr/pp2pppp/2p5/3p4/3PP3/5P2/PPP3PP/RNBQKBNR b
B12	Caro-Kann Defense: Maróczy Variation, Maróczy Gambit		rnbqkbnr/pp3ppp/2p5/8/2BpP3/5N2/PPP3PP/RNBQK2R b
B12	Caro-Kann Defense: Mieses Attack, Landau Gambit		rnbqkb1r/pp1npppp/2p1P3/3p4/3P4/3B4/PPP2PPP/RNBQK1NR b
B12	Caro-Kann Defense: Mieses Gambit		rnbqkbnr/pp2pppp/2p5/3p4/3PP3/4B3/PPP2PPP/RN1QKBNR b
B12	Caro-Kann Defense: Modern Variation		rnbqkbnr/pp2pppp/2p5/3p4/3PP3/8/PPPN1PPP/R1BQKBNR b
B12	Caro-Kann Defense: Ulysses Gambit		rnbqkbnr/pp2pppp/2p5/6N1/3Pp3/8/PPP2PPP/RNBQKB1R b
B12	Slav Defense: Diemer Gambit		rnbqkbnr/pp2pppp/2p5/3p4/2PPP3/8/PP3PPP/RNBQKBNR b
B13	Caro-Kann Defense: Exchange Variation		rnbqkbnr/pp2pppp/2p5/3P4/3P4/8/PPP2PPP/RNBQKBNR b
B13	Caro-Kann Defense: Exchange Variation, Bulla Attack		rnbqkbnr/pp2pppp/8/3p4/3P2P1/8/PPP2P1P/RNBQKBNR b
B13	Caro-Kann Defense: Exchange Variation, Rubinstein Variation		r1bqkb1r/pp2pppp/2n2n2/3p4/3P1B2/2PB4/PP3PPP/RN1QK1NR b
B13	Caro-Kann Defense: Panov Attack		rnbqkb1r/pp2pppp/5n2/3p4/2PP4/2N5/PP3PPP/R1BQKBNR b
B13	Caro-Kann Defense: Panov Attack #2		rnbqkbnr/pp2pppp/8/3p4/2PP4/8/PP3PPP/RNBQKBNR b
B13	Caro-Kann Defense: Panov Attack, Fianchetto Defense, Fianchetto Gambit		rnbqk2r/pp2ppbp/5np1/3P4/3P4/2N5/PP3PPP/R1BQKBNR w
B13	Caro-Kann Defense: Panov Attack, Gunderam Attack		rnbqkb1r/pp2pppp/5n2/2Pp4/3P4/8/PP3PPP/RNBQKBNR b
B13	Caro-Kann Defense: Panov Attack, Modern Defense		r1bqkb1r/pp2pppp/2n2n2/3p4/2PP4/2N5/PP3PPP/R1BQKBNR w
B13	Caro-Kann Defense: Panov Attack, Modern Defense, Carlsbad Line		r1bqkb1r/pp3ppp/2n1pn2/3p2B1/2PP4/2N5/PP3PPP/R2QKBNR w
B13	Caro-Kann Defense: Panov Attack, Modern Defense, Czerniak Line		r1b1kb1r/pp2pppp/2n2n2/q2p2B1/2PP4/2N5/PP3PPP/R2QKBNR w
B13	Caro-Kann Defense: Panov Attack, Modern Defense, Mieses Line		r2qkb1r/pp2pppp/2n2n2/3p4/2PP2b1/2N2N2/PP3PPP/R1BQKB1R w
B13	Caro-Kann Defense: Panov Attack, Modern Defense, Reifir-Spielmann Line		r1b1kb1r/pp2pppp/1qn2n2/3p2B1/2PP4/2N5/PP3PPP/R2QKBNR w
B13	Caro-Kann Defense: Panov-Botvinnik, Herzog Defense		r1bqkb1r/pp2pppp/5n2/n2P2B1/2p5/2N5/PP3PPP/R2QKBNR w
B14	Caro-Kann Defense: Panov Attack		rnbqkb1r/pp3ppp/4pn2/3p4/2PP4/2N5/PP3PPP/R1BQKBNR w
B14	Caro-Kann Defense: Panov Attack, Fianchetto Defense		rnbqkb1r/pp2pp1p/5np1/3p4/2PP4/2N5/PP3PPP/R1BQKBNR w
B15	Caro-Kann Defense		rnbqkbnr/pp2pppp/2p5/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR b
B15	Caro-Kann Defense: Alekhine Gambit		rnbqkb1r/pp2pppp/2p2n2/8/3PN3/3B4/PPP2PPP/R1BQK1NR b
B15	Caro-Kann Defense: Forgacs Variation		rnbqkb1r/pp3ppp/2p2p2/8/2BP4/8/PPP2PPP/R1BQK1NR b
B15	Caro-Kann Defense: Gurgenidze Counterattack		rnbqkbnr/p3pppp/2p5/1p1p4/3PP3/2N5/PPP2PPP/R1BQKBNR w
B15	Caro-Kann Defense: Gurgenidze System		rnbqkbnr/pp2pp1p/2p3p1/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR w
B15	Caro-Kann Defense: Main Line		rnbqkbnr/pp2pppp/2p5/8/3PN3/8/PPP2PPP/R1BQKBNR b
B15	Caro-Kann Defense: Rasa-Studier Gambit		rnbqkbnr/pp2pppp/2p5/8/3Pp3/2N2P2/PPP3PP/R1BQKBNR b
B15	Caro-Kann Defense: Tartakower Variation		rnbqkb1r/pp3ppp/2p2p2/8/3P4/8/PPP2PPP/R1BQKBNR w
B15	Caro-Kann Defense: von Hennig Gambit		rnbqkbnr/pp2pppp/2p5/8/2BPp3/2N5/PPP2PPP/R1BQK1NR b
B16	Caro-Kann Defense: Bronstein-Larsen Variation		rnbqkb1r/pp2pp1p/2p2p2/8/3P4/8/PPP2PPP/R1BQKBNR w
B16	Caro-Kann Defense: Finnish Variation		rnbqkbnr/pp2ppp1/2p4p/8/3PN3/8/PPP2PPP/R1BQKBNR w
B17	Caro-Kann Defense: Karpov Variation		r1bqkbnr/pp1npppp/2p5/8/3PN3/8/PPP2PPP/R1BQKBNR w
B17	Caro-Kann Defense: Karpov Variation, Modern Main Line		r1bqk2r/pp1n1pp1/2pbp2p/8/3PQ3/3B1N2/PPP2PPP/R1B1K2R b
B17	Caro-Kann Defense: Karpov Variation, Modern Variation		r1bqkbnr/pp1npppp/2p5/6N1/3P4/8/PPP2PPP/R1BQKBNR b
B17	Caro-Kann Defense: Karpov Variation, Modern Variation, Ivanchuk Defense		r1bqkbnr/pp2pppp/2p2n2/6N1/3P4/8/PPP2PPP/R1BQKBNR w
B17	Caro-Kann Defense: Karpov Variation, Modern Variation, Kasparov Attack		r1bqkb1r/pp1npppp/2p2n2/8/3P4/5NN1/PPP2PPP/R1BQKB1R b
B17	Caro-Kann Defense: Karpov Variation, Smyslov Variation		r1bqkb1r/pp3ppp/1np1pn2/6N1/2BP4/8/PPP1QPPP/R1B1K1NR w
B17	Caro-Kann Defense: Karpov Variation, Smyslov Variation, Main Line		r1bqkb1r/pp3ppp/1np1pn2/6N1/3P4/1B6/PPP1QPPP/R1B1K1NR b
B17	Caro-Kann Defense: Karpov Variation, Tiviakov-Fischer Attack		r1bqkb1r/pp2pppp/2p2n2/8/2BP4/8/PPP2PPP/R1BQK1NR w
B18	Caro-Kann Defense: Classical Variation		rn1qkbnr/pp2pppp/2p5/5b2/3PN3/8/PPP2PPP/R1BQKBNR w
B18	Caro-Kann Defense: Classical Variation, Flohr Variation		rn1qkbnr/pp2pppp/2p3b1/8/3P4/6NN/PPP2PPP/R1BQKB1R b
B18	Caro-Kann Defense: Classical Variation, Main Line		rn1qkbnr/pp2pppp/2p3b1/8/3P3P/6N1/PPP2PP1/R1BQKBNR b
B18	Caro-Kann Defense: Classical Variation, Maróczy Attack		rn1qkbnr/pp2pppp/2p3b1/8/3P1P2/6N1/PPP3PP/R1BQKBNR b
B19	Caro-Kann Defense: Classical Variation, Lobron System		r2qk2r/pp1nbpp1/2p1pn1p/7P/3P4/3Q1NN1/PPPB1PP1/2KR3R w
B19	Caro-Kann Defense: Classical Variation, Seirawan Variation		r2qk2r/pp1n1pp1/2pbpn1p/7P/3P4/3Q1NN1/PPPB1PP1/2KR3R w
B19	Caro-Kann Defense: Classical, 7...Nd7		r2qkbnr/pp1nppp1/2p3bp/8/3P3P/5NN1/PPP2PP1/R1BQKB1R w
B19	Caro-Kann Defense: Classical, Spassky Variation		r2qkbnr/pp1nppp1/2p3bp/7P/3P4/5NN1/PPP2PP1/R1BQKB1R b
B20	King's Pawn Opening: Speers		rnbqkb1r/pppp1ppp/5n2/4pQ2/4P3/8/PPPP1PPP/RNB1KBNR b
B20	King's Pawn Opening: Van Hooydoon Gambit		r1bqk2r/pppp1ppp/5n2/2b5/3nP3/5N2/PP2QPPP/RNB1KB1R w
B20	Sicilian Defense		rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w
B20	Sicilian Defense: Amazon Attack		rnbqkbnr/pp1ppppp/8/2p5/4P1Q1/8/PPPP1PPP/RNB1KBNR b
B20	Sicilian Defense: Big Clamp Formation		r1bqkbnr/pp2pppp/2np4/2p5/4PP2/2PP4/PP4PP/RNBQKBNR b
B20	Sicilian Defense: Bowdler Attack		rnbqkbnr/pp1ppppp/8/2p5/2B1P3/8/PPPP1PPP/RNBQK1NR b
B20	Sicilian Defense: Brick Variation		rnbqkbnr/pp1ppppp/8/2p5/4P3/7N/PPPP1PPP/RNBQKB1R b
B20	Sicilian Defense: Gloria Variation		r1bqkbnr/pp2ppp1/2np4/2p4p/2P1P3/2N3P1/PP1P1PBP/R1BQK1NR b
B20	Sicilian Defense: Grob Variation		rnbqkbnr/pp1ppppp/8/2p5/4P1P1/8/PPPP1P1P/RNBQKBNR b
B20	Sicilian Defense: Keres Variation		rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPPNPPP/RNBQKB1R b
B20	Sicilian Defense: Kronberger Variation		rnbqkbnr/pp1ppppp/8/2p5/4P3/N7/PPPP1PPP/R1BQKBNR b
B20	Sicilian Defense: Kronberger Variation, Nemeth Gambit		rr4k1/pR2n2p/2n2p1P/2p4R/2Pb1p2/3P1P2/P3K3/8 w
B20	Sicilian Defense: Lasker-Dunne Attack		rnbqkbnr/pp1ppppp/8/2p5/4P3/6P1/PPPP1P1P/RNBQKBNR b
B20	Sicilian Defense: Mengarini Variation		rnbqkbnr/pp1ppppp/8/2p5/4P3/P7/1PPP1PPP/RNBQKBNR b
B20	Sicilian Defense: Myers Attack		rnbqkbnr/pp1ppppp/8/2p5/4P2P/8/PPPP1PP1/RNBQKBNR b
B20	Sicilian Defense: Myers Attack #2		rnbqkbnr/pp1ppppp/8/2p5/P3P3/8/1PPP1PPP/RNBQKBNR b
B20	Sicilian Defense: Snyder Variation		rnbqkbnr/pp1ppppp/8/2p5/4P3/1P6/P1PP1PPP/RNBQKBNR b
B20	Sicilian Defense: Snyder Variation, Queen Fianchetto Variation		rnbqkbnr/p2ppppp/1p6/2p5/4P3/1P6/P1PP1PPP/RNBQKBNR w
B20	Sicilian Defense: Staunton-Cochrane Variation		rnbqkbnr/pp1ppppp/8/2p5/2P1P3/8/PP1P1PPP/RNBQKBNR b
B20	Sicilian Defense: Wing Gambit		rnbqkbnr/pp1ppppp/8/2p5/1P2P3/8/P1PP1PPP/RNBQKBNR b
B20	Sicilian Defense: Wing Gambit, Abrahams Variation		rnbqkbnr/pp1ppppp/8/8/1p2P3/8/PBPP1PPP/RN1QKBNR b
B20	Sicilian Defense: Wing Gambit, Carlsbad Variation		rnbqkbnr/pp1ppppp/8/8/4P3/p7/2PP1PPP/RNBQKBNR w
B20	Sicilian Defense: Wing Gambit, Marienbad Variation		rnb1kbnr/pp2pppp/8/3q4/1p6/P7/1BPP1PPP/RN1QKBNR b
B20	Sicilian Defense: Wing Gambit, Marshall Variation		rnbqkbnr/pp1ppppp/8/8/1p2P3/P7/2PP1PPP/RNBQKBNR b
B20	Sicilian Defense: Wing Gambit, Nanu Gambit		rnb1kbnr/pp3ppp/4q3/4p3/1pP5/P2B1N2/3P1PPP/RNBQK2R b
B20	Sicilian Defense: Wing Gambit, Romanian Defense		r1b1kbnr/pp3ppp/2n1q3/4p3/1pP5/P4N2/1B1P1PPP/RN1QKB1R w
B20	Sicilian Defense: Wing Gambit, Santasiere Variation		rnbqkbnr/pp1ppppp/8/8/1pP1P3/8/P2P1PPP/RNBQKBNR b
B21	Sicilian Defense: Coles Sicilian Gambit		r1bqkb1r/pp1ppppp/2n2n2/8/2B1P3/8/PPP2PPP/RNBQK1NR b
B21	Sicilian Defense: Halasz Gambit		rnbqkbnr/pp1ppppp/8/8/3pPP2/8/PPP3PP/RNBQKBNR b
B21	Sicilian Defense: McDonnell Attack		rnbqkbnr/pp1ppppp/8/2p5/4PP2/8/PPPP2PP/RNBQKBNR b
B21	Sicilian Defense: McDonnell Attack, Tal Gambit		rnbqkb1r/pp2pppp/5n2/2pP4/5P2/8/PPPP2PP/RNBQKBNR w
B21	Sicilian Defense: Morphy Gambit		rnbqkbnr/pp1ppppp/8/8/3pP3/5N2/PPP2PPP/RNBQKB1R b
B21	Sicilian Defense: Morphy Gambit, Andreaschek Gambit		rnbqkbnr/pp1p1ppp/8/4p3/3pP3/2P2N2/PP3PPP/RNBQKB1R b
B21	Sicilian Defense: Smith-Morra Gambit		rnbqkbnr/pp1ppppp/8/8/3pP3/2P5/PP3PPP/RNBQKBNR b
B21	Sicilian Defense: Smith-Morra Gambit #2		rnbqkbnr/pp1ppppp/8/2p5/3PP3/8/PPP2PPP/RNBQKBNR b
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Chicago Defense		rnbqkb1r/1p3ppp/p2ppn2/8/2B1P3/2N2N2/PP3PPP/R1BQ1RK1 w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Classical Formation		r1bqkb1r/1p2pppp/p1np1n2/8/2B1P3/2N2N2/PP3PPP/R1BQ1RK1 w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Fianchetto Defense		r1bqkbnr/pp1ppp1p/2n3p1/8/4P3/2N2N2/PP3PPP/R1BQKB1R w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Kan Formation		rnbqkbnr/1p1p1ppp/p3p3/8/4P3/2N2N2/PP3PPP/R1BQKB1R w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Larsen Defense		r1b1k1nr/1pqp1ppp/p1nbp3/8/2B1P3/2N2N2/PP2QPPP/R1B2RK1 w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Morphy Defense		r1bqk1nr/pp1p1ppp/2n1p3/2b5/2B1P3/2N2N2/PP3PPP/R1BQK2R w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Morphy Defense Deferred		r1bqk1nr/3p1ppp/p1n1p3/1pb5/4P3/1BN2N2/PP3PPP/R1BQ1RK1 w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Paulsen Formation		r1bqkbnr/1p1p1ppp/p1n1p3/8/2B1P3/2N2N2/PP3PPP/R1BQK2R w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Pin Defense		r1bqk1nr/pp1p1ppp/2n1p3/8/1bB1P3/2N2N2/PP3PPP/R1BQK2R w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Scheveningen Formation		r1bqkbnr/pp3ppp/2npp3/8/2B1P3/2N2N2/PP3PPP/R1BQK2R w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Sozin Formation		r1bqkbnr/5ppp/p1npp3/1p6/2B1P3/2N2N2/PP2QPPP/R1B2RK1 w
B21	Sicilian Defense: Smith-Morra Gambit Accepted, Taimanov Formation		rnbqkb1r/1p1pnppp/p3p3/8/2B1P3/2N2N2/PP3PPP/R1BQK2R w
B21	Sicilian Defense: Smith-Morra Gambit Declined, Alapin Formation		rnbqkb1r/pp1ppppp/5n2/8/3pP3/2P5/PP3PPP/RNBQKBNR w
B21	Sicilian Defense: Smith-Morra Gambit Declined, Center Formation		rnbqkbnr/pp1p1ppp/8/4p3/3pP3/2P5/PP3PPP/RNBQKBNR w
B21	Sicilian Defense: Smith-Morra Gambit Declined, Dubois Variation		rnbqkbnr/pp1ppppp/8/8/2P1P3/3p4/PP3PPP/RNBQKBNR b
B21	Sicilian Defense: Smith-Morra Gambit Declined, Push Variation		rnbqkbnr/pp1ppppp/8/8/4P3/2Pp4/PP3PPP/RNBQKBNR w
B21	Sicilian Defense: Smith-Morra Gambit Declined, Scandinavian Formation		rnbqkbnr/pp2pppp/8/3p4/3pP3/2P5/PP3PPP/RNBQKBNR w
B21	Sicilian Defense: Smith-Morra Gambit Declined, Wing Formation		rnb1kbnr/pp1ppppp/8/q7/3pP3/2P5/PP3PPP/RNBQKBNR w
B22	Sicilian Defense: 2.c3, Heidenfeld Variation		8/2kbp2p/p2ppnp1/q1p5/1r3Q2/2N2N2/3P1PPP/4R1K1 w
B22	Sicilian Defense: Alapin Variation		rnbqkbnr/pp1ppppp/8/2p5/4P3/2P5/PP1P1PPP/RNBQKBNR b
B22	Sicilian Defense: Alapin Variation, Barmen Defense		rnb1kbnr/pp2pppp/8/2pq4/8/2P5/PP1P1PPP/RNBQKBNR w
B22	Sicilian Defense: Alapin Variation, Barmen Defense, Central Exchange		r3kbnr/pp2pppp/2n5/3q4/3P2b1/5N2/PP3PPP/RNBQKB1R w
B22	Sicilian Defense: Alapin Variation, Barmen Defense, Endgame Variation		r3kbnr/pp2pppp/8/8/3n4/2N2P2/PP3P1P/R1B1KB1R w
B22	Sicilian Defense: Alapin Variation, Barmen Defense, Milner-Barry Attack		r1b1k1nr/pp3ppp/2n5/3qp3/1b1P4/2N2N2/PP2BPPP/R1BQK2R b
B22	Sicilian Defense: Alapin Variation, Barmen Defense, Modern Line		rn2kb1r/pp2pppp/5n2/2pq4/3P2b1/2P2N2/PP3PPP/RNBQKB1R w
B22	Sicilian Defense: Alapin Variation, Sherzer Variation		r1bqkb1r/pp1p1ppp/2n1p3/2pnP3/3P4/2P2N2/PP3PPP/RNBQKB1R w
B22	Sicilian Defense: Alapin Variation, Smith-Morra Declined		rnbqkb1r/pp1ppppp/8/3nP3/3p4/2P5/PP3PPP/RNBQKBNR w
B22	Sicilian Defense: Alapin Variation, Stoltz Attack		r1bqkb1r/pp1ppppp/1nn5/2p1P3/8/1BP2N2/PP1P1PPP/RNBQK2R b
B22	Sicilian Defense: Alapin Variation, Stoltz Attack, Ivanchuk Line		r1b1kb1r/ppqppp1p/1nn5/4P1p1/2p5/2P2N2/PPBPQPPP/RNB1K2R w
B23	Sicilian Defense: Closed		rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/2N5/PPPP1PPP/R1BQKBNR w
B23	Sicilian Defense: Closed #2		rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/2N3P1/PPPP1P1P/R1BQKBNR b
B23	Sicilian Defense: Closed Variation		rnbqkbnr/pp1ppppp/8/2p5/4P3/2N5/PPPP1PPP/R1BQKBNR b
B23	Sicilian Defense: Closed Variation, Chameleon Variation		r1bqkbnr/pp1ppppp/2n5/2p5/4P3/2N5/PPPPNPPP/R1BQKB1R b
B23	Sicilian Defense: Closed Variation, Grob Attack		r1bqkbnr/pp1ppppp/2n5/2p5/4P1P1/2N5/PPPP1P1P/R1BQKBNR b
B23	Sicilian Defense: Closed Variation, Korchnoi Defense		rnbqkbnr/pp3ppp/4p3/2pp4/4P3/2N3P1/PPPP1P1P/R1BQKBNR w
B23	Sicilian Defense: Grand Prix Attack		r1bqkbnr/pp1ppppp/2n5/2p5/4PP2/2N5/PPPP2PP/R1BQKBNR b
B23	Sicilian Defense: Grand Prix Attack, Schofman Variation		r1bqk1nr/pp1p1pbp/2n1p1p1/2p2P2/2B1P3/2N2N2/PPPP2PP/R1BQK2R b
B24	Sicilian Defense: Closed		r1bqkbnr/pp1ppp1p/2n3p1/2p5/4P3/2N3P1/PPPP1P1P/R1BQKBNR w
B24	Sicilian Defense: Closed #2		r1bqk1nr/pp1pppbp/2n3p1/2p5/4P3/2N3P1/PPPP1PBP/R1BQK1NR w
B24	Sicilian Defense: Closed Variation, Fianchetto Variation		r1bqkbnr/pp1ppppp/2n5/2p5/4P3/2N3P1/PPPP1P1P/R1BQKBNR b
B24	Sicilian Defense: Closed, Smyslov Variation		r1bqk1nr/pp1p1pbp/4p1p1/2p5/3nP3/3PB1P1/PPP1NPBP/R2QK1NR b
B25	Sicilian Defense: Closed		r1bqk1nr/pp2ppbp/2np2p1/2p5/4P3/2NP2P1/PPP2PBP/R1BQK1NR w
B25	Sicilian Defense: Closed Variation		r1bqk1nr/pp2ppbp/2np2p1/2p5/4PP2/2NP2P1/PPP3BP/R1BQK1NR b
B25	Sicilian Defense: Closed Variation, Botvinnik Defense I		r1bqk1nr/pp3pbp/2np2p1/2p1p3/4PP2/2NP2P1/PPP3BP/R1BQK1NR w
B25	Sicilian Defense: Closed Variation, Botvinnik Defense I, Edge Variation		r1bqk2r/pp2npbp/2np2p1/2p1p3/4PP2/2NP2PN/PPP3BP/R1BQK2R w
B25	Sicilian Defense: Closed Variation, Botvinnik Defense II		r1bqk1nr/pp3pbp/2np2p1/2p1p3/4P3/2NP2P1/PPP1NPBP/R1BQK2R w
B25	Sicilian Defense: Closed Variation, Traditional		r1bqkbnr/pp1ppppp/2n5/2p5/4P3/2N5/PPPP1PPP/R1BQKBNR w
B26	Sicilian Defense: Closed Variation		r1bqk1nr/pp2ppbp/2np2p1/2p5/4P3/2NPB1P1/PPP2PBP/R2QK1NR b
B27	Sicilian Defense		rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b
B27	Sicilian Defense: Acton Extension		k2r4/ppR5/8/3PB1p1/B5P1/b6r/3P4/2RK4 w
B27	Sicilian Defense: Brussels Gambit		rnbqkbnr/pp1pp1pp/8/2p2p2/4P3/5N2/PPPP1PPP/RNBQKB1R w
B27	Sicilian Defense: Buecker Variation		rnbqkbnr/pp1pppp1/7p/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B27	Sicilian Defense: Frederico Variation		rr5k/4b1q1/p2p1p1p/3Qn3/P3BR1P/6P1/1P5K/5R2 b
B27	Sicilian Defense: Hyperaccelerated Dragon		rnbqkbnr/pp1ppp1p/6p1/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B27	Sicilian Defense: Hyperaccelerated Pterodactyl		rnbqk1nr/pp1pppbp/6p1/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R w
B27	Sicilian Defense: Hyperaccelerated Pterodactyl, Exchange Variation		rnb1k1nr/pp1ppp1p/6p1/2P5/4P3/2q2N2/P1P2PPP/R1BQKB1R w
B27	Sicilian Defense: Jalalabad Variation		rnbqkbnr/pp1p1ppp/8/2p1p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
B27	Sicilian Defense: Katalimov Variation		rnbqkbnr/p2ppppp/1p6/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B27	Sicilian Defense: Mongoose Variation		rnb1kbnr/pp1ppppp/8/q1p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B27	Sicilian Defense: Polish Gambit		rnbqkbnr/p2ppppp/8/1pp5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B27	Sicilian Defense: Quinteros Variation		rnb1kbnr/ppqppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B28	Sicilian Defense: Double-Dutch Gambit		rr6/q2n1kpp/4bp2/3pp3/pp6/1P1N4/PBPNBPPP/1K1R3R w
B28	Sicilian Defense: O'Kelly Variation		rnbqkbnr/1p1ppppp/p7/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Aronin System		rnbqkbnr/1p1ppppp/p7/2p5/4P3/5N2/PPPPBPPP/RNBQK2R b
B28	Sicilian Defense: O'Kelly Variation, Kieseritzky System		rnbqkbnr/1p1ppppp/p7/2p5/4P3/1P3N2/P1PP1PPP/RNBQKB1R b
B28	Sicilian Defense: O'Kelly Variation, Maróczy Bind		rnbqkbnr/1p1ppppp/p7/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R b
B28	Sicilian Defense: O'Kelly Variation, Maróczy Bind, Geller Line		r1bqkbnr/1p1p1ppp/p1n5/4p3/2PNP3/8/PP3PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Maróczy Bind, Paulsen Line		rnbqkbnr/1p1p1ppp/p3p3/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Maróczy Bind, Robatsch Line		rnbqkbnr/1p2pppp/p2p4/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Normal System		rnbqkbnr/1p1ppppp/p7/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R b
B28	Sicilian Defense: O'Kelly Variation, Normal System, Cortlever Gambit		rnbqkbnr/1p1ppppp/p7/8/2BpP3/5N2/PPP2PPP/RNBQK2R b
B28	Sicilian Defense: O'Kelly Variation, Normal System, Smith-Morra Line		rnbqkbnr/1p1ppppp/p7/8/3pP3/2P2N2/PP3PPP/RNBQKB1R b
B28	Sicilian Defense: O'Kelly Variation, Normal System, Taimanov Line		rnbqkbnr/1p1p1ppp/p7/4p3/3NP3/8/PPP2PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Normal System, Zagorovsky Line		rnbqkbnr/1p1ppppp/p7/8/3QP3/5N2/PPP2PPP/RNB1KB1R b
B28	Sicilian Defense: O'Kelly Variation, Quiet System		rnbqkbnr/1p1ppppp/p7/2p5/4P3/3P1N2/PPP2PPP/RNBQKB1R b
B28	Sicilian Defense: O'Kelly Variation, Réti System		rnbqkbnr/1p1ppppp/p7/2p5/4P3/5NP1/PPPP1P1P/RNBQKB1R b
B28	Sicilian Defense: O'Kelly Variation, Venice System		rnbqkbnr/1p1ppppp/p7/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R b
B28	Sicilian Defense: O'Kelly Variation, Venice System, Barcza Line		rnbqkb1r/1p1ppppp/p4n2/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Venice System, Gambit Line		rnbqkb1r/1p2pppp/p4n2/2pP4/8/2P2N2/PP1P1PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Venice System, Ljubojevic Line		rnbqkbnr/3ppppp/p7/1pp5/4P3/2P2N2/PP1P1PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Venice System, Steiner Line		rnbqkbnr/1p2pppp/p2p4/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R w
B28	Sicilian Defense: O'Kelly Variation, Wing Gambit		rnbqkbnr/1p1ppppp/p7/2p5/1P2P3/5N2/P1PP1PPP/RNBQKB1R b
B28	Sicilian Defense: O'Kelly Variation, Yerevan System		rnbqkbnr/1p1ppppp/p7/2p5/4P3/2N2N2/PPPP1PPP/R1BQKB1R b
B29	Sicilian Defense: Nimzowitsch Variation		rnbqkb1r/pp1ppppp/5n2/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B29	Sicilian Defense: Nimzowitsch Variation, Advance Variation		rnbqkb1r/pp1ppppp/5n2/2p1P3/8/5N2/PPPP1PPP/RNBQKB1R b
B29	Sicilian Defense: Nimzowitsch Variation, Closed Variation		rnbqkb1r/pp1ppppp/5n2/2p5/4P3/2N2N2/PPPP1PPP/R1BQKB1R b
B29	Sicilian Defense: Nimzowitsch Variation, Exchange Variation		rnbqkb1r/pp1ppppp/8/2p1P3/8/2n2N2/PPPP1PPP/R1BQKB1R w
B29	Sicilian Defense: Nimzowitsch Variation, Main Line		r1bqkb1r/pp1p1ppp/2n5/2ppP3/3P4/5N2/PPP2PPP/R1BQKB1R w
B30	Sicilian Defense: Closed Sicilian, Anti-Sveshnikov Variation, Kharlov-Kramnik Line		r1bqk1nr/pp3ppp/2np4/2p1p1b1/2B1P3/2NP4/PPPN1PPP/R1BQK2R w
B30	Sicilian Defense: Nyezhmetdinov-Rossolimo Attack		r1bqkbnr/pp1ppppp/2n5/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R b
B30	Sicilian Defense: Old Sicilian		r1bqkbnr/pp1ppppp/2n5/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B31	Sicilian Defense: Nyezhmetdinov-Rossolimo Attack, Fianchetto Variation		r1bqkbnr/pp1ppp1p/2n3p1/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R w
B31	Sicilian Defense: Nyezhmetdinov-Rossolimo Attack, Fianchetto Variation, Gufeld Gambit		r1bqk1nr/pp1p1pbp/2n3p1/1Bp1p3/3PP3/2P2N2/PP3PPP/RNBQ1RK1 b
B31	Sicilian Defense: Nyezhmetdinov-Rossolimo Attack, Fianchetto Variation, Lutikov Gambit		r1bqk2r/pp1pppbp/2n2np1/1Bp5/3PP3/2P2N2/PP3PPP/RNBQ1RK1 b
B31	Sicilian Defense: Nyezhmetdinov-Rossolimo Attack, Fianchetto Variation, Totsky Attack		r1bqk2r/pp1pppbp/2n2np1/1Bp5/Q3P3/2P2N2/PP1P1PPP/RNB2RK1 b
B31	Sicilian Defense: Nyezhmetdinov-Rossolimo Attack, Gurgenidze Variation		r1bqk1nr/pp1p1pbp/2n3p1/1Bp1p3/1P2P3/5N2/P1PP1PPP/RNBQR1K1 b
B31	Sicilian Defense: Nyezhmetdinov-Rossolimo Attack, San Francisco Gambit		r1bqkbnr/pp1ppppp/8/nBp5/1P2P3/5N2/P1PP1PPP/RNBQK2R b
B31	Sicilian Defense: Rossolimo Variation, Brooklyn Retreat Defense		rnbqkbnr/pp1ppppp/8/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R w
B32	Sicilian Defense: Flohr Variation		r1b1kbnr/ppqppppp/2n5/8/3NP3/8/PPP2PPP/RNBQKB1R w
B32	Sicilian Defense: Franco-Sicilian Variation		r1bqkbnr/pp1p1ppp/2n1p3/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R w
B32	Sicilian Defense: Godiva Variation		r1b1kbnr/pp1ppppp/1qn5/8/3NP3/8/PPP2PPP/RNBQKB1R w
B32	Sicilian Defense: Kalashnikov Variation		r1bqkbnr/pp3ppp/2np4/1N2p3/4P3/8/PPP2PPP/RNBQKB1R w
B32	Sicilian Defense: Loewenthal Variation		r1bqkbnr/pp1p1ppp/2n5/4p3/3NP3/8/PPP2PPP/RNBQKB1R w
B32	Sicilian Defense: Nimzo-American Variation		r1bqkbnr/pp2pppp/2n5/3p4/3NP3/8/PPP2PPP/RNBQKB1R w
B32	Sicilian Defense: Open		r1bqkbnr/pp1ppppp/2n5/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R b
B32	Sicilian Defense: Open #2		r1bqkbnr/pp1ppppp/2n5/8/3pP3/5N2/PPP2PPP/RNBQKB1R w
B32	Sicilian Defense: Open #3		r1bqkbnr/pp1ppppp/2n5/8/3NP3/8/PPP2PPP/RNBQKB1R b
B33	Sicilian Defense: Lasker-Pelikan Variation		r1bqkb1r/pp1p1ppp/2n2n2/4p3/3NP3/2N5/PPP2PPP/R1BQKB1R w
B33	Sicilian Defense: Lasker-Pelikan Variation, Bird Variation		r2qkb1r/1p3ppp/p1npbn2/4p1B1/4P3/N1N5/PPP2PPP/R2QKB1R w
B33	Sicilian Defense: Lasker-Pelikan Variation, Exchange Variation		r1bqkb1r/pp1p1ppp/2N2n2/4p3/4P3/2N5/PPP2PPP/R1BQKB1R b
B33	Sicilian Defense: Lasker-Pelikan Variation, Retreat Variation		r1bqkb1r/pp1p1ppp/2n2n2/4p3/4P3/2N2N2/PPP2PPP/R1BQKB1R b
B33	Sicilian Defense: Lasker-Pelikan Variation, Schlechter Variation		r1bqkb1r/pp1p1ppp/2n2n2/4p3/4P3/1NN5/PPP2PPP/R1BQKB1R b
B33	Sicilian Defense: Lasker-Pelikan Variation, Sveshnikov Variation		r1bqkb1r/5ppp/p1np1n2/1p2p1B1/4P3/N1N5/PPP2PPP/R2QKB1R w
B33	Sicilian Defense: Lasker-Pelikan Variation, Sveshnikov Variation #2		r1bqkb1r/5p1p/p1np4/1p1Npp2/4P3/N7/PPP2PPP/R2QKB1R w
B33	Sicilian Defense: Lasker-Pelikan Variation, Sveshnikov Variation, Chelyabinsk Variation		r1bqkb1r/5ppp/p1np1n2/1p1Np1B1/4P3/N7/PPP2PPP/R2QKB1R b
B33	Sicilian Defense: Lasker-Pelikan Variation, Sveshnikov Variation, Novosibirsk Variation		r1bqk2r/5pbp/p1np1p2/1p1Np3/4P3/N7/PPP2PPP/R2QKB1R w
B33	Sicilian Defense: Lasker-Pelikan Variation, Sveshnikov Variation, Peresypkin's Sacrifice		r1bqkb1r/5p1p/2np4/1N1Npp2/4P3/8/PPP2PPP/R2QK2R b
B33	Sicilian Defense: Open		r1bqkb1r/pp1ppppp/2n2n2/8/3NP3/8/PPP2PPP/RNBQKB1R w
B34	Sicilian Defense: Accelerated Dragon		rnbqkbnr/pp2pp1p/3p2p1/8/3NP3/8/PPP2PPP/RNBQKB1R w
B34	Sicilian Defense: Accelerated Dragon, Exchange Variation		r1bqkbnr/pp1ppp1p/2N3p1/8/4P3/8/PPP2PPP/RNBQKB1R b
B34	Sicilian Defense: Accelerated Dragon, Modern Variation		r1bqkbnr/pp1ppp1p/2n3p1/8/3NP3/2N5/PPP2PPP/R1BQKB1R b
B35	Sicilian Defense: Accelerated Dragon, Modern Bc4 Variation		r1bqk2r/pp1pppbp/2n2np1/8/2BNP3/2N1B3/PPP2PPP/R2QK2R b
B35	Sicilian Defense: Dragon Variation, Modern Bc4 Variation		r1bqk2r/pp2ppbp/2np1np1/8/2BNP3/2N1B3/PPP2PPP/R2QK2R w
B36	Sicilian Defense: Accelerated Dragon		r1bqkbnr/pp1ppp1p/2n3p1/8/3NP3/8/PPP2PPP/RNBQKB1R w
B36	Sicilian Defense: Accelerated Dragon, Maróczy Bind		r1bqkbnr/pp1ppp1p/2n3p1/8/2PNP3/8/PP3PPP/RNBQKB1R b
B36	Sicilian Defense: Accelerated Dragon, Maróczy Bind, Gurgenidze Variation		r1bqkb1r/pp2pp1p/3p1np1/8/2PQP3/2N5/PP3PPP/R1B1KB1R w
B37	Sicilian Defense: Accelerated Fianchetto, Maróczy Bind, 5...Bg7		r1bqk1nr/pp1pppbp/2n3p1/8/2PNP3/8/PP3PPP/RNBQKB1R w
B37	Sicilian Defense: Accelerated Fianchetto, Simagin Variation		r1br2k1/1p5p/pR1B1b2/2Pp4/6n1/5N2/P3BPP1/4R1K1 w
B38	Sicilian Defense: Accelerated Dragon, Maróczy Bind		r1bqk1nr/pp1pppbp/2n3p1/8/2PNP3/4B3/PP3PPP/RN1QKB1R b
B39	Sicilian Defense: Accelerated Dragon, Maróczy Bind, Breyer Variation		r1bqk2r/pp1pppbp/2n3p1/8/2PNP1n1/2N1B3/PP3PPP/R2QKB1R w
B40	Sicilian Defense: Delayed Alapin Variation		rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R b
B40	Sicilian Defense: Drazic Variation		rnbqkbnr/1p1p1ppp/p3p3/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R w
B40	Sicilian Defense: French Variation		rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B40	Sicilian Defense: French Variation, Normal		rnbqkb1r/pp1p1ppp/4pn2/8/3NP3/8/PPP2PPP/RNBQKB1R w
B40	Sicilian Defense: French Variation, Open		rnbqkbnr/pp1p1ppp/4p3/8/3pP3/5N2/PPP2PPP/RNBQKB1R w
B40	Sicilian Defense: French Variation, Westerinen Attack		rnbqkbnr/pp1p1ppp/4p3/2p5/4P3/1P3N2/P1PP1PPP/RNBQKB1R b
B40	Sicilian Defense: Gaw-Paw Variation		rnb1kb1r/pp1p1ppp/1q2pn2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B40	Sicilian Defense: Kramnik Variation		rnbqkbnr/pp1p1ppp/4p3/2p5/2P1P3/5N2/PP1P1PPP/RNBQKB1R b
B40	Sicilian Defense: Kveinis Variation		rnb1kbnr/pp1p1ppp/1q2p3/8/3NP3/8/PPP2PPP/RNBQKB1R w
B40	Sicilian Defense: Marshall Counterattack		rnbqkbnr/pp3ppp/4p3/2pp4/3PP3/5N2/PPP2PPP/RNBQKB1R w
B40	Sicilian Defense: Paulsen-Basman Defense		rnbqk1nr/pp1p1ppp/4p3/2b5/3NP3/8/PPP2PPP/RNBQKB1R w
B40	Sicilian Defense: Pin Variation		rnbqk2r/pp1p1ppp/4pn2/8/1b1NP3/2N5/PPP2PPP/R1BQKB1R w
B40	Sicilian Defense: Pin Variation, Jaffe Variation		rnbqk2r/pp1p1ppp/5n2/4p3/1b1NP3/2NB4/PPP2PPP/R1BQK2R w
B40	Sicilian Defense: Pin Variation, Koch Variation		rnbqk2r/pp1p1ppp/4pn2/4P3/1b1N4/2N5/PPP2PPP/R1BQKB1R b
B40	Sicilian Defense: Smith-Morra Gambit Deferred		rnbqkbnr/pp1p1ppp/4p3/8/3pP3/2P2N2/PP3PPP/RNBQKB1R b
B40	Sicilian Defense: Wing Gambit Deferred		rnbqkbnr/pp1p1ppp/4p3/2p5/1P2P3/5N2/P1PP1PPP/RNBQKB1R b
B41	Sicilian Defense: Kan Variation		rnbqkbnr/1p1p1ppp/p3p3/8/3NP3/8/PPP2PPP/RNBQKB1R w
B41	Sicilian Defense: Kan Variation, Maróczy Bind, Bronstein Variation		r1bqk2r/1p1p1ppp/p1n1pn2/8/1bPNP3/2NB4/PP3PPP/R1BQK2R w
B41	Sicilian Defense: Kan Variation, Maróczy Bind, Hedgehog Variation		rnbqkbnr/1p1p1p1p/p3p1p1/8/2PNP3/8/PP3PPP/RNBQKB1R w
B41	Sicilian Defense: Kan Variation, Maróczy Bind, Réti Variation		rnbqkbnr/1p1p1ppp/p3p3/8/2PNP3/8/PP3PPP/RNBQKB1R b
B41	Sicilian Defense: Kan, Maróczy Bind, Bronstein Variation		r4rk1/1p1q2pp/pB1p2p1/3Pb2n/1P6/P3Q3/2B3PP/4RR1K w
B42	Sicilian Defense: Kan Variation, Modern Variation		rnbqkbnr/1p1p1ppp/p3p3/8/3NP3/3B4/PPP2PPP/RNBQK2R b
B42	Sicilian Defense: Kan Variation, Polugaevsky Variation		rnbqk1nr/1p1p1ppp/p3p3/2b5/3NP3/3B4/PPP2PPP/RNBQK2R w
B42	Sicilian Defense: Kan Variation, Swiss Cheese Variation		rnbqkbnr/1p1p1p1p/p3p1p1/8/3NP3/3B4/PPP2PPP/RNBQK2R w
B42	Sicilian Defense: Kan, Gipslis Variation		rnbqkb1r/1p3p1p/p2ppnp1/8/2PNP3/3B4/PP3PPP/RNBQ1RK1 w
B43	Sicilian Defense: Kan Variation, Knight Variation		rnbqkbnr/1p1p1ppp/p3p3/8/3NP3/2N5/PPP2PPP/R1BQKB1R b
B43	Sicilian Defense: Kan Variation, Wing Attack		rnbqkbnr/3p1ppp/p3p3/1p6/3NP3/2N5/PPP2PPP/R1BQKB1R w
B43	Sicilian Defense: Kan Variation, Wing Attack, Fianchetto Variation		rnbqkbnr/3p1ppp/p3p3/1p6/3NP3/2N3P1/PPP2P1P/R1BQKB1R b
B43	Sicilian Defense: Kan Variation, Wing Attack, Spraggett Attack		rnb1kbnr/3p1ppp/pq2p3/1p6/4P3/2NB1N2/PPP2PPP/R1BQK2R b
B44	Sicilian Defense: Paulsen Variation		r1bqkbnr/pp1p1ppp/2n1p3/8/3NP3/8/PPP2PPP/RNBQKB1R w
B44	Sicilian Defense: Paulsen Variation, Gary Gambit		r1bqkb1r/1p3ppp/p1n2n2/3p4/2P5/N1N5/PP3PPP/R1BQKB1R w
B44	Sicilian Defense: Paulsen Variation, Modern Line		r1bq1rk1/4bppp/ppnppn2/8/2P1P3/N1N5/PP2BPPP/R1BQ1RK1 w
B44	Sicilian Defense: Paulsen Variation, Szén Variation		r1bqkbnr/pp1p1ppp/2n1p3/1N6/4P3/8/PPP2PPP/RNBQKB1R b
B45	Sicilian Defense: Four Knights Variation		r1bqkb1r/pp1p1ppp/2n1pn2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B45	Sicilian Defense: Four Knights Variation, Cobra Variation		r1bqk2r/pp1p1ppp/2n1pn2/1Nb5/4P3/2N5/PPP2PPP/R1BQKB1R w
B45	Sicilian Defense: Four Knights Variation, Exchange Variation		r1bqkb1r/pp1p1ppp/2N1pn2/8/4P3/2N5/PPP2PPP/R1BQKB1R b
B45	Sicilian Defense: Paulsen Variation, American Attack		r1bqk2r/pp1p1ppp/2nNpn2/8/1b2P3/2N5/PPP2PPP/R1BQKB1R b
B45	Sicilian Defense: Paulsen Variation, Normal Variation		r1bqkbnr/pp1p1ppp/2n1p3/8/3NP3/2N5/PPP2PPP/R1BQKB1R b
B46	Sicilian Defense: Paulsen Variation		r1bqkbnr/1p1p1ppp/p1n1p3/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B46	Sicilian Defense: Paulsen Variation, Taimanov Variation		r1bqkb1r/1p1pnppp/p1n1p3/8/3NP3/2N5/PPP1BPPP/R1BQK2R w
B47	Sicilian Defense: Paulsen Variation, Bastrikov Variation		r1b1kbnr/ppqp1ppp/2n1p3/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B48	Sicilian Defense: Paulsen Variation, Bastrikov Variation		r1b1kbnr/ppqp1ppp/2n1p3/8/3NP3/2N1B3/PPP2PPP/R2QKB1R b
B48	Sicilian Defense: Paulsen Variation, Bastrikov Variation, English Attack		r1b1kbnr/1pqp1ppp/p1n1p3/8/3NP3/2N1B3/PPPQ1PPP/R3KB1R b
B48	Sicilian Defense: Paulsen Variation, Bastrikov Variation, Ponomariov Gambit		rqb1kbnr/1p1p1ppp/pBn1p3/1N6/4P3/2N5/PPP2PPP/R2QKB1R b
B49	Sicilian Defense: Paulsen Variation, Bastrikov Variation		r1b1kbnr/1pqp1ppp/p1n1p3/8/3NP3/2N1B3/PPP1BPPP/R2QK2R b
B49	Sicilian Defense: Paulsen Variation, Bastrikov Variation, English Attack		r1b1kbnr/1pqp1ppp/p1n1p3/8/3NP3/2N1BP2/PPP3PP/R2QKB1R b
B50	Sicilian Defense		rnbqkbnr/pp2pppp/3p4/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R w
B50	Sicilian Defense: Delayed Alapin		rnbqkbnr/pp2pppp/3p4/2p5/4P3/2P2N2/PP1P1PPP/RNBQKB1R b
B50	Sicilian Defense: Delayed Alapin, Basman-Palatnik Double Gambit		r1b1kb1r/pp2pppp/2np4/q2P4/8/2P2N2/P3BPPP/R1BQK2R b
B50	Sicilian Defense: Delayed Alapin, Basman-Palatnik Gambit		r1bqkb1r/pp2pppp/2np4/8/3Pn3/5N2/PP2BPPP/RNBQK2R w
B50	Sicilian Defense: Kotov Gambit		rnbqkbnr/p3pppp/3p4/1pp5/4P3/5NP1/PPPP1P1P/RNBQKB1R w
B50	Sicilian Defense: Modern Variations, Anti-Qxd4 Move Order		rnbqkb1r/pp2pppp/3p1n2/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R w
B50	Sicilian Defense: Modern Variations, Anti-Qxd4 Move Order Accepted		rnbqkb1r/pp2pppp/3p4/2P5/4n3/5N2/PPP2PPP/RNBQKB1R w
B50	Sicilian Defense: Wing Gambit, Deferred Variation		rnbqkbnr/pp2pppp/3p4/2p5/1P2P3/5N2/P1PP1PPP/RNBQKB1R b
B51	Sicilian Defense: Canal Attack, Haag Gambit		r3kb1r/pp1qpppp/2np1n2/2p5/3PP3/2P2N2/PP3PPP/RNBQ1RK1 b
B51	Sicilian Defense: Canal Attack, Moscow Gambit		r2qkb1r/1p2pppp/p2p1n2/2p3B1/3Pb3/2P2N2/PP3PPP/RN1QR1K1 b
B51	Sicilian Defense: Canal-Sokolsky Attack		rnbqkbnr/pp2pppp/3p4/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R b
B52	Sicilian Defense: Canal Attack, Dorfman Gambit		r2qkbnr/pp1bpp1p/2np2p1/1Bp1P3/8/5N2/PPPPQPPP/RNB2RK1 b
B52	Sicilian Defense: Canal Attack, Main Line		rn1qkbnr/pp1bpppp/3p4/1Bp5/4P3/5N2/PPPP1PPP/RNBQK2R w
B52	Sicilian Defense: Canal-Sokolsky Attack, Sokolsky Variation		rn2kbnr/pp1qpppp/3p4/2p5/2P1P3/5N2/PP1P1PPP/RNBQK2R b
B53	Sicilian Defense: Chekhover Variation		rnbqkbnr/pp2pppp/3p4/8/3QP3/5N2/PPP2PPP/RNB1KB1R b
B53	Sicilian Defense: Chekhover Variation, Zaitsev Defense		r1b1kbnr/pp1qpppp/2np4/1B6/3QP3/5N2/PPP2PPP/RNB1K2R w
B53	Sicilian Defense: Modern Variations, Tartakower		rnbqkbnr/pp2pppp/3p4/8/3pP3/2P2N2/PP3PPP/RNBQKB1R b
B54	Sicilian Defense		rnbqkbnr/pp2pppp/3p4/2p5/3PP3/5N2/PPP2PPP/RNBQKB1R b
B54	Sicilian Defense #2		rnbqkbnr/pp2pppp/3p4/8/3pP3/5N2/PPP2PPP/RNBQKB1R w
B54	Sicilian Defense: Prins Variation		rnbqkb1r/pp2pppp/3p1n2/8/3NP3/5P2/PPP3PP/RNBQKB1R b
B55	Sicilian Defense: Prins Variation, Venice Attack		rnbqkb1r/pp3ppp/3p1n2/1B2p3/3NP3/5P2/PPP3PP/RNBQK2R b
B56	Sicilian Defense: Classical Variation		r1bqkb1r/pp2pppp/2np1n2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B56	Sicilian Defense: Kupreichik Variation		rn1qkb1r/pp1bpppp/3p1n2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B56	Sicilian Defense: Modern Variations		rnbqkb1r/pp2pppp/3p1n2/8/3NP3/2N5/PPP2PPP/R1BQKB1R b
B56	Sicilian Defense: Spielmann Variation		r1bqkb1r/pp2pppp/2np1n2/8/4P3/2N5/PPP1NPPP/R1BQKB1R b
B56	Sicilian Defense: Venice Attack		rnbqkb1r/pp3ppp/3p1n2/1B2p3/3NP3/2N5/PPP2PPP/R1BQK2R b
B56	Sicilian Defense: Yates Variation		r1bqkb1r/pp2pppp/2np1n2/8/3NP3/2NB4/PPP2PPP/R1BQK2R b
B57	Sicilian Defense: Classical Variation, Anti-Sozin Variation		r1b1kb1r/pp2pppp/1qnp1n2/8/2BNP3/2N5/PPP2PPP/R1BQK2R w
B57	Sicilian Defense: Magnus Smith Trap		r1bqkb1r/p3pp1p/2pp1np1/4P3/2B5/2N5/PPP2PPP/R1BQK2R b
B57	Sicilian Defense: Modern Variations, Main Line		rnbqkb1r/pp2pppp/3p1n2/8/3NP3/8/PPP2PPP/RNBQKB1R w
B57	Sicilian Defense: Sozin, not Scheveningen		r1bqkb1r/pp2pppp/2np1n2/8/2BNP3/2N5/PPP2PPP/R1BQK2R b
B58	Sicilian Defense: Boleslavsky Variation		r1bqkb1r/pp3ppp/2np1n2/4p3/3NP3/2N5/PPP1BPPP/R1BQK2R w
B58	Sicilian Defense: Boleslavsky Variation, Louma Variation		r1bqkb1r/p4ppp/2pp1n2/4p3/4P3/2N5/PPP1BPPP/R1BQK2R w
B58	Sicilian Defense: Classical Variation		r1bqkb1r/pp2pppp/2np1n2/8/3NP3/2N5/PPP1BPPP/R1BQK2R b
B58	Sicilian Defense: Classical Variation, Dragon Transfer		r1bqkb1r/pp2pp1p/3p1np1/8/3QP3/2N5/PPP1BPPP/R1B1K2R w
B58	Sicilian Defense: Classical Variation, Fianchetto Variation		r1bqkb1r/pp2pppp/2np1n2/8/3NP3/2N3P1/PPP2P1P/R1BQKB1R b
B59	Sicilian Defense: Boleslavsky Variation		r1bqkb1r/pp3ppp/2np1n2/4p3/4P3/1NN5/PPP1BPPP/R1BQK2R b
B60	Sicilian Defense: Richter-Rauzer Variation		r1bqkb1r/pp2pppp/2np1n2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R b
B60	Sicilian Defense: Richter-Rauzer Variation, Dragon Variation		r1bqkb1r/pp2pp1p/2np1np1/6B1/3NP3/2N5/PPP2PPP/R2QKB1R w
B60	Sicilian Defense: Richter-Rauzer Variation, Modern Variation		r2qkb1r/pp1bpppp/2np1n2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R w
B61	Sicilian Defense: Richter-Rauzer Variation, Modern Variation		r2qkb1r/pp1bpppp/2np1n2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R b
B62	Sicilian Defense: Richter-Rauzer Variation		r1bqkb1r/pp3ppp/2nppn2/6B1/3NP3/2NQ4/PPP2PPP/R3KB1R b
B62	Sicilian Defense: Richter-Rauzer Variation, Exchange Variation		r1bqkb1r/p4ppp/2pppn2/6B1/4P3/2N5/PPP2PPP/R2QKB1R w
B62	Sicilian Defense: Richter-Rauzer Variation, Neo-Modern Variation, Early Deviations		r1bqkb1r/1p3ppp/p1nppn2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R w
B62	Sicilian Defense: Richter-Rauzer Variation, Podebrady Variation		r1bqkb1r/pp3ppp/2nppn2/6B1/4P3/1NN5/PPP2PPP/R2QKB1R b
B62	Sicilian Defense: Richter-Rauzer Variation, Vitolins Variation		r1bqkb1r/pp3ppp/2nppn2/1B4B1/3NP3/2N5/PPP2PPP/R2QK2R b
B62	Sicilian Defense: Richter-Rauzer, 6...e6		r1bqkb1r/pp3ppp/2nppn2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R w
B63	Sicilian Defense: Richter-Rauzer Variation, Classical Variation		r1bqk2r/pp2bppp/2nppn2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R w
B63	Sicilian Defense: Richter-Rauzer Variation, Ivanov Variation		r1b1kb1r/pp3ppp/1qnppn2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R w
B63	Sicilian Defense: Richter-Rauzer Variation, Traditional Variation		r1bqkb1r/pp3ppp/2nppn2/6B1/3NP3/2N5/PPPQ1PPP/R3KB1R b
B64	Sicilian Defense: Richter-Rauzer Variation, Classical Variation		r1bq1rk1/pp2bppp/2nppn2/6B1/3NPP2/2N5/PPPQ2PP/2KR1B1R b
B64	Sicilian Defense: Richter-Rauzer Variation, Classical Variation #2		r1bq1rk1/pp2bppp/2np1n2/4p1B1/3NPP2/2N5/PPPQ2PP/2KR1B1R w
B65	Sicilian Defense: Richter-Rauzer Variation, Classical Variation		r1bq1rk1/pp2bppp/3ppn2/6B1/3QPP2/2N5/PPP3PP/2KR1B1R b
B66	Sicilian Defense: Richter-Rauzer Variation, Classical Variation, Kantscher Line		r1bqk2r/4bppp/p2ppn2/1p4B1/3QPP2/2N5/PPP3PP/2KR1B1R w
B67	Sicilian Defense: Richter-Rauzer Variation, Neo-Modern Variation		r2qkb1r/1p1b1ppp/p1nppn2/6B1/3NP3/2N5/PPPQ1PPP/2KR1B1R w
B68	Sicilian Defense: Richter-Rauzer Variation, Neo-Modern Variation		r2qk2r/1p1bbppp/p1nppn2/6B1/3NPP2/2N5/PPPQ2PP/2KR1B1R w
B70	Sicilian Defense: Dragon Variation		rnbqkb1r/pp2pp1p/3p1np1/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B70	Sicilian Defense: Dragon Variation, Fianchetto Variation		rnbqkb1r/pp2pp1p/3p1np1/8/3NP3/2N3P1/PPP2P1P/R1BQKB1R b
B71	Sicilian Defense: Dragon Variation, Levenfish Variation		rnbqkb1r/pp2pp1p/3p1np1/8/3NPP2/2N5/PPP3PP/R1BQKB1R b
B71	Sicilian Defense: Dragon Variation, Levenfish Variation, Main Line		r1bqkb1r/pp1npp1p/3p1np1/8/3NPP2/2N5/PPP3PP/R1BQKB1R w
B72	Sicilian Defense: Dragon Variation, Classical Variation		r1bqk2r/pp2ppbp/2np1np1/8/4P3/1NN1B3/PPP1BPPP/R2QK2R b
B72	Sicilian Defense: Dragon Variation, Classical Variation #2		rnbqkb1r/pp2pp1p/3p1np1/8/3NP3/2N5/PPP1BPPP/R1BQK2R b
B72	Sicilian Defense: Dragon, 6.Be3		rnbqkb1r/pp2pp1p/3p1np1/8/3NP3/2N1B3/PPP2PPP/R2QKB1R b
B72	Sicilian Defense: Dragon, Classical Attack		rnbqk2r/pp2ppbp/3p1np1/8/3NP3/2N1B3/PPP1BPPP/R2QK2R b
B72	Sicilian Defense: Dragon, Classical, Amsterdam Variation		r1bqk2r/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPPQBPPP/R3K2R b
B72	Sicilian Defense: Dragon, Classical, Grigoriev Variation		r1bq1rk1/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPPQBPPP/2KR3R b
B73	Sicilian Defense: Dragon Variation, Classical Variation		r1bqk2r/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPP1BPPP/R2Q1RK1 b
B73	Sicilian Defense: Dragon Variation, Classical Variation #2		r1bqk2r/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPP1BPPP/R2QK2R w
B73	Sicilian Defense: Dragon Variation, Classical Variation, Battery Variation		r1bq1rk1/pp2ppbp/2np1np1/8/3NP3/2N1B3/PPPQBPPP/R4RK1 b
B73	Sicilian Defense: Dragon, Classical, Zollner Gambit		r1b2rk1/pp2ppbp/1qn2np1/4P3/3N4/2N1B3/PPP1B1PP/R2Q1RK1 b
B74	Sicilian Defense: Dragon Variation, Classical Variation, Alekhine Line		r1bq1rk1/1p2ppbp/2np1np1/p7/4P3/1NN1B3/PPP1BPPP/R2Q1RK1 w
B74	Sicilian Defense: Dragon Variation, Classical Variation, Maróczy Line		r2q1rk1/pp2ppbp/3pbnp1/n7/4PP2/1NN1B3/PPP1B1PP/R2Q1RK1 w
B74	Sicilian Defense: Dragon Variation, Classical Variation, Normal Line		r1bq1rk1/pp2ppbp/2np1np1/8/4P3/1NN1B3/PPP1BPPP/R2Q1RK1 b
B74	Sicilian Defense: Dragon Variation, Classical Variation, Tartakower Line		r1q2rk1/pp2ppbp/2npbnp1/8/4PP2/1NN1B3/PPP1B1PP/R2Q1RK1 w
B74	Sicilian Defense: Dragon, Classical, Spielmann Variation		2r5/1p2p1k1/p2p1p2/2q1n1p1/3RP3/1PP5/1P2Q1PP/5R1K w
B75	Sicilian Defense: Dragon Variation, Yugoslav Attack, Early Deviations		rnbqk2r/pp2ppbp/3p1np1/8/3NP3/2N1BP2/PPP3PP/R2QKB1R b
B76	Sicilian Defense: Dragon Variation, Yugoslav Attack		rnbq1rk1/pp2ppbp/3p1np1/8/3NP3/2N1BP2/PPP3PP/R2QKB1R w
B76	Sicilian Defense: Dragon Variation, Yugoslav Attack, Belezky Line		r1bqk2r/pp2ppbp/2np1np1/8/3NP3/2N1BP2/PPP3PP/R2QKB1R w
B76	Sicilian Defense: Dragon Variation, Yugoslav Attack, Modern Line		r1bq1rk1/pp2ppbp/2np1np1/8/3NP3/2N1BP2/PPPQ2PP/2KR1B1R b
B76	Sicilian Defense: Dragon Variation, Yugoslav Attack, Panov Variation		r1bq1rk1/pp2ppbp/2np1np1/8/3NP1P1/2N1BP2/PPPQ3P/R3KB1R b
B77	Sicilian Defense: Dragon Variation, Yugoslav Attack		r2q1rk1/pp1bppbp/2np1np1/8/2BNP3/2N1BP2/PPPQ2PP/R3K2R w
B77	Sicilian Defense: Dragon Variation, Yugoslav Attack #2		r1bq1rk1/pp2ppbp/2np1np1/8/3NP3/2N1BP2/PPPQ2PP/R3KB1R w
B77	Sicilian Defense: Dragon Variation, Yugoslav Attack, Czerniak Variation		r2q1rk1/pp2ppbp/3pbnp1/8/2BBP3/2N2P2/PPPQ2PP/R3K2R w
B77	Sicilian Defense: Dragon Variation, Yugoslav Attack, Main Line		r1bq1rk1/pp2ppbp/2np1np1/8/2BNP3/2N1BP2/PPPQ2PP/R3K2R b
B77	Sicilian Defense: Dragon Variation, Yugoslav Attack, Sosonko Variation		r1bq1rk1/pp1nppbp/2np2p1/8/2BNP3/2N1BP2/PPPQ2PP/R3K2R w
B77	Sicilian Defense: Dragon, Yugoslav Attack, Byrne Variation		r1bq1rk1/1p2ppbp/2np1np1/p7/2BNP3/2N1BP2/PPPQ2PP/R3K2R w
B78	Sicilian Defense: Dragon Variation, Yugoslav Attack		r2q1rk1/pp1bppbp/2np1np1/8/2BNP3/2N1BP2/PPPQ2PP/2KR3R b
B78	Sicilian Defense: Dragon Variation, Yugoslav Attack, Old Line		2rq1rk1/pp1bppbp/2np1np1/8/2BNP3/2N1BP2/PPPQ2PP/2KR3R w
B79	Sicilian Defense: Dragon Variation, Yugoslav Attack, Soltis Variation		r1r3k1/pp1bppb1/2np1np1/q6p/3NP2P/1BN1BP2/PPPQ2P1/2KR3R w
B79	Sicilian Defense: Dragon, Yugoslav Attack, 12.h4		r1r3k1/pp1bppbp/2np1np1/q7/3NP2P/1BN1BP2/PPPQ2P1/2KR3R b
B80	Sicilian Defense: Scheveningen Variation		rnbqkb1r/pp3ppp/3ppn2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B80	Sicilian Defense: Scheveningen Variation, English Attack		rnbqkb1r/1p3ppp/p2ppn2/8/3NP3/2N1B3/PPPQ1PPP/R3KB1R b
B80	Sicilian Defense: Scheveningen Variation, English Attack #2		rnbqkb1r/1p3ppp/p2ppn2/8/3NP3/2N1BP2/PPP3PP/R2QKB1R b
B80	Sicilian Defense: Scheveningen Variation, Fianchetto Variation		rnbqkb1r/pp3ppp/3ppn2/8/3NP3/2N3P1/PPP2P1P/R1BQKB1R b
B80	Sicilian Defense: Scheveningen Variation, Vitolins Variation		rnbqkb1r/pp3ppp/3ppn2/1B6/3NP3/2N5/PPP2PPP/R1BQK2R b
B81	Sicilian Defense: Scheveningen Variation, Delayed Keres Attack		rnbqkb1r/1p3ppp/p2ppn2/8/3NP1P1/2N1B3/PPP2P1P/R2QKB1R b
B81	Sicilian Defense: Scheveningen Variation, Delayed Keres Attack, Perenyi Gambit		rnbqkb1r/1p3p1p/p2p1np1/4pNP1/4P3/2N1B3/PPP2P1P/R2QKB1R b
B81	Sicilian Defense: Scheveningen Variation, Keres Attack		rnbqkb1r/pp3ppp/3ppn2/8/3NP1P1/2N5/PPP2P1P/R1BQKB1R b
B82	Sicilian Defense: Scheveningen Variation, Matanovic Attack		rnbqkb1r/pp3ppp/3ppn2/8/3NPP2/2N5/PPP3PP/R1BQKB1R b
B82	Sicilian Defense: Scheveningen Variation, Tal Variation		r1bqk2r/pp2bppp/2nppn2/8/3NPP2/2N1BQ2/PPP3PP/R3KB1R b
B83	Sicilian Defense: Scheveningen Variation, Classical Variation		rnbqkb1r/pp3ppp/3ppn2/8/3NP3/2N5/PPP1BPPP/R1BQK2R b
B83	Sicilian Defense: Scheveningen Variation, Modern Variation		r1bq1rk1/pp2bppp/2nppn2/8/3NPP2/2N1B3/PPP1B1PP/R2Q1RK1 b
B83	Sicilian Defense: Scheveningen Variation, Modern Variation #2		1q6/1p1bNk1n/p2p1p1p/4pP2/Pn2P2P/1N2B1Q1/1Pr3B1/3R3K w
B83	Sicilian Defense: Scheveningen Variation, Modern Variation #3		r1bqkb1r/pp3ppp/2nppn2/8/3NP3/2N5/PPP1BPPP/R1BQK2R w
B84	Sicilian Defense: Najdorf Variation, Scheveningen Variation		r1bqkb1r/1p1n1ppp/p2ppn2/8/3NP3/2N5/PPP1BPPP/R1BQ1RK1 w
B84	Sicilian Defense: Scheveningen Variation, Classical Variation		rnbqkb1r/1p3ppp/p2ppn2/8/3NP3/2N5/PPP1BPPP/R1BQK2R w
B84	Sicilian Defense: Scheveningen Variation, Classical Variation #2		rnb1kb1r/1pq2ppp/p2ppn2/8/3NP3/2N5/PPP1BPPP/R1BQ1RK1 w
B85	Sicilian Defense: Scheveningen Variation, Classical Variation, Paulsen Variation		r1b1kb1r/1pq2ppp/p1nppn2/8/3NPP2/2N1B3/PPP1B1PP/R2Q1RK1 b
B85	Sicilian Defense: Scheveningen Variation, Classical Variation, Paulsen Variation #2		r1b1kb1r/1pq2ppp/p1nppn2/8/3NPP2/2N5/PPP1B1PP/R1BQ1RK1 w
B85	Sicilian Defense: Scheveningen Variation, Classical Variation, Paulsen Variation #3		r1b1k2r/1pq1bppp/p1nppn2/8/P2NPP2/2N5/1PP1B1PP/R1BQ1R1K b
B85	Sicilian Defense: Scheveningen, Classical Main Line		r1b2rk1/1pq1bppp/p1nppn2/8/3NPP2/2N1B3/PPP1B1PP/R3QRK1 w
B86	Sicilian Defense: Sozin Attack		rnbqkb1r/pp3ppp/3ppn2/8/2BNP3/2N5/PPP2PPP/R1BQK2R b
B87	Sicilian Defense: Sozin Attack, Flank Variation		rnbqkb1r/5ppp/p2ppn2/1p6/3NP3/1BN5/PPP2PPP/R1BQK2R w
B88	Sicilian Defense: Sozin Attack, Leonhardt Variation		r1bqkb1r/pp3ppp/2nppn2/8/2BNP3/2N5/PPP2PPP/R1BQK2R w
B88	Sicilian Defense: Sozin, Fischer Variation		r1bq1rk1/pp2bppp/2nppn2/8/3NPP2/1BN1B3/PPP3PP/R2QK2R b
B89	Sicilian Defense: Sozin Attack, Main Line		r1bqkb1r/pp3ppp/2nppn2/8/2BNP3/2N1B3/PPP2PPP/R2QK2R b
B89	Sicilian Defense: Velimirovic Attack		r1bqk2r/pp2bppp/2nppn2/8/2BNP3/2N1B3/PPP1QPPP/R3K2R b
B90	Sicilian Defense: Najdorf Variation		rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N5/PPP2PPP/R1BQKB1R w
B90	Sicilian Defense: Najdorf Variation, Adams Attack		rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N4P/PPP2PP1/R1BQKB1R b
B90	Sicilian Defense: Najdorf Variation, Dekker Gambit		rnbqkb1r/1p2pppp/p2p1n2/8/3NP1P1/2N5/PPP2P1P/R1BQKB1R b
B90	Sicilian Defense: Najdorf Variation, English Attack		rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N1B3/PPP2PPP/R2QKB1R b
B90	Sicilian Defense: Najdorf Variation, English Attack, Anti-English		rnbqkb1r/1p2pppp/p2p4/8/3NP1n1/2N1B3/PPP2PPP/R2QKB1R w
B90	Sicilian Defense: Najdorf, Lipnitsky Attack		rnbqkb1r/1p2pppp/p2p1n2/8/2BNP3/2N5/PPP2PPP/R1BQK2R b
B91	Sicilian Defense: Najdorf Variation, Zagreb (Fianchetto) Variation		rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N3P1/PPP2P1P/R1BQKB1R b
B92	Sicilian Defense: Najdorf Variation, Opocensky Variation		rnbqkb1r/1p2pppp/p2p1n2/8/3NP3/2N5/PPP1BPPP/R1BQK2R b
B92	Sicilian Defense: Najdorf Variation, Opocensky Variation, Modern Line		rn1qk2r/1p2bppp/p2pbn2/4p3/4P3/1NN5/PPP1BPPP/R1BQ1RK1 w
B92	Sicilian Defense: Najdorf Variation, Opocensky Variation, Traditional Line		rnbq1rk1/1p2bppp/p2p1n2/4p3/4P3/1NN5/PPP1BPPP/R1BQ1RK1 w
B93	Sicilian Defense: Najdorf Variation, Amsterdam Variation		rnbqkb1r/1p2pppp/p2p1n2/8/3NPP2/2N5/PPP3PP/R1BQKB1R b
B94	Sicilian Defense: Najdorf Variation		rnbqkb1r/1p2pppp/p2p1n2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R b
B95	Sicilian Defense: Najdorf Variation		rnbqkb1r/1p3ppp/p2ppn2/6B1/3NP3/2N5/PPP2PPP/R2QKB1R w
B96	Sicilian Defense: Najdorf Variation		rnbqkb1r/1p3ppp/p2ppn2/6B1/3NPP2/2N5/PPP3PP/R2QKB1R b
B96	Sicilian Defense: Najdorf Variation, Neo-Classical Defense		r1bqkb1r/1p3ppp/p1nppn2/6B1/3NPP2/2N5/PPP3PP/R2QKB1R w
B96	Sicilian Defense: Najdorf Variation, Polugaevsky Variation		rnbqkb1r/5ppp/p2ppn2/1p4B1/3NPP2/2N5/PPP3PP/R2QKB1R w
B96	Sicilian Defense: Najdorf Variation, Polugaevsky Variation, Simagin Line		rnb1kb1r/2qn1ppp/p3p3/1p2P1B1/3N4/2N5/PPP1Q1PP/R3KB1R w
B97	Sicilian Defense: Najdorf Variation, Poisoned Pawn Variation		rnb1kb1r/1p3ppp/pq1ppn2/6B1/3NPP2/2N5/PPP3PP/R2QKB1R w
B98	Sicilian Defense: Najdorf Variation		rnbqk2r/1p2bppp/p2ppn2/6B1/3NPP2/2N5/PPP3PP/R2QKB1R w
B98	Sicilian Defense: Najdorf Variation, Browne Variation		rnb1k2r/1pq1bpp1/p2ppn1p/8/3NPP1B/2N2Q2/PPP3PP/R3KB1R w
B98	Sicilian Defense: Najdorf Variation, Goteborg (Argentine)		rnbqk2r/1p2bp2/p2ppn1p/6p1/3NPP1B/2N2Q2/PPP3PP/R3KB1R w
B98	Sicilian Defense: Najdorf Variation, Traditional Line		rnb1k2r/1pq1bppp/p2ppn2/6B1/3NPP2/2N2Q2/PPP3PP/R3KB1R w
B99	Sicilian Defense: Najdorf Variation, Main Line		r1b1k2r/1pqnbppp/p2ppn2/6B1/3NPP2/2N2Q2/PPP3PP/2KR1B1R w
C00	French Defense		rnbqkbnr/ppp2ppp/4p3/3p4/3PP3/8/PPP2PPP/RNBQKBNR w
C00	French Defense #2		rnbqkbnr/pppp1ppp/4p3/8/4P3/8/PPPP1PPP/RNBQKBNR w
C00	French Defense: Alapin Gambit		rnbqkbnr/ppp2ppp/4p3/3p4/3PP3/4B3/PPP2PPP/RN1QKBNR b
C00	French Defense: Banzai-Leong Gambit		rnbqkbnr/pppp1ppp/4p3/8/1P2P3/8/P1PP1PPP/RNBQKBNR b
C00	French Defense: Bird Invitation		rnbqkbnr/pppp1ppp/4p3/1B6/4P3/8/PPPP1PPP/RNBQK1NR b
C00	French Defense: Chigorin Variation		rnbqkbnr/pppp1ppp/4p3/8/4P3/8/PPPPQPPP/RNB1KBNR b
C00	French Defense: Diemer-Duhm Gambit		rnbqkbnr/ppp2ppp/4p3/8/2PPp3/8/PP3PPP/RNBQKBNR w
C00	French Defense: Franco-Hiva Gambit II		rnbqkbnr/pppp2pp/4p3/5p2/4P3/5N2/PPPP1PPP/RNBQKB1R w
C00	French Defense: Franco-Hiva Gambit III		rnbqkbnr/pppp2pp/4p3/5p2/4P3/3P4/PPP2PPP/RNBQKBNR w
C00	French Defense: Horwitz Attack		rnbqkbnr/pppp1ppp/4p3/8/4P3/1P6/P1PP1PPP/RNBQKBNR b
C00	French Defense: Horwitz Attack, Papa-Ticulat Gambit		rnbqkbnr/ppp2ppp/4p3/3p4/4P3/1P6/PBPP1PPP/RN1QKBNR b
C00	French Defense: King's Indian Attack		rnbqkbnr/pppp1ppp/4p3/8/4P3/3P4/PPP2PPP/RNBQKBNR b
C00	French Defense: Knight Variation		rnbqkbnr/pppp1ppp/4p3/8/4P3/5N2/PPPP1PPP/RNBQKB1R b
C00	French Defense: La Bourdonnais Variation		rnbqkbnr/pppp1ppp/4p3/8/4PP2/8/PPPP2PP/RNBQKBNR b
C00	French Defense: La Bourdonnais Variation, Reuter Gambit		rnbqkbnr/ppp2ppp/4p3/8/4pP2/5N2/PPPP2PP/RNBQKB1R w
C00	French Defense: Normal Variation		rnbqkbnr/pppp1ppp/4p3/8/3PP3/8/PPP2PPP/RNBQKBNR b
C00	French Defense: Orthoschnapp Gambit		rnbqkbnr/ppp2ppp/8/3p4/4P3/1Q6/PP1P1PPP/RNB1KBNR b
C00	French Defense: Pelikan Variation		rnbqkbnr/ppp2ppp/4p3/3p4/4PP2/2N5/PPPP2PP/R1BQKBNR b
C00	French Defense: Queen's Knight		rnbqkbnr/pppp1ppp/4p3/8/4P3/2N5/PPPP1PPP/R1BQKBNR b
C00	French Defense: Reversed Philidor Formation		r1bqkb1r/ppp2ppp/2n1pn2/3p4/4P3/3P1N2/PPPNBPPP/R1BQK2R b
C00	French Defense: Réti-Spielmann Attack		rnbqkbnr/pppp1ppp/4p3/8/4P3/6P1/PPPP1P1P/RNBQKBNR b
C00	French Defense: Schlechter Variation		rnbqkbnr/ppp2ppp/4p3/3p4/3PP3/3B4/PPP2PPP/RNBQK1NR b
C00	French Defense: Steiner Variation		rnbqkbnr/pppp1ppp/4p3/8/2P1P3/8/PP1P1PPP/RNBQKBNR b
C00	French Defense: Steinitz Attack		rnbqkbnr/pppp1ppp/4p3/4P3/8/8/PPPP1PPP/RNBQKBNR b
C00	French Defense: Two Knights Variation		rnbqkbnr/ppp2ppp/4p3/3p4/4P3/2N2N2/PPPP1PPP/R1BQKB1R b
C00	French Defense: Wing Gambit		rnbqkbnr/pp3ppp/4p3/2ppP3/1P6/5N2/P1PP1PPP/RNBQKB1R b
C00	Queen's Pawn Game: Franco-Sicilian Defense		rnbqkbnr/pp1p1ppp/4p3/2p5/3PP3/8/PPP2PPP/RNBQKBNR w
C00	Rat Defense: Small Center Defense		rnbqkbnr/ppp2ppp/3pp3/8/3PP3/8/PPP2PPP/RNBQKBNR w
C00	St. George Defense		rnbqkbnr/1ppp1ppp/p3p3/8/3PP3/8/PPP2PPP/RNBQKBNR w
C01	French Defense: Baeuerle Gambit		rnbqkbnr/p1pp1ppp/4p3/1p6/3PP3/8/PPP2PPP/RNBQKBNR w
C01	French Defense: Carlson Gambit		rnbqkbnr/ppp2ppp/4p3/4N3/3Pp3/8/PPP2PPP/RNBQKB1R b
C01	French Defense: Exchange Variation		rnbqkbnr/ppp2ppp/4p3/3P4/3P4/8/PPP2PPP/RNBQKBNR b
C01	French Defense: Exchange Variation, Monte Carlo Variation		rnbqkbnr/ppp2ppp/8/3p4/2PP4/8/PP3PPP/RNBQKBNR b
C01	French Defense: Exchange Variation, Svenonius Variation		rnbqkb1r/ppp2ppp/5n2/3p2B1/3P4/2N5/PPP2PPP/R2QKBNR b
C01	French Defense: Exchange, Bogoljubov Variation		r1bqkb1r/ppp2ppp/2n2n2/3p2B1/3P4/2N5/PPP2PPP/R2QKBNR w
C01	French Defense: Franco-Hiva Gambit I		rnbqkbnr/pppp2pp/4p3/5p2/3PP3/8/PPP2PPP/RNBQKBNR w
C01	French Defense: Franco-Hiva Gambit I Accepted		rnbqkb1r/pppp2pp/4pn2/5P2/3P4/8/PPP2PPP/RNBQKBNR w
C01	French Defense: Mediterranean Defense		rnbqkb1r/pppp1ppp/4pn2/8/3PP3/8/PPP2PPP/RNBQKBNR w
C01	French Defense: Morphy Gambit		rnbqkbnr/ppp2ppp/4p3/3p4/3PP3/7N/PPP2PPP/RNBQKB1R b
C01	French Defense: Perseus Gambit		rnbqkbnr/ppp2ppp/4p3/3p4/3PP3/5N2/PPP2PPP/RNBQKB1R b
C01	French Defense: Winawer Variation, Delayed Exchange Variation		rnbqk1nr/ppp2ppp/4p3/3P4/1b1P4/2N5/PPP2PPP/R1BQKBNR b
C01	French Defense: Winawer Variation, Exchange Variation, Canal Attack		rnbqk2r/ppp1nppp/8/3p3Q/1b1P4/2NB4/PPP2PPP/R1B1K1NR b
C02	French Defense: Advance Variation		r1bqkbnr/pp3ppp/2n1p3/2ppP3/3P4/2P5/PP3PPP/RNBQKBNR w
C02	French Defense: Advance Variation #2		rnbqkbnr/pp3ppp/4p3/2ppP3/3P4/2P5/PP3PPP/RNBQKBNR b
C02	French Defense: Advance Variation #3		rnbqkbnr/ppp2ppp/4p3/3pP3/3P4/8/PPP2PPP/RNBQKBNR b
C02	French Defense: Advance Variation #4		rnbqkbnr/pp3ppp/4p3/2ppP3/3P4/8/PPP2PPP/RNBQKBNR w
C02	French Defense: Advance Variation, Euwe Variation		r2qkbnr/pp1b1ppp/2n1p3/2ppP3/3P4/2P2N2/PP3PPP/RNBQKB1R w
C02	French Defense: Advance Variation, Extended Bishop Swap		rn1qkbnr/pppb1ppp/4p3/3pP3/3P4/8/PPP2PPP/RNBQKBNR w
C02	French Defense: Advance Variation, Frenkel Gambit		rnbqkbnr/pp3ppp/4p3/2ppP3/1P1P4/8/P1P2PPP/RNBQKBNR b
C02	French Defense: Advance Variation, Lputian Variation		r1b1kb1r/pp3ppp/1qn1p2n/2ppP3/3P4/P1P2N2/1P3PPP/RNBQKB1R w
C02	French Defense: Advance Variation, Main Line		r1b1kbnr/pp3ppp/1qn1p3/2ppP3/3P4/P1P2N2/1P3PPP/RNBQKB1R b
C02	French Defense: Advance Variation, Milner-Barry Gambit		r1b1kbnr/pp3ppp/1qn1p3/2ppP3/3P4/2PB1N2/PP3PPP/RNBQK2R b
C02	French Defense: Advance Variation, Nimzowitsch Attack		rnbqkbnr/pp3ppp/4p3/2ppP3/3P2Q1/8/PPP2PPP/RNB1KBNR b
C02	French Defense: Advance Variation, Nimzowitsch Gambit		rnbqkbnr/pp3ppp/4p3/3pP3/3p2Q1/5N2/PPP2PPP/RNB1KB1R b
C02	French Defense: Advance Variation, Nimzowitsch System		rnbqkbnr/pp3ppp/4p3/2ppP3/3P4/5N2/PPP2PPP/RNBQKB1R b
C02	French Defense: Advance Variation, Paulsen Attack		r1bqkbnr/pp3ppp/2n1p3/2ppP3/3P4/2P2N2/PP3PPP/RNBQKB1R b
C02	French Defense: Advance Variation, Ruisdonk Gambit		rnbqkbnr/pp3ppp/4p3/3pP3/3p4/3B1N2/PPP2PPP/RNBQK2R b
C02	French Defense: Advance Variation, Wade Variation		rn2kbnr/pp1b1ppp/1q2p3/2ppP3/3P4/2P2N2/PP3PPP/RNBQKB1R w
C02	French Defense: Advance, Steinitz Variation		rnbqkbnr/pp3ppp/4p3/2PpP3/8/8/PPP2PPP/RNBQKBNR b
C03	French Defense: Guimard Variation, Thunderbunny Variation		r1bqkbnr/ppp2ppp/2n5/4p3/3PN3/2P5/PP3PPP/R1BQKBNR w
C03	French Defense: Tarrasch Variation		rnbqkbnr/ppp2ppp/4p3/3p4/3PP3/8/PPPN1PPP/R1BQKBNR b
C03	French Defense: Tarrasch Variation, Guimard Defense		r1bqkbnr/ppp2ppp/2n1p3/3p4/3PP3/8/PPPN1PPP/R1BQKBNR w
C03	French Defense: Tarrasch Variation, Haberditz Variation		rnbqkbnr/ppp3pp/4p3/3p1p2/3PP3/8/PPPN1PPP/R1BQKBNR w
C03	French Defense: Tarrasch Variation, Modern System		rnbqkbnr/1pp2ppp/p3p3/3p4/3PP3/8/PPPN1PPP/R1BQKBNR w
C03	French Defense: Tarrasch Variation, Morozevich Variation		rnbqk1nr/ppp1bppp/4p3/3p4/3PP3/8/PPPN1PPP/R1BQKBNR w
C04	French Defense: Tarrasch Variation, Guimard Defense, Main Line		r1bqkb1r/ppp2ppp/2n1pn2/3p4/3PP3/5N2/PPPN1PPP/R1BQKB1R w
C05	French Defense: Tarrasch Variation, Botvinnik Variation		rnbqkb1r/p2n1ppp/1p2p3/2ppP3/3P4/2PB4/PP1N1PPP/R1BQK1NR w
C05	French Defense: Tarrasch Variation, Closed Variation		rnbqkb1r/ppp2ppp/4pn2/3p4/3PP3/8/PPPN1PPP/R1BQKBNR w
C05	French Defense: Tarrasch Variation, Closed Variation #2		r1bqkb1r/pp1n1ppp/2n1p3/2ppP3/3P4/2PB4/PP1N1PPP/R1BQK1NR w
C05	French Defense: Tarrasch Variation, Pawn Center Variation		rnbqkb1r/pppn1ppp/4p3/3pP3/3P1P2/8/PPPN2PP/R1BQKBNR b
C06	French Defense: Tarrasch Variation, Closed Variation, Main Line		r1bqkb1r/pp1n1ppp/2n1p3/3pP3/3P4/3B4/PP1NNPPP/R1BQK2R b
C06	French Defense: Tarrasch Variation, Leningrad Variation		r1bqkb1r/pp3ppp/1nn1p3/3pP3/3P4/3B4/PP1NNPPP/R1BQK2R w
C07	French Defense: Tarrasch Variation, Chistyakov Defense		rnb1kbnr/pp3ppp/4p3/2pq4/3P4/8/PPPN1PPP/R1BQKBNR w
C07	French Defense: Tarrasch Variation, Chistyakov Defense, Modern Line		rnb1kbnr/pp3ppp/3qp3/8/2Bp4/5N2/PPPN1PPP/R1BQK2R w
C07	French Defense: Tarrasch Variation, Open System		rnbqkbnr/pp3ppp/4p3/2pp4/3PP3/8/PPPN1PPP/R1BQKBNR w
C07	French Defense: Tarrasch Variation, Open System, Euwe-Keres Line		rnbqkbnr/pp3ppp/4p3/2pp4/3PP3/5N2/PPPN1PPP/R1BQKB1R b
C07	French Defense: Tarrasch Variation, Open System, Shaposhnikov Gambit		rnbqkb1r/pp3ppp/4pn2/2pP4/3P4/8/PPPN1PPP/R1BQKBNR w
C07	French Defense: Tarrasch Variation, Open System, Suechting Line		rnbqkbnr/pp3ppp/4p3/2pp4/3PP3/2P5/PP1N1PPP/R1BQKBNR b
C07	French Defense: Tarrasch, Eliskases Variation		rnbqkbnr/pp3ppp/4p3/8/2Bp4/5N2/PPPN1PPP/R1BQK2R w
C08	French Defense: Tarrasch Variation, Open System, Advance Line		rnbqkbnr/pp3ppp/8/3p4/2pP4/5N2/PPPN1PPP/R1BQKB1R w
C08	French Defense: Tarrasch, Open, 4.exd5 exd5		rnbqkbnr/pp3ppp/8/2pp4/3P4/8/PPPN1PPP/R1BQKBNR w
C09	French Defense: Tarrasch Variation, Open System, Main Line		r1bqkbnr/pp3ppp/2n5/2pp4/3P4/5N2/PPPN1PPP/R1BQKB1R w
C10	French Defense: Classical Variation, Svenonius Variation		r1bqkbnr/ppp2ppp/2n1p3/3P4/3P4/2N5/PPP2PPP/R1BQKBNR b
C10	French Defense: Marshall Variation		rnbqkbnr/pp3ppp/4p3/2pp4/3PP3/2N5/PPP2PPP/R1BQKBNR w
C10	French Defense: Paulsen Variation		rnbqkbnr/ppp2ppp/4p3/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR b
C10	French Defense: Rubinstein Variation		rnbqkbnr/ppp2ppp/4p3/8/3Pp3/2N5/PPP2PPP/R1BQKBNR w
C10	French Defense: Rubinstein Variation, Blackburne Defense		r1bqkbnr/pppn1ppp/4p3/8/3PN3/8/PPP2PPP/R1BQKBNR w
C10	French Defense: Rubinstein Variation, Capablanca Line		r1bqkb1r/ppp2ppp/4pn2/4N3/3P4/8/PPP2PPP/R1BQKB1R b
C10	French Defense: Rubinstein Variation, Ellis Gambit		rnbqkbnr/ppp2ppp/8/4p3/3PN3/8/PPP2PPP/R1BQKBNR w
C10	French Defense: Rubinstein Variation, Fort Knox Variation		rn1qkbnr/ppp2ppp/2b1p3/8/3PN3/5N2/PPP2PPP/R1BQKB1R w
C10	French Defense: Rubinstein Variation, Kasparov Attack		r1bqkb1r/ppp2ppp/4pn2/8/3P4/2P2N2/PP3PPP/R1BQKB1R b
C10	French Defense: Rubinstein Variation, Maric Variation		rnb1kbnr/ppp2ppp/4p3/3q4/3PN3/8/PPP2PPP/R1BQKBNR w
C11	French Defense: Burn Variation		rnbqkb1r/ppp2ppp/4pn2/3p2B1/3PP3/2N5/PPP2PPP/R2QKBNR b
C11	French Defense: Classical Variation		rnbqkb1r/ppp2ppp/4pn2/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR w
C11	French Defense: Classical Variation, Burn Variation, Main Line		rnbq1rk1/ppp2ppp/4pb2/8/3PN3/5N2/PPP2PPP/R2QKB1R w
C11	French Defense: Classical Variation, Burn Variation, Morozevich Line		rnbqk2r/ppp1bp1p/4pp2/8/3PN3/8/PPP2PPP/R2QKBNR w
C11	French Defense: Classical Variation, Delayed Exchange Variation		rnbqkb1r/ppp2ppp/4pn2/3P4/3P4/2N5/PPP2PPP/R1BQKBNR b
C11	French Defense: Classical Variation, Steinitz Variation		rnbqkb1r/ppp2ppp/4pn2/3pP3/3P4/2N5/PPP2PPP/R1BQKBNR b
C11	French Defense: Classical Variation, Swiss Variation		rnbqkb1r/ppp2ppp/4pn2/3p4/3PP3/2NB4/PPP2PPP/R1BQK1NR b
C11	French Defense: Henneberger Variation		rnbqkb1r/ppp2ppp/4pn2/3p4/3PP3/2N1B3/PPP2PPP/R2QKBNR b
C11	French Defense: Steinitz Variation		r1bqkb1r/pp1n1ppp/2n1p3/2PpP3/5P2/2N5/PPP3PP/R1BQKBNR w
C11	French Defense: Steinitz Variation #2		rnbqkb1r/pp1n1ppp/4p3/2ppP3/3P1P2/2N2N2/PPP3PP/R1BQKB1R b
C11	French Defense: Steinitz Variation, Boleslavsky Variation		r1bqkb1r/pp1n1ppp/2n1p3/2ppP3/3P1P2/2N1BN2/PPP3PP/R2QKB1R b
C11	French Defense: Steinitz Variation, Bradford Attack Variation		rnbqk2r/pp1n1ppp/4p3/2bpP3/5PQ1/2N5/PPP3PP/R1B1KBNR b
C11	French Defense: Steinitz Variation, Gledhill Attack		rnbqkb1r/pppn1ppp/4p3/3pP3/3P2Q1/2N5/PPP2PPP/R1B1KBNR b
C12	French Defense: MacCutcheon Variation		rnbqk2r/ppp2ppp/4pn2/3p2B1/1b1PP3/2N5/PPP2PPP/R2QKBNR w
C12	French Defense: MacCutcheon Variation, Bernstein Variation		rnbqk2r/ppp2pp1/4pn1p/3pP3/1b1P3B/2N5/PPP2PPP/R2QKBNR b
C12	French Defense: MacCutcheon Variation, Chigorin Variation		rnbqk2r/ppp2pp1/4pP1p/3p2B1/1b1P4/2N5/PPP2PPP/R2QKBNR b
C12	French Defense: MacCutcheon Variation, Dr. Olland (Dutch) Variation		rnbqk2r/ppp2pp1/4pn1p/3pP3/1b1P4/2N5/PPP2PPP/R1BQKBNR b
C12	French Defense: MacCutcheon Variation, Exchange Variation		rnbqk2r/ppp2ppp/4pn2/3P2B1/1b1P4/2N5/PPP2PPP/R2QKBNR b
C12	French Defense: MacCutcheon Variation, Janowski Variation		rnbqk2r/ppp2pp1/4pn1p/3pP3/1b1P4/2N1B3/PPP2PPP/R2QKBNR b
C12	French Defense: MacCutcheon Variation, Lasker Variation		rnbqk2r/ppp2pp1/4pn1p/3pP3/3P4/2b5/PPPB1PPP/R2QKBNR w
C12	French Defense: MacCutcheon Variation, Lasker Variation #2		rnbqk2r/ppp2p2/4p1pp/3pP3/3Pn1Q1/2P5/P1PB1PPP/R3KBNR w
C12	French Defense: MacCutcheon Variation, Tartakower Variation		rnbqk2r/pppn1pp1/4p2p/3pP3/1b1P4/2N5/PPPB1PPP/R2QKBNR w
C12	French Defense: MacCutcheon Variation, Wolf Gambit		rnbqk2r/ppp2ppp/4pn2/3p2B1/1b1PP3/2N5/PPP1NPPP/R2QKB1R b
C12	French Defense: MacCutcheon, Advance Variation		rnbqk2r/ppp2ppp/4pn2/3pP1B1/1b1P4/2N5/PPP2PPP/R2QKBNR b
C12	French Defense: MacCutcheon, Bogoljubov Variation		rnb1k2r/ppp2p1p/4pp2/q7/1b1P4/2N5/PPPQ1PPP/R3KBNR w
C13	French Defense: Alekhine-Chatard Attack		rnbqk2r/pppnbppp/4p3/3pP1B1/3P3P/2N5/PPP2PP1/R2QKBNR b
C13	French Defense: Alekhine-Chatard Attack, Albin-Chatard Gambit		rnb1k2r/pppn1ppp/4p3/3pP1q1/3P4/2N5/PPP2PP1/R2QKBNR w
C13	French Defense: Alekhine-Chatard Attack, Breyer Variation		rnbqk2r/pp1nbppp/4p3/2ppP1B1/3P3P/2N5/PPP2PP1/R2QKBNR w
C13	French Defense: Alekhine-Chatard Attack, Maróczy Variation		rnbqk2r/1ppnbppp/p3p3/3pP1B1/3P3P/2N5/PPP2PP1/R2QKBNR w
C13	French Defense: Alekhine-Chatard Attack, Spielmann Variation		rnbq1rk1/pppnbppp/4p3/3pP1B1/3P3P/2N5/PPP2PP1/R2QKBNR w
C13	French Defense: Alekhine-Chatard Attack, Teichmann Variation		rnbqk2r/pppnb1pp/4pp2/3pP1B1/3P3P/2N5/PPP2PP1/R2QKBNR w
C13	French Defense: Classical Variation, Frankfurt Variation		r3kn2/2p2p2/1p2p1pQ/3pP3/p2P4/2q5/P2N3P/5R1K w
C13	French Defense: Classical Variation, Normal Variation		rnbqk2r/ppp1bppp/4pn2/3p2B1/3PP3/2N5/PPP2PPP/R2QKBNR w
C13	French Defense: Classical Variation, Richter Attack		rnbqk2r/ppp1bppp/4p3/3pP3/3P2Q1/2N5/PPP2PPP/R3KBNR b
C13	French Defense: Classical Variation, Richter Attack #2		rnbqk2r/ppp1bppp/4pB2/3p4/3PP3/2N5/PPP2PPP/R2QKBNR b
C13	French Defense: Classical Variation, Tartakower Variation		rnbqk2r/ppp1bppp/4p3/3pP1B1/3Pn3/2N5/PPP2PPP/R2QKBNR w
C13	French Defense: Classical Variation, Vistaneckis (Nimzowitsch) Variation		rnbqk1nr/ppp1bppp/4p3/3pP1B1/3P4/2N5/PPP2PPP/R2QKBNR w
C14	French Defense: Classical Variation		rnb1k2r/pppnqppp/4p3/3pP3/3P4/2N5/PPP2PPP/R2QKBNR w
C14	French Defense: Classical Variation, Alapin Variation		rnb1k2r/pppnqppp/4p3/1N1pP3/3P4/8/PPP2PPP/R2QKBNR b
C14	French Defense: Classical Variation, Pollock Variation		rnb1k2r/pppnqppp/4p3/3pP3/3P2Q1/2N5/PPP2PPP/R3KBNR b
C14	French Defense: Classical Variation, Rubinstein Variation		rnb1k2r/pppnqppp/4p3/3pP3/3P4/2N5/PPPQ1PPP/R3KBNR b
C14	French Defense: Classical Variation, Steinitz Variation		rnb1k2r/pppnqppp/4p3/3pP3/3P1P2/2N5/PPP3PP/R2QKBNR b
C14	French Defense: Classical Variation, Tarrasch Variation		rnb1k2r/pppnqppp/4p3/3pP3/3P4/2NB4/PPP2PPP/R2QK1NR b
C14	French Defense: Classical, Stahlberg Variation		r1b2rk1/pp1nqppp/2n1p3/3pP3/2pP1PP1/2N2N2/PPPQ3P/2KR1B1R b
C15	French Defense: Winawer Variation		rnbqk1nr/ppp2ppp/4p3/3p4/1b1PP3/2N5/PPP2PPP/R1BQKBNR w
C15	French Defense: Winawer Variation, Alekhine Gambit Accepted		rnbqk1nr/ppp2ppp/4p3/8/3Pp3/P1b5/1PP1NPPP/R1BQKB1R w
C15	French Defense: Winawer Variation, Alekhine Gambit, Kan Variation		r1bqk1nr/ppp2ppp/2n1p3/8/3Pp3/P1N5/1PP2PPP/R1BQKB1R w
C15	French Defense: Winawer Variation, Alekhine-Maróczy Gambit		rnbqk1nr/ppp2ppp/4p3/3p4/1b1PP3/2N5/PPP1NPPP/R1BQKB1R b
C15	French Defense: Winawer Variation, Fingerslip Variation		rnbqk1nr/ppp2ppp/4p3/3p4/1b1PP3/2N5/PPPB1PPP/R2QKBNR b
C15	French Defense: Winawer Variation, Fingerslip Variation, Kunin Double Gambit		rnb1k1nr/ppp2ppp/4p3/8/1b1qp1Q1/2N5/PPPB1PPP/R3KBNR w
C15	French Defense: Winawer Variation, Kondratiyev Variation		rnb1k1nr/pp3ppp/4p3/2pq4/1b1P4/2NB4/PPPB1PPP/R2QK1NR b
C15	French Defense: Winawer Variation, Winckelmann-Riemer Gambit		rnbqk1nr/ppp2ppp/4p3/3p4/1b1PP3/P1N5/1PP2PPP/R1BQKBNR b
C16	French Defense: Winawer Variation, Advance Variation		rnbqk1nr/ppp2ppp/4p3/3pP3/1b1P4/2N5/PPP2PPP/R1BQKBNR b
C16	French Defense: Winawer Variation, Petrosian Variation		rnb1k1nr/pppq1ppp/4p3/3pP3/1b1P4/2N5/PPP2PPP/R1BQKBNR w
C17	French Defense: Winawer Variation, Advance Variation		rnbqk1nr/pp3ppp/4p3/2ppP3/1b1P4/P1N5/1PP2PPP/R1BQKBNR b
C17	French Defense: Winawer Variation, Advance Variation #2		rnbqk1nr/pp3ppp/4p3/3pP3/1P6/2p2N2/1PP2PPP/R1BQKB1R b
C17	French Defense: Winawer Variation, Advance Variation #3		rnbqk1nr/pp3ppp/4p3/2ppP3/1b1P4/2N5/PPP2PPP/R1BQKBNR w
C17	French Defense: Winawer Variation, Advance Variation, Moscow Variation		rnbqk1nr/pp3ppp/4p3/2ppP3/1b1P2Q1/2N5/PPP2PPP/R1B1KBNR b
C17	French Defense: Winawer Variation, Bogoljubov Variation		rnbqk1nr/pp3ppp/4p3/2ppP3/1b1P4/2N5/PPPB1PPP/R2QKBNR b
C17	French Defense: Winawer Variation, Bogoljubov Variation, Icelandic Defense		rnbqk2r/pp2nppp/4p3/2ppP3/1b1P1P2/2N5/PPPB2PP/R2QKBNR b
C18	French Defense: Winawer Variation, Advance Variation		rnbqk2r/pp2nppp/4p3/2ppP3/3P4/P1P5/2P2PPP/R1BQKBNR w
C18	French Defense: Winawer Variation, Advance Variation #2		rnbqk1nr/pp3ppp/4p3/2ppP3/3P4/P1P5/2P2PPP/R1BQKBNR b
C18	French Defense: Winawer Variation, Classical Variation		rnb1k1nr/ppq2ppp/4p3/2ppP3/3P4/P1P5/2P2PPP/R1BQKBNR w
C18	French Defense: Winawer Variation, Maróczy-Wallis Variation		rnbqk1nr/pp3ppp/4p3/3pP3/1P6/2p5/1PP2PPP/R1BQKBNR w
C18	French Defense: Winawer Variation, Poisoned Pawn Variation		rnbqk2r/pp2nppp/4p3/2ppP3/3P2Q1/P1P5/2P2PPP/R1B1KBNR b
C18	French Defense: Winawer Variation, Retreat Variation		rnbqk1nr/pp3ppp/4p3/b1ppP3/3P4/P1N5/1PP2PPP/R1BQKBNR w
C18	French Defense: Winawer Variation, Retreat Variation, Armenian Line		rnbqk1nr/pp3ppp/4p3/b2pP3/1P1p4/P1N5/2P2PPP/R1BQKBNR w
C19	French Defense: Winawer Variation, Advance Variation		rnbqk2r/pp2nppp/4p3/2ppP3/P2P4/2P5/2P2PPP/R1BQKBNR b
C19	French Defense: Winawer Variation, Positional Variation		rnbqk2r/pp2nppp/4p3/2ppP3/3P4/P1P2N2/2P2PPP/R1BQKB1R b
C20	Bishop's Opening: Boi Variation		rnbqk1nr/pppp1ppp/8/2b1p3/2B1P3/8/PPPP1PPP/RNBQK1NR w
C20	English Opening: The Whale		rnbqkbnr/pppp1ppp/8/4p3/2P1P3/8/PP1P1PPP/RNBQKBNR b
C20	King's Pawn Game		rnbqkbnr/pppp1ppp/8/4p3/4P3/8/PPPP1PPP/RNBQKBNR w
C20	King's Pawn Game: Alapin Opening		rnbqkbnr/pppp1ppp/8/4p3/4P3/8/PPPPNPPP/RNBQKB1R b
C20	King's Pawn Game: Bavarian Gambit		rnbqkbnr/ppp2ppp/8/3pp3/2P1P3/8/PP1P1PPP/RNBQKBNR w
C20	King's Pawn Game: Clam Variation, King's Gambit Reversed		rnbqkbnr/pppp2pp/8/4pp2/4P3/3P4/PPP2PPP/RNBQKBNR w
C20	King's Pawn Game: Clam Variation, Radisch Gambit		rnbqk2r/pppp1ppp/5n2/2b1p3/4PP2/3P4/PPP3PP/RNBQKBNR w
C20	King's Pawn Game: Damiano Defense, Damiano Gambit, Chigorin Gambit		rnb1kbnr/ppp1q1pp/5p2/3p4/4P3/5N2/PPPP1PPP/RNBQKB1R w
C20	King's Pawn Game: King's Head Opening		rnbqkb1r/pppp1ppp/5n2/4p3/4P3/2N2P2/PPPP2PP/R1BQKBNR b
C20	King's Pawn Game: King's Head Opening #2		rnbqkbnr/pppp1ppp/8/4p3/4P3/5P2/PPPP2PP/RNBQKBNR b
C20	King's Pawn Game: Leonardis Variation		rnbqkbnr/pppp1ppp/8/4p3/4P3/3P4/PPP2PPP/RNBQKBNR b
C20	King's Pawn Game: Macleod Attack		rnbqkbnr/pppp1ppp/8/4p3/4P3/2P5/PP1P1PPP/RNBQKBNR b
C20	King's Pawn Game: Macleod Attack, Lasa Gambit		rnbqkbnr/pppp2pp/8/4pp2/4P3/2P5/PP1P1PPP/RNBQKBNR w
C20	King's Pawn Game: Mengarini's Opening		rnbqkbnr/pppp1ppp/8/4p3/4P3/P7/1PPP1PPP/RNBQKBNR b
C20	King's Pawn Game: Napoleon Attack		rnbqkbnr/pppp1ppp/8/4p3/4P3/5Q2/PPPP1PPP/RNB1KBNR b
C20	King's Pawn Game: Tortoise Opening		rnbqkbnr/pppp1ppp/8/4p3/4P3/3B4/PPPP1PPP/RNBQK1NR b
C20	King's Pawn Game: Wayward Queen Attack		rnbqkbnr/pppp1ppp/8/4p2Q/4P3/8/PPPP1PPP/RNB1KBNR b
C20	King's Pawn Game: Wayward Queen Attack, Kiddie Countergambit		rnbqkb1r/pppp1ppp/5n2/4p2Q/4P3/8/PPPP1PPP/RNB1KBNR w
C20	King's Pawn Game: Weber Gambit		r1bqkbnr/pp3ppp/2n5/4p3/8/3P4/PPP2PPP/RNBQKBNR w
C20	King's Pawn Opening: 2.b3		rnbqkbnr/pppp1ppp/8/4p3/4P3/1P6/P1PP1PPP/RNBQKBNR b
C20	Portuguese Opening		rnbqkbnr/pppp1ppp/8/1B2p3/4P3/8/PPPP1PPP/RNBQK1NR b
C20	Portuguese Opening: Miguel Gambit		rnbqk1nr/pppp1ppp/8/1Bb1p3/1P2P3/8/P1PP1PPP/RNBQK1NR b
C20	Portuguese Opening: Portuguese Gambit		rnbqkb1r/pppp1ppp/5n2/1B2p3/3PP3/8/PPP2PPP/RNBQK1NR b
C21	Center Game		rnbqkbnr/pppp1ppp/8/8/3QP3/8/PPP2PPP/RNB1KBNR b
C21	Center Game #2		rnbqkbnr/pppp1ppp/8/4p3/3PP3/8/PPP2PPP/RNBQKBNR b
C21	Center Game Accepted		rnbqkbnr/pppp1ppp/8/8/3pP3/8/PPP2PPP/RNBQKBNR w
C21	Center Game: Halasz-McDonnell Gambit		rnbqkbnr/pppp1ppp/8/8/3pPP2/8/PPP3PP/RNBQKBNR b
C21	Center Game: Halasz-McDonnell Gambit, Crocodile Variation		r1bqk1nr/pppp1ppp/2n5/2b5/3pPP2/2P2N2/PP4PP/RNBQKB1R b
C21	Center Game: Kieseritzky Variation		rnbqkbnr/p2p1ppp/8/1pp5/2BpP3/5N2/PPP2PPP/RNBQK2R w
C21	Center Game: Kieseritzky Variation #2		rnbqkbnr/pppp1ppp/8/8/3pP3/5N2/PPP2PPP/RNBQKB1R b
C21	Center Game: Kieseritzky Variation #3		rnbqkbnr/pp1p1ppp/8/2p5/3pP3/5N2/PPP2PPP/RNBQKB1R w
C21	Center Game: Kieseritzky Variation #4		rnbqkbnr/pp1p1ppp/8/2p5/2BpP3/5N2/PPP2PPP/RNBQK2R b
C21	Center Game: Lanc-Arnold Gambit		rnbqk1nr/pppp1ppp/8/2b5/3pP3/2P2N2/PP3PPP/RNBQKB1R b
C21	Center Game: Lanc-Arnold Gambit, Schippler Gambit		rnbqk1nr/pppp1ppp/8/2b5/2B1P3/2p2N2/PP3PPP/RNBQK2R b
C21	Center Game: Ross Gambit		rnbqkbnr/pppp1ppp/8/8/3pP3/3B4/PPP2PPP/RNBQK1NR b
C21	Center Game: von der Lasa Gambit		rnbqkbnr/pppp1ppp/8/8/2BpP3/8/PPP2PPP/RNBQK1NR b
C21	Danish Gambit		rnbqkbnr/pppp1ppp/8/8/3pP3/2P5/PP3PPP/RNBQKBNR b
C21	Danish Gambit Accepted		rnbqkbnr/pppp1ppp/8/8/2B1P3/8/PB3PPP/RN1QK1NR b
C21	Danish Gambit Accepted, Chigorin Defense		rnb1kbnr/ppppqppp/8/8/2B1P3/8/PB3PPP/RN1QK1NR w
C21	Danish Gambit Accepted, Classical Defense		rnbqkb1r/pppp1ppp/5n2/8/2B1P3/8/PB3PPP/RN1QK1NR w
C21	Danish Gambit Accepted, Copenhagen Defense		rnbqk1nr/pppp1ppp/8/8/1bB1P3/8/PB3PPP/RN1QK1NR w
C21	Danish Gambit Accepted, Schlechter Defense		rnbqkbnr/ppp2ppp/8/3p4/2B1P3/8/PB3PPP/RN1QK1NR w
C21	Danish Gambit Accepted, Svenonius Defense		rnbqkb1r/ppppnppp/8/8/3pP3/2P5/PP3PPP/RNBQKBNR w
C21	Danish Gambit Declined, Sorensen Defense		rnbqkbnr/ppp2ppp/8/3p4/3pP3/2P5/PP3PPP/RNBQKBNR w
C21	King's Pawn Game: Beyer Gambit		rnbqkbnr/ppp2ppp/8/3pp3/3PP3/8/PPP2PPP/RNBQKBNR w
C21	Sicilian Defense: Smith-Morra Gambit, Danish Variation		rnbqkbnr/pp1ppppp/8/8/4P3/2p2N2/PP3PPP/RNBQKB1R b
C22	Center Game: Berger Variation		r1bqkb1r/pppp1ppp/2n2n2/8/4P3/4Q3/PPP2PPP/RNB1KBNR w
C22	Center Game: Charousek Variation		r1bqk1nr/ppppbppp/2n5/8/4P3/2P1Q3/PP3PPP/RNB1KBNR w
C22	Center Game: Hall Variation		r1bqkbnr/pppp1ppp/2n5/8/2Q1P3/8/PPP2PPP/RNB1KBNR b
C22	Center Game: Normal Variation		r1bqkbnr/pppp1ppp/2n5/8/3QP3/8/PPP2PPP/RNB1KBNR w
C22	Center Game: l'Hermet Variation		r1bqkbnr/pppp2pp/2n5/5p2/4P3/4Q3/PPP2PPP/RNB1KBNR w
C23	Bishop's Opening		rnbqkbnr/pppp1ppp/8/4p3/2B1P3/8/PPPP1PPP/RNBQK1NR b
C23	Bishop's Opening: Anderssen Gambit		rnbqkbnr/p2p1ppp/2p5/1B2p3/4P3/8/PPPP1PPP/RNBQK1NR w
C23	Bishop's Opening: Berlin Defense, Greco Gambit		rnbqkb1r/pppp1ppp/5n2/4p3/2B1PP2/8/PPPP2PP/RNBQK1NR b
C23	Bishop's Opening: Calabrese Countergambit		rnbqkbnr/pppp2pp/8/4pp2/2B1P3/8/PPPP1PPP/RNBQK1NR w
C23	Bishop's Opening: Calabrese Countergambit, Jaenisch Variation		rnbqkbnr/pppp2pp/8/4pp2/2B1P3/3P4/PPP2PPP/RNBQK1NR b
C23	Bishop's Opening: Horwitz Gambit		rnbqkb1r/p1pp1ppp/5n2/1p2p3/2B1P3/2N5/PPPP1PPP/R1BQK1NR w
C23	Bishop's Opening: Khan Gambit		rnbqkbnr/ppp2ppp/8/3pp3/2B1P3/8/PPPP1PPP/RNBQK1NR w
C23	Bishop's Opening: Lewis Countergambit #2		rnbqk1nr/ppp2ppp/8/2bpp3/2B1P3/2P5/PP1P1PPP/RNBQK1NR w
C23	Bishop's Opening: Lewis Countergambit, Walker Variation		rnbqk2r/ppp2ppp/5n2/2bB4/3PP3/8/PP3PPP/RNBQK1NR b
C23	Bishop's Opening: Lewis Gambit		rnbqk1nr/pppp1ppp/8/2b1p3/2BPP3/8/PPP2PPP/RNBQK1NR b
C23	Bishop's Opening: Lisitsyn Variation		r2qkbnr/pp3ppp/8/3pn3/8/8/PPP1NPPP/RNBQK2R b
C23	Bishop's Opening: Lopez Gambit		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2B1PP2/2P5/PP1PQ1PP/RNB1K1NR b
C23	Bishop's Opening: Lopez Variation		rnbqk1nr/pppp1ppp/8/2b1p3/2B1P3/8/PPPPQPPP/RNB1K1NR b
C23	Bishop's Opening: McDonnell Gambit		rnbqk1nr/pppp1ppp/8/2b1p3/1PB1P3/8/P1PP1PPP/RNBQK1NR b
C23	Bishop's Opening: McDonnell Gambit, La Bourdonnais-Denker Gambit		rnbqk1nr/pppp1ppp/8/4p3/1bB1P3/2P5/P2P1PPP/RNBQK1NR b
C23	Bishop's Opening: McDonnell Gambit, McDonnell Double Gambit		rnbqk1nr/pppp1ppp/8/4p3/1bB1PP2/8/P1PP2PP/RNBQK1NR b
C23	Bishop's Opening: Philidor Counterattack		rnbqkbnr/pp1p1ppp/2p5/4p3/2B1P3/8/PPPP1PPP/RNBQK1NR w
C23	Bishop's Opening: Philidor Variation		rnbqk1nr/pppp1ppp/8/2b1p3/2B1P3/2P5/PP1P1PPP/RNBQK1NR b
C23	Bishop's Opening: Stein Gambit		rnbqk1nr/pppp1ppp/8/2b1p3/2B1PP2/8/PPPP2PP/RNBQK1NR b
C23	Bishop's Opening: del Rio Variation		rnb1k1nr/pppp1ppp/8/2b1p1q1/2B1P3/2P5/PP1P1PPP/RNBQK1NR w
C23	Vienna Game: Fyfe Gambit		r1bqkbnr/pppp1ppp/2n5/4p3/3PP3/2N5/PPP2PPP/R1BQKBNR b
C23	Vienna Game: Philidor Countergambit		r1bqkbnr/pppp2pp/2n5/4pp2/3PP3/2N5/PPP2PPP/R1BQKBNR w
C23	Vienna Game: Stanley Variation, Bronstein Gambit		r1bqkb1r/pppp1ppp/2n5/4p3/2B1nP2/2N2N2/PPPP2PP/R1BQK2R b
C23	Vienna Game: Stanley Variation, Meitner-Mieses Gambit		r1b1k1nr/pppp1ppp/2n2q2/2bNp3/2B1P1Q1/8/PPPP1PPP/R1B1K1NR b
C24	Bishop's Opening: Berlin Defense		rnbqkb1r/pppp1ppp/5n2/4p3/2B1P3/8/PPPP1PPP/RNBQK1NR w
C24	Bishop's Opening: Kitchener Folly		rnbq1rk1/ppppbppp/5n2/4p3/2B1P3/3P1N2/PPP2PPP/RNBQK2R w
C24	Bishop's Opening: Ponziani Gambit		rnbqkb1r/pppp1ppp/5n2/4p3/2BPP3/8/PPP2PPP/RNBQK1NR b
C24	Bishop's Opening: Urusov Gambit		rnbqkb1r/pppp1ppp/5n2/8/2BpP3/5N2/PPP2PPP/RNBQK2R b
C24	Bishop's Opening: Urusov Gambit, Keidansky Gambit		rnbqkb1r/pppp1ppp/8/8/2BQn3/5N2/PPP2PPP/RNB1K2R b
C24	Bishop's Opening: Urusov Gambit, Panov Variation		rnb1k2r/ppp1qppp/5n2/3P4/1bBp4/2P2N2/PP3PPP/RNBQK2R w
C24	Bishop's Opening: Warsaw Gambit		rnbqkb1r/pppp1ppp/5n2/8/2BpP3/2P5/PP3PPP/RNBQK1NR b
C25	Vienna Game: Anderssen Defense		rnbqk1nr/pppp1ppp/8/2b1p3/4P3/2N5/PPPP1PPP/R1BQKBNR w
C25	Vienna Game: Giraffe Attack		rnbqk1nr/pppp1ppp/8/2b1p3/4P1Q1/2N5/PPPP1PPP/R1B1KBNR b
C25	Vienna Game: Hamppe-Allgaier Gambit, Alapin Variation		r1bqkbnr/ppp2p1p/2np4/6N1/4PppP/2N5/PPPP2P1/R1BQKB1R w
C25	Vienna Game: Hamppe-Meitner Variation		rnbqk1nr/pppp1ppp/8/2b1p3/N3P3/8/PPPP1PPP/R1BQKBNR b
C25	Vienna Game: Hamppe-Muzio Gambit		rr6/pR2nk1p/3p1p2/8/1B1pP3/1P1P3b/P3KP1P/8 w
C25	Vienna Game: Hamppe-Muzio, Dubois Variation		r1b1kbnr/pppp1p1p/5q2/4n3/2B1PQ2/2N5/PPPP2PP/R1B2RK1 w
C25	Vienna Game: Max Lange Defense		r1bqkbnr/pppp1ppp/2n5/4p3/4P3/2N5/PPPP1PPP/R1BQKBNR w
C25	Vienna Game: Mieses Variation, Erben Gambit		rnbqkb1r/pp3ppp/2p2n2/3Pp3/8/2N3P1/PPPP1P1P/R1BQKBNR w
C25	Vienna Game: Omaha Gambit		rnbqkbnr/ppp2ppp/3p4/4p3/4PP2/2N5/PPPP2PP/R1BQKBNR b
C25	Vienna Game: Paulsen Variation		r1bqkbnr/pppp1ppp/2n5/4p3/4P3/2N3P1/PPPP1P1P/R1BQKBNR b
C25	Vienna Game: Paulsen Variation, Mariotti Gambit		4k3/1pp5/3p1nb1/2PPp1p1/4Pp2/5B1P/4KB2/8 w
C25	Vienna Game: Paulsen Variation, Pollock Gambit		r1bqk2r/ppp2ppp/2n2n2/2bPp3/8/2N3P1/PPPPNPBP/R1BQK2R b
C25	Vienna Game: Vienna Gambit		r1bqkbnr/pppp1ppp/2n5/4p3/4PP2/2N5/PPPP2PP/R1BQKBNR b
C25	Vienna Game: Vienna Gambit, Cunningham Defense		r1bqk1nr/ppppbppp/2n5/8/4Pp2/2N2N2/PPPP2PP/R1BQKB1R w
C25	Vienna Game: Vienna Gambit, Hamppe-Allgaier Gambit		r1bqkbnr/pppp1p1p/2n5/6N1/4PppP/2N5/PPPP2P1/R1BQKB1R b
C25	Vienna Game: Vienna Gambit, Hamppe-Muzio Gambit		r1bqkbnr/pppp1p1p/2n5/8/2B1Pp2/2N2p2/PPPP2PP/R1BQ1RK1 w
C25	Vienna Game: Vienna Gambit, Quelle Gambit		r1bqk1nr/ppp2ppp/2np4/2b1P3/4P3/2N5/PPPP2PP/R1BQKBNR w
C25	Vienna Game: Vienna Gambit, Steinitz Gambit		r1bqkbnr/pppp1ppp/2n5/8/3PPp2/2N5/PPP3PP/R1BQKBNR b
C25	Vienna Game: Vienna Gambit, Steinitz Gambit, Knight Variation		r1bqkbnr/pppp1ppp/2n5/8/4Pp2/2N2N2/PPPP2PP/R1BQKB1R b
C25	Vienna Game: Zhuravlev Countergambit		rnbqk2r/pppp1ppp/5n2/4p3/1b2P1Q1/2N5/PPPP1PPP/R1B1KBNR w
C25	Vienna Game: Zhuravlev Countergambit #2		rnbqk1nr/pppp1ppp/8/4p3/1b2P3/2N5/PPPP1PPP/R1BQKBNR w
C25	Vienna Game: Zhuravlev Countergambit #3		rnbqk1nr/pppp1ppp/8/4p3/1b2P1Q1/2N5/PPPP1PPP/R1B1KBNR b
C26	Bishop's Opening: Vienna Hybrid, Spielmann Attack		rnbqk2r/pppp1ppp/5n2/2b1p3/2B1P3/2NP4/PPP2PPP/R1BQK1NR b
C26	Vienna Game: Falkbeer Variation		rnbqkb1r/pppp1ppp/5n2/4p3/4P3/2N5/PPPP1PPP/R1BQKBNR w
C26	Vienna Game: Mengarini Variation		rnbqkb1r/pppp1ppp/5n2/4p3/4P3/P1N5/1PPP1PPP/R1BQKBNR b
C26	Vienna Game: Mieses Variation		rnbqkb1r/pppp1ppp/5n2/4p3/4P3/2N3P1/PPPP1P1P/R1BQKBNR b
C26	Vienna Game: Stanley Variation		rnbqkb1r/pppp1ppp/5n2/4p3/2B1P3/2N5/PPPP1PPP/R1BQK1NR b
C26	Vienna Game: Stanley Variation, Reversed Spanish		rnbqk2r/pppp1ppp/5n2/4p3/1bB1P3/2N5/PPPP1PPP/R1BQK1NR w
C27	Bishop's Opening: Boden-Kieseritzky Gambit		rnbqkb1r/pppp1ppp/8/4p3/2B1n3/2N2N2/PPPP1PPP/R1BQK2R b
C27	Boden-Kieseritzky Gambit, Lichtenhein Defense		rnbqkb1r/ppp2ppp/8/3pp3/2B1n3/2N2N2/PPPP1PPP/R1BQK2R w
C27	Vienna Game		rnbqkbnr/pppp1ppp/8/4p3/4P3/2N5/PPPP1PPP/R1BQKBNR b
C27	Vienna Game #2		rnbqkb1r/pppp1ppp/8/4p3/2B1n3/2N5/PPPP1PPP/R1BQK1NR w
C27	Vienna Game: Adams' Gambit		3r4/pb4kp/5pp1/1p1Np3/6P1/1B5P/P1P2K2/8 w
C27	Vienna Game: Stanley Variation, Alekhine Variation		r1bqk2r/ppppbppp/2nn4/4N2Q/8/1BN5/PPPP1PPP/R1B1K2R b
C27	Vienna Game: Stanley Variation, Monster Declined		rnbqk2r/ppppbppp/3n4/4p2Q/8/1BN5/PPPP1PPP/R1B1K1NR w
C28	Bishop's Opening: Vienna Hybrid		r1bqkb1r/pppp1ppp/2n2n2/4p3/2B1P3/2NP4/PPP2PPP/R1BQK1NR b
C28	Bishop's Opening: Vienna Hybrid, Hromádka Variation		r1bqk2r/pppp1ppp/2n2n2/4p3/1bB1P3/2NP4/PPP1NPPP/R1BQK2R b
C28	Vienna Game: Stanley Variation, Three Knights Variation		r1bqkb1r/pppp1ppp/2n2n2/4p3/2B1P3/2N5/PPPP1PPP/R1BQK1NR w
C28	Vienna Game: Vienna Gambit		rnbqkb1r/pppp1ppp/5n2/4p3/4PP2/2N5/PPPP2PP/R1BQKBNR b
C28	Vienna Game: Vienna Gambit, Pierce Gambit		r1bqkbnr/pppp1p1p/2n5/6p1/3PPp2/2N2N2/PPP3PP/R1BQKB1R b
C29	Vienna Game: Heyde Variation		rnbqk2r/ppp3pp/8/3pPp2/1b1Pn3/2N2Q2/PPP3PP/R1B1KBNR w
C29	Vienna Game: Vienna Gambit, Bardeleben Variation		rnbqkb1r/ppp3pp/8/3pPp2/4n3/2N2Q2/PPPP2PP/R1B1KBNR w
C29	Vienna Game: Vienna Gambit, Breyer Variation		rnbqk2r/ppp1bppp/8/3pP3/4n3/2N2N2/PPPP2PP/R1BQKB1R w
C29	Vienna Game: Vienna Gambit, Kaufmann Variation		rn1qkb1r/ppp2ppp/8/3pP3/4n1b1/2N2N2/PPPPQ1PP/R1B1KB1R b
C29	Vienna Game: Vienna Gambit, Main Line		rnbqkb1r/ppp2ppp/5n2/3pp3/4PP2/2N5/PPPP2PP/R1BQKBNR w
C29	Vienna Game: Vienna Gambit, Modern Variation		rnbqkb1r/ppp2ppp/8/3pP3/4n3/2NP4/PPP3PP/R1BQKBNR b
C29	Vienna Game: Vienna Gambit, Paulsen Attack		rnbqkb1r/ppp2ppp/8/3pP3/4n3/2N2Q2/PPPP2PP/R1B1KBNR b
C29	Vienna Game: Vienna Gambit, Steinitz Variation		rnbqkb1r/ppp2ppp/5n2/3pp3/4PP2/2NP4/PPP3PP/R1BQKBNR b
C29	Vienna Game: Vienna Gambit, Wurzburger Trap		rnb1kb1r/ppp2ppp/8/3NP2q/8/3P1Nn1/PPP4P/R1BQKB1R b
C30	King's Gambit		rnbqkbnr/pppp1ppp/8/4p3/4PP2/8/PPPP2PP/RNBQKBNR b
C30	King's Gambit Accepted, Dodo Variation		rnbqkbnr/pppp1ppp/8/8/4PpQ1/8/PPPP2PP/RNB1KBNR b
C30	King's Gambit Accepted, Eisenberg Variation		rnbqkbnr/pppp1ppp/8/8/4Pp2/7N/PPPP2PP/RNBQKB1R b
C30	King's Gambit Declined, Classical Variation		rnbqk1nr/pppp1ppp/8/2b1p3/4PP2/8/PPPP2PP/RNBQKBNR w
C30	King's Gambit Declined, Classical Variation #2		rnbqk1nr/ppp2ppp/3p4/2b1p3/4PP2/2P2N2/PP1P2PP/RNBQKB1R b
C30	King's Gambit Declined, Classical Variation, Euwe Attack		rn1qk1nr/ppp2ppp/8/2b1p3/Q3P1b1/2P2N2/PP1P2PP/RNB1KB1R b
C30	King's Gambit Declined, Classical Variation, Rotlewi Countergambit		rnbqk1nr/ppp2ppp/3p4/2b1p3/1P2PP2/5N2/P1PP2PP/RNBQKB1R b
C30	King's Gambit Declined, Classical Variation, Rubinstein Countergambit		rnbqk1nr/ppp3pp/3p4/2b1pp2/4PP2/2P2N2/PP1P2PP/RNBQKB1R w
C30	King's Gambit Declined, Keene Defense		rnb1kbnr/ppppqppp/8/4p3/4PP2/6P1/PPPP3P/RNBQKBNR w
C30	King's Gambit Declined, Mafia Defense		rnbqkbnr/pp1p1ppp/8/2p1p3/4PP2/8/PPPP2PP/RNBQKBNR w
C30	King's Gambit Declined, Miles Defense		r1bqkbnr/pppp2pp/2n5/4pp2/4PP2/5N2/PPPP2PP/RNBQKB1R w
C30	King's Gambit Declined, Norwalde Variation		rnb1kbnr/pppp1ppp/5q2/4p3/4PP2/8/PPPP2PP/RNBQKBNR w
C30	King's Gambit Declined, Norwalde Variation, Schubert Variation		rnb1kbnr/pppp1ppp/8/4p3/3PPq2/2N5/PPP3PP/R1BQKBNR b
C30	King's Gambit Declined, Petrov's Defense		rnbqkb1r/pppp1ppp/5n2/4p3/4PP2/8/PPPP2PP/RNBQKBNR w
C30	King's Gambit Declined, Queen's Knight Defense		r1bqkbnr/pppp1ppp/2n5/4p3/4PP2/8/PPPP2PP/RNBQKBNR w
C30	King's Gambit Declined, Senechaud Countergambit		rnbqk1nr/pppp1p1p/8/2b1p1p1/4PP2/5N2/PPPP2PP/RNBQKB1R w
C30	King's Gambit Declined, Soller-Zilbermints Gambit		r1bqkbnr/pppp2pp/2n2p2/4P3/4P3/8/PPPP2PP/RNBQKBNR w
C30	King's Gambit Declined, Zilbermints Double Gambit I		r1bqkbnr/pppp1p1p/2n5/4p1p1/4PP2/5N2/PPPP2PP/RNBQKB1R w
C30	King's Gambit Declined: Classical, Soldatenkov Variation		rnbqk1nr/ppp2ppp/3p4/2b1P3/4P3/5N2/PPPP2PP/RNBQKB1R b
C30	King's Gambit Declined: Classical, Svenonius Variation		r2qk2r/ppp2ppp/2np1n2/2b5/2B1PB2/2NP1Q1P/PPP3P1/R3K2R b
C30	King's Gambit Declined: Keene's Defense		rnb1kbnr/pppp1ppp/8/4p3/4PP1q/8/PPPP2PP/RNBQKBNR w
C30	King's Gambit Declined: Keene's Defense #2		rnb1kbnr/pppp1ppp/8/4p3/4PP1q/6P1/PPPP3P/RNBQKBNR b
C30	King's Gambit Declined: Norwalde Variation, Buecker Gambit		rnb1k1nr/pppp1ppp/8/4p3/2B1Pq2/2b2N2/PPPP2PP/R1BQK2R w
C30	King's Gambit: Panteldakis Countergambit		rnbqkbnr/pppp2pp/8/4pp2/4PP2/8/PPPP2PP/RNBQKBNR w
C30	King's Gambit: Panteldakis Countergambit, Greco Variation		rnb1kbnr/pppp2pp/8/4pP2/5P1q/6P1/PPPP3P/RNBQKBNR b
C30	King's Gambit: Zilbermints Double Countergambit		rnbqkbnr/pppp1p1p/8/4p1p1/4PP2/8/PPPP2PP/RNBQKBNR w
C31	King's Gambit Declined: Falkbeer Countergambit		rnbqkbnr/ppp2ppp/8/3pp3/4PP2/8/PPPP2PP/RNBQKBNR w
C31	King's Gambit Declined: Falkbeer, Milner-Barry Variation		rnbqkbnr/ppp2ppp/8/3pp3/4PP2/2N5/PPPP2PP/R1BQKBNR b
C31	King's Gambit Declined: Falkbeer, Rubinstein Variation		rnbqkb1r/ppp2ppp/5n2/3P4/4pP2/2N5/PPPPQ1PP/R1B1KBNR b
C31	King's Gambit, Falkbeer Countergambit Accepted		rnbqkbnr/ppp2ppp/8/3Pp3/5P2/8/PPPP2PP/RNBQKBNR b
C31	King's Gambit, Falkbeer Countergambit, Anderssen Attack		rnbqkbnr/ppp2ppp/8/1B1P4/4pP2/8/PPPP2PP/RNBQK1NR b
C31	King's Gambit, Falkbeer Countergambit, Blackburne Attack		rnbqkbnr/ppp2ppp/8/3pp3/4PP2/5N2/PPPP2PP/RNBQKB1R b
C31	King's Gambit, Falkbeer Countergambit, Charousek Gambit		rnbqkbnr/ppp2ppp/8/3P4/4pP2/3P4/PPP3PP/RNBQKBNR b
C31	King's Gambit, Falkbeer Countergambit, Charousek Gambit, Morphy Defense		rnbqk2r/ppp2ppp/5n2/3P4/1b3P2/2NPp3/PPPB2PP/R2QKBNR w
C31	King's Gambit, Falkbeer Countergambit, Hinrichsen Gambit		rnbqkbnr/ppp2ppp/8/3pp3/3PPP2/8/PPP3PP/RNBQKBNR b
C31	King's Gambit, Falkbeer Countergambit, Miles Gambit		rnbqk1nr/ppp2ppp/8/2bPp3/5P2/8/PPPP2PP/RNBQKBNR w
C31	King's Gambit, Falkbeer Countergambit, Nimzowitsch-Marshall Countergambit		rnbqkbnr/pp3ppp/2p5/3Pp3/5P2/8/PPPP2PP/RNBQKBNR w
C31	King's Gambit, Falkbeer Countergambit, Pickler Gambit		rnbqk1nr/pp3ppp/2P5/2b1p3/5P2/8/PPPP2PP/RNBQKBNR w
C31	King's Gambit, Falkbeer Countergambit, Staunton Line		rnbqkbnr/ppp2ppp/8/3P4/4pP2/8/PPPP2PP/RNBQKBNR w
C32	King's Gambit Declined: Falkbeer, Charousek Gambit		rnbqkb1r/ppp2ppp/8/3P4/4nP2/8/PPP1Q1PP/RNB1KBNR b
C32	King's Gambit Declined: Falkbeer, Charousek Variation		rnb1kb1r/ppp3pp/8/3q1p2/4nPP1/8/PPPNQ2P/R1B1KBNR b
C32	King's Gambit Declined: Falkbeer, Tarrasch Variation		rn1qr1k1/ppp2ppp/8/2bPNP2/4nP2/8/PPP1Q2P/RNB1KB1R b
C32	King's Gambit, Falkbeer Countergambit, Charousek Gambit Accepted		rnbqkb1r/ppp2ppp/5n2/3P4/4PP2/8/PPP3PP/RNBQKBNR b
C32	King's Gambit, Falkbeer Countergambit, Charousek Gambit, Keres Variation		rnbqkb1r/ppp2ppp/5n2/3P4/4pP2/3P4/PPPN2PP/R1BQKBNR b
C32	King's Gambit, Falkbeer Countergambit, Charousek Gambit, Main Line		rn1qk2r/ppp2ppp/8/2bP1b2/4nP2/5N2/PPP1Q1PP/RNB1KB1R w
C32	King's Gambit, Falkbeer Countergambit, Charousek Gambit, Old Line		rnbqkb1r/ppp2ppp/5n2/3P4/4pP2/3P4/PPP1Q1PP/RNB1KBNR b
C32	King's Gambit, Falkbeer Countergambit, Modern Transfer		rnbqkbnr/ppp2ppp/8/3P4/5p2/8/PPPP2PP/RNBQKBNR w
C33	King's Gambit Accepted		rnbqkbnr/pppp1ppp/8/8/4Pp2/8/PPPP2PP/RNBQKBNR w
C33	King's Gambit Accepted, Basman Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp2/8/PPPPQ1PP/RNB1KBNR b
C33	King's Gambit Accepted, Bishop's Gambit		rnbqkbnr/pppp1ppp/8/8/2B1Pp2/8/PPPP2PP/RNBQK1NR b
C33	King's Gambit Accepted, Bishop's Gambit, Anderssen Defense		rnbqkbnr/pppp1p1p/8/6p1/2B1Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted, Bishop's Gambit, Bledow Countergambit		rnbqkb1r/ppp2ppp/5n2/3B4/4Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted, Bishop's Gambit, Bogoljubov Defense		rnbqkb1r/pp1p1ppp/2p2n2/8/2B1Pp2/2N5/PPPP2PP/R1BQK1NR w
C33	King's Gambit Accepted, Bishop's Gambit, Bogoljubov Variation		rnbqkb1r/pppp1ppp/5n2/8/2B1Pp2/2N5/PPPP2PP/R1BQK1NR b
C33	King's Gambit Accepted, Bishop's Gambit, Cozio Defense		rnbqkb1r/pppp1ppp/5n2/8/2B1Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted, Bishop's Gambit, Gianutio Gambit		rnbqkbnr/pppp2pp/8/5p2/2B1Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted, Bishop's Gambit, Kieseritzky Gambit		rnbqkbnr/p1pp1ppp/8/1p6/2B1Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted, Bishop's Gambit, Lopez Defense		rnbqkbnr/pp1p1ppp/2p5/8/2B1Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted, Bishop's Gambit, Maurian Defense		r1bqkbnr/pppp1ppp/2n5/8/2B1Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted, Breyer Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp2/5Q2/PPPP2PP/RNB1KBNR b
C33	King's Gambit Accepted, Carrera Gambit		rnbqkbnr/pppp1ppp/8/7Q/4Pp2/8/PPPP2PP/RNB1KBNR b
C33	King's Gambit Accepted, Gaga Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp2/6P1/PPPP3P/RNBQKBNR b
C33	King's Gambit Accepted, Mason-Keres Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp2/2N5/PPPP2PP/R1BQKBNR b
C33	King's Gambit Accepted, Orsini Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp2/1P6/P1PP2PP/RNBQKBNR b
C33	King's Gambit Accepted, Paris Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp2/8/PPPPN1PP/RNBQKB1R b
C33	King's Gambit Accepted, Polerio Gambit		rnbqkbnr/pppp1ppp/8/8/3PPp2/8/PPP3PP/RNBQKBNR b
C33	King's Gambit Accepted, Schurig Gambit		rnbqkbnr/pppp1ppp/8/1B6/4Pp2/8/PPPP2PP/RNBQK1NR b
C33	King's Gambit Accepted, Stamma Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp1P/8/PPPP2P1/RNBQKBNR b
C33	King's Gambit Accepted, Tartakower Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp2/8/PPPPB1PP/RNBQK1NR b
C33	King's Gambit Accepted: Bishop's Gambit, Anderssen Variation		rnbqkbnr/pp3ppp/2p5/3B4/4Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted: Bishop's Gambit, Bledow Variation		rnbqkbnr/ppp2ppp/8/3p4/2B1Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted: Bishop's Gambit, Paulsen Attack		rnbqk2r/pppp1ppp/5n2/4P3/1bB2p2/2N5/PPPP2PP/R1BQK1NR b
C33	King's Gambit Accepted: Bishop's Gambit, Steinitz Defense		rnbqkb1r/ppppnppp/8/8/2B1Pp2/8/PPPP2PP/RNBQK1NR w
C33	King's Gambit Accepted: Schurig Gambit #2		rnbqkbnr/pppp1ppp/8/8/4Pp2/3B4/PPPP2PP/RNBQK1NR b
C34	King's Gambit Accepted, Becker Defense		rnbqkbnr/pppp1pp1/7p/8/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C34	King's Gambit Accepted, Bonsch-Osmolovsky Variation		rnbqkb1r/ppppnppp/8/8/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C34	King's Gambit Accepted, Fischer Defense		rnbqkbnr/ppp2ppp/3p4/8/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C34	King's Gambit Accepted, Fischer Defense, Schulder Gambit		rnbqkbnr/ppp2ppp/3p4/8/1P2Pp2/5N2/P1PP2PP/RNBQKB1R b
C34	King's Gambit Accepted, Fischer Defense, Spanish Variation		rnbqkb1r/ppp2ppp/3p1n2/8/3PPp2/3B1N2/PPP3PP/RNBQK2R b
C34	King's Gambit Accepted, Gianutio Countergambit		rnbqkbnr/pppp2pp/8/5p2/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C34	King's Gambit Accepted, King's Knight Gambit		rnbqkbnr/pppp1ppp/8/8/4Pp2/5N2/PPPP2PP/RNBQKB1R b
C34	King's Gambit Accepted, MacLeod Defense		r1bqkbnr/pppp1ppp/2n5/8/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C34	King's Gambit Accepted, Schallopp Defense		rnbqkb1r/pppp1ppp/5n2/8/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C34	King's Gambit Accepted, Schallopp Defense, Tashkent Attack		rnbqkb1r/pppp1ppp/8/4P2n/5pP1/5N2/PPPP3P/RNBQKB1R b
C35	King's Gambit Accepted, Cunningham Defense		rnbqk1nr/ppppbppp/8/8/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C35	King's Gambit Accepted, Cunningham Defense, Bertin Gambit		rnbqk1nr/pppp1ppp/8/8/2B1P2b/5N2/PPPP3p/RNBQ1R1K b
C35	King's Gambit Accepted, Cunningham Defense, McCormick Defense		rnbqk2r/ppppbppp/5n2/8/2B1Pp2/5N2/PPPP2PP/RNBQK2R w
C35	King's Gambit Accepted: Cunningham, Bertin Gambit		rnbqk1nr/pppp1ppp/8/8/2B1Pp1b/5NP1/PPPP3P/RNBQK2R b
C36	King's Gambit Accepted, Abbazia Defense		rnbqkb1r/ppp2ppp/5n2/3P4/5p2/5N2/PPPP2PP/RNBQKB1R w
C36	King's Gambit Accepted, Abbazia Defense, Main Line		rnbqkb1r/p4ppp/2p5/3n4/2B2p2/5N2/PPPP2PP/RNBQ1RK1 b
C36	King's Gambit Accepted, Modern Defense		rnbqkbnr/ppp2ppp/8/3p4/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C36	King's Gambit Accepted: Modern Defense #2		rnbqkbnr/ppp2ppp/8/3P4/5p2/5N2/PPPP2PP/RNBQKB1R b
C37	King's Gambit Accepted, Australian Gambit		rnbqkbnr/pppp1p1p/8/8/2B1PppP/5N2/PPPP2P1/RNBQK2R b
C37	King's Gambit Accepted, Blachly Gambit		r1bqkbnr/pppp1p1p/2n5/6p1/2B1Pp2/5N2/PPPP2PP/RNBQK2R w
C37	King's Gambit Accepted, Double Muzio Gambit		rnb1kbnr/pppp1B1p/8/4q3/5p2/5Q2/PPPP2PP/RNB2RK1 b
C37	King's Gambit Accepted, Double Muzio Gambit, Bello Gambit		rnb1kbnr/pppp1p1p/5q2/8/2B1Pp2/2N2Q2/PPPP2PP/R1B2RK1 b
C37	King's Gambit Accepted, Double Muzio Gambit, Paulsen Defense		r1b1k2r/ppppnp1p/2n4b/4q3/2B2p2/2NP1Q2/PPPB2PP/4RRK1 b
C37	King's Gambit Accepted, Ghulam-Kassim Gambit		rnbqkbnr/pppp1p1p/8/8/2BPPp2/5Q2/PPP3PP/RNB1K2R b
C37	King's Gambit Accepted, King's Knight Gambit		rnbqkbnr/pppp1p1p/8/6p1/2B1Pp2/5N2/PPPP2PP/RNBQK2R b
C37	King's Gambit Accepted, Kotov Gambit		r7/pp6/2p2nk1/4B1N1/6p1/8/PPPK4/r7 w
C37	King's Gambit Accepted, Lolli Gambit		rnbqkbnr/pppp1B1p/8/8/4Ppp1/5N2/PPPP2PP/RNBQK2R b
C37	King's Gambit Accepted, McDonnell Gambit		rnbqkbnr/pppp1p1p/8/8/2B1Ppp1/2N2N2/PPPP2PP/R1BQK2R b
C37	King's Gambit Accepted, Muzio Gambit Accepted, From's Defense		rnb1kbnr/ppppqp1p/8/8/2B1Pp2/5Q2/PPPP2PP/RNB2RK1 w
C37	King's Gambit Accepted, Muzio Gambit, Sarratt Defense		rnb1kbnr/pppp1p1p/5q2/8/2B1Pp2/5Q2/PPPP2PP/RNB2RK1 w
C37	King's Gambit Accepted, Muzio Gambit, Wild Muzio Gambit		rnbqkbnr/pppp1p1p/8/8/2B1Ppp1/5N2/PPPP2PP/RNBQ1RK1 b
C37	King's Gambit Accepted, Quade Gambit		rnbqkbnr/pppp1p1p/8/6p1/4Pp2/2N2N2/PPPP2PP/R1BQKB1R b
C37	King's Gambit Accepted, Rosentreter Gambit		rnbqkbnr/pppp1p1p/8/6p1/3PPp2/5N2/PPP3PP/RNBQKB1R b
C37	King's Gambit Accepted, Rosentreter Gambit, Bird Gambit		rnb1kbnr/pppp1p1p/8/4N3/3PP1pq/6p1/PPP4P/RNBQKB1R w
C37	King's Gambit Accepted, Rosentreter Gambit, Soerensen Gambit		rnbqkbnr/pppp1p1p/8/8/3PPp2/2N2p2/PPP3PP/R1BQKB1R w
C37	King's Gambit Accepted, Rosentreter-Testa Gambit		rnbqkbnr/pppp1p1p/8/8/3PPBp1/5N2/PPP3PP/RN1QKB1R b
C37	King's Gambit Accepted, Salvio Gambit		rnbqkbnr/pppp1p1p/8/4N3/2B1Ppp1/8/PPPP2PP/RNBQK2R b
C37	King's Gambit Accepted: Ghulam-Kassim Gambit #2		rnbqkbnr/pppp1p1p/8/8/2BPPpp1/5N2/PPP3PP/RNBQK2R b
C37	King's Gambit Accepted: King Knight's Gambit		rnbqkbnr/pppp1p1p/8/6p1/4Pp2/5N2/PPPP2PP/RNBQKB1R w
C37	King's Gambit Accepted: Muzio Gambit, Holloway Defense		r1bqkbnr/pppp1p1p/2n5/8/2B1Pp2/5Q2/PPPP2PP/RNB2RK1 w
C37	King's Gambit Accepted: Muzio Gambit, Kling and Horwitz Counterattack		rnb1kbnr/ppppqp1p/8/8/2B1Ppp1/5N2/PPPP2PP/RNBQ1RK1 w
C37	King's Gambit Accepted: Soerensen Gambit		rnbqkbnr/pppp1p1p/8/4N3/3PPpp1/8/PPP3PP/RNBQKB1R b
C38	King's Gambit Accepted, Greco Gambit		rnbqk1nr/ppp2pb1/3p3p/6p1/2BPPp1P/5N2/PPP3P1/RNBQK2R w
C38	King's Gambit Accepted, Hanstein Gambit		rnbqk1nr/pppp1pbp/8/6p1/2B1Pp2/5N2/PPPP2PP/RNBQ1RK1 b
C38	King's Gambit Accepted, Mayet Gambit		rnbqk1nr/ppp2pbp/3p4/6p1/2BPPp2/2P2N2/PP4PP/RNBQK2R b
C38	King's Gambit Accepted, Philidor Gambit		rnbqk1nr/pppp1pbp/8/6p1/2B1Pp1P/5N2/PPPP2P1/RNBQK2R b
C38	King's Gambit Accepted, Traditional Variation		rnbqk1nr/pppp1pbp/8/6p1/2B1Pp2/5N2/PPPP2PP/RNBQK2R w
C39	King's Gambit Accepted, Allgaier Gambit		rnbqkbnr/pppp1p1p/8/6N1/4PppP/8/PPPP2P1/RNBQKB1R b
C39	King's Gambit Accepted, Kieseritzky Gambit, Anderssen Defense		rnbqk2r/ppp2p1p/3b1n2/3PN3/2B2ppP/8/PPPP2P1/RNBQK2R w
C39	King's Gambit Accepted, Kieseritzky Gambit, Berlin Defense		rnbqkb1r/pppp1p1p/5n2/4N3/4PppP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted, Kieseritzky Gambit, Berlin Defense #2		rnbqkb1r/pppp1p1p/5n2/4N3/2B1PppP/8/PPPP2P1/RNBQK2R b
C39	King's Gambit Accepted, Kieseritzky Gambit, Cotter Gambit		rnbqkbnr/pppp1N2/7p/8/4PppP/8/PPPP2P1/RNBQKB1R b
C39	King's Gambit Accepted, Kieseritzky Gambit, Kolisch Defense		rnbqkbnr/ppp2p1p/3p4/4N3/4PppP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted, Kieseritzky Gambit, Long Whip		rnbqkbnr/pppp1p2/8/4N2p/4PppP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted, Kieseritzky Gambit, Neumann Defense		r1bqkbnr/pppp1p1p/2n5/4N3/4PppP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted, Kieseritzky Gambit, Paulsen Defense		rnbqk1nr/pppp1pbp/8/4N3/4PppP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted, Kieseritzky Gambit, Rosenthal Defense		rnb1kbnr/ppppqp1p/8/4N3/4PppP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted: Allgaier, Schlechter Defense		rnbqkb1r/pppp1p1p/5n2/6N1/4PppP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted: Kieseritzky, Berlin Defense: de Riviere Variation		rnbqkb1r/ppp2p1p/5n2/3p4/4PpNP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted: Kieseritzky, Polerio Defense		rnbqk1nr/ppppbp1p/8/4N3/4PppP/8/PPPP2P1/RNBQKB1R w
C39	King's Gambit Accepted: King Knight's Gambit		rnbqkbnr/pppp1p1p/8/6p1/4Pp1P/5N2/PPPP2P1/RNBQKB1R b
C40	Elephant Gambit		rnbqkbnr/ppp2ppp/8/3pp3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C40	Elephant Gambit: Maróczy Gambit		rnbqk1nr/ppp2ppp/3b4/3Pp3/8/5N2/PPPP1PPP/RNBQKB1R w
C40	Elephant Gambit: Paulsen Countergambit		rnbqkbnr/ppp2ppp/8/3P4/4p3/5N2/PPPP1PPP/RNBQKB1R w
C40	Elephant Gambit: Wasp Variation		rnb1kbnr/ppp2ppp/8/4N1q1/2B1p3/8/PPPP1PPP/RNBQK2R w
C40	Gunderam Defense		rnb1kbnr/ppppqppp/8/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C40	King's Knight Opening		rnbqkbnr/pppp1ppp/8/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R b
C40	King's Pawn Game: Busch-Gass Gambit		rnbqk1nr/pppp1ppp/8/2b1p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C40	King's Pawn Game: Busch-Gass Gambit, Chiodini Gambit		r1bqk1nr/pppp1ppp/2n5/2b1N3/4P3/8/PPPP1PPP/RNBQKB1R w
C40	King's Pawn Game: Damiano Defense		rnbqkbnr/pppp2pp/5p2/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C40	King's Pawn Game: Gunderam Defense, Gunderam Gambit		rnb1kbnr/ppppq1pp/8/4pp2/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C40	King's Pawn Game: Gunderam Gambit		rnbqkbnr/pp1p1ppp/2p5/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C40	King's Pawn Game: La Bourdonnais Gambit		rnb1kbnr/pppp1ppp/6q1/4p3/2B1P3/5N2/PPPP1PPP/RNBQ1RK1 b
C40	King's Pawn Game: McConnell Defense		rnb1kbnr/pppp1ppp/5q2/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C40	King's Pawn Game: Schulze-Mueller Gambit		r1bqkbnr/pppp1ppp/8/4n3/3PP3/8/PPP2PPP/RNBQKB1R b
C40	Latvian Gambit		rnbqkbnr/pppp2pp/8/4pp2/4P3/5N2/PPPP1PPP/RNBQKB1R w
C40	Latvian Gambit Accepted		rnbqkbnr/pppp2pp/8/4pP2/8/5N2/PPPP1PPP/RNBQKB1R b
C40	Latvian Gambit Accepted, Bilguer Variation		rnb1kbnr/ppp3pp/3p1q2/5p2/2NPP3/8/PPP2PPP/RNBQKB1R b
C40	Latvian Gambit Accepted, Bronstein Attack		rnb1kbnr/ppp3pp/3p1q2/8/2NPp3/8/PPP1BPPP/RNBQK2R b
C40	Latvian Gambit Accepted, Bronstein Gambit		rnb1kbnr/ppp4p/3p1qp1/8/2NPp3/8/PPP1QPPP/RNB1KB1R b
C40	Latvian Gambit Accepted, Foltys Variation		rnb1kbnr/pppp2pp/5q2/8/2N1p3/3P4/PPP2PPP/RNBQKB1R b
C40	Latvian Gambit Accepted, Foltys-Leonhardt Variation		rnb1kbnr/pppp2pp/5q2/5p2/2N1P3/8/PPPP1PPP/RNBQKB1R b
C40	Latvian Gambit Accepted, Leonhardt Variation		rnb1kbnr/pppp2pp/5q2/8/2N1p3/2N5/PPPP1PPP/R1BQKB1R b
C40	Latvian Gambit Accepted, Main Line		rnb1kbnr/pppp2pp/5q2/4Np2/3PP3/8/PPP2PPP/RNBQKB1R b
C40	Latvian Gambit Accepted, Nimzowitsch Attack		rnb1kbnr/ppp3pp/3p1q2/8/3Pp3/4N3/PPP2PPP/RNBQKB1R b
C40	Latvian Gambit: Clam Gambit		r1bqkbnr/pppp2pp/2n5/4pP2/8/3P1N2/PPP2PPP/RNBQKB1R b
C40	Latvian Gambit: Corkscrew Countergambit		rnbqkb1r/pppp2pp/5n2/4N3/2B1p3/8/PPPP1PPP/RNBQK2R w
C40	Latvian Gambit: Diepstraten Countergambit		rnbqkbnr/pppp2pp/8/4pp2/2P1P3/5N2/PP1P1PPP/RNBQKB1R b
C40	Latvian Gambit: Fraser Defense		r1bqkbnr/pppp2pp/2n5/4Np2/4P3/8/PPPP1PPP/RNBQKB1R w
C40	Latvian Gambit: Greco Variation		rnb1kbnr/ppppq1pp/8/4Np2/4P3/8/PPPP1PPP/RNBQKB1R w
C40	Latvian Gambit: Lobster Gambit		5kr1/pp5p/2p4p/4n1b1/8/2N1N3/PPb1KP1P/3R4 w
C40	Latvian Gambit: Mason Countergambit		rnbqkbnr/pppp2pp/8/4pp2/3PP3/5N2/PPP2PPP/RNBQKB1R b
C40	Latvian Gambit: Mayet Attack		rnbqkbnr/pppp2pp/8/4pp2/2B1P3/5N2/PPPP1PPP/RNBQK2R b
C40	Latvian Gambit: Mayet Attack, Morgado Defense		rnbqkb1r/pppp2pp/5n2/4pp2/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C40	Latvian Gambit: Mayet Attack, Poisoned Pawn Variation		rnb1kbnr/pppp2pp/8/4N3/2BPp3/8/PPP2PqP/RNBQK2R w
C40	Latvian Gambit: Mayet Attack, Polerio-Svedenborg Variation		rnbqkbnr/ppp3pp/8/3pN3/2B1p3/8/PPPP1PPP/RNBQK2R w
C40	Latvian Gambit: Mayet Attack, Strautins Gambit		rnbqkbnr/p1pp2pp/8/1p2pp2/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C40	Latvian Gambit: Mlotkowski Variation		rnbqkbnr/pppp2pp/8/4pp2/4P3/2N2N2/PPPP1PPP/R1BQKB1R b
C40	Latvian Gambit: Senechaud Gambit		r3k3/1ppn3p/p7/8/4P1P1/8/6KP/2q5 b
C41	Philidor Defense		rnbqkbnr/ppp2ppp/3p4/4p3/3PP3/5N2/PPP2PPP/RNBQKB1R b
C41	Philidor Defense #2		rnbqkbnr/ppp2ppp/3p4/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C41	Philidor Defense #3		rnbqkbnr/ppp2ppp/3p4/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R b
C41	Philidor Defense #4		rnbqk1nr/ppp1bppp/3p4/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C41	Philidor Defense: Alapin-Blackburne Gambit		r2qkbnr/pppn1ppp/3p4/4P3/4P1b1/5N2/PPP2PPP/RNBQKB1R w
C41	Philidor Defense: Berger Variation		rr4k1/2qnbppp/2p3b1/4p3/p3PnN1/N1B2Q1P/BPP2P2/1K1RR3 w
C41	Philidor Defense: Bird Gambit		rnbqkbnr/ppp2ppp/3p4/8/3pP3/2P2N2/PP3PPP/RNBQKB1R b
C41	Philidor Defense: Boden Variation		rn1qkbnr/pppb1ppp/3p4/8/3QP3/5N2/PPP2PPP/RNB1KB1R w
C41	Philidor Defense: Exchange Variation		rnbqkb1r/ppp2ppp/3p1n2/8/3NP3/8/PPP2PPP/RNBQKB1R w
C41	Philidor Defense: Exchange Variation #2		rnbqkbnr/ppp2ppp/3p4/8/3NP3/8/PPP2PPP/RNBQKB1R b
C41	Philidor Defense: Exchange Variation #3		rnbqkbnr/ppp2ppp/3p4/8/3pP3/5N2/PPP2PPP/RNBQKB1R w
C41	Philidor Defense: Hanham Variation		r1bqkbnr/pppn1ppp/3p4/4p3/3PP3/5N2/PPP2PPP/RNBQKB1R w
C41	Philidor Defense: Hanham Variation, Delmar Variation		r1bqkbnr/pp1n1ppp/2pp4/4p3/2BPP3/2P2N2/PP3PPP/RNBQK2R b
C41	Philidor Defense: Hanham Variation, Krause Variation		r1bqkbnr/pp1n1ppp/2pp4/4p3/2BPP3/5N2/PPP2PPP/RNBQ1RK1 b
C41	Philidor Defense: Hanham Variation, Schlechter Variation		r1bqkbnr/pp1n1ppp/2pp4/4p3/2BPP3/2N2N2/PPP2PPP/R1BQK2R b
C41	Philidor Defense: Hanham Variation, Sharp Variation		r1bqkbnr/ppp2ppp/1n1p4/4p3/2BPP3/5N2/PPP2PPP/RNBQK2R w
C41	Philidor Defense: Hanham Variation, Steiner Variation		r1bqk1nr/pp1nbppp/2pp4/4P3/2B1P3/5N2/PPP2PPP/RNBQ1RK1 b
C41	Philidor Defense: Hanham, Kmoch Variation		r1bqkbnr/pp1n1ppp/2pp4/4p3/2BPP3/5N2/PPP2PPP/RNBQK2R w
C41	Philidor Defense: Larsen Variation		rnbqkbnr/ppp2p1p/3p2p1/8/3NP3/8/PPP2PPP/RNBQKB1R w
C41	Philidor Defense: Lion Variation		r1bqkb1r/pppn1ppp/3p1n2/4p3/3PP3/2N2N2/PPP2PPP/R1BQKB1R w
C41	Philidor Defense: Lion Variation, Bishop Sacrifice		r1bq3r/pppnbkpp/3p1n2/4p3/3PP3/2N2N2/PPP2PPP/R1BQK2R w
C41	Philidor Defense: Lion Variation, Delayed Bishop Sacrifice		r1bq3r/pppnbkpp/5n2/4p1N1/4P3/2N5/PPP2PPP/R1BQK2R b
C41	Philidor Defense: Lion Variation, Forcing Line		r1bq2k1/pppnbrpp/3pNn2/4p3/3PP3/2N5/PPP2PPP/R1BQK2R b
C41	Philidor Defense: Lion Variation, Lion's Claw I		r1bqk2r/pp1nbppp/2pp1n2/4p3/3PP3/2N2N2/PPP1BPPP/R1BQ1RK1 w
C41	Philidor Defense: Lion Variation, Lion's Claw II		r1bqk2r/pppnbpp1/3p1n1p/4p3/2BPP3/2N2N2/PPP2PPP/R1BQ1RK1 w
C41	Philidor Defense: Lion Variation, Shirov Gambit		r1bqkb1r/pppn1ppp/3p1n2/4p3/3PP1P1/2N2N2/PPP2P1P/R1BQKB1R b
C41	Philidor Defense: Lion Variation, Sozin Variation		r5k1/1p1qrpp1/p2p3p/2pP4/P3P1nP/1P1Q2P1/4RP2/4R1K1 w
C41	Philidor Defense: Lopez Countergambit		rnbqkbnr/ppp3pp/3p4/4pp2/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C41	Philidor Defense: Morphy Gambit		rnbqkbnr/ppp2ppp/3p4/8/2BpP3/5N2/PPP2PPP/RNBQK2R b
C41	Philidor Defense: Nimzowitsch Variation		rnbqkb1r/ppp2ppp/3p1n2/4P3/4P3/5N2/PPP2PPP/RNBQKB1R b
C41	Philidor Defense: Nimzowitsch Variation #2		rnbqkb1r/ppp2ppp/3p1n2/4p3/3PP3/5N2/PPP2PPP/RNBQKB1R w
C41	Philidor Defense: Nimzowitsch Variation, Klein Variation		rnbqkb1r/ppp2ppp/3p1n2/4p3/2BPP3/5N2/PPP2PPP/RNBQK2R b
C41	Philidor Defense: Nimzowitsch Variation, Rellstab Variation		rnbqkb1r/ppp2ppp/3p4/3QP3/4n3/5N2/PPP2PPP/RNB1KB1R b
C41	Philidor Defense: Nimzowitsch Variation, Sokolsky Variation		rnbqkb1r/ppp2ppp/3p4/4P3/4n3/5N2/PPPN1PPP/R1BQKB1R b
C41	Philidor Defense: Nimzowitsch, Locock Variation		rnbqkb1r/ppp2ppp/3p1n2/4p1N1/3PP3/8/PPP2PPP/RNBQKB1R b
C41	Philidor Defense: Paulsen Attack		rnbqkbnr/ppp2ppp/8/3P4/3N4/8/PPP2PPP/RNBQKB1R b
C41	Philidor Defense: Philidor Countergambit		rnbqkbnr/ppp3pp/3p4/4pp2/3PP3/5N2/PPP2PPP/RNBQKB1R w
C41	Philidor Defense: Philidor Countergambit, Berger Variation		rr6/2p3pp/2ppk3/5p2/2Q5/2N5/PPP2PPP/2KR4 b
C41	Philidor Defense: Philidor Countergambit, Zukertort Variation		rnbqkbnr/ppp3pp/3p4/4pp2/3PP3/2N2N2/PPP2PPP/R1BQKB1R b
C41	Philidor Defense: Philidor Countergambit, del Rio Attack		rnbqkbnr/ppp3pp/4P3/3p2N1/4p3/8/PPP2PPP/RNBQKB1R b
C41	Philidor Defense: Philidor Gambit		rn1qkbnr/pppb1ppp/3p4/4p3/3PP3/5N2/PPP2PPP/RNBQKB1R w
C41	Philidor Defense: Steinitz Variation		rnbqk1nr/ppp1bppp/3p4/4p3/2B1P3/2P2N2/PP1P1PPP/RNBQK2R b
C42	Petrov's Defense		rnbqkb1r/pppp1ppp/5n2/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C42	Petrov's Defense #2		rnbqkb1r/pppp1ppp/5n2/4N3/4P3/8/PPPP1PPP/RNBQKB1R b
C42	Petrov's Defense #3		rnbqkb1r/ppp2ppp/3p1n2/4N3/4P3/8/PPPP1PPP/RNBQKB1R w
C42	Petrov's Defense #4		rnbqkb1r/ppp2ppp/3p1n2/8/4P3/5N2/PPPP1PPP/RNBQKB1R b
C42	Petrov's Defense #5		rnbqkb1r/ppp2ppp/3p4/8/4n3/5N2/PPPP1PPP/RNBQKB1R w
C42	Russian Game: Classical Attack		rnbqkb1r/ppp2ppp/3p4/8/3Pn3/5N2/PPP2PPP/RNBQKB1R b
C42	Russian Game: Classical Attack, Berger Variation		r2qk2r/ppp1b1pp/2n5/3p1p2/3Pn1b1/2PB1N2/PP1N1PPP/R1BQR1K1 b
C42	Russian Game: Classical Attack, Chigorin Variation		r1bqk2r/ppp1bppp/2n5/3p4/3Pn3/3B1N2/PPP2PPP/RNBQR1K1 b
C42	Russian Game: Classical Attack, Chigorin Variation, Browne Attack		r1bqk2r/ppp1bppp/8/3P4/1n1Pn3/3B1N2/PP3PPP/RNBQ1RK1 b
C42	Russian Game: Classical Attack, Chigorin Variation, Main Line		r1bqk2r/ppp1bppp/8/3p4/1nPPn3/5N2/PP2BPPP/RNBQ1RK1 b
C42	Russian Game: Classical Attack, Closed Variation		rnbqkb1r/ppp2ppp/3p1n2/8/3P4/5N2/PPP2PPP/RNBQKB1R w
C42	Russian Game: Classical Attack, Jaenisch Variation		r1bqk2r/ppp1bppp/2n5/3p4/2PPn3/3B1N2/PP3PPP/RNBQ1RK1 b
C42	Russian Game: Classical Attack, Marshall Trap		rn1q1rk1/ppp3pp/8/3P1p2/3P4/3Q1b2/PP4PK/RNB1R3 w
C42	Russian Game: Classical Attack, Marshall Variation		rnbqk2r/ppp2ppp/3b4/3p4/3Pn3/3B1N2/PPP2PPP/RNBQK2R w
C42	Russian Game: Classical Attack, Marshall Variation, Chinese Gambit		rn1q1rk1/pp3ppp/2pb4/3p4/2PPn1b1/3B1N2/PP3PPP/RNBQR1K1 w
C42	Russian Game: Classical Attack, Mason Variation		rnbq1rk1/ppp1bppp/8/3p4/3Pn3/3B1N2/PPP2PPP/RNBQ1RK1 w
C42	Russian Game: Classical Attack, Mason-Showalter Variation		r1bqkb1r/ppp2ppp/2n5/3p4/3Pn3/3B1N2/PPP2PPP/RNBQK2R w
C42	Russian Game: Classical Attack, Staunton Variation		rnbq1rk1/pp3ppp/2pb4/3p4/2PPn3/3B1N2/PP3PPP/RNBQ1RK1 w
C42	Russian Game: Classical Attack, Tarrasch Variation		rn1q1rk1/ppp2ppp/3b4/3p4/2PPn1b1/3B1N2/PP3PPP/RNBQ1RK1 w
C42	Russian Game: Cochrane Gambit		rnbqkb1r/ppp2Npp/3p1n2/8/4P3/8/PPPP1PPP/RNBQKB1R b
C42	Russian Game: Cozio (Lasker) Attack		rnbqkb1r/ppp2ppp/3p4/8/4n3/5N2/PPPPQPPP/RNB1KB1R b
C42	Russian Game: Damiano Variation		rnbqkb1r/pppp1ppp/8/4N3/4n3/8/PPPP1PPP/RNBQKB1R w
C42	Russian Game: Damiano Variation, Kholmov Gambit		rnb1kb1r/ppppqppp/8/4N3/4n3/8/PPPPQPPP/RNB1KB1R w
C42	Russian Game: French Attack		rnbqkb1r/ppp2ppp/3p4/8/4n3/3P1N2/PPP2PPP/RNBQKB1R b
C42	Russian Game: Karklins-Martinovsky Variation		rnbqkb1r/ppp2ppp/3p1n2/8/4P3/3N4/PPPP1PPP/RNBQKB1R b
C42	Russian Game: Kaufmann Attack		rnbqkb1r/ppp2ppp/3p4/8/2P1n3/5N2/PP1P1PPP/RNBQKB1R b
C42	Russian Game: Millennium Attack		rnbqkb1r/ppp2ppp/3p4/8/4n3/3B1N2/PPPP1PPP/RNBQK2R b
C42	Russian Game: Moody Gambit		r1bqkb1r/pppp1ppp/2n2n2/4p3/3PP3/5N2/PPP1QPPP/RNB1KB1R b
C42	Russian Game: Nimzowitsch Attack		rnbqkb1r/ppp2ppp/3p4/8/4n3/2N2N2/PPPP1PPP/R1BQKB1R b
C42	Russian Game: Paulsen Attack		rnbqkb1r/ppp2ppp/3p1n2/8/2N1P3/8/PPPP1PPP/RNBQKB1R b
C42	Russian Game: Stafford Gambit		r1bqkb1r/pppp1ppp/2n2n2/4N3/4P3/8/PPPP1PPP/RNBQKB1R w
C42	Russian Game: Three Knights Game		rnbqkb1r/pppp1ppp/5n2/4p3/4P3/2N2N2/PPPP1PPP/R1BQKB1R b
C42	Russian Game: Urusov Gambit		rnbqkb1r/pppp1ppp/5n2/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R b
C43	Petrov: Modern Attack		rnbqkb1r/pppp1ppp/5n2/8/3pP3/5N2/PPP2PPP/RNBQKB1R w
C43	Russian Game: Modern Attack		rnbqkb1r/pppp1ppp/5n2/4p3/3PP3/5N2/PPP2PPP/RNBQKB1R b
C43	Russian Game: Modern Attack, Bardeleben Variation		2kn3Q/ppp3pp/3bp3/8/8/4B1P1/1PP2P1P/2KR4 b
C43	Russian Game: Modern Attack, Center Attack		rnbqkb1r/pppp1ppp/8/4P3/3Qn3/5N2/PPP2PPP/RNB1KB1R b
C43	Russian Game: Modern Attack, Center Variation		rnbqkb1r/pppp1ppp/8/4p3/3Pn3/3B1N2/PPP2PPP/RNBQK2R b
C43	Russian Game: Modern Attack, Murrey Variation		r1bqkb1r/pppp1ppp/2n5/4p3/3Pn3/3B1N2/PPP2PPP/RNBQK2R w
C43	Russian Game: Modern Attack, Steinitz Variation		rnbqkb1r/pppp1ppp/8/4P3/3pn3/5N2/PPP1QPPP/RNB1KB1R b
C43	Russian Game: Modern Attack, Symmetrical Variation		rnbqkb1r/ppp2ppp/5n2/3pp3/3PP3/5N2/PPP2PPP/RNBQKB1R w
C43	Russian Game: Modern Attack, Symmetrical Variation #2		rnbqkb1r/pppp1ppp/8/4p3/3Pn3/5N2/PPP2PPP/RNBQKB1R w
C43	Russian Game: Modern Attack, Tal Gambit		rnbqkb1r/pppp1ppp/8/1B2P3/3pn3/5N2/PPP2PPP/RNBQK2R b
C43	Russian Game: Modern Attack, Trifunovic Variation		rnbq1rk1/ppp2ppp/8/3pP3/2P1n3/3B4/PP3PPP/RNBQ1RK1 b
C44	Irish Gambit		r1bqkbnr/pppp1ppp/2n5/4N3/4P3/8/PPPP1PPP/RNBQKB1R b
C44	King's Knight Opening: Konstantinopolsky		r1bqkbnr/pppp1ppp/2n5/4p3/4P3/5NP1/PPPP1P1P/RNBQKB1R b
C44	King's Knight Opening: Normal Variation		r1bqkbnr/pppp1ppp/2n5/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w
C44	King's Pawn Game: Dresden Opening		r1bqkbnr/pppp1ppp/2n5/4p3/2P1P3/5N2/PP1P1PPP/RNBQKB1R b
C44	King's Pawn Game: Pachman Wing Gambit		r1bqkbnr/pppp1ppp/2n5/4p3/1P2P3/5N2/P1PP1PPP/RNBQKB1R b
C44	King's Pawn Game: Tayler Opening		r1bqkbnr/pppp1ppp/2n5/4p3/4P3/5N2/PPPPBPPP/RNBQK2R b
C44	King's Pawn Game: Tayler Opening, Basman Gambit		r1bqkb1r/pppp1ppp/2n2n2/4P3/3p4/5N2/PPP1BPPP/RNBQK2R b
C44	King's Pawn Game: Tayler Opening, Inverted Hanham		r1bqkb1r/ppp2ppp/2n2n2/3pp3/4P3/3P1N2/PPPNBPPP/R1BQK2R b
C44	Ponziani Opening		r1bqkbnr/pppp1ppp/2n5/4p3/4P3/2P2N2/PP1P1PPP/RNBQKB1R b
C44	Ponziani Opening: Caro Gambit		r2qkbnr/pppb1ppp/2n5/3pp3/Q3P3/2P2N2/PP1P1PPP/RNB1KB1R w
C44	Ponziani Opening: Jaenisch Counterattack		r1bqkb1r/pppp1ppp/2n2n2/4p3/4P3/2P2N2/PP1P1PPP/RNBQKB1R w
C44	Ponziani Opening: Leonhardt Variation		r1bqkb1r/ppp2ppp/2n2n2/3pp3/Q3P3/2P2N2/PP1P1PPP/RNB1KB1R w
C44	Ponziani Opening: Ponziani Countergambit		r1bqkbnr/pppp2pp/2n5/4pp2/4P3/2P2N2/PP1P1PPP/RNBQKB1R w
C44	Ponziani Opening: Ponziani Countergambit, Schmidt Attack		r1bqkbnr/ppp3pp/2np4/3Ppp2/4P3/2P2N2/PP3PPP/RNBQKB1R b
C44	Ponziani Opening: Romanishin Variation		r1bqk1nr/ppppbppp/2n5/4p3/4P3/2P2N2/PP1P1PPP/RNBQKB1R w
C44	Ponziani Opening: Réti Variation		r1bqkb1r/ppppnppp/2n5/4p3/4P3/2P2N2/PP1P1PPP/RNBQKB1R w
C44	Ponziani Opening: Spanish Variation		r1bqkbnr/ppp2ppp/2n5/1B1pp3/4P3/2P2N2/PP1P1PPP/RNBQK2R b
C44	Ponziani Opening: Spanish Variation, Harrwitz Attack, Nikitin Gambit		r1b1kbnr/ppp2ppp/2n5/1B1qN3/Q3p3/2P5/PP1P1PPP/RNB1K2R b
C44	Ponziani Opening: Steinitz Variation		r1bqkbnr/ppp3pp/2n2p2/3pp3/Q3P3/2P2N2/PP1P1PPP/RNB1KB1R w
C44	Ponziani Opening: Vukovic Gambit		r1bqk2r/pppp1ppp/2n5/2bPp3/4n3/2P2N2/PP3PPP/RNBQKB1R w
C44	Scotch Gambit		r1bqk1nr/pppp1ppp/2n5/8/1bB1P3/2P2N2/P4PPP/RNBQK2R b
C44	Scotch Game		r1bqkbnr/pppp1ppp/2n5/4p3/3PP3/5N2/PPP2PPP/RNBQKB1R b
C44	Scotch Game: Benima Defense		r1bqk1nr/ppppbppp/2n5/8/2BpP3/5N2/PPP2PPP/RNBQK2R w
C44	Scotch Game: Cochrane Variation		r1bqk1nr/pppp1ppp/2n5/b3P3/2B5/2P2N2/P4PPP/RNBQK2R b
C44	Scotch Game: Göring Gambit		r1bqkbnr/pppp1ppp/2n5/8/3pP3/2P2N2/PP3PPP/RNBQKB1R b
C44	Scotch Game: Göring Gambit, Bardeleben Variation		r1bqk2r/pppp1ppp/2n2n2/8/1bB1P3/2N2N2/PP3PPP/R1BQK2R w
C44	Scotch Game: Göring Gambit, Double Pawn Sacrifice		r1bqkbnr/pppp1ppp/2n5/8/2B1P3/2p2N2/PP3PPP/RNBQK2R b
C44	Scotch Game: Göring Gambit, Main Line		r1bqk1nr/pppp1ppp/2n5/8/1b2P3/2N2N2/PP3PPP/R1BQKB1R w
C44	Scotch Game: Hanneken Variation		8/6p1/3k1p2/6p1/p7/1bR4P/5PP1/5K2 w
C44	Scotch Game: Lolli Variation		r1bqkbnr/pppp1ppp/8/4p3/3nP3/5N2/PPP2PPP/RNBQKB1R w
C44	Scotch Game: Napoleon Gambit		r1bqkbnr/pppp1ppp/8/8/2BpP3/8/PPP2PPP/RNBQK2R b
C44	Scotch Game: Relfsson Gambit		r1bqkbnr/pppp1ppp/2n5/1B6/3pP3/5N2/PPP2PPP/RNBQK2R b
C44	Scotch Game: Scotch Gambit		r1bqkbnr/pppp1ppp/2n5/8/2BpP3/5N2/PPP2PPP/RNBQK2R b
C44	Scotch Game: Scotch Gambit, Cochrane-Anderssen Variation		r2qk1nr/ppp2ppp/2np4/2b5/2BpP1b1/2P2N2/PP3PPP/RNBQ1RK1 w
C44	Scotch Game: Scotch Gambit, Dubois Réti Defense		r1bqkb1r/pppp1ppp/2n2n2/8/2BpP3/5N2/PPP2PPP/RNBQK2R w
C44	Scotch Game: Scotch Gambit, Göring Gambit Declined		r1bqkbnr/ppp2ppp/2n5/3p4/3pP3/2P2N2/PP3PPP/RNBQKB1R w
C44	Scotch Game: Scotch Gambit, London Defense		r1bqk1nr/pppp1ppp/2n5/8/1bBpP3/5N2/PPP2PPP/RNBQK2R w
C44	Scotch Game: Scotch Gambit, Sarratt Variation		r1bqk1nr/pppp1ppp/2n5/2b3N1/2BpP3/8/PPP2PPP/RNBQK2R b
C44	Scotch Game: Vitzthum Attack		r1bqk2r/pppp1ppp/2n4n/2b3NQ/2BpP3/8/PPP2PPP/RNB1K2R b
C44	Tayler Opening		r1bqkb1r/pppp1ppp/2n2n2/4p3/3PP3/5N2/PPP1BPPP/RNBQK2R b
C45	Scotch Game		r1bqkbnr/pppp1ppp/2n5/8/3NP3/8/PPP2PPP/RNBQKB1R b
C45	Scotch Game #2		r1b1k1nr/pppp1ppp/2n5/1N6/1b2P2q/8/PPPB1PPP/RN1QKB1R b
C45	Scotch Game #3		r1bqkbnr/pppp1ppp/2n5/8/3pP3/5N2/PPP2PPP/RNBQKB1R w
C45	Scotch Game: Alekhine Gambit		r1bqkb1r/pppp1ppp/2n2n2/4P3/3N4/8/PPP2PPP/RNBQKB1R b
C45	Scotch Game: Blumenfeld Attack		r1b1k1nr/pppp1ppp/2n2q2/1Nb5/4P3/4B3/PPP2PPP/RN1QKB1R b
C45	Scotch Game: Braune Variation		r1b1kbnr/pppp1ppp/2n5/8/3NP2q/4B3/PPP2PPP/RN1QKB1R b
C45	Scotch Game: Classical Variation		r1bqk1nr/pppp1ppp/2n5/2b5/3NP3/8/PPP2PPP/RNBQKB1R w
C45	Scotch Game: Classical Variation, Blackburne Attack		r1b1k2r/ppppnppp/2n2q2/2b5/3NP3/2P1B3/PP1Q1PPP/RN2KB1R b
C45	Scotch Game: Classical Variation, Intermezzo Variation		r1b1k1nr/pppp1ppp/2N2q2/2b5/4P3/8/PPP2PPP/RNBQKB1R w
C45	Scotch Game: Classical Variation, Millennium Variation		r1b1k1nr/pppp1ppp/2n3q1/2b5/3NP3/2P1B3/PP3PPP/RN1QKB1R w
C45	Scotch Game: Ghulam-Kassim Variation		r1bqkbnr/ppp2ppp/3p4/8/3QP3/3B4/PPP2PPP/RNB1K2R b
C45	Scotch Game: Haxo Gambit		r1bqk1nr/pppp1ppp/2n5/2b5/2BpP3/5N2/PPP2PPP/RNBQK2R w
C45	Scotch Game: Horwitz Attack		r1b1kbnr/pppp1ppp/2n5/1N6/4P2q/8/PPP2PPP/RNBQKB1R b
C45	Scotch Game: Malaniuk Variation		r1bqk1nr/pppp1ppp/2n5/8/1b1NP3/8/PPP2PPP/RNBQKB1R w
C45	Scotch Game: Meitner Variation		r1b1k2r/ppppnppp/2n2q2/2b5/4P3/2P1B3/PPN2PPP/RN1QKB1R b
C45	Scotch Game: Mieses Variation		r1bqkb1r/p1pp1ppp/2p2n2/4P3/8/8/PPP2PPP/RNBQKB1R b
C45	Scotch Game: Modern Defense		r1b1k1nr/pppp1ppp/2n5/8/1b1NP2q/2N5/PPP2PPP/R1BQKB1R w
C45	Scotch Game: Paulsen Attack		r1b1k2r/ppppnppp/2n2q2/1Bb5/3NP3/2P1B3/PP3PPP/RN1QK2R b
C45	Scotch Game: Paulsen Variation		r1b1kbnr/pppp1ppp/2n5/5N2/4q3/8/PPP2PPP/RNBQKB1R w
C45	Scotch Game: Potter Variation		r1bqk1nr/pppp1ppp/2n5/2b5/4P3/1N6/PPP2PPP/RNBQKB1R b
C45	Scotch Game: Romanishin Variation		r1bqk1nr/pppp1ppp/2n5/8/1b2P3/1N6/PPP2PPP/RNBQKB1R w
C45	Scotch Game: Schmidt Variation		r1bqkb1r/pppp1ppp/2n2n2/8/3NP3/8/PPP2PPP/RNBQKB1R w
C45	Scotch Game: Scotch Gambit, Advance Variation		r1bqkb1r/pppp1ppp/2n2n2/4P3/2Bp4/5N2/PPP2PPP/RNBQK2R b
C45	Scotch Game: Scotch Gambit, Kingside Variation		r1bqkb1r/pppp1ppp/2n5/4P3/2Bp2n1/5N2/PPP2PPP/RNBQK2R w
C45	Scotch Game: Steinitz Variation		r1b1kbnr/pppp1ppp/2n5/8/3NP2q/8/PPP2PPP/RNBQKB1R w
C45	Scotch Game: Steinitz Variation #2		r1b1kbnr/pppp1ppp/2n5/8/3NP2q/2N5/PPP2PPP/R1BQKB1R b
C45	Scotch Game: Tartakower Variation		r1bqkb1r/p1pp1ppp/2p2n2/8/4P3/8/PPPN1PPP/R1BQKB1R b
C46	Four Knights Game		r1bqkb1r/pppp1ppp/2n2n2/4p3/4P3/2N2N2/PPPP1PPP/R1BQKB1R w
C46	Four Knights Game: Gunsberg Variation		r1bqkb1r/pppp1ppp/2n2n2/4p3/4P3/P1N2N2/1PPP1PPP/R1BQKB1R b
C46	Four Knights Game: Halloween Gambit		r1bqkb1r/pppp1ppp/2n2n2/4N3/4P3/2N5/PPPP1PPP/R1BQKB1R b
C46	Four Knights Game: Italian Variation		r1bqkb1r/pppp1ppp/2n2n2/4p3/2B1P3/2N2N2/PPPP1PPP/R1BQK2R b
C46	Three Knights Opening		r1bqkbnr/pppp1ppp/2n5/4p3/4P3/2N2N2/PPPP1PPP/R1BQKB1R b
C46	Three Knights Opening #2		r1bqk1nr/pppp1ppp/2n5/4p3/1b2P3/2N2N2/PPPP1PPP/R1BQKB1R w
C46	Three Knights Opening: Schlechter Variation		r1bqk2r/pppp1ppp/2n2n2/3Np3/1b2P3/5N2/PPPP1PPP/R1BQKB1R w
C46	Three Knights Opening: Steinitz Defense		r1bqkbnr/pppp1p1p/2n3p1/4p3/4P3/2N2N2/PPPP1PPP/R1BQKB1R w
C46	Three Knights Opening: Steinitz-Rosenthal Variation		r1bqkbnr/pppp1p1p/2n3p1/3N4/3pP3/5N2/PPP2PPP/R1BQKB1R b
C46	Three Knights Opening: Winawer Defense		r1bqkbnr/pppp2pp/2n5/4pp2/4P3/2N2N2/PPPP1PPP/R1BQKB1R w
C47	Four Knights Game: Italian Variation, Noa Gambit		r1bqkb1r/pppp1Bpp/2n5/4p3/4n3/2N2N2/PPPP1PPP/R1BQK2R b
C47	Four Knights Game: Scotch Variation		r1bqkb1r/pppp1ppp/2n2n2/4p3/3PP3/2N2N2/PPP2PPP/R1BQKB1R b
C47	Four Knights Game: Scotch Variation Accepted		r1bqkb1r/pppp1ppp/2n2n2/8/3pP3/2N2N2/PPP2PPP/R1BQKB1R w
C47	Four Knights Game: Scotch Variation, Belgrade Gambit		r1bqkb1r/pppp1ppp/2n2n2/3N4/3pP3/5N2/PPP2PPP/R1BQKB1R b
C47	Four Knights Game: Scotch Variation, Krause Gambit		r1bqk2r/pppp1ppp/2n2n2/4N3/1b1PP3/2N5/PPP2PPP/R1BQKB1R b
C47	Four Knights Game: Scotch Variation, Krause Gambit, Leonhardt Defense		r1b1k2r/ppppqppp/2n2n2/4N3/1b1PP3/2N5/PPP2PPP/R1BQKB1R w
C47	Four Knights Game: Scotch Variation, Oxford Gambit		r1bqk2r/pppp1ppp/5n2/3Pp3/1b1nP3/2N2N2/PPP2PPP/R1BQKB1R w
C47	Four Knights Game: Scotch Variation, Schmid Defense		r1bqkb1r/pppp1ppp/2n5/8/3Nn3/2N5/PPP2PPP/R1BQKB1R w
C47	Four Knights: Scotch Variation, Belgrade Gambit, Modern Defense		r1bqkb1r/pppp2pp/2n5/3N1p2/3pn3/5N2/PPP1QPPP/R1B1KB1R w
C48	Four Knights Game: Marshall Variation		r1bq1rk1/pppp1ppp/5n2/1Bb1N3/3nP3/2N5/PPPP1PPP/R1BQ1RK1 w
C48	Four Knights Game: Ranken Variation		r1bqkb1r/1ppp1ppp/p1B2n2/4p3/4P3/2N2N2/PPPP1PPP/R1BQK2R b
C48	Four Knights Game: Rubinstein Countergambit, 5.Be2		r1bqkb1r/pppp1ppp/5n2/4p3/3nP3/2N2N2/PPPPBPPP/R1BQK2R b
C48	Four Knights Game: Rubinstein Countergambit, Henneberger Variation		r1bqkb1r/pppp1ppp/5n2/1B2p3/3nP3/2N2N2/PPPP1PPP/R1BQ1RK1 b
C48	Four Knights Game: Spanish Variation		r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/2N2N2/PPPP1PPP/R1BQK2R b
C48	Four Knights Game: Spanish Variation, Classical Variation		r1bqk2r/pppp1ppp/2n2n2/1Bb1p3/4P3/2N2N2/PPPP1PPP/R1BQK2R w
C48	Four Knights Game: Spanish Variation, Classical Variation, Marshall Gambit		r1bq1rk1/pppp1ppp/5n2/2b1N3/B2nP3/2N5/PPPP1PPP/R1BQK2R w
C48	Four Knights Game: Spanish Variation, Rubinstein Variation		r1bqkb1r/pppp1ppp/5n2/1B2p3/3nP3/2N2N2/PPPP1PPP/R1BQK2R w
C48	Four Knights Game: Spanish Variation, Rubinstein Variation #2		r1b1kb1r/ppppqppp/5n2/1B2N3/3nPP2/2N5/PPPP2PP/R1BQK2R b
C48	Four Knights Game: Spanish Variation, Rubinstein Variation Accepted		r1bqkb1r/pppp1ppp/5n2/1B2p3/3NP3/2N5/PPPP1PPP/R1BQK2R b
C48	Four Knights Game: Spielmann Variation		r3kb1r/1pp2ppp/p1p1b3/3q4/3PN3/8/PPP2PPP/R1BQR1K1 w
C49	Four Knights Game: Double Spanish		r1bqk2r/pppp1ppp/2n2n2/1B2p3/1b2P3/2N2N2/PPPP1PPP/R1BQK2R w
C49	Four Knights Game: Double Spanish, with 5.O-O		r1bq1rk1/pppp1ppp/2n2n2/1B2p3/1b2P3/2NP1N2/PPP2PPP/R1BQ1RK1 b
C49	Four Knights Game: Gunsberg Counterattack		r1bq1rk1/pppp1ppp/2n5/1B1P4/1b2p3/5N2/PPPP1PPP/R1BQ1RK1 w
C49	Four Knights Game: Janowski Variation		r1bq1rk1/ppp2ppp/2np1n2/1B2p3/4P3/2PP1N2/P1P2PPP/R1BQR1K1 b
C49	Four Knights Game: Nimzowitsch (Paulsen)		r1bq1rk1/pppp1ppp/2B2n2/4p3/1b2P3/2N2N2/PPPP1PPP/R1BQ1RK1 b
C49	Four Knights Game: Spanish Variation		r1bq1rk1/pppp1ppp/2n2n2/1B2p3/4P3/2PP1N2/P1P2PPP/R1BQ1RK1 b
C49	Four Knights Game: Spanish Variation, Symmetrical Variation		rr4k1/5pp1/3p1n2/4p3/1R2PpP1/PB3Q1P/5P2/3q2K1 w
C49	Four Knights Game: Spanish Variation, Symmetrical Variation #2		r1bq1rk1/ppp1nppp/3p1n2/1B2p1B1/1b2P3/2NP1N2/PPP2PPP/R2Q1RK1 w
C49	Four Knights Game: Spanish Variation, Symmetrical Variation #3		r1bq1rk1/ppp2ppp/2np1n2/1B2p3/1b2P3/2NP1N2/PPP2PPP/R1BQ1RK1 w
C49	Four Knights Game: Spanish Variation, Symmetrical Variation #4		r1bq1rk1/ppp2ppp/2np1n2/1B2p3/1b2P3/3P1N2/PPP1NPPP/R1BQ1RK1 b
C49	Four Knights Game: Symmetrical, Metger Unpin		r1b2rk1/ppp1qppp/2np1n2/1B2p1B1/4P3/2PP1N2/P1P2PPP/R2Q1RK1 w
C49	Four Knights Game: Symmetrical, Tarrasch Variation		r2q1rk1/ppp2ppp/2npbn2/1B2p1B1/1b2P3/2NP1N2/PPP2PPP/R2Q1RK1 w
C50	Four Knights Game: Italian Variation		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2B1P3/2N2N2/PPPP1PPP/R1BQK2R w
C50	Giuoco Piano		r1bqk1nr/pppp1ppp/2n5/2b1p3/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C50	Italian Game		r1bqkbnr/pppp1ppp/2n5/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R b
C50	Italian Game: Giuoco Pianissimo		r1bqk1nr/pppp1ppp/2n5/2b1p3/2B1P3/3P1N2/PPP2PPP/RNBQK2R b
C50	Italian Game: Giuoco Pianissimo, Canal Variation		r1bqk2r/ppp2ppp/2np1n2/2b1p1B1/2B1P3/2NP1N2/PPP2PPP/R2QK2R b
C50	Italian Game: Giuoco Pianissimo, Dubois Variation		rr6/p1b1qp1k/2ppbn2/4p1B1/RP2P1PP/1PN2P2/2K5/3Q3R b
C50	Italian Game: Giuoco Pianissimo, Italian Four Knights Variation		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2B1P3/2NP1N2/PPP2PPP/R1BQK2R b
C50	Italian Game: Giuoco Pianissimo, Lucchini Gambit		r1bqk1nr/pppp2pp/2n5/2b1pp2/2B1P3/3P1N2/PPP2PPP/RNBQK2R w
C50	Italian Game: Giuoco Pianissimo, Normal		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2B1P3/3P1N2/PPP2PPP/RNBQK2R w
C50	Italian Game: Hungarian Defense		r1bqk1nr/ppppbppp/2n5/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C50	Italian Game: Hungarian Defense, Tartakower Variation		r1bqk2r/ppppbppp/2n5/4P3/2Bpn3/2P2N2/PP3PPP/RNBQK2R w
C50	Italian Game: Rosentreter Gambit		r1bqk1nr/pppp1ppp/2n5/2b1p3/2BPP3/5N2/PPP2PPP/RNBQK2R b
C50	Italian Game: Rousseau Gambit		r1bqkbnr/pppp2pp/2n5/4pp2/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C50	Italian Game: Schilling-Kostic Gambit		r1bqkbnr/pppp1ppp/8/4p3/2BnP3/5N2/PPPP1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit		r1bqk1nr/pppp1ppp/2n5/2b1p3/1PB1P3/5N2/P1PP1PPP/RNBQK2R b
C51	Italian Game: Evans Gambit #2		r2qk1nr/ppp2ppp/1bnp4/8/2BPP1b1/2N2N2/P4PPP/R1BQ1RK1 w
C51	Italian Game: Evans Gambit Accepted		r1bqk1nr/pppp1ppp/2n5/4p3/1bB1P3/5N2/P1PP1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit Declined		r1bqk1nr/pppp1ppp/1bn5/4p3/1PB1P3/5N2/P1PP1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit Declined, 5.a4		r1bqk1nr/pppp1ppp/1bn5/4p3/PPB1P3/5N2/2PP1PPP/RNBQK2R b
C51	Italian Game: Evans Gambit Declined, Cordel Variation		r1bqk1nr/pppp1ppp/1bn5/4p3/1PB1P3/5N2/PBPP1PPP/RN1QK2R b
C51	Italian Game: Evans Gambit Declined, Lange Variation		r1bqk2r/pppp1ppp/1b5n/nP2N3/2B1P3/8/P1PP1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit Declined, Showalter Variation		r1bqk1nr/1ppp1ppp/pbn5/4p3/PPB1P3/2N2N2/2PP1PPP/R1BQK2R b
C51	Italian Game: Evans Gambit, Anderssen Variation		r1bqk1nr/ppppbppp/2n5/4p3/2B1P3/2P2N2/P2P1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit, Anderssen Variation, Cordel Line		r1bqk1nr/ppppbppp/8/n3p3/2BPP3/2P2N2/P4PPP/RNBQK2R w
C51	Italian Game: Evans Gambit, Bronstein Defense		r1bqk1nr/ppp2ppp/2np4/b3p3/2BPP3/2P2N2/P4PPP/RNBQK2R w
C51	Italian Game: Evans Gambit, Fontaine Countergambit		r1bqk1nr/p1pp1ppp/2n5/1pb1p3/1PB1P3/5N2/P1PP1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit, Harding Variation		r1bqk1nr/pppp1ppp/2n5/8/1bBPP3/5N2/P2B1PPP/RN1QK2R b
C51	Italian Game: Evans Gambit, Hein Countergambit		r1bqk1nr/ppp2ppp/2n5/2bpp3/1PB1P3/5N2/P1PP1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit, Mayet Defense		r1bqkbnr/pppp1ppp/2n5/4p3/2B1P3/2P2N2/P2P1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit, McDonnell Defense		r1bqk1nr/pppp1ppp/2n5/2b1p3/2B1P3/2P2N2/P2P1PPP/RNBQK2R w
C51	Italian Game: Evans Gambit, McDonnell Defense, Main Line		r1bqk1nr/ppp2ppp/1bnp4/8/2BPP3/5N2/P4PPP/RNBQ1RK1 w
C51	Italian Game: Evans Gambit, Morphy Attack		r1bqk1nr/ppp2ppp/1bnp4/8/2BPP3/2N2N2/P4PPP/R1BQ1RK1 b
C51	Italian Game: Evans Gambit, Stone-Ware Variation		r1bqk1nr/pppp1ppp/2nb4/4p3/2B1P3/2P2N2/P2P1PPP/RNBQK2R w
C51	Italian Game: Jerome Gambit		r1bqk1nr/pppp1Bpp/2n5/2b1p3/4P3/5N2/PPPP1PPP/RNBQK2R b
C52	Italian Game: Evans Gambit		r1bqk1nr/ppp2ppp/2np4/b3p3/2B1P3/2P2N2/P2P1PPP/RNBQ1RK1 w
C52	Italian Game: Evans Gambit, Alapin-Steinitz Variation		r2qk1nr/ppp2ppp/2np4/b3p3/2BPP1b1/2P2N2/P4PPP/RNBQ1RK1 w
C52	Italian Game: Evans Gambit, Anderssen Defense		r1bqk2r/pppp1ppp/2n2n2/b7/2BpP3/2P2N2/P4PPP/RNBQ1RK1 w
C52	Italian Game: Evans Gambit, Compromised Defense		r1bqk1nr/pppp1ppp/2n5/b7/2B1P3/2p2N2/P4PPP/RNBQ1RK1 w
C52	Italian Game: Evans Gambit, Compromised Defense, Main Line		r1b1k2r/ppppnppp/2n3q1/b3P3/2B5/BQN2N2/P4PPP/R4RK1 b
C52	Italian Game: Evans Gambit, Dufresne Defense		r1bqk1nr/pppp1ppp/2n5/b7/2B1P3/2Pp1N2/P4PPP/RNBQ1RK1 w
C52	Italian Game: Evans Gambit, Lasker Defense		r1bqk1nr/ppp2ppp/1bnp4/4p3/2BPP3/2P2N2/P4PPP/RNBQ1RK1 w
C52	Italian Game: Evans Gambit, Leonhardt Countergambit		r1bqk1nr/p1pp1ppp/2n5/bp2p3/2BPP3/2P2N2/P4PPP/RNBQK2R w
C52	Italian Game: Evans Gambit, Main Line		r1bqk1nr/pppp1ppp/2n5/b3p3/2B1P3/2P2N2/P2P1PPP/RNBQK2R w
C52	Italian Game: Evans Gambit, Mieses Defense		r1bqk2r/ppppnppp/2n5/b7/2BpP3/2P2N2/P4PPP/RNBQ1RK1 w
C52	Italian Game: Evans Gambit, Pierce Defense		r1bqk1nr/pppp1ppp/2n5/b7/2BpP3/2P2N2/P4PPP/RNBQK2R w
C52	Italian Game: Evans Gambit, Richardson Attack		r1bq1rk1/pppp1ppp/2n2n2/b3N3/2BPP3/2P5/P4PPP/RNBQ1RK1 b
C52	Italian Game: Evans Gambit, Sanders-Alapin Variation		r2qk1nr/pppb1ppp/2np4/b3p3/2BPP3/2P2N2/P4PPP/RNBQ1RK1 w
C52	Italian Game: Evans Gambit, Slow Variation		r1bqk1nr/pppp1ppp/2n5/b3p3/2B1P3/2P2N2/P2P1PPP/RNBQ1RK1 b
C52	Italian Game: Evans Gambit, Sokolsky Variation		4r1k1/pppq2pp/1b1p4/3Pp2B/7P/2P3R1/P2NQrP1/n4K2 w
C52	Italian Game: Evans Gambit, Tartakower Attack		r1bqk1nr/ppp2ppp/2np4/b3p3/2BPP3/1QP2N2/P4PPP/RNB1K2R b
C52	Italian Game: Evans Gambit, Waller Attack		r1bqk1nr/ppp2ppp/2np4/b7/2BpP3/1QP2N2/P4PPP/RNB2RK1 b
C53	Italian Game: Bird's Attack		r1bqk2r/pppp1ppp/2n2n2/2b1p3/1PB1P3/2P2N2/P2P1PPP/RNBQK2R b
C53	Italian Game: Classical Variation		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2B1P3/2P2N2/PP1P1PPP/RNBQK2R w
C53	Italian Game: Classical Variation #2		r1bqk1nr/pppp1ppp/2n5/2b1p3/2B1P3/2P2N2/PP1P1PPP/RNBQK2R b
C53	Italian Game: Classical Variation, Albin Gambit		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2B1P3/2P2N2/PP1P1PPP/RNBQ1RK1 b
C53	Italian Game: Classical Variation, Alexandre Gambit		r1bqk1nr/pppp2pp/2n5/2b1pp2/2B1P3/2P2N2/PP1P1PPP/RNBQK2R w
C53	Italian Game: Classical Variation, Center Attack		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2BPP3/2P2N2/PP3PPP/RNBQK2R b
C53	Italian Game: Classical Variation, Center Holding Variation		r1b1k1nr/ppppqppp/1bn5/4p3/2BPP3/2P2N2/PP3PPP/RNBQK2R w
C53	Italian Game: Classical Variation, Closed Variation		r1b1k1nr/ppppqppp/2n5/2b1p3/2B1P3/2P2N2/PP1P1PPP/RNBQK2R w
C53	Italian Game: Classical Variation, Giuoco Pianissimo		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2B1P3/2PP1N2/PP3PPP/RNBQK2R b
C53	Italian Game: Classical Variation, Giuoco Pianissimo, Main Line		r1bq1rk1/bpp2ppp/p1np1n2/4p3/4P3/1BPP1N1P/PP3PP1/RNBQR1K1 b
C53	Italian Game: Classical Variation, Greco Gambit		r1bqk2r/ppp2ppp/2n2n2/2bpP3/2Bp4/2P2N2/PP3PPP/RNBQK2R w
C53	Italian Game: Classical Variation, La Bourdonnais Variation		r1bqk1nr/ppp2ppp/1bnp4/8/2BPP3/5N2/PP3PPP/RNBQK2R w
C53	Italian Game: Classical Variation, Tarrasch Variation		r1b1k2r/1pp1qppp/pbnp1n2/4p3/P1BPP3/2P2N1P/1P3PP1/RNBQR1K1 b
C53	Italian Game: Giuoco Piano, Eisinger Variation		rnb1k1nr/pp1pqppp/1b1p4/4p3/2B1P3/2P2N2/PP3PPP/RNBQK2R w
C53	Italian Game: Giuoco Piano, Mestel Variation		r1b1k1nr/ppppqppp/1bn5/4p1B1/2BPP3/2P2N2/PP3PPP/RN1QK2R b
C54	Italian Game: Classical Variation, Greco Gambit, Anderssen Variation		r1bqk2r/ppp2ppp/2n5/1B1pP3/1b1Pn3/5N2/PP3PPP/RNBQK2R w
C54	Italian Game: Classical Variation, Greco Gambit, Greco Variation		r1bqk2r/pppp1ppp/2n5/8/1bBP4/2n2N2/PP3PPP/R1BQ1RK1 w
C54	Italian Game: Classical Variation, Greco Gambit, Main Line		r1bqk2r/pppp1ppp/2n5/8/2BPn3/2b2N2/PP3PPP/R1BQ1RK1 w
C54	Italian Game: Classical Variation, Greco Gambit, Moeller-Bayonet Attack		r1bqk2r/ppp1nppp/3p1b2/3P4/2B1R1P1/5N2/PP3P1P/R1BQ2K1 b
C54	Italian Game: Classical Variation, Greco Gambit, Moeller-Therkatz Attack		r1bqk2r/pppp1ppp/2n5/3P4/2B1n3/2b2N2/PP3PPP/R1BQ1RK1 b
C54	Italian Game: Classical Variation, Greco Gambit, Traditional Line		r1bqk2r/pppp1ppp/2n2n2/2b5/2BPP3/5N2/PP3PPP/RNBQK2R b
C54	Italian Game: Giuoco Piano, Aitken Variation		r1bqk2r/pppp1ppp/2n5/8/2BP4/B1b2N2/P4PPP/R2Q1RK1 b
C54	Italian Game: Giuoco Piano, Greco's Attack		r1bqk2r/pppp1ppp/2n2n2/8/1bBPP3/2N2N2/PP3PPP/R1BQK2R b
C54	Italian Game: Giuoco Piano, Steinitz Variation		r1bqk2r/ppp2ppp/2n5/3p4/2BPn3/B1P2N2/P4PPP/R2Q1RK1 b
C54	Italian Game: Giuoco Piano, Therkatz-Herzog Variation		r1bq1rk1/ppp1nppN/3p4/3P4/2B1R3/8/PP3PPP/R2Q2K1 b
C55	Italian Game: Anti-Fried Liver Defense		r1bqkbnr/pppp1pp1/2n4p/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C55	Italian Game: Deutz Gambit		r1bqk2r/pppp1ppp/2n2n2/2b1p3/2BPP3/5N2/PPP2PPP/RNBQ1RK1 b
C55	Italian Game: Giuoco Piano		r1bqk2r/ppp2ppp/3p1n2/4p1B1/2BnP3/8/PPP2PPP/RN1Q1RK1 w
C55	Italian Game: Giuoco Piano, Rosentreter Variation		r1bqk2r/pppp1p2/5n1p/4p1p1/2BnPP1B/8/PPP3PP/RN1Q1RK1 b
C55	Italian Game: Scotch Gambit		r1bqkb1r/pppp1ppp/2n2n2/8/2BpP3/5N2/PPP2PPP/RNBQ1RK1 b
C55	Italian Game: Scotch Gambit Declined		r1bqkb1r/ppp2ppp/2np1n2/4p3/2BPP3/5N2/PPP2PPP/RNBQK2R w
C55	Italian Game: Scotch Gambit Declined #2		r1bqk2r/pppp1ppp/2nb1n2/4p3/2BPP3/5N2/PPP2PPP/RNBQK2R w
C55	Italian Game: Scotch Gambit, Janowski Defense		r1bqkb1r/ppp2ppp/2np1n2/8/2BpP3/5N2/PPP2PPP/RNBQ1RK1 w
C55	Italian Game: Scotch Gambit, Max Lange Attack		r1bqk2r/pppp1ppp/2n2n2/2b5/2BpP3/5N2/PPP2PPP/RNBQ1RK1 w
C55	Italian Game: Scotch Gambit, Max Lange Attack, Long Variation		r3k2r/ppp2ppp/2n1bP2/2b2qN1/2ppN3/8/PPP2PPP/R1BQR1K1 b
C55	Italian Game: Scotch Gambit, Walbrodt-Baird Gambit		r1bqk2r/pppp1ppp/2n2n2/2b5/2BpP3/2P2N2/PP3PPP/RNBQ1RK1 b
C55	Italian Game: Scotch Gambit, de Riviere Defense		r1bqk2r/ppppbppp/2n2n2/8/2BpP3/5N2/PPP2PPP/RNBQ1RK1 w
C55	Italian Game: Two Knights Defense		r1bqkb1r/pppp1ppp/2n2n2/4p3/2B1P3/5N2/PPPP1PPP/RNBQK2R w
C55	Italian Game: Two Knights Defense #2		r1bqkb1r/pppp1ppp/2n5/4p3/2BPn3/5N2/PPP2PPP/RNBQK2R w
C55	Italian Game: Two Knights Defense, Max Lange Attack		r1bqk2r/pppp1ppp/2n2n2/2b1P3/2Bp4/5N2/PPP2PPP/RNBQ1RK1 b
C55	Italian Game: Two Knights Defense, Max Lange Attack, Berger Variation		2kr3r/ppp2ppp/1bn2P2/5qN1/2ppN3/8/PPP4P/R1BQR1K1 w
C55	Italian Game: Two Knights Defense, Max Lange Attack, Krause Variation		r1bqk2r/pppp1ppp/2n5/2b1P3/2Bp2n1/2P2N2/PP3PPP/RNBQ1RK1 b
C55	Italian Game: Two Knights Defense, Max Lange Attack, Loman Defense		r2qk2r/ppp2p1p/2n1bPp1/2b3N1/2pp4/8/PPP2PPP/RNBQR1K1 w
C55	Italian Game: Two Knights Defense, Modern Bishop's Opening		r1bqkb1r/pppp1ppp/2n2n2/4p3/2B1P3/3P1N2/PPP2PPP/RNBQK2R b
C55	Italian Game: Two Knights Defense, Open Variation		r1bqkb1r/pppp1ppp/2n2n2/4p3/2BPP3/5N2/PPP2PPP/RNBQK2R b
C55	Italian Game: Two Knights Defense, Perreux Variation		r1bqkb1r/pppp1ppp/2n2n2/6N1/2BpP3/8/PPP2PPP/RNBQK2R b
C55	Italian Game: Two Knights Defense, de Riviere Gambit		r1bqkb1r/pppp1ppp/2n2n2/4p3/2B1P3/2P2N2/PP1P1PPP/RNBQK2R b
C56	Italian Game: Scotch Gambit, Anderssen Attack		r1b1kb1r/ppp2ppp/2n5/3q4/3pn3/2N2N2/PPP2PPP/R1BQR1K1 b
C56	Italian Game: Scotch Gambit, Anderssen Attack, Main Line		r3kb1r/ppp2ppp/2n1b3/3q2B1/3pN3/5N2/PPP2PPP/R2QR1K1 b
C56	Italian Game: Scotch Gambit, Canal Variation		r1bqkb1r/ppp2ppp/2n5/3p4/2Bpn3/2N2N2/PPP2PPP/R1BQR1K1 b
C56	Italian Game: Scotch Gambit, Double Gambit Accepted		r1bqkb1r/pppp1ppp/2n5/8/2Bpn3/5N2/PPP2PPP/RNBQ1RK1 w
C56	Italian Game: Scotch Gambit, Max Lange Attack Accepted		r2qk1r1/ppp2pPp/2n1b3/2b5/2pp4/5N2/PPP2PPP/RNBQR1K1 w
C56	Italian Game: Scotch Gambit, Max Lange Attack, Spielmann Defense		r1bqk2r/pppp1ppp/2n5/2b1P3/2Bp2n1/5N2/PPP2PPP/RNBQ1RK1 w
C56	Italian Game: Scotch Gambit, Nakhmanson Gambit		r1bqkb1r/pppp1ppp/2n5/8/2Bpn3/2N2N2/PPP2PPP/R1BQ1RK1 b
C57	Italian Game: Two Knights Defense, Fried Liver Attack		r1bqkb1r/ppp2Npp/2n5/3np3/2B5/8/PPPP1PPP/RNBQK2R b
C57	Italian Game: Two Knights Defense, Fritz Variation		r1bqkb1r/ppp2ppp/5n2/3Pp1N1/2Bn4/8/PPPP1PPP/RNBQK2R w
C57	Italian Game: Two Knights Defense, Fritz, Gruber Variation		r1bqkb1r/p1p2ppp/8/1p1np3/3nN3/2P5/PP1P1PPP/RNBQKB1R b
C57	Italian Game: Two Knights Defense, Kloss Gambit		r1bqkb1r/ppp2ppp/5n2/3Pp1N1/1nB5/8/PPPP1PPP/RNBQK2R w
C57	Italian Game: Two Knights Defense, Knight Attack		r1bqkb1r/pppp1ppp/2n2n2/4p1N1/2B1P3/8/PPPP1PPP/RNBQK2R b
C57	Italian Game: Two Knights Defense, Knight Attack, Normal Variation		r1bqkb1r/ppp2ppp/2n2n2/3pp1N1/2B1P3/8/PPPP1PPP/RNBQK2R w
C57	Italian Game: Two Knights Defense, Lolli Attack		r1bqkb1r/ppp2ppp/2n5/3np1N1/2BP4/8/PPP2PPP/RNBQK2R b
C57	Italian Game: Two Knights Defense, Pincus Variation		r1bqk2r/ppp2ppp/2n5/3np1N1/1bBP4/8/PPP2PPP/RNBQK2R w
C57	Italian Game: Two Knights Defense, Polerio Defense		r1bqkb1r/ppp2ppp/5n2/n2Pp1N1/2B5/8/PPPP1PPP/RNBQK2R w
C57	Italian Game: Two Knights Defense, Ponziani-Steinitz Gambit		r1bqkb1r/pppp1ppp/2n5/4p1N1/2B1n3/8/PPPP1PPP/RNBQK2R w
C57	Italian Game: Two Knights Defense, Traxler Counterattack		r1bqk2r/pppp1ppp/2n2n2/2b1p1N1/2B1P3/8/PPPP1PPP/RNBQK2R w
C57	Italian Game: Two Knights Defense, Traxler Counterattack, Bishop Sacrifice Line		r1bqk2r/pppp1Bpp/2n2n2/2b1p1N1/4P3/8/PPPP1PPP/RNBQK2R b
C57	Italian Game: Two Knights Defense, Traxler Counterattack, Knight Sacrifice Line		r1bqk2r/pppp1Npp/2n2n2/2b1p3/2B1P3/8/PPPP1PPP/RNBQK2R b
C57	Italian Game: Two Knights Defense, Ulvestad Variation		r1bqkb1r/p1p2ppp/2n2n2/1p1Pp1N1/2B5/8/PPPP1PPP/RNBQK2R w
C57	Italian Game: Two Knights Defense, Ulvestad Variation, Kurkin Gambit		r1bq1b1r/p1p2kp1/2P2n1p/1p2p3/8/8/PPPP1PPP/RNBQKB1R b
C58	Italian Game: Two Knights Defense		r1bqkb1r/p4ppp/2p2n2/n3p1N1/8/8/PPPPBPPP/RNBQK2R b
C58	Italian Game: Two Knights Defense, Blackburne Variation		r1bqkb1r/p4ppp/5n2/np2p1N1/8/5Q2/PPPP1PPP/RNB1K2R w
C58	Italian Game: Two Knights Defense, Polerio Defense, Bishop Check Line		r1bqkb1r/ppp2ppp/5n2/nB1Pp1N1/8/8/PPPP1PPP/RNBQK2R b
C58	Italian Game: Two Knights Defense, Polerio Defense, Bogoljubov Variation		r1bqkb1r/p4ppp/2p2n2/nB2p1N1/8/5Q2/PPPP1PPP/RNB1K2R b
C58	Italian Game: Two Knights Defense, Polerio Defense, Kieseritzky Variation		r1bqkb1r/ppp2ppp/5n2/n2Pp1N1/2B5/3P4/PPP2PPP/RNBQK2R b
C58	Italian Game: Two Knights Defense, Polerio Defense, Yankovich Variation		r1bq1rk1/ppp2pp1/5n1p/2bP4/2P1p3/8/PPPNQPPP/RNB1K2R w
C59	Italian Game: Two Knights Defense, Polerio Defense, Göring Variation		r1b1kb1r/p1q2pp1/2p2n1p/n3N3/4p3/8/PPPPBPPP/RNBQK2R w
C59	Italian Game: Two Knights Defense, Polerio Defense, Suhle Defense		r1bqkb1r/p4pp1/2p2n1p/n3p1N1/8/8/PPPPBPPP/RNBQK2R w
C59	Italian Game: Two Knights Defense, Steinitz Variation		r1bqkb1r/p4pp1/2p2n1p/n3p3/8/7N/PPPPBPPP/RNBQK2R b
C60	Ruy Lopez		r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R b
C60	Ruy Lopez: Alapin Defense		r1bqk1nr/pppp1ppp/2n5/1B2p3/1b2P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Alapin Defense, Alapin Gambit		r1bqk1nr/ppp2ppp/2p5/b3p3/4P3/2P2N2/PP1P1PPP/RNBQK2R w
C60	Ruy Lopez: Brentano Gambit		r1bqkbnr/pppp1p1p/2n5/1B2p1p1/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Bulgarian Variation		r1bqkbnr/1ppp1ppp/2n5/pB2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Cozio Defense		r1bqkb1r/ppppnppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Cozio Defense, Paulsen Variation		r1bqkb1r/ppppnp1p/2n3p1/1B2p3/4P3/2N2N2/PPPP1PPP/R1BQK2R w
C60	Ruy Lopez: Cozio Defense, Tartakower Gambit		r2qr1k1/1bp4p/p4Np1/1p1P1p2/3Q4/8/PPP2PPP/3R1RK1 b
C60	Ruy Lopez: Fianchetto Defense		r1bqkbnr/pppp1p1p/2n3p1/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Lucena Variation		r1bqk1nr/ppppbppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Nuernberg Variation		r1bqkbnr/pppp2pp/2n2p2/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Pollock Defense		r1bqkbnr/pppp1ppp/8/nB2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Retreat Variation		rnbqkbnr/pppp1ppp/8/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Rotary-Albany Gambit		r1bqkbnr/p1pp1ppp/1pn5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Schliemann Defense, Jaenisch Gambit Accepted		r1bqkbnr/pppp2pp/2n5/1B2pP2/8/5N2/PPPP1PPP/RNBQK2R b
C60	Ruy Lopez: Spanish Countergambit		r1bqkbnr/ppp2ppp/2n5/1B1pp3/4P3/5N2/PPPP1PPP/RNBQK2R w
C60	Ruy Lopez: Spanish Countergambit, Harding Gambit		r1b1kbnr/ppp2ppp/2N5/1B1p2q1/4P3/8/PPPP1PPP/RNBQK2R b
C60	Ruy Lopez: Vinogradov Variation		r1b1kbnr/ppppqppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C61	Ruy Lopez: Bird Variation		r1bqkbnr/pppp1ppp/8/1B2p3/3nP3/5N2/PPPP1PPP/RNBQK2R w
C61	Ruy Lopez: Bird Variation, Paulsen Variation		r1bqkb1r/ppppnppp/8/1B6/3pP3/8/PPPP1PPP/RNBQ1RK1 w
C62	Ruy Lopez: Old Steinitz Defense: Semi-Duras Variation		r2qkbnr/pppb1ppp/2np4/1B2p3/2PPP3/5N2/PP3PPP/RNBQK2R b
C62	Ruy Lopez: Steinitz Defense		r1bqkbnr/ppp2ppp/2np4/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C62	Ruy Lopez: Steinitz Defense, Center Gambit		r1bqkbnr/ppp2ppp/2np4/1B6/3pP3/5N2/PPP2PPP/RNBQ1RK1 b
C62	Ruy Lopez: Steinitz Defense, Nimzowitsch Attack		r2qkb1r/pppb1ppp/2Bp1n2/4p3/3PP3/2N2N2/PPP2PPP/R1BQK2R b
C63	Ruy Lopez: Schliemann Defense		r1bqkbnr/pppp2pp/2n5/1B2pp2/4P3/5N2/PPPP1PPP/RNBQK2R w
C63	Ruy Lopez: Schliemann Defense, Classical Variation		r1b1kbnr/ppp3pp/2N5/1B4q1/4p3/8/PPPP1PPP/R1BQK2R w
C63	Ruy Lopez: Schliemann Defense, Dyckhoff Variation		r1bqkbnr/pppp2pp/2n5/1B2pp2/4P3/2N2N2/PPPP1PPP/R1BQK2R b
C63	Ruy Lopez: Schliemann Defense, Exchange Variation		r1bqkbnr/pppp2pp/2B5/4pp2/4P3/5N2/PPPP1PPP/RNBQK2R b
C63	Ruy Lopez: Schliemann Defense, Moehring Variation		r1b1kbnr/ppp3pp/2N5/1B1q4/4p3/8/PPPP1PPP/R1BQK2R w
C63	Ruy Lopez: Schliemann Defense, Schoenemann Attack		r1bqkbnr/pppp2pp/2n5/1B2pp2/3PP3/5N2/PPP2PPP/RNBQK2R b
C63	Ruy Lopez: Schliemann Defense, Tartakower Variation		r1bqkb1r/pppp2pp/2n2n2/1B2p3/4N3/5N2/PPPP1PPP/R1BQK2R w
C64	Ruy Lopez: Classical Defense: Benelux Variation		r1bq1rk1/pppp1ppp/1bn2n2/1B2p3/3PP3/2P2N2/PP3PPP/RNBQ1RK1 w
C64	Ruy Lopez: Classical Defense: Boden Variation		r1b1k1nr/ppppqppp/2n5/1Bb1p3/4P3/2P2N2/PP1P1PPP/RNBQK2R w
C64	Ruy Lopez: Classical Variation		r1bqk1nr/pppp1ppp/2n5/1Bb1p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C64	Ruy Lopez: Classical Variation, Central Variation		r1bqk1nr/pppp1ppp/2n5/1Bb1p3/4P3/2P2N2/PP1P1PPP/RNBQK2R b
C64	Ruy Lopez: Classical Variation, Charousek Variation		r1bqk1nr/pppp1ppp/1bn5/1B2p3/4P3/2P2N2/PP1P1PPP/RNBQK2R w
C64	Ruy Lopez: Classical Variation, Cordel Gambit		r1bqk1nr/pppp2pp/2n5/1Bb1pp2/4P3/2P2N2/PP1P1PPP/RNBQK2R w
C64	Ruy Lopez: Classical Variation, Konikowski Gambit		r1bqk1nr/ppp2ppp/2n5/1Bbpp3/4P3/2P2N2/PP1P1PPP/RNBQK2R w
C64	Ruy Lopez: Classical Variation, Spanish Wing Gambit		r1bqk1nr/pppp1ppp/2n5/1Bb1p3/1P2P3/5N2/P1PP1PPP/RNBQK2R b
C64	Ruy Lopez: Classical Variation, Zukertort Gambit		r1bqk2r/pppp1ppp/2n2n2/1Bb1p3/4P3/2P2N2/PP1P1PPP/RNBQ1RK1 b
C65	Ruy Lopez Defense, Halloween Attack		r1bqkb1r/pppp1ppp/2n2n2/1B2N3/4P3/8/PPPP1PPP/RNBQK2R b
C65	Ruy Lopez: Berlin Defense		r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 b
C65	Ruy Lopez: Berlin Defense #2		r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C65	Ruy Lopez: Berlin Defense, Anderssen Variation		r1bqkb1r/ppp2ppp/2Bp1n2/4p3/4P3/3P1N2/PPP2PPP/RNBQK2R b
C65	Ruy Lopez: Berlin Defense, Beverwijk Variation		r1bqk2r/pppp1ppp/2n2n2/1Bb1p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w
C65	Ruy Lopez: Berlin Defense, Duras Variation		r1bqkb1r/ppp2ppp/2np1n2/1B2p3/2P1P3/3P1N2/PP3PPP/RNBQK2R b
C65	Ruy Lopez: Berlin Defense, Fishing Pole Variation		r1bqkb1r/pppp1ppp/2n5/1B2p3/4P1n1/5N2/PPPP1PPP/RNBQ1RK1 w
C65	Ruy Lopez: Berlin Defense, Nyholm Attack		r1bqkb1r/pppp1ppp/2n2n2/1B6/3pP3/5N2/PPP2PPP/RNBQ1RK1 b
C65	Ruy Lopez: Berlin Defense: Kaufmann Variation		r1bqk2r/pppp1ppp/2n2n2/1Bb1p3/4P3/3PBN2/PPP2PPP/RN1QK2R b
C65	Ruy Lopez: Berlin Defense: Mortimer Variation		r1bqkb1r/ppppnppp/5n2/1B2p3/4P3/3P1N2/PPP2PPP/RNBQK2R w
C66	Ruy Lopez: Berlin Defense, Closed Bernstein Variation		r2qk2r/pppbbppp/2np1n2/1B2p1B1/3PP3/2N2N2/PPP2PPP/R2Q1RK1 b
C66	Ruy Lopez: Berlin Defense, Closed Showalter Variation		r2qk2r/pppbbppp/2Bp1n2/4p3/3PP3/2N2N2/PPP2PPP/R1BQ1RK1 b
C66	Ruy Lopez: Berlin Defense, Closed Wolf Variation		r2qkb1r/pppb1ppp/2np1n2/1B6/3pP3/2N2N2/PPP2PPP/R1BQ1RK1 w
C66	Ruy Lopez: Berlin Defense, Hedgehog Variation		r2qk2r/pppbbppp/2np1n2/1B2p3/3PP3/2N2N2/PPP2PPP/R1BQ1RK1 w
C66	Ruy Lopez: Berlin Defense, Improved Steinitz Defense		r1bqkb1r/ppp2ppp/2np1n2/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w
C66	Ruy Lopez: Berlin Defense, Tarrasch Trap		r2q1rk1/pppbbppp/2np1n2/1B2p3/3PP3/2N2N2/PPP2PPP/R1BQR1K1 w
C66	Ruy Lopez: Closed Berlin Defense: Chigorin Variation		r1bqkb1r/pppn1ppp/2np4/1B2p3/3PP3/5N2/PPP2PPP/RNBQ1RK1 w
C67	Dresden Opening: The Goblin		r1bqkb1r/pppp1ppp/2n2n2/4N3/2P1P3/8/PP1P1PPP/RNBQKB1R b
C67	Ruy Lopez: Berlin Defense, Cordel Variation		r1bqk2r/p1ppbppp/2p5/4Pn2/8/5N2/PPP1QPPP/RNB2RK1 w
C67	Ruy Lopez: Berlin Defense, Minckwitz Variation		r1bqk2r/ppppbppp/2n5/1B2P3/4n3/5N2/PPP2PPP/RNBQ1RK1 b
C67	Ruy Lopez: Berlin Defense, Rio Gambit Accepted		r1bqkb1r/pppp1ppp/2n5/1B2p3/4n3/5N2/PPPP1PPP/RNBQ1RK1 w
C67	Ruy Lopez: Berlin Defense, Rio de Janeiro Variation		r1bqk2r/ppppbppp/2n5/1B2p3/3Pn3/5N2/PPP2PPP/RNBQ1RK1 w
C67	Ruy Lopez: Berlin Defense, Rosenthal Variation		r1bqkb1r/1ppp1ppp/p1n5/1B2p3/3Pn3/5N2/PPP2PPP/RNBQ1RK1 w
C67	Ruy Lopez: Berlin Defense, l'Hermet Variation		r1bqkb1r/pppp1ppp/2nn4/1B2p3/3P4/5N2/PPP2PPP/RNBQ1RK1 w
C67	Ruy Lopez: Berlin Defense, l'Hermet Variation, Westerinen Line		r1bqkb1r/ppp2ppp/2p5/4P3/4n3/5N2/PPP2PPP/RNBQ1RK1 w
C67	Ruy Lopez: Open Berlin Defense: Showalter Variation		r1bqkb1r/pppp1ppp/2nn4/4p3/B2P4/5N2/PPP2PPP/RNBQ1RK1 b
C67	Ruy Lopez: Open Berlin Defense: l'Hermet Variation		r1bqkb1r/pppp1ppp/2nn4/1B2P3/8/5N2/PPP2PPP/RNBQ1RK1 b
C68	Ruy Lopez: Columbus Variation		r1bqkbnr/1ppp1ppp/p1n5/4p3/B3P3/5N2/PPPP1PPP/RNBQK2R b
C68	Ruy Lopez: Exchange Variation		r1bqkbnr/1ppp1ppp/p1B5/4p3/4P3/5N2/PPPP1PPP/RNBQK2R b
C68	Ruy Lopez: Exchange Variation, Alekhine Variation		r1b1k1nr/1pp2ppp/p1pb4/8/3NP3/8/PPP2PPP/RNB1K2R w
C68	Ruy Lopez: Exchange Variation, Keres Variation		r1bqkbnr/1pp2ppp/p1p5/4p3/4P3/2N2N2/PPPP1PPP/R1BQK2R b
C68	Ruy Lopez: Exchange Variation, King's Bishop Variation		r1bqk1nr/1pp2ppp/p1pb4/4p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w
C68	Ruy Lopez: Exchange Variation, Lutikov Variation		r1bqkbnr/2pp1ppp/p1p5/4p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C68	Ruy Lopez: Exchange Variation, Romanovsky Variation		r1bqkbnr/1pp3pp/p1p2p2/4p3/4P3/2NP1N2/PPP2PPP/R1BQK2R b
C68	Ruy Lopez: Exchange, Alekhine Variation		r3kbnr/1ppb1ppp/p1p5/8/3NP3/8/PPP2PPP/RNB1K2R w
C69	Ruy Lopez: Exchange Variation, Alapin Gambit		r2qkbnr/1pp2pp1/p1p5/4p2p/4P1b1/5N1P/PPPP1PP1/RNBQ1RK1 w
C69	Ruy Lopez: Exchange Variation, Bronstein Variation		r1b1kbnr/1pp2ppp/p1pq4/4p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w
C69	Ruy Lopez: Exchange Variation, Gligoric Variation		r1bqkbnr/1pp3pp/p1p2p2/4p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 w
C69	Ruy Lopez: Exchange Variation, Normal Variation		r1bqkbnr/1pp2ppp/p1p5/4p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 b
C70	Ruy Lopez		r1bqkbnr/1ppp1ppp/p1n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Bird's Defense Deferred		r1bqkbnr/1ppp1ppp/p7/4p3/B2nP3/5N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Morphy Defense, Alapin's Defense Deferred		r1bqk1nr/1ppp1ppp/p1n5/4p3/Bb2P3/5N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Morphy Defense, Caro Variation		r1bqkbnr/2pp1ppp/p1n5/1p2p3/B3P3/5N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Morphy Defense, Classical Defense Deferred		r1bqk1nr/1ppp1ppp/p1n5/2b1p3/B3P3/5N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Morphy Defense, Cozio Defense		r1bqkb1r/1pppnppp/p1n5/4p3/B3P3/5N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Morphy Defense, Fianchetto Defense Deferred		r1bqkbnr/1ppp1p1p/p1n3p1/4p3/B3P3/5N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Morphy Defense, Graz Variation		r1bqk1nr/2pp1ppp/p1n5/1pb1p3/4P3/1B3N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Morphy Defense, Norwegian Variation		r1bqkbnr/2pp1ppp/p7/np2p3/4P3/1B3N2/PPPP1PPP/RNBQK2R w
C70	Ruy Lopez: Morphy Defense, Norwegian Variation, Nightingale Gambit		r1bqkbnr/2pp1Bpp/p7/np2p3/4P3/5N2/PPPP1PPP/RNBQK2R b
C70	Ruy Lopez: Morphy Defense, Schliemann Defense Deferred		r1bqkbnr/1ppp2pp/p1n5/4pp2/B3P3/5N2/PPPP1PPP/RNBQK2R w
C71	Ruy Lopez: Morphy Defense, Modern Steinitz Defense		r1bqkbnr/1pp2ppp/p1np4/4p3/B3P3/2N2N2/PPPP1PPP/R1BQK2R b
C71	Ruy Lopez: Morphy Defense, Modern Steinitz Defense #2		r1bqkbnr/1pp2ppp/p1np4/4p3/B3P3/5N2/PPPP1PPP/RNBQK2R w
C71	Ruy Lopez: Morphy Defense, Modern Steinitz Defense #3		r1bqkbnr/1pp2ppp/p1np4/4p3/B1P1P3/5N2/PP1P1PPP/RNBQK2R b
C71	Ruy Lopez: Noah's Ark Trap		r1bqkbnr/5ppp/p2p4/1pp5/3QP3/1B6/PPP2PPP/RNB1K2R w
C72	Ruy Lopez: Morphy Defense, Modern Steinitz Defense		r1bqkbnr/1pp2ppp/p1np4/4p3/B3P3/5N2/PPPP1PPP/RNBQ1RK1 b
C73	Ruy Lopez: Morphy Defense, Modern Steinitz Defense		r1bqkbnr/2p3pp/p1pp1p2/4p3/3PP3/5N2/PPP2PPP/RNBQK2R w
C73	Ruy Lopez: Morphy Defense, Modern Steinitz Defense #2		r1bqkbnr/2p2ppp/p1pp4/4p3/3PP3/5N2/PPP2PPP/RNBQK2R b
C74	Ruy Lopez: Morphy Defense, Jaffe Gambit		r1bqkb1r/1ppp1ppp/p1n2n2/4p3/B3P3/2P2N2/PP1P1PPP/RNBQK2R b
C74	Ruy Lopez: Morphy Defense, Modern Steinitz Defense		r1bqkbnr/1pp2ppp/p1np4/4p3/B3P3/2P2N2/PP1P1PPP/RNBQK2R b
C74	Ruy Lopez: Morphy Defense, Modern Steinitz Defense #2		r2qkbnr/1pp3pp/p1np4/4pb2/B7/2P2N2/PP1P1PPP/RNBQ1RK1 b
C74	Ruy Lopez: Morphy Defense, Modern Steinitz Defense, Siesta Variation		r1bqkbnr/1pp3pp/p1np4/4pp2/B3P3/2P2N2/PP1P1PPP/RNBQK2R w
C75	Ruy Lopez: Morphy Defense, Modern Steinitz Defense		r2qkb1r/1ppbnppp/p1np4/4p3/B2PP3/2P2N2/PP3PPP/RNBQK2R w
C75	Ruy Lopez: Morphy Defense, Modern Steinitz Defense #2		r2qkbnr/1ppb1ppp/p1np4/4p3/B3P3/2P2N2/PP1P1PPP/RNBQK2R w
C76	Ruy Lopez: Morphy Defense, Modern Steinitz Defense, Fianchetto Variation		r2qkbnr/1ppb1p1p/p1np2p1/4p3/B2PP3/2P2N2/PP3PPP/RNBQK2R w
C77	Ruy Lopez: Morphy Defense, Anderssen Variation		r1bqkb1r/1ppp1ppp/p1n2n2/4p3/B3P3/3P1N2/PPP2PPP/RNBQK2R b
C77	Ruy Lopez: Morphy Defense, Bayreuth Variation		r1bqkb1r/1ppp1ppp/p1B2n2/4p3/4P3/5N2/PPPP1PPP/RNBQK2R b
C77	Ruy Lopez: Morphy Defense, Duras Variation		r1bqkb1r/1pp2ppp/p1np1n2/4p3/B1P1P3/3P1N2/PP3PPP/RNBQK2R b
C77	Ruy Lopez: Morphy Defense, Mackenzie Variation		r1bqkb1r/1ppp1ppp/p1n2n2/4p3/B2PP3/5N2/PPP2PPP/RNBQK2R b
C77	Ruy Lopez: Morphy Defense, Tarrasch Variation		r1bqkb1r/1ppp1ppp/p1n2n2/4p3/B3P3/2N2N2/PPPP1PPP/R1BQK2R b
C77	Ruy Lopez: Morphy Defense, Wormald Attack		r1bqkb1r/1ppp1ppp/p1n2n2/4p3/B3P3/5N2/PPPPQPPP/RNB1K2R b
C77	Ruy Lopez: Wormald Attack, Grünfeld Variation		r2qk2r/2p1bppp/p1np1n2/1p2p3/3PP1b1/1BP2N2/PP2QPPP/RNB1K2R w
C78	Ruy Lopez: Closed Variations, Martinez Variation		r1bqk2r/1pppbppp/p1n2n2/4p3/B3P3/3P1N2/PPP2PPP/RNBQ1RK1 b
C78	Ruy Lopez: Closed Variations, Morphy Attack		r1bqk2r/1pppbppp/p1n2n2/4p3/B3P3/2N2N2/PPPP1PPP/R1BQ1RK1 b
C78	Ruy Lopez: Morphy Defense		r1bqkb1r/1ppp1ppp/p1n2n2/4p3/B3P3/5N2/PPPP1PPP/RNBQ1RK1 b
C78	Ruy Lopez: Morphy Defense #2		r1bqkb1r/2p2ppp/p1np1n2/1p2p3/4P3/1B3N2/PPPP1PPP/RNBQ1RK1 w
C78	Ruy Lopez: Morphy Defense, Arkhangelsk Variation		r2qkb1r/1bpp1ppp/p1n2n2/1p2p3/4P3/1B3N2/PPPP1PPP/RNBQ1RK1 w
C78	Ruy Lopez: Morphy Defense, Neo-Arkhangelsk Variation		r1bqk2r/1ppp1ppp/p1n2n2/2b1p3/B3P3/5N2/PPPP1PPP/RNBQ1RK1 w
C78	Ruy Lopez: Morphy Defense, Wing Attack		r1bqk2r/2ppbppp/p1n2n2/1p2p3/P3P3/1B3N2/1PPP1PPP/RNBQ1RK1 b
C79	Ruy Lopez: Brix Variation		r1bqkb1r/1ppp1p1p/p1n2np1/4p3/B3P3/5N2/PPPP1PPP/RNBQ1RK1 w
C79	Ruy Lopez: Central Countergambit		r1bqkb1r/1pp2ppp/p1n2n2/3pp3/B3P3/5N2/PPPP1PPP/RNBQ1RK1 w
C79	Ruy Lopez: Morphy Defense, Steinitz Deferred		r1bqkb1r/1pp2ppp/p1np1n2/4p3/B3P3/5N2/PPPP1PPP/RNBQ1RK1 w
C79	Ruy Lopez: Morphy Defense, Steinitz Deferred #2		r1bqkb1r/2p2ppp/p1pp4/4p3/3Pn3/5N2/PPP2PPP/RNBQ1RK1 w
C79	Ruy Lopez: Steinitz Defense Deferred, Lipnitsky Variation		r2qkb1r/2p2ppp/p1pp1n2/4p3/3PP1b1/5N2/PPP2PPP/RNBQ1RK1 w
C80	Ruy Lopez: Morphy Defense, Tartakower Variation		r1bqkb1r/1ppp1ppp/p1n5/4p3/B3n3/5N2/PPPPQPPP/RNB2RK1 b
C80	Ruy Lopez: Open Variation, Skipworth Gambit		r1bqkb1r/1pp2ppp/p1n5/3pp3/B3n3/5N2/PPPP1PPP/RNBQR1K1 w
C80	Ruy Lopez: Open Variations		r1bqkb1r/2pp1ppp/p1n5/1p2p3/3Pn3/1B3N2/PPP2PPP/RNBQ1RK1 b
C80	Ruy Lopez: Open Variations #2		r1bqkb1r/2p2ppp/p1n5/1p1pP3/4n3/1B3N2/PPP2PPP/RNBQ1RK1 b
C80	Ruy Lopez: Open Variations, Bernstein Variation		r2qkb1r/2p2ppp/p1n1b3/1p1pP3/4n3/1B3N2/PPPN1PPP/R1BQ1RK1 b
C80	Ruy Lopez: Open Variations, Karpov Gambit		r2qkb1r/2p2ppp/p1n1b3/1pn1P1N1/3p4/1BP5/PP1N1PPP/R1BQ1RK1 b
C80	Ruy Lopez: Open Variations, Main Line		r2qkb1r/2p2ppp/p1n1b3/1p1pP3/4n3/1B3N2/PPP2PPP/RNBQ1RK1 w
C80	Ruy Lopez: Open Variations, Open Variation		r1bqkb1r/1ppp1ppp/p1n5/4p3/B3n3/5N2/PPPP1PPP/RNBQ1RK1 w
C80	Ruy Lopez: Open Variations, Richter Variation		r1bqkb1r/2pp1ppp/p1n5/1p1Pp3/B3n3/5N2/PPP2PPP/RNBQ1RK1 b
C80	Ruy Lopez: Open Variations, Riga Variation		r1bqkb1r/1ppp1ppp/p1n5/8/B2pn3/5N2/PPP2PPP/RNBQ1RK1 w
C80	Ruy Lopez: Open, 6.d4		r1bqkb1r/1ppp1ppp/p1n5/4p3/B2Pn3/5N2/PPP2PPP/RNBQ1RK1 b
C80	Ruy Lopez: Open, 6.d4 b5		r1bqkb1r/2pp1ppp/p1n5/1p2p3/B2Pn3/5N2/PPP2PPP/RNBQ1RK1 w
C81	Ruy Lopez: Open Variations, Howell Attack #2		r2qkb1r/2p2ppp/p1n1b3/1p1pP3/4n3/1B3N2/PPP1QPPP/RNB2RK1 b
C82	Ruy Lopez: Open Variations, Berlin Variation		r2qkb1r/2p2ppp/p1n1b3/1pnpP3/8/1BP2N2/PP3PPP/RNBQ1RK1 w
C82	Ruy Lopez: Open Variations, Dilworth Variation		r2q1rk1/2p2ppp/p1n1b3/1pbpP3/8/2P2N2/PPBN1RPP/R1BQ2K1 b
C82	Ruy Lopez: Open Variations, Motzko Attack II		r2q1rk1/2p2ppp/p1n1b3/1pbpP3/4n3/1BPQ1N2/PP3PPP/RNB2RK1 w
C82	Ruy Lopez: Open Variations, St. Petersburg Variation		r2qk2r/2p2ppp/p1n1b3/1pbpP3/4n3/1BP2N2/PP1N1PPP/R1BQ1RK1 b
C82	Ruy Lopez: Open, 9.c3		r2qkb1r/2p2ppp/p1n1b3/1p1pP3/4n3/1BP2N2/PP3PPP/RNBQ1RK1 b
C83	Ruy Lopez: Open Variations, Classical Defense		r2qk2r/2p1bppp/p1n1b3/1p1pP3/4n3/1BP2N2/PP3PPP/RNBQ1RK1 w
C84	Ruy Lopez: Closed Variations		r1bqk2r/1pppbppp/p1n2n2/4p3/B3P3/5N2/PPPP1PPP/RNBQ1RK1 w
C84	Ruy Lopez: Closed Variations, Center Attack		r1bqk2r/1pppbppp/p1n2n2/4p3/B2PP3/5N2/PPP2PPP/RNBQ1RK1 b
C84	Ruy Lopez: Closed Variations, Center Attack, Basque Gambit		r1bqk2r/1pppbppp/p1n5/4P3/B3n3/2p2N2/PP3PPP/RNBQ1RK1 w
C85	Ruy Lopez: Closed Variations, Delayed Exchange		r1bqk2r/1pppbppp/p1B2n2/4p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 b
C86	Ruy Lopez: Closed Variations, Worrall Attack		r1bqk2r/1pppbppp/p1n2n2/4p3/B3P3/5N2/PPPPQPPP/RNB2RK1 b
C86	Ruy Lopez: Closed Variations, Worrall Attack, Castling Line		r1bq1rk1/2ppbppp/p1n2n2/1p2p3/4P3/1B3N2/PPPPQPPP/RNB2RK1 w
C86	Ruy Lopez: Closed Variations, Worrall Attack, Delayed Castling Line		r1bqk2r/2p1bppp/p1np1n2/1p2p3/4P3/1B3N2/PPPPQPPP/RNB2RK1 w
C87	Ruy Lopez: Closed Variations, Averbakh Variation		r1bqk2r/1pp1bppp/p1np1n2/4p3/B3P3/5N2/PPPP1PPP/RNBQR1K1 w
C88	Ruy Lopez: Closed		r1bqk2r/2ppbppp/p1n2n2/1p2p3/4P3/1B3N2/PPPP1PPP/RNBQR1K1 b
C88	Ruy Lopez: Closed Variations, Balla Variation		r1b1k2r/2q1bppp/p2p1n2/npp1p3/P2PP3/2P2N2/1PB2PPP/RNBQR1K1 b
C88	Ruy Lopez: Closed Variations, Rosen Attack		r1bqk2r/2p1bppp/p1np1n2/1p2p3/3PP3/1B3N2/PPP2PPP/RNBQR1K1 b
C88	Ruy Lopez: Closed Variations, Trajkovic Counterattack		r2qk2r/1bppbppp/p1n2n2/1p2p3/4P3/1B3N2/PPPP1PPP/RNBQR1K1 w
C88	Ruy Lopez: Closed, 7...O-O		r1bq1rk1/2ppbppp/p1n2n2/1p2p3/4P3/1B3N2/PPPP1PPP/RNBQR1K1 w
C88	Ruy Lopez: Closed, 8.c3		r1bq1rk1/2ppbppp/p1n2n2/1p2p3/4P3/1BP2N2/PP1P1PPP/RNBQR1K1 b
C88	Ruy Lopez: Closed, Anti-Marshall 8.a4		r1bq1rk1/2ppbppp/p1n2n2/1p2p3/P3P3/1B3N2/1PPP1PPP/RNBQR1K1 b
C88	Ruy Lopez: Noah's Ark Trap		r1bqk2r/4bppp/p2p1n2/1pp5/3QP3/1B6/PPP2PPP/RNB1R1K1 w
C89	Ruy Lopez: Marshall Attack		r1bq1rk1/2p1bppp/p1n2n2/1p1pp3/4P3/1BP2N2/PP1P1PPP/RNBQR1K1 w
C89	Ruy Lopez: Marshall Attack, Main Line		r1bq1rk1/4bppp/p1p5/1p1nR3/3P4/1BP5/PP3PPP/RNBQ2K1 b
C89	Ruy Lopez: Marshall Attack, Modern Main Line		r1b2rk1/5ppp/p1pb4/1p1n4/3P4/1BP3Pq/PP3P1P/RNBQR1K1 w
C89	Ruy Lopez: Marshall Attack, Modern Variation		r1bq1rk1/4bppp/p1p5/1p1nR3/8/1BP5/PP1P1PPP/RNBQ2K1 w
C89	Ruy Lopez: Marshall Attack, Original Marshall Attack		r1bq1rk1/2p1bppp/p4n2/1p2R3/8/1BP5/PP1P1PPP/RNBQ2K1 w
C89	Ruy Lopez: Marshall Attack, Steiner Variation		r1bq1rk1/2p1bppp/p1n2n2/1p1P4/4p3/1BP2N2/PP1P1PPP/RNBQR1K1 w
C89	Ruy Lopez: Marshall, Main Line, Spassky Variation		5rk1/5ppp/p1pbr3/1p1n3q/P2P2b1/1BPQB1P1/1P1N1P1P/R3R1K1 w
C90	Ruy Lopez: Closed Variations, Closed Defense		r1bq1rk1/2p1bppp/p1np1n2/1p2p3/4P3/1BP2N2/PP1P1PPP/RNBQR1K1 w
C90	Ruy Lopez: Closed Variations, Lutikov Variation		r1bq1rk1/2p1bppp/p1np1n2/1p2p3/4P3/2P2N2/PPBP1PPP/RNBQR1K1 b
C90	Ruy Lopez: Closed Variations, Pilnik Variation		r1bq1rk1/2p1bppp/p1np1n2/1p2p3/4P3/1BPP1N2/PP3PPP/RNBQR1K1 b
C90	Ruy Lopez: Closed Variations, Suetin Variation		r1bq1rk1/2p1bppp/p1np1n2/1p2p3/4P3/PBP2N2/1P1P1PPP/RNBQR1K1 b
C91	Ruy Lopez: Closed Variations, Bogoljubov Variation		r2q1rk1/2p1bppp/p1np1n2/1p2p3/3PP1b1/1BP2N2/PP3PPP/RNBQR1K1 w
C91	Ruy Lopez: Closed Variations, Yates Variation		r1bq1rk1/2p1bppp/p1np1n2/1p2p3/3PP3/1BP2N2/PP3PPP/RNBQR1K1 b
C91	Ruy Lopez: Closed Variations, Yates Variation, Short Attack		r2q1rk1/2p1bppp/p1np1n2/1p2p3/P2PP1b1/1BP2N2/1P3PPP/RNBQR1K1 b
C92	Ruy Lopez: Closed Variations		r1bq1rk1/2p1bppp/p1np1n2/1p2p3/4P3/1BP2N1P/PP1P1PP1/RNBQR1K1 b
C92	Ruy Lopez: Closed Variations, Flohr System		r2q1rk1/1bp1bppp/p1np1n2/1p2p3/4P3/1BP2N1P/PP1P1PP1/RNBQR1K1 w
C92	Ruy Lopez: Closed Variations, Keres Defense		r1bq1rk1/2p1bppp/2np1n2/pp2p3/4P3/1BP2N1P/PP1P1PP1/RNBQR1K1 w
C92	Ruy Lopez: Closed Variations, Keres Defense #2		r1bq1rk1/2pnbppp/p1np4/1p2p3/4P3/1BP2N1P/PP1P1PP1/RNBQR1K1 w
C92	Ruy Lopez: Closed Variations, Kholmov Variation		r2q1rk1/2p1bppp/p1npbn2/1p2p3/4P3/1BP2N1P/PP1P1PP1/RNBQR1K1 w
C92	Ruy Lopez: Closed Variations, Zaitsev System		r1bqr1k1/2p1bppp/p1np1n2/1p2p3/4P3/1BP2N1P/PP1P1PP1/RNBQR1K1 w
C93	Ruy Lopez: Closed Variations, Smyslov Defense		r1bq1rk1/2p1bpp1/p1np1n1p/1p2p3/4P3/1BP2N1P/PP1P1PP1/RNBQR1K1 w
C93	Ruy Lopez: Closed Variations, Smyslov-Breyer-Zaitsev Hybrid		r2qrbk1/1bp2pp1/p1np1n1p/1p2p3/3PP3/PBP2N1P/1P1N1PP1/R1BQR1K1 w
C94	Ruy Lopez: Morphy Defense, Breyer Defense		rnbq1rk1/2p1bppp/p2p1n2/1p2p3/4P3/1BP2N1P/PP1P1PP1/RNBQR1K1 w
C94	Ruy Lopez: Morphy Defense, Breyer Defense, Quiet Variation		rnbq1rk1/2p1bppp/p2p1n2/1p2p3/4P3/1BPP1N1P/PP3PP1/RNBQR1K1 b
C95	Ruy Lopez: Closed Variations, Breyer Defense		r2q1rk1/1b1nbppp/p2p1n2/1pp1p3/3PP3/2P2N1P/PPBN1PP1/R1BQR1K1 w
C95	Ruy Lopez: Closed, Breyer, 10.d4		rnbq1rk1/2p1bppp/p2p1n2/1p2p3/3PP3/1BP2N1P/PP3PP1/RNBQR1K1 b
C95	Ruy Lopez: Morphy Defense, Breyer Defense, Zaitsev Hybrid		r1bq1rk1/2pnbppp/p2p1n2/1p2p3/3PP3/1BP2N1P/PP3PP1/RNBQR1K1 w
C96	Ruy Lopez: Closed Variations, Borisenko Variation		r1bq1rk1/4bppp/p1np1n2/1pp1p3/3PP3/2P2N1P/PPB2PP1/RNBQR1K1 w
C96	Ruy Lopez: Closed Variations, Closed Defense		r1bq1rk1/4bppp/p2p1n2/npp1p3/4P3/2P2N1P/PPBP1PP1/RNBQR1K1 w
C96	Ruy Lopez: Closed Variations, Closed Defense #2		r1bq1rk1/2p1bppp/p2p1n2/np2p3/4P3/2P2N1P/PPBP1PP1/RNBQR1K1 b
C96	Ruy Lopez: Closed Variations, Keres Defense		r1bq1rk1/3nbppp/p2p4/npp1p3/3PP3/2P2N1P/PPB2PP1/RNBQR1K1 w
C96	Ruy Lopez: Closed, Rossolimo Defense		r1b2rk1/2q1bppp/p1pp1n2/np2p3/3PP3/2P2N1P/PPB2PP1/RNBQR1K1 w
C97	Ruy Lopez: Closed Variations, Chigorin Defense		r1b2rk1/2q1bppp/p2p1n2/npp1p3/3PP3/2P2N1P/PPB2PP1/RNBQR1K1 w
C97	Ruy Lopez: Closed, Chigorin, Yugoslav System		3rrbk1/1n3p1p/3q2p1/p1nPp3/1p2P3/1P2QN1P/R2B1PP1/2RB2K1 w
C98	Ruy Lopez: Closed Variations, Chigorin Defense		r1b2rk1/2q1bppp/p1np1n2/1pp1p3/3PP3/2P2N1P/PPBN1PP1/R1BQR1K1 w
C98	Ruy Lopez: Closed Variations, Chigorin Defense #2		3b2k1/5pp1/p1q1b2p/2p1p3/3nP1P1/1PQ1B3/P2N1PP1/3B2K1 w
C99	Ruy Lopez: Morphy Defense, Chigorin Defense, Panov System		r1b2rk1/2q1bppp/p2p1n2/np2p3/3PP3/5N1P/PPBN1PP1/R1BQR1K1 b
D00	Amazon Attack		rnbqkbnr/ppp1pppp/8/3p4/3P4/3Q4/PPP1PPPP/RNB1KBNR b
D00	Amazon Attack: Siberian Attack		rnbqkb1r/ppp1pppp/5n2/3p4/3P4/2NQ4/PPP1PPPP/R1B1KBNR b
D00	Blackmar-Diemer Gambit		rnbqkbnr/ppp1pppp/8/3p4/3PP3/8/PPP2PPP/RNBQKBNR b
D00	Blackmar-Diemer Gambit #2		rnbqkb1r/ppp1pppp/5n2/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR b
D00	Blackmar-Diemer Gambit Declined, Brombacher Countergambit		rnbqkb1r/pp2pppp/5n2/2p5/3Pp3/2N2P2/PPP3PP/R1BQKBNR w
D00	Blackmar-Diemer Gambit Declined, Elbert Countergambit		rnbqkb1r/ppp2ppp/5n2/4p3/3Pp3/2N2P2/PPP3PP/R1BQKBNR w
D00	Blackmar-Diemer Gambit Declined, Grosshans Defense		rn1qkbnr/pppbpppp/8/8/3Pp3/2N5/PPP2PPP/R1BQKBNR w
D00	Blackmar-Diemer Gambit Declined, Lamb Defense		r1bqkb1r/ppp1pppp/2n2n2/8/3Pp3/2N2P2/PPP3PP/R1BQKBNR w
D00	Blackmar-Diemer Gambit Declined, Langeheinecke Defense		rnbqkb1r/ppp1pppp/5n2/8/3P4/2N1pP2/PPP3PP/R1BQKBNR w
D00	Blackmar-Diemer Gambit Declined, O'Kelly Defense		rnbqkb1r/pp2pppp/2p2n2/8/3Pp3/2N2P2/PPP3PP/R1BQKBNR w
D00	Blackmar-Diemer Gambit Declined, Weinsbach Declination		rnbqkb1r/ppp2ppp/4pn2/8/3Pp3/2N2P2/PPP3PP/R1BQKBNR w
D00	Blackmar-Diemer Gambit: Bogoljubov Defense, Mad Dog Attack		rr4k1/ppp2ppp/4p3/4P3/3P4/2P2R2/P1P3PP/5RK1 b
D00	Blackmar-Diemer Gambit: Bogoljubov Variation		rnbqkb1r/ppp1pp1p/5np1/8/3P4/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: Bogoljubov Variation, Kloss Attack		rnbq1rk1/ppp1ppbp/5np1/8/2BP4/2N2N2/PPP3PP/R1BQ1R1K b
D00	Blackmar-Diemer Gambit: Bogoljubov Variation, Nimzowitsch Attack		rnbqk2r/ppp1ppbp/5np1/4N3/2BP4/2N5/PPP3PP/R1BQK2R b
D00	Blackmar-Diemer Gambit: Bogoljubov Variation, Studier Attack		rnbq1rk1/ppp1ppbp/5np1/8/2BP4/2N2N2/PPP3PP/R1B1QRK1 b
D00	Blackmar-Diemer Gambit: Diemer-Rosenberg Attack		rnbqkbnr/ppp1pppp/8/8/3Pp3/4B3/PPP2PPP/RN1QKBNR b
D00	Blackmar-Diemer Gambit: Euwe Defense		rnbqkb1r/ppp2ppp/4pn2/8/3P4/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: Fritz Attack		rnbqkbnr/ppp1pppp/8/8/2BPp3/8/PPP2PPP/RNBQK1NR b
D00	Blackmar-Diemer Gambit: Gedult Gambit		rnbqkbnr/ppp1pppp/8/8/3Pp3/5P2/PPP3PP/RNBQKBNR b
D00	Blackmar-Diemer Gambit: Kaulich Defense		rnbqkb1r/pp2pppp/5n2/2p5/3P4/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: Lemberger Countergambit, Diemer Attack		rnbqkbnr/ppp2ppp/8/4p3/3Pp3/2N1B3/PPP2PPP/R2QKBNR b
D00	Blackmar-Diemer Gambit: Lemberger Countergambit, Endgame Variation		rnbqkbnr/ppp2ppp/8/4P3/4p3/2N5/PPP2PPP/R1BQKBNR b
D00	Blackmar-Diemer Gambit: Lemberger Countergambit, Rassmussen Attack		rnbqkbnr/ppp2ppp/8/4p3/3Pp3/2N5/PPP1NPPP/R1BQKB1R b
D00	Blackmar-Diemer Gambit: Lemberger Countergambit, Simple Variation		rnbqkbnr/ppp2ppp/8/4p3/3PN3/8/PPP2PPP/R1BQKBNR b
D00	Blackmar-Diemer Gambit: Lemberger Countergambit, Sneider Attack		rnbqkbnr/ppp2ppp/8/4p2Q/3Pp3/2N5/PPP2PPP/R1B1KBNR b
D00	Blackmar-Diemer Gambit: Netherlands Variation		rnbqkbnr/ppp1p1pp/8/5p2/3Pp3/2N5/PPP2PPP/R1BQKBNR w
D00	Blackmar-Diemer Gambit: Pietrowsky Defense		r1bqkb1r/ppp1pppp/2n2n2/8/3P4/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: Rasa-Studier Gambit		rnbqkb1r/ppp1pppp/5n2/8/3Pp3/2N1B3/PPP2PPP/R2QKBNR b
D00	Blackmar-Diemer Gambit: Ritter Defense		rnbqkb1r/p1p1pppp/1p3n2/8/3P4/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: Rook Pawn Defense		N1b2b1r/1p1kppp1/p1n2n2/q6p/3P4/5N2/PPPB2PP/R2QKB1R b
D00	Blackmar-Diemer Gambit: Ryder Gambit		rnbqkb1r/ppp1pppp/5n2/8/3P4/2N2Q2/PPP3PP/R1B1KBNR b
D00	Blackmar-Diemer Gambit: Schlutter Defense		r1bqkb1r/pppnpppp/5n2/8/3P4/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: Tartakower Variation		rn1qkb1r/ppp1pppp/5n2/5b2/3P4/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: Teichmann Variation		rn1qkb1r/ppp1pppp/5n2/8/3P2b1/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: Vienna Variation		rn1qkb1r/ppp1pppp/5n2/5b2/3Pp3/2N2P2/PPP3PP/R1BQKBNR w
D00	Blackmar-Diemer Gambit: Zeller Defense		rn1qkbnr/ppp1pppp/8/5b2/3Pp3/2N5/PPP2PPP/R1BQKBNR w
D00	Blackmar-Diemer Gambit: Zeller Defense, Soller Attack		rn1qkb1r/ppp1pppp/5n2/5b2/2BPp3/2N2P2/PPP3PP/R1BQK1NR b
D00	Blackmar-Diemer Gambit: Ziegler Defense		rnbqkb1r/pp2pppp/2p2n2/8/3P4/2N2N2/PPP3PP/R1BQKB1R w
D00	Blackmar-Diemer Gambit: von Popiel Gambit		rnbqkb1r/ppp1pppp/5n2/6B1/3Pp3/2N5/PPP2PPP/R2QKBNR b
D00	Blackmar-Diemer, Lemberger Countergambit		rnbqkb1r/ppp2ppp/5n2/3pp3/3PP3/2N5/PPP2PPP/R1BQKBNR w
D00	Queen's Pawn Game		rnbqkbnr/ppp1pppp/8/3p4/3P4/8/PPP1PPPP/RNBQKBNR w
D00	Queen's Pawn Game #2		rnbqkbnr/ppp1pppp/8/3p4/3P4/4P3/PPP2PPP/RNBQKBNR b
D00	Queen's Pawn Game #3		rnbqkb1r/ppp1pppp/5n2/3p4/3P4/4P3/PPP2PPP/RNBQKBNR w
D00	Queen's Pawn Game: Chigorin Variation		rnbqkbnr/ppp1pppp/8/3p4/3P4/2N5/PPP1PPPP/R1BQKBNR b
D00	Queen's Pawn Game: Huebsch Gambit		rnbqkb1r/ppp1pppp/8/3p4/3Pn3/2N5/PPP2PPP/R1BQKBNR w
D00	Queen's Pawn Game: Levitsky Attack		rnbqkbnr/ppp1pppp/8/3p2B1/3P4/8/PPP1PPPP/RN1QKBNR b
D00	Queen's Pawn Game: Levitsky Attack, Euwe Variation, Modern Line		rnb1kbnr/pp2ppp1/1qp4p/3p4/3P3B/5N2/PPP1PPPP/RN1QKB1R w
D00	Queen's Pawn Game: Levitsky Attack, Welling Variation		rn1qkbnr/ppp1pppp/8/3p2B1/3P2b1/8/PPP1PPPP/RN1QKBNR w
D00	Queen's Pawn Game: Mason Attack		rnbqkbnr/ppp1pppp/8/3p4/3P1B2/8/PPP1PPPP/RN1QKBNR b
D00	Queen's Pawn Game: Morris Countergambit		rnbqkbnr/pp2pppp/8/2pp4/3PPB2/8/PPP2PPP/RN1QKBNR b
D00	Queen's Pawn Game: Steinitz Countergambit		rnbqkbnr/pp2pppp/8/2pp4/3P1B2/8/PPP1PPPP/RN1QKBNR w
D00	Queen's Pawn Game: Stonewall Attack		rnbqkb1r/ppp1pppp/5n2/3p4/3P4/3BP3/PPP2PPP/RNBQK1NR b
D00	Queen's Pawn Game: Veresov Attack, Alburt Defense		rn1qkbnr/ppp1pppp/8/3p1b2/3P4/2N5/PPP1PPPP/R1BQKBNR w
D00	Queen's Pawn Game: Veresov Attack, Anti-Veresov		rn1qkbnr/ppp1pppp/8/3p4/3P2b1/2N5/PPP1PPPP/R1BQKBNR w
D00	Queen's Pawn Game: Veresov Attack, Fianchetto Defense		rnbqk1nr/ppp1ppbp/6p1/3p4/3P4/2N2N2/PPP1PPPP/R1BQKB1R w
D00	Queen's Pawn Game: Veresov Attack, Shaviliuk Gambit		rnbqkbnr/ppp2ppp/8/3pp3/3P4/2N5/PPP1PPPP/R1BQKBNR w
D00	Queen's Pawn Game: Veresov Attack, Shropshire Defense		rnbqkbnr/ppp1ppp1/8/3p3p/3P4/2N5/PPP1PPPP/R1BQKBNR w
D00	Queen's Pawn Game: Zurich Gambit		rnbqkbnr/ppp1pppp/8/3p4/3P2P1/8/PPP1PP1P/RNBQKBNR b
D00	Queen's Pawn Opening: Veresov Attack, Irish Gambit		rnbqkbnr/pp2pppp/8/2pp4/3P4/2N5/PPP1PPPP/R1BQKBNR w
D00	Queen's Pawn Opening: Veresov, Richter Attack		rnbqkb1r/ppp1pppp/5n2/3p4/3P4/2N2P2/PPP1P1PP/R1BQKBNR b
D00	Veresov Opening: Malich Gambit		rnbqkb1r/pp2pp1p/5p2/2pP4/4p3/2N5/PPP2PPP/R2QKBNR b
D01	Queen's Pawn Game: Chigorin Variation		rnbqkb1r/ppp1pppp/5n2/3p4/3P4/2N5/PPP1PPPP/R1BQKBNR w
D01	Queen's Pawn Game: Veresov Attack, Boyce Defense		rnbqkb1r/ppp1pppp/8/3p2B1/3Pn3/2N5/PPP1PPPP/R2QKBNR w
D01	Queen's Pawn Game: Veresov Attack, Classical Defense		rnbqkb1r/ppp2ppp/4pn2/3p2B1/3P4/2N2N2/PPP1PPPP/R2QKB1R b
D01	Queen's Pawn Game: Veresov Attack, Richter Variation		rn1qkb1r/ppp1pppp/5n2/3p1bB1/3P4/2N2P2/PPP1P1PP/R2QKBNR b
D01	Queen's Pawn Game: Veresov Attack, Two Knights System		r1bqkb1r/pppnpppp/5n2/3p2B1/3P4/2N2N2/PPP1PPPP/R2QKB1R b
D01	Queen's Pawn Game: Veresov Attack, Two Knights System, Grünfeld Defense		r1bqkb1r/pppnpp1p/5np1/3p2B1/3P4/2N2N2/PPP1PPPP/R2QKB1R w
D01	Queen's Pawn Game: Veresov Attack, Veresov Variation		rn1qkb1r/ppp1pppp/5B2/3p1b2/3P4/2N5/PPP1PPPP/R2QKBNR b
D01	Richter-Veresov Attack		rnbqkb1r/ppp1pppp/5n2/3p2B1/3P4/2N5/PPP1PPPP/R2QKBNR b
D01	Richter-Veresov Attack #2		rn1qkb1r/ppp1pppp/5n2/3p1bB1/3P4/2N5/PPP1PPPP/R2QKBNR w
D02	London System: Poisoned Pawn Variation		rnb1kb1r/pp2pppp/1q3n2/2pp4/3P1B2/2N1PN2/PPP2PPP/R2QKB1R b
D02	Queen's Gambit Refused: Baltic Defense		rn1qkbnr/ppp1pppp/8/3p1b2/2PP4/8/PP2PPPP/RNBQKBNR w
D02	Queen's Gambit Refused: Baltic Defense, Pseudo-Chigorin		r2qkbnr/ppp2ppp/2n1p3/3p1b2/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D02	Queen's Gambit Refused: Baltic Defense, Pseudo-Slav		rn1qkbnr/pp3ppp/2p1p3/3p1b2/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D02	Queen's Gambit Refused: Baltic Defense, Queen Attack		rn1qkbnr/ppp1pppp/8/3p1b2/2PP4/1Q6/PP2PPPP/RNB1KBNR b
D02	Queen's Gambit Refused: Baltic Defense, Queen Attack Deferred		rn1qkbnr/ppp2ppp/4p3/3p1b2/2PP4/1QN5/PP2PPPP/R1B1KBNR b
D02	Queen's Pawn Game: Anti-Torre		rn1qkbnr/ppp1pppp/8/3p4/3P2b1/5N2/PPP1PPPP/RNBQKB1R w
D02	Queen's Pawn Game: Barry Attack, Grünfeld Variation		rnbq1rk1/ppp1ppbp/5np1/3p4/3P1B2/2N1PN2/PPP1BPPP/R2QK2R b
D02	Queen's Pawn Game: Chandler Gambit		rnbqkbnr/pp2pppp/8/3p4/3p4/5NP1/PPP1PPBP/RNBQK2R b
D02	Queen's Pawn Game: Chigorin Variation		r1bqkbnr/ppp1pppp/2n5/3p4/3P4/5N2/PPP1PPPP/RNBQKB1R w
D02	Queen's Pawn Game: Krause Variation		rnbqkbnr/pp2pppp/8/2pp4/3P4/5N2/PPP1PPPP/RNBQKB1R w
D02	Queen's Pawn Game: London System		rnbqkb1r/ppp1pppp/5n2/3p4/3P1B2/5N2/PPP1PPPP/RN1QKB1R b
D02	Queen's Pawn Game: London System, Pterodactyl Variation		rnb1k1nr/pp1pppbp/6p1/q7/3P1B2/5N2/PP2PPPP/RN1QKB1R w
D02	Queen's Pawn Game: Symmetrical Variation		rnbqkb1r/ppp1pppp/5n2/3p4/3P4/5N2/PPP1PPPP/RNBQKB1R w
D02	Queen's Pawn Game: Symmetrical Variation, Pseudo-Catalan		rnbqkb1r/ppp1pppp/5n2/3p4/3P4/5NP1/PPP1PP1P/RNBQKB1R b
D02	Queen's Pawn Game: Zilbermints Countergambit		rnbqkb1r/p1p1pppp/5n2/1p1p4/2PP4/5N2/PP2PPPP/RNBQKB1R w
D02	Queen's Pawn Game: Zukertort Variation		rnbqkbnr/ppp1pppp/8/3p4/3P4/5N2/PPP1PPPP/RNBQKB1R b
D03	Queen's Pawn Game: Torre Attack		rnbqkb1r/ppp1pppp/5n2/3p2B1/3P4/5N2/PPP1PPPP/RN1QKB1R b
D03	Queen's Pawn Game: Torre Attack, Breyer Variation		rnb1kb1r/pp3ppp/1q2pn2/2pp2B1/3P4/2P1PN2/PP3PPP/RN1QKB1R w
D03	Queen's Pawn Game: Torre Attack, Gossip Variation		rnbqkb1r/ppp1pppp/8/3p2B1/3Pn3/5N2/PPP1PPPP/RN1QKB1R w
D03	Queen's Pawn Game: Torre Attack, Grünfeld Variation		rnbqkb1r/ppp1pp1p/5np1/3p2B1/3P4/5N2/PPP1PPPP/RN1QKB1R w
D03	Queen's Pawn Game: Torre Attack, Grünfeld Variation, Main Line		rnbq1rk1/ppp1ppbp/5np1/3p2B1/3P4/4PN2/PPPN1PPP/R2QKB1R w
D04	Queen's Pawn Game: Colle System		rnbqkb1r/ppp1pppp/5n2/3p4/3P4/4PN2/PPP2PPP/RNBQKB1R b
D04	Queen's Pawn Game: Colle System, Anti-Colle		rn1qkb1r/ppp1pppp/5n2/3p1b2/3P4/4PN2/PPP2PPP/RNBQKB1R w
D05	Colle System		rnbqkb1r/ppp2ppp/4pn2/3p4/3P4/4PN2/PPP2PPP/RNBQKB1R w
D05	Colle System #2		rnbqkb1r/ppp2ppp/4pn2/3p4/3P4/3BPN2/PPP2PPP/RNBQK2R b
D05	Colle System: Pterodactyl Variation		2r2rk1/1p3p1p/p2p1pp1/Pb6/1P2PQ2/7P/2q2PP1/R3R1K1 w
D05	Colle System: Rhamphorhynchus Variation		rnb1k1nr/pp1pppbp/6p1/q1P5/8/4PN2/PPP2PPP/RNBQKB1R w
D05	Colle System: Siroccopteryx Variation		rnb1k1nr/pp1pppbp/6p1/q7/3N4/2PBP3/PP3PPP/RNBQK2R b
D05	Colle System: Traditional Colle		rnbqkb1r/pp3ppp/4pn2/2pp4/3P4/2PBPN2/PP3PPP/RNBQK2R b
D05	Queen's Pawn Game, Zukertort Variation		rnbqkb1r/pp3ppp/4pn2/2pp4/3P4/1P2PN2/P1PN1PPP/R1BQKB1R b
D05	Rubinstein Opening		rnbqkb1r/pp3ppp/4pn2/2pp4/3P4/1P1BPN2/P1P2PPP/RNBQK2R b
D05	Rubinstein Opening: Bogoljubov Defense		r1bq1rk1/pp3ppp/2nbpn2/2pp4/3P4/1P1BPN2/PBP2PPP/RN1Q1RK1 w
D05	Rubinstein Opening: Classical Defense		r1bq1rk1/pp2bppp/2n1pn2/2pp4/3P4/1P1BPN2/PBP2PPP/RN1Q1RK1 w
D05	Rubinstein Opening: Semi-Slav Defense		r1bq1rk1/pppn1ppp/3bpn2/3p4/3P4/1P1BPN2/PBP2PPP/RN1Q1RK1 b
D06	Queen's Gambit		rnbqkbnr/ppp1pppp/8/3p4/2PP4/8/PP2PPPP/RNBQKBNR b
D06	Queen's Gambit Refused: Austrian Attack, Salvio Countergambit		rnbqkbnr/pp2pppp/8/2P5/2Pp4/8/PP2PPPP/RNBQKBNR w
D06	Queen's Gambit Refused: Austrian Defense		rnbqkbnr/pp2pppp/8/2pp4/2PP4/8/PP2PPPP/RNBQKBNR w
D06	Queen's Gambit Refused: Austrian Defense, Gusev Countergambit		rnbqkb1r/pp2pppp/5n2/2pP4/3P4/8/PP2PPPP/RNBQKBNR w
D06	Queen's Gambit Refused: Austrian Defense, Haberditz Variation		rnb1kb1r/pp2pppp/8/q1PP4/4n3/8/PP3PPP/RNBQKBNR w
D06	Queen's Gambit Refused: Chigorin Defense, Lazard Gambit		r1bqkbnr/ppp2ppp/2n5/3pp3/2PP4/5N2/PP2PPPP/RNBQKB1R w
D06	Queen's Gambit Refused: Chigorin Defense, Modern Gambit		r1bqkb1r/ppp1pppp/2n2n2/8/2pP4/2N2N2/PP2PPPP/R1BQKB1R w
D06	Queen's Gambit Refused: Chigorin Defense, Tartakower Gambit		r1bqkbnr/ppp2ppp/2n5/3pp3/2PP4/2N5/PP2PPPP/R1BQKBNR w
D06	Queen's Gambit Refused: Marshall Defense		rnbqkb1r/ppp1pppp/5n2/3p4/2PP4/8/PP2PPPP/RNBQKBNR w
D06	Queen's Gambit Refused: Marshall Defense, Tan Gambit		rnbqkb1r/pp2pppp/2p2n2/3P4/3P4/8/PP2PPPP/RNBQKBNR w
D06	Queen's Gambit Refused: Zilbermints Gambit		rnbqkbnr/p1p1pppp/8/1p1p4/2PP4/8/PP2PPPP/RNBQKBNR w
D07	Queen's Gambit Refused: Chigorin Defense		r1bqkbnr/ppp1pppp/2n5/3p4/2PP4/8/PP2PPPP/RNBQKBNR w
D07	Queen's Gambit Refused: Chigorin Defense #2		r1bqkbnr/ppp1pppp/2n5/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR b
D07	Queen's Gambit Refused: Chigorin Defense #3		r1bqkbnr/ppp1pppp/2n5/8/2pP4/2N5/PP2PPPP/R1BQKBNR w
D07	Queen's Gambit Refused: Chigorin Defense, Exchange Variation		r1b1kbnr/ppp1pppp/2n5/3q4/3P4/8/PP2PPPP/RNBQKBNR w
D07	Queen's Gambit Refused: Chigorin Defense, Exchange Variation, Costa's Line		r1b1k1nr/ppp2ppp/2n5/3q4/3p4/2B1P3/PP2NPPP/R2QKB1R b
D07	Queen's Gambit Refused: Chigorin Defense, Janowski Variation		r1bqkbnr/ppp1pppp/2n5/8/2pP4/2N2N2/PP2PPPP/R1BQKB1R b
D07	Queen's Gambit Refused: Chigorin Defense, Main Line		r2qkbnr/ppp1pppp/2n5/3p4/2PP2b1/5N2/PP2PPPP/RNBQKB1R w
D07	Queen's Gambit Refused: Chigorin Defense, Main Line, Alekhine Variation		r2qkbnr/ppp1pppp/2n5/3p4/Q1PP2b1/5N2/PP2PPPP/RNB1KB1R b
D08	Queen's Gambit Declined: Albin Countergambit, Janowski Variation		r1bqkbnr/ppp3pp/2n2p2/4P3/2Pp4/5N2/PP1NPPPP/R1BQKB1R w
D08	Queen's Gambit Declined: Albin Countergambit, Krenosz Variation		r3k1nr/ppp1qppp/2n5/4P3/1bPp4/5N1P/PP1BPPP1/R2QKB1R w
D08	Queen's Gambit Refused: Albin Countergambit		rnbqkbnr/ppp2ppp/8/3pp3/2PP4/8/PP2PPPP/RNBQKBNR w
D08	Queen's Gambit Refused: Albin Countergambit, Lasker Trap		rnbqk1nr/ppp2ppp/8/4P3/1bP5/4p3/PP1B1PPP/RN1QKBNR w
D08	Queen's Gambit Refused: Albin Countergambit, Normal Line		rnbqkbnr/ppp2ppp/8/4P3/2Pp4/5N2/PP2PPPP/RNBQKB1R b
D08	Queen's Gambit Refused: Albin Countergambit, Tartakower Defense		rnbqkbnr/pp3ppp/8/2p1P3/2Pp4/5N2/PP2PPPP/RNBQKB1R w
D09	Queen's Gambit Refused: Albin Countergambit, Fianchetto Variation		r1bqkbnr/ppp2ppp/2n5/4P3/2Pp4/5NP1/PP2PP1P/RNBQKB1R b
D09	Queen's Gambit Refused: Albin Countergambit, Fianchetto Variation, Be6 Line		r2qkbnr/ppp2ppp/2n1b3/4P3/2Pp4/5NP1/PP2PP1P/RNBQKB1R w
D09	Queen's Gambit Refused: Albin Countergambit, Fianchetto Variation, Bg4 Line		r2qkbnr/ppp2ppp/2n5/4P3/2Pp2b1/5NP1/PP2PP1P/RNBQKB1R w
D09	Queen's Gambit Refused: Albin Countergambit, Modern Line		r1bqkbnr/ppp2ppp/2n5/4P3/2Pp4/5N2/PP1NPPPP/R1BQKB1R b
D10	Slav Defense		rnbqkbnr/pp2pppp/2p5/3p4/2PP4/8/PP2PPPP/RNBQKBNR w
D10	Slav Defense #2		rnbqkbnr/pp2pppp/2p5/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR b
D10	Slav Defense #3		rnbqkbnr/pp2pppp/2p5/8/2pP4/2N5/PP2PPPP/R1BQKBNR w
D10	Slav Defense: Exchange Variation		rnbqkbnr/pp2pppp/2p5/3P4/3P4/8/PP2PPPP/RNBQKBNR b
D10	Slav Defense: Slav Gambit, Alekhine Attack		rnbqkbnr/pp2pppp/2p5/8/2pPP3/2N5/PP3PPP/R1BQKBNR b
D10	Slav Defense: Winawer Countergambit		rnbqkbnr/pp3ppp/2p5/3pp3/2PP4/2N5/PP2PPPP/R1BQKBNR w
D10	Slav Defense: Winawer Countergambit, Anti-Winawer Gambit		rnbqkbnr/pp3ppp/2p5/3pp3/2PPP3/2N5/PP3PPP/R1BQKBNR b
D11	Slav Defense: Bonet Gambit		rnbqkb1r/pp2pppp/2p2n2/3p2B1/2PP4/5N2/PP2PPPP/RN1QKB1R b
D11	Slav Defense: Breyer Variation		rnbqkb1r/pp2pppp/2p2n2/3p4/2PP4/5N2/PP1NPPPP/R1BQKB1R b
D11	Slav Defense: Modern		rnbqkb1r/pp2pppp/2p2n2/3p4/2PP4/5N2/PP2PPPP/RNBQKB1R w
D11	Slav Defense: Modern Line		rnbqkbnr/pp2pppp/2p5/3p4/2PP4/5N2/PP2PPPP/RNBQKB1R b
D11	Slav Defense: Quiet Variation		rnbqkb1r/pp2pppp/2p2n2/3p4/2PP4/4PN2/PP3PPP/RNBQKB1R b
D12	Queen's Gambit Declined: Slav, Amsterdam Variation		rn1qkb1r/pp1n1ppp/4p3/3pNb2/3P4/2N1P3/PP3PPP/R1BQKB1R w
D12	Slav Defense: Exchange Variation, Schallopp Variation		rn1qkb1r/pp2pppp/5n2/3p1b2/3P4/2N1PN2/PP3PPP/R1BQKB1R b
D12	Slav Defense: Quiet Variation, Pin Defense		rn1qkb1r/pp2pppp/2p2n2/3p4/2PP2b1/4PN2/PP3PPP/RNBQKB1R w
D12	Slav Defense: Quiet Variation, Schallopp Defense		rn1qkb1r/pp2pppp/2p2n2/3p1b2/2PP4/4PN2/PP3PPP/RNBQKB1R w
D13	Slav Defense: Exchange Variation		rnbqkb1r/pp2pppp/5n2/3p4/3P4/5N2/PP2PPPP/RNBQKB1R w
D14	Slav Defense: Exchange Variation, Symmetrical Line		r2qkb1r/pp2pppp/2n2n2/3p1b2/3P1B2/2N2N2/PP2PPPP/R2QKB1R w
D14	Slav Defense: Exchange Variation, Trifunovic Variation		r2qk2r/pp3ppp/2n1pn2/3p1b2/1b1P1B2/1QN1PN2/PP3PPP/R3KB1R w
D15	Slav Defense: Alekhine Variation		rnbqkb1r/pp2pppp/2p2n2/8/2pP4/2N1PN2/PP3PPP/R1BQKB1R b
D15	Slav Defense: Chameleon Variation		rnbqkb1r/1p2pppp/p1p2n2/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D15	Slav Defense: Chameleon Variation, Advance System		rnbqkb1r/1p2pppp/p1p2n2/2Pp4/3P4/2N2N2/PP2PPPP/R1BQKB1R b
D15	Slav Defense: Geller Gambit		rnbqkb1r/p3pppp/2p2n2/1p2P3/2pP4/2N2N2/PP3PPP/R1BQKB1R b
D15	Slav Defense: Geller Gambit #2		rnbqkb1r/pp2pppp/2p2n2/8/2pPP3/2N2N2/PP3PPP/R1BQKB1R b
D15	Slav Defense: Schlechter Variation		rnbqkb1r/pp2pp1p/2p2np1/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D15	Slav Defense: Suechting Variation		rnb1kb1r/pp2pppp/1qp2n2/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D15	Slav Defense: Three Knights Variation		rnbqkb1r/pp2pppp/2p2n2/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R b
D15	Slav Defense: Two Knights Attack		rnbqkb1r/pp2pppp/2p2n2/8/2pP4/2N2N2/PP2PPPP/R1BQKB1R w
D16	Queen's Gambit Declined: Slav, Smyslov Variation		r2qkb1r/pp2pppp/n1p2n2/8/P1pPP1b1/2N2N2/1P3PPP/R1BQKB1R w
D16	Slav Defense: Alapin Variation		rnbqkb1r/pp2pppp/2p2n2/8/P1pP4/2N2N2/1P2PPPP/R1BQKB1R b
D16	Slav Defense: Smyslov Variation		r1bqkb1r/pp2pppp/n1p2n2/8/P1pP4/2N2N2/1P2PPPP/R1BQKB1R w
D16	Slav Defense: Soultanbeieff Variation		rnbqkb1r/pp3ppp/2p1pn2/8/P1pP4/2N2N2/1P2PPPP/R1BQKB1R w
D16	Slav Defense: Steiner Variation		rn1qkb1r/pp2pppp/2p2n2/8/P1pP2b1/2N2N2/1P2PPPP/R1BQKB1R w
D17	Slav Defense: Czech Variation		rn1qkb1r/pp2pppp/2p2n2/5b2/P1pP4/2N2N2/1P2PPPP/R1BQKB1R w
D17	Slav Defense: Czech Variation, Bled Attack		rn1qkb1r/pp2pppp/2p2n2/5b2/P1pP3N/2N5/1P2PPPP/R1BQKB1R b
D17	Slav Defense: Czech Variation, Carlsbad Variation		r3kb1r/ppqn1ppp/2p2n2/4pb2/P1NP4/2N3P1/1P2PP1P/R1BQKB1R w
D17	Slav Defense: Czech Variation, Carlsbad Variation, Morozevich Variation		r3kb1r/ppqn1p1p/2p5/4nbp1/P1N2B2/2N3P1/1P2PPBP/R2QK2R w
D17	Slav Defense: Czech Variation, Krause Attack		rn1qkb1r/pp2pppp/2p2n2/4Nb2/P1pP4/2N5/1P2PPPP/R1BQKB1R b
D17	Slav Defense: Czech Variation, Wiesbaden Variation		rn1qkb1r/pp3ppp/2p1pn2/4Nb2/P1pP4/2N5/1P2PPPP/R1BQKB1R w
D17	Slav Defense: Czech Variation, Wiesbaden Variation, Sharp Line		rn1qk2r/pp3ppp/2p1pn2/4Nb2/PbpPP3/2N2P2/1P4PP/R1BQKB1R b
D18	Slav Defense: Czech Variation, Classical System		rn1qkb1r/pp2pppp/2p2n2/5b2/P1pP4/2N1PN2/1P3PPP/R1BQKB1R b
D18	Slav Defense: Czech Variation, Lasker Variation		r2qkb1r/pp2pppp/2p2n2/5b2/PnBP4/2N1PN2/1P3PPP/R1BQK2R w
D19	Queen's Gambit Declined: Slav, Dutch Variation		rn1qk2r/pp3ppp/2p1pn2/5b2/PbBP4/2N1PN2/1P3PPP/R1BQ1RK1 b
D19	Slav Defense: Czech Variation, Classical System, Main Line		rn1q1rk1/pp3ppp/2p1pn2/5b2/PbBP4/2N1PN2/1P2QPPP/R1B2RK1 b
D20	Queen's Gambit Accepted		rnbqkbnr/ppp1pppp/8/8/2pP4/8/PP2PPPP/RNBQKBNR w
D20	Queen's Gambit Accepted: Accelerated Mannheim Variation		rnbqkbnr/ppp1pppp/8/8/Q1pP4/8/PP2PPPP/RNB1KBNR b
D20	Queen's Gambit Accepted: Central Variation, Alekhine System		rnbqkb1r/ppp1pppp/5n2/8/2pPP3/8/PP3PPP/RNBQKBNR w
D20	Queen's Gambit Accepted: Central Variation, Greco Variation		rnbqkbnr/p1p1pppp/8/1p6/2pPP3/8/PP3PPP/RNBQKBNR w
D20	Queen's Gambit Accepted: Central Variation, McDonnell Defense		rnbqkbnr/ppp2ppp/8/4p3/2pPP3/8/PP3PPP/RNBQKBNR w
D20	Queen's Gambit Accepted: Central Variation, McDonnell Defense, Somov Gambit		rnbqkbnr/ppp2ppp/8/4p3/2BPP3/8/PP3PPP/RNBQK1NR b
D20	Queen's Gambit Accepted: Central Variation, Modern Defense		r1bqkbnr/ppp1pppp/2n5/8/2pPP3/8/PP3PPP/RNBQKBNR w
D20	Queen's Gambit Accepted: Central Variation, Rubinstein Defense		rnbqkbnr/pp2pppp/8/2p5/2pPP3/8/PP3PPP/RNBQKBNR w
D20	Queen's Gambit Accepted: Central Variation, Rubinstein Defense, Yefimov Gambit		rnbqkbnr/p3pppp/8/1ppP4/2p1P3/8/PP3PPP/RNBQKBNR w
D20	Queen's Gambit Accepted: Linares Variation		rnbqkb1r/p3pppp/5n2/1ppP4/2p1P3/2N5/PP3PPP/R1BQKBNR w
D20	Queen's Gambit Accepted: Old Variation		rnbqkbnr/ppp1pppp/8/8/2pP4/4P3/PP3PPP/RNBQKBNR b
D20	Queen's Gambit Accepted: Old Variation, Billinger Gambit		rnb1kbnr/ppp1qppp/8/8/2Bp4/PQ2P3/1P3PPP/RNB1K1NR b
D20	Queen's Gambit Accepted: Old Variation, Christensen Gambit		rnb1kbnr/ppp1qppp/8/8/2Bp4/1Q2PN2/PP3PPP/RNB1K2R b
D20	Queen's Gambit Accepted: Saduleto Variation		rnbqkbnr/ppp1pppp/8/8/2pPP3/8/PP3PPP/RNBQKBNR b
D20	Queen's Gambit Accepted: Schwartz Defense		rnbqkbnr/ppp1p1pp/8/5p2/2pPP3/8/PP3PPP/RNBQKBNR w
D21	Queen's Gambit Accepted: Alekhine Defense, Borisenko-Furman Variation		rnbqkbnr/1pp1pppp/p7/8/2pPP3/5N2/PP3PPP/RNBQKB1R b
D21	Queen's Gambit Accepted: Godes Variation		r1bqkbnr/pppnpppp/8/8/2pP4/5N2/PP2PPPP/RNBQKB1R w
D21	Queen's Gambit Accepted: Gunsberg Defense		rnbqkbnr/pp2pppp/8/2p5/2pP4/5N2/PP2PPPP/RNBQKB1R w
D21	Queen's Gambit Accepted: Normal Variation		rnbqkbnr/ppp1pppp/8/8/2pP4/5N2/PP2PPPP/RNBQKB1R b
D21	Queen's Gambit Accepted: Rosenthal Variation		rnbqkbnr/ppp2ppp/4p3/8/2pP4/5N2/PP2PPPP/RNBQKB1R w
D21	Queen's Gambit Accepted: Slav Gambit		rnbqkbnr/p1p1pppp/8/1p6/2pP4/5N2/PP2PPPP/RNBQKB1R w
D22	Queen's Gambit Accepted: Alekhine Defense		rnbqkbnr/1pp1pppp/p7/8/2pP4/5N2/PP2PPPP/RNBQKB1R w
D22	Queen's Gambit Accepted: Alekhine Defense, Haberditz Variation		rnbqkbnr/2p1pppp/p7/1p6/2pP4/4PN2/PP3PPP/RNBQKB1R w
D23	Queen's Gambit Accepted: Mannheim Variation		rnbqkb1r/ppp1pppp/5n2/8/Q1pP4/5N2/PP2PPPP/RNB1KB1R b
D24	Queen's Gambit Accepted: Bogoljubov Defense		rnbqkb1r/1pp1pppp/p4n2/8/2pPP3/2N2N2/PP3PPP/R1BQKB1R b
D24	Queen's Gambit Accepted: Gunsberg Defense, Prianishenmo Gambit		rnbqkb1r/pp3ppp/5n2/2ppP3/2p5/2N2N2/PP3PPP/R1BQKB1R b
D24	Queen's Gambit Accepted: Showalter Variation		rnbqkb1r/ppp1pppp/5n2/8/2pP4/2N2N2/PP2PPPP/R1BQKB1R b
D25	Queen's Gambit Accepted: Deferred Variation		rnbqkb1r/ppp1pppp/5n2/8/2pP4/5N2/PP2PPPP/RNBQKB1R w
D25	Queen's Gambit Accepted: Janowski-Larsen Variation		rn1qkb1r/ppp1pppp/5n2/8/2pP2b1/4PN2/PP3PPP/RNBQKB1R w
D25	Queen's Gambit Accepted: Normal Variation		rnbqkb1r/ppp1pppp/5n2/8/2pP4/4PN2/PP3PPP/RNBQKB1R b
D25	Queen's Gambit Accepted: Smyslov Variation		rnbqkb1r/ppp1pp1p/5np1/8/2pP4/4PN2/PP3PPP/RNBQKB1R w
D25	Queen's Gambit Accepted: Winawer Defense		rn1qkb1r/ppp1pppp/4bn2/8/2pP4/4PN2/PP3PPP/RNBQKB1R w
D26	Queen's Gambit Accepted: Classical Defense		rnbqkb1r/pp3ppp/4pn2/2p5/2BP4/4PN2/PP3PPP/RNBQK2R w
D26	Queen's Gambit Accepted: Classical Defense, Normal Line		rnbqkb1r/pp3ppp/4pn2/2p5/2BP4/4PN2/PP3PPP/RNBQ1RK1 b
D26	Queen's Gambit Accepted: Classical Defense, Steinitz Variation, Development Variation		r1bqkb1r/pp3ppp/2n1pn2/2p5/2BP4/4PN2/PP3PPP/RNBQ1RK1 w
D26	Queen's Gambit Accepted: Classical Defense, Steinitz Variation, Exchange Variation		rnbqkb1r/pp3ppp/4pn2/8/2Bp4/4PN2/PP3PPP/RNBQ1RK1 w
D26	Queen's Gambit Accepted: Normal Variation, Traditional System		rnbqkb1r/ppp2ppp/4pn2/8/2pP4/4PN2/PP3PPP/RNBQKB1R w
D27	Queen's Gambit Accepted: Classical Defense, Main Line		rnbqkb1r/1p3ppp/p3pn2/2p5/2BP4/4PN2/PP3PPP/RNBQ1RK1 w
D27	Queen's Gambit Accepted: Classical Defense, Rubinstein Variation		rnbqkb1r/1p3ppp/p3pn2/2p5/P1BP4/4PN2/1P3PPP/RNBQ1RK1 b
D27	Queen's Gambit Accepted: Classical Defense, Russian Gambit		rr4k1/5ppn/P1p1pq1p/2Pn4/1PB5/7P/3BQPP1/RR4K1 w
D27	Queen's Gambit Accepted: Furman Variation		rnbqk2r/1p3ppp/p3pn2/2b5/2B5/4PN2/PP3PPP/RNBQ1RK1 w
D28	Queen's Gambit Accepted: Classical Defense, Alekhine System		rnbqkb1r/1p3ppp/p3pn2/2p5/2BP4/4PN2/PP2QPPP/RNB2RK1 b
D28	Queen's Gambit Accepted: Classical Defense, Alekhine System (Except Main Line)		rnbqkb1r/5ppp/p3pn2/1pp5/2BP4/4PN2/PP2QPPP/RNB2RK1 w
D29	Queen's Gambit Accepted: Classical Defense, Alekhine System, Main Line		rn1qkb1r/1b3ppp/p3pn2/1pp5/3P4/1B2PN2/PP2QPPP/RNB2RK1 w
D29	Queen's Gambit Accepted: Classical Defense, Alekhine System, Smyslov Variation		4r3/3r1pk1/p3p2p/1p4p1/3q4/P4b1P/1PB1Q1PK/5R2 w
D30	Queen's Gambit Declined		rnbqkbnr/ppp2ppp/4p3/3p4/2PP4/8/PP2PPPP/RNBQKBNR w
D30	Queen's Gambit Declined: Capablanca		rnbqkb1r/ppp2pp1/4pn1p/3p2B1/2PP4/5N2/PP2PPPP/RN1QKB1R w
D30	Queen's Gambit Declined: Capablanca Variation		r1bqkb1r/pp1n1ppp/2p1pn2/3p2B1/2PP4/4PN2/PP1N1PPP/R2QKB1R b
D30	Queen's Gambit Declined: Hastings Variation		rnb1kb1r/pp3pp1/2p1pq1p/3p4/2PP4/1QN2N2/PP2PPPP/R3KB1R b
D30	Queen's Gambit Declined: Semmering Variation		r1bqkb1r/pp1n1ppp/4pn2/2pp4/2PP4/3BPN2/PP1N1PPP/R1BQK2R w
D30	Queen's Gambit Declined: Spielmann Variation		rnbqkb1r/pp3p1p/2p1pnp1/3p4/2PP4/4PN2/PP1N1PPP/R1BQKB1R w
D30	Queen's Gambit Declined: Stonewall Variation		rnbqkb1r/pp4pp/2p1p3/3p1p2/2PPn3/3BPN2/PP1N1PPP/R1BQK2R w
D30	Queen's Gambit Declined: Tarrasch Defense, Pseudo-Tarrasch		rnbqkbnr/pp3ppp/4p3/2pp4/2PP4/5N2/PP2PPPP/RNBQKB1R w
D30	Queen's Gambit Declined: Tarrasch Defense, Pseudo-Tarrasch Bishop Attack		rnbqkbnr/pp3ppp/8/2pp2B1/3P4/5N2/PP2PPPP/RN1QKB1R b
D30	Queen's Gambit Declined: Traditional Variation		rnbqkb1r/ppp2ppp/4pn2/3p2B1/2PP4/5N2/PP2PPPP/RN1QKB1R b
D30	Queen's Gambit Declined: Vienna Variation		rnbqk2r/ppp2ppp/4pn2/3p2B1/1bPP4/5N2/PP2PPPP/RN1QKB1R w
D30	Semi-Slav Defense: Quiet Variation		r1bqkb1r/pp1n1ppp/2p1pn2/3p4/2PP4/4PN2/PP1N1PPP/R1BQKB1R w
D30	Semi-Slav Defense: Quiet Variation #2		rnbqkb1r/pp3ppp/2p1pn2/3p4/2PP4/4PN2/PP1N1PPP/R1BQKB1R b
D31	Queen's Gambit Declined: Alapin Variation		rnbqkbnr/p1p2ppp/1p2p3/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR w
D31	Queen's Gambit Declined: Charousek (Petrosian) Variation		rnbqk1nr/ppp1bppp/4p3/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR w
D31	Queen's Gambit Declined: Charousek (Petrosian) Variation, Miladinovic Gambit		rnbqk1nr/ppp1bppp/4p3/8/2PPp3/2N2P2/PP4PP/R1BQKBNR b
D31	Queen's Gambit Declined: Janowski Variation		rnbqkbnr/1pp2ppp/p3p3/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR w
D31	Queen's Gambit Declined: Queen's Knight Variation		rnbqkbnr/ppp2ppp/4p3/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR b
D31	Queen's Gambit Declined: Semi-Slav, Abrahams Variation		rnbqk1nr/5ppp/2p1p3/pp6/PbpP4/2N1PN2/1P1B1PPP/R2QKB1R w
D31	Queen's Gambit Declined: Semi-Slav, Junge Variation		rnb1k1nr/p4ppp/1qp1p3/1p6/PbpP4/2N1PN2/1P1B1PPP/R2QKB1R w
D31	Queen's Gambit Declined: Semi-Slav, Koomen Variation		rnb1k1nr/p3qppp/2p1p3/1p6/PbpP4/2N1PN2/1P1B1PPP/R2QKB1R w
D31	Queen's Gambit Refused: Baltic Defense, Argentinian Gambit		r2qkbnr/pp2pppp/2n5/8/Q2P4/8/PP2PPPP/1RB1KBNR b
D31	Semi-Slav Defense: Accelerated Move Order		rnbqkbnr/pp3ppp/2p1p3/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR w
D31	Semi-Slav Defense: Anti-Noteboom, Stonewall Variation, Portisch Gambit		rnbqkbnr/pp4pp/2p1p3/3p1p2/2PP2P1/2N1P3/PP3P1P/R1BQKBNR b
D31	Semi-Slav Defense: Gunderam Gambit		rnbqkbnr/pp3ppp/2p1p3/8/2PPp3/2N2P2/PP4PP/R1BQKBNR b
D31	Semi-Slav Defense: Marshall Gambit		rnbqkbnr/pp3ppp/2p1p3/3p4/2PPP3/2N5/PP3PPP/R1BQKBNR b
D31	Semi-Slav Defense: Marshall Gambit, Forgotten Variation		rnbqk1nr/pp3ppp/2p1p3/8/1bPP4/2N5/PP3PPP/R1BQKBNR b
D31	Semi-Slav Defense: Marshall Gambit, Main Line		rnbqk1nr/pp3ppp/2p1p3/8/1bPPN3/8/PP1B1PPP/R2QKBNR b
D31	Semi-Slav Defense: Marshall Gambit, Tolush Variation		rnb1k1nr/pp3ppp/4p3/2B5/2P5/8/PP2BPqP/R2QK1NR w
D31	Semi-Slav Defense: Noteboom Variation		rnbqkbnr/pp3ppp/2p1p3/8/2pP4/2N2N2/PP2PPPP/R1BQKB1R w
D31	Semi-Slav Defense: Noteboom Variation, Abrahams Variation		rnbqk1nr/p4ppp/4p3/1p6/2pP4/2P1PN2/5PPP/R1BQKB1R w
D31	Semi-Slav Defense: Noteboom Variation, Anti-Noteboom Gambit		rnbqkbnr/pp3ppp/2p1p3/8/2pP4/2N2NP1/PP2PP1P/R1BQKB1R b
D31	Semi-Slav Defense: Noteboom Variation, Anti-Noteboom Variation		rnbqkbnr/pp3ppp/2p1p3/6B1/2pP4/2N2N2/PP2PPPP/R2QKB1R b
D31	Semi-Slav Defense: Noteboom Variation, Anti-Noteboom Variation, Belyavsky Line		rnbqkbnr/pp4pp/2p1pp2/6B1/2pP4/2N2N2/PP2PPPP/R2QKB1R w
D32	Queen's Gambit Declined: Tarrasch Defense: 4.cxd5 exd5		rnbqkbnr/pp3ppp/8/2pp4/3P4/2N5/PP2PPPP/R1BQKBNR w
D32	Tarrasch Defense		rnbqkbnr/pp3ppp/4p3/2pp4/2PP4/2N5/PP2PPPP/R1BQKBNR w
D32	Tarrasch Defense: Grünfeld Gambit		r1bqkbnr/p4ppp/2n5/1pP5/N2p4/5N2/PP2PPPP/R1BQKB1R w
D32	Tarrasch Defense: Marshall Gambit		rnbqkbnr/pp3ppp/8/2pp4/3PP3/2N5/PP3PPP/R1BQKBNR b
D32	Tarrasch Defense: Schara Gambit		rnbqkbnr/pp3ppp/4p3/3P4/3p4/2N5/PP2PPPP/R1BQKBNR w
D32	Tarrasch Defense: Symmetrical Variation		r1bqkb1r/pp3ppp/2n1pn2/2pp4/2PP4/2N1PN2/PP3PPP/R1BQKB1R w
D32	Tarrasch Defense: Tarrasch Gambit		rnbqkbnr/5ppp/1p6/8/N2p4/8/PP2PPPP/R1BQKBNR w
D32	Tarrasch Defense: Two Knights Variation		rnbqkbnr/pp3ppp/8/2pp4/3P4/2N2N2/PP2PPPP/R1BQKB1R b
D32	Tarrasch Defense: von Hennig Gambit		r2qkbnr/pp3ppp/2n1b3/3Q4/8/2N5/PP2PPPP/R1B1KBNR w
D33	Tarrasch Defense: Prague Variation		r1bqkb1r/pp3ppp/2n2n2/2pp4/3P4/2N2NP1/PP2PP1P/R1BQKB1R w
D33	Tarrasch Defense: Rubinstein System		r1bqkbnr/pp3ppp/2n5/2pp4/3P4/2N2NP1/PP2PP1P/R1BQKB1R b
D33	Tarrasch Defense: Swedish Variation		r1bqkbnr/pp3ppp/2n5/3p4/2pP4/2N2NP1/PP2PP1P/R1BQKB1R w
D33	Tarrasch Defense: Swedish Variation, Central Break		rr4k1/5pp1/3q3p/1Q1pN3/2pPnP2/1p2P1PP/6BK/5R2 w
D33	Tarrasch Defense: Wagner Variation		r2qkb1r/pp3ppp/2n2n2/2pp4/3P2b1/2N2NP1/PP2PPBP/R1BQK2R w
D34	Queen's Gambit Declined: Tarrasch, Stoltz Variation		2rq1rk1/p3bppp/1pn1bn2/2pp2B1/3P4/2N2NP1/PP2PPBP/2RQR1K1 w
D34	Tarrasch Defense: Classical Variation		r1bq1rk1/pp2bppp/2n2n2/2pp4/3P4/2N2NP1/PP2PPBP/R1BQ1RK1 w
D34	Tarrasch Defense: Classical Variation, Advance Variation		r1bq1rk1/pp2bppp/2n2n2/3p2B1/2pP4/2N2NP1/PP2PPBP/R2Q1RK1 w
D34	Tarrasch Defense: Classical Variation, Bogoljubov Variation		r2q1rk1/pp2bppp/2n1bn2/3p2B1/2pP4/2N2NP1/PP2PPBP/2RQ1RK1 w
D34	Tarrasch Defense: Classical Variation, Carlsbad Variation		r1bq1rk1/pp2bppp/2n2n2/2pp2B1/3P4/2N2NP1/PP2PPBP/R2Q1RK1 b
D34	Tarrasch Defense: Classical Variation, Chandler Variation		2r1r1k1/1p1q2p1/3b1n1p/p2p4/2n1p3/6PP/Q3PBBK/1R1R4 w
D34	Tarrasch Defense: Classical Variation, Classical Tarrasch Gambit		r1bq1rk1/pp2bppp/2n2n2/2P5/3p4/2N2NP1/PP2PPBP/R1BQ1RK1 w
D34	Tarrasch Defense: Classical Variation, Endgame Variation		r2q1rk1/pp2bppp/2n1bn2/2pp2B1/3P4/2N2NP1/PP2PPBP/R2Q1RK1 w
D34	Tarrasch Defense: Classical Variation, Main Line		r1bqr1k1/pp2bpp1/2n2n1p/3p4/3N4/2N1B1P1/PP2PPBP/R2Q1RK1 w
D34	Tarrasch Defense: Classical Variation, Petursson Variation		r1bqr1k1/pp2bppp/2n2n2/3p2B1/3N4/2N3P1/PP2PPBP/R2Q1RK1 w
D34	Tarrasch Defense: Classical Variation, Réti Variation		r1bq1rk1/pp3ppp/2n2n2/2bp4/N7/5NP1/PP2PPBP/R1BQ1RK1 b
D34	Tarrasch Defense: Classical Variation, Spassky Variation		r2q1rk1/pp2bpp1/2n2n1p/3p4/3N2b1/2N1B1P1/PP2PPBP/R2Q1RK1 w
D34	Tarrasch Defense: Prague Variation, Main Line		r1bqk2r/pp2bppp/2n2n2/2pp4/3P4/2N2NP1/PP2PPBP/R1BQK2R w
D35	Queen's Gambit Declined: Exchange Variation		rnbqkb1r/ppp2ppp/4pn2/3P4/3P4/2N5/PP2PPPP/R1BQKBNR b
D35	Queen's Gambit Declined: Exchange Variation, Positional Variation		rnbqkb1r/pp3ppp/2p2n2/3p2B1/3P4/2N5/PP2PPPP/R2QKBNR w
D35	Queen's Gambit Declined: Exchange Variation, Positional Variation #2		rnbqkb1r/ppp2ppp/5n2/3p2B1/3P4/2N5/PP2PPPP/R2QKBNR b
D35	Queen's Gambit Declined: Exchange Variation, Sämisch Variation		r1bqkb1r/pppn1ppp/5n2/3p4/3P1B2/2N2N2/PP2PPPP/R2QKB1R b
D35	Queen's Gambit Declined: Exchange, Chameleon Variation		r1bqrnk1/ppp1bppp/5n2/3p2B1/3P4/2NBP3/PPQ1NPPP/2KR3R b
D35	Queen's Gambit Declined: Harrwitz Attack		rnbqkb1r/ppp2ppp/4pn2/3p4/2PP1B2/2N5/PP2PPPP/R2QKBNR b
D35	Queen's Gambit Declined: Normal Defense		rnbqkb1r/ppp2ppp/4pn2/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR w
D36	Queen's Gambit Declined: Exchange Variation, Reshevsky Variation		rnbqkb1r/pp3ppp/2p2n2/3p2B1/3P4/2N5/PPQ1PPPP/R3KBNR b
D37	Queen's Gambit Declined: Barmen Variation		r1bqkb1r/pppn1ppp/4pn2/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D37	Queen's Gambit Declined: Harrwitz Attack		rnbqk2r/ppp1bppp/4pn2/3p4/2PP1B2/2N2N2/PP2PPPP/R2QKB1R b
D37	Queen's Gambit Declined: Harrwitz Attack #2		r1b2rk1/pp3ppp/2n1pn2/q1bp4/2P2B2/P1N1PN2/1PQ2PPP/2KR1B1R b
D37	Queen's Gambit Declined: Harrwitz Attack, Fianchetto Defense		rnbq1rk1/p1p1bppp/1p2pn2/3p4/2PP1B2/2N1PN2/PP3PPP/R2QKB1R w
D37	Queen's Gambit Declined: Harrwitz Attack, Main Line		rnbq1rk1/pp3ppp/4pn2/2bp4/2P2B2/2N1PN2/PP3PPP/R2QKB1R w
D37	Queen's Gambit Declined: Harrwitz Attack, Orthodox Defense		rnbq1rk1/pp2bppp/2p1pn2/3p4/2PP1B2/2N1PN2/PP3PPP/R2QKB1R w
D37	Queen's Gambit Declined: Harrwitz Attack, Two Knights Defense		r1bq1rk1/pppnbppp/4pn2/3p4/2PP1B2/2N1PN2/PP3PPP/R2QKB1R w
D37	Queen's Gambit Declined: Harrwitz Attack, Two Knights Defense, Blockade Line		r1bq1rk1/pppnbppp/4pn2/2Pp4/3P1B2/2N1PN2/PP3PPP/R2QKB1R b
D37	Queen's Gambit Declined: Three Knights Variation		rnbqkb1r/ppp2ppp/4pn2/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R b
D38	Queen's Gambit Declined: Ragozin Defense, Alekhine Variation		rnbqk2r/ppp2ppp/4pn2/3p4/QbPP4/2N2N2/PP2PPPP/R1B1KB1R b
D39	Queen's Gambit Declined: Ragozin Defense, Vienna Variation		rnbqk2r/ppp2ppp/4pn2/6B1/1bpP4/2N2N2/PP2PPPP/R2QKB1R w
D40	Queen's Gambit Declined: Semi-Tarrasch Defense		rnbqkb1r/pp3ppp/4pn2/2pp4/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D40	Queen's Gambit Declined: Semi-Tarrasch Defense, Pillsbury Variation		rnbqkb1r/pp3ppp/4pn2/2pp2B1/2PP4/2N2N2/PP2PPPP/R2QKB1R b
D40	Queen's Gambit Declined: Semi-Tarrasch Defense, Symmetrical Variation		r1bq1rk1/pp3ppp/2nbpn2/2pp4/2PP4/2NBPN2/PP3PPP/R1BQ1RK1 w
D41	Queen's Gambit Declined: Semi-Tarrasch Defense, Exchange Variation		rnbqkb1r/pp3ppp/4p3/2pn4/3PP3/2N2N2/PP3PPP/R1BQKB1R b
D41	Queen's Gambit Declined: Semi-Tarrasch Defense, Pillsbury Variation		rnbqkb1r/pp3ppp/4p3/2pn4/3P4/2N1PN2/PP3PPP/R1BQKB1R b
D41	Queen's Gambit Declined: Semi-Tarrasch, 5.cxd5		rnbqkb1r/pp3ppp/4pn2/2pP4/3P4/2N2N2/PP2PPPP/R1BQKB1R b
D41	Queen's Gambit Declined: Semi-Tarrasch, Kmoch Variation		rnbq1rk1/pp3ppp/4p3/1B6/3PP3/5N2/P2Q1PPP/R3K2R b
D41	Queen's Gambit Declined: Semi-Tarrasch, San Sebastian Variation		rnb1k2r/pp3ppp/4p3/q7/1b1PP3/5N2/P2B1PPP/R2QKB1R w
D42	Queen's Gambit Declined: Semi-Tarrasch Defense, Main Line		r1bqkb1r/pp3ppp/2n1p3/2pn4/3P4/2NBPN2/PP3PPP/R1BQK2R b
D43	Semi-Slav Defense		rnbqkb1r/pp3ppp/2p1pn2/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D44	Queen's Gambit Declined: Vienna Variation		rnbqkb1r/ppp2ppp/4pn2/8/2pP4/2N2N2/PP2PPPP/R1BQKB1R w
D44	Queen's Gambit Declined: Vienna Variation, Quiet Variation		rnbqkb1r/ppp2ppp/4pn2/8/2pP4/2N1PN2/PP3PPP/R1BQKB1R b
D44	Semi-Slav Defense Accepted		rnbqkb1r/pp3ppp/2p1pn2/6B1/2pP4/2N2N2/PP2PPPP/R2QKB1R w
D44	Semi-Slav Defense: Anti-Moscow Gambit		rnbqkb1r/pp3pp1/2p1pn1p/3p4/2PP3B/2N2N2/PP2PPPP/R2QKB1R b
D44	Semi-Slav Defense: Botvinnik System		rnbqkb1r/p4p2/2p1pn1p/1p2P1N1/2pP3B/2N5/PP3PPP/R2QKB1R b
D44	Semi-Slav Defense: Botvinnik System, Alatortsev System		rnbqkb1r/p4p2/2p1p2p/1p1nP1N1/2pP3B/2N5/PP3PPP/R2QKB1R w
D44	Semi-Slav Defense: Botvinnik System, Ekstrom Variation		rnbqkb1r/p4p2/2p1pP1p/1p2N3/2pP3p/2N5/PP3PPP/R2QKB1R b
D44	Semi-Slav Defense: Botvinnik System, Lilienthal Variation		r1bqkb1r/p2n1p2/2p1pn2/1p2P1B1/2pP4/2N3P1/PP3P1P/R2QKB1R b
D44	Semi-Slav Defense: Botvinnik System, Szabo Variation		r1bqkb1r/p2n1p2/2p1pn2/1p2P1B1/2pP4/2N2Q2/PP3PPP/R3KB1R b
D44	Semi-Slav Defense: Botvinnik Variation		rnbqkb1r/pp3ppp/2p1pn2/6B1/2pPP3/2N2N2/PP3PPP/R2QKB1R b
D45	Semi-Slav Defense: Accelerated Meran Variation		rnbqkb1r/1p3ppp/p1p1pn2/3p4/2PP4/2N1PN2/PP3PPP/R1BQKB1R w
D45	Semi-Slav Defense: Main Line		rnbqkb1r/pp3ppp/2p1pn2/3p4/2PP4/2N1PN2/PP3PPP/R1BQKB1R b
D45	Semi-Slav Defense: Normal Variation		r1bqkb1r/pp1n1ppp/2p1pn2/3p4/2PP4/2N1PN2/PP3PPP/R1BQKB1R w
D45	Semi-Slav Defense: Rubinstein (Anti-Meran) System		r1bqkb1r/pp1n1ppp/2p1pn2/3pN3/2PP4/2N1P3/PP3PPP/R1BQKB1R b
D45	Semi-Slav Defense: Stoltz Variation		r1bqkb1r/pp1n1ppp/2p1pn2/3p4/2PP4/2N1PN2/PPQ2PPP/R1B1KB1R b
D45	Semi-Slav Defense: Stoltz Variation, Center Variation		r1bqk2r/pp1n1ppp/2pbpn2/3p4/2PPP3/2N2N2/PPQ2PPP/R1B1KB1R b
D45	Semi-Slav Defense: Stoltz Variation, Shabalov Attack		r1bqk2r/pp1n1ppp/2pbpn2/3p4/2PP2P1/2N1PN2/PPQ2P1P/R1B1KB1R b
D45	Semi-Slav Defense: Stonewall Defense		rnbqkb1r/pp4pp/2p1p3/3p1p2/2PPn3/2NBPN2/PP3PPP/R1BQK2R w
D46	Semi-Slav Defense: Bogoljubov Variation		r1bqk2r/pp1nbppp/2p1pn2/3p4/2PP4/2NBPN2/PP3PPP/R1BQK2R w
D46	Semi-Slav Defense: Chigorin Defense		r1bqk2r/pp1n1ppp/2pbpn2/3p4/2PP4/2NBPN2/PP3PPP/R1BQK2R w
D46	Semi-Slav Defense: Main Line		r1bqkb1r/pp1n1ppp/2p1pn2/3p4/2PP4/2NBPN2/PP3PPP/R1BQK2R b
D46	Semi-Slav Defense: Romih Variation		r1bqk2r/pp1n1ppp/2p1pn2/3p4/1bPP4/2NBPN2/PP3PPP/R1BQK2R w
D47	Semi-Slav Defense: Meran Variation		r1bqkb1r/p2n1ppp/2p1pn2/1p6/2BP4/2N1PN2/PP3PPP/R1BQK2R w
D47	Semi-Slav Defense: Meran Variation, Lundin Variation		r1bqkb1r/p2n1ppp/2p1pn2/8/1p1P4/2NBPN2/PP3PPP/R1BQK2R w
D47	Semi-Slav Defense: Meran Variation, Wade Variation		r2qkb1r/pb1n1ppp/2p1pn2/1p6/3P4/2NBPN2/PP3PPP/R1BQK2R w
D47	Semi-Slav Defense: Meran Variation, Wade Variation, Larsen Variation		r2qkb1r/pb1n1ppp/4p3/2pnP3/Np1P4/3B1N2/PP3PPP/R1BQK2R w
D47	Semi-Slav Defense: Semi-Meran Variation		r1bqkb1r/pp1n1ppp/2p1pn2/8/2BP4/2N1PN2/PP3PPP/R1BQK2R b
D48	Queen's Gambit Declined: Semi-Slav, Meran		r1bqkb1r/3n1ppp/p3pn2/1pp5/3PP3/2NB1N2/PP3PPP/R1BQK2R w
D48	Semi-Slav Defense: Meran Variation		r1bqkb1r/3n1ppp/p1p1pn2/1p6/3P4/2NBPN2/PP3PPP/R1BQK2R w
D48	Semi-Slav Defense: Meran Variation, Old Variation		r1bqkb1r/3n1ppp/p3pn2/1pp1P3/3P4/2NB1N2/PP3PPP/R1BQK2R b
D48	Semi-Slav Defense: Meran Variation, Pirc Variation		r1bqkb1r/3n1ppp/p1p1pn2/8/1p1PP3/2NB1N2/PP3PPP/R1BQK2R w
D48	Semi-Slav Defense: Meran Variation, Reynolds' Variation		r1bqkb1r/3n1ppp/p3pn2/1ppP4/4P3/2NB1N2/PP3PPP/R1BQK2R b
D49	Semi-Slav Defense: Meran Variation, Blumenfeld Variation		r1bqkb1r/3n1ppp/p3pn2/1N2P3/3p4/3B1N2/PP3PPP/R1BQK2R b
D49	Semi-Slav Defense: Meran Variation, Sozin Variation II		r2qkb1r/3b1ppp/4pn2/1B2N3/3p4/8/PP3PPP/R1BQK2R w
D50	Queen's Gambit Declined: Been-Koomen Variation		rnbqkb1r/pp3ppp/4pn2/2pp2B1/2PP4/2N5/PP2PPPP/R2QKBNR w
D50	Queen's Gambit Declined: Modern Variation		rnbqkb1r/ppp2ppp/4pn2/3p2B1/2PP4/2N5/PP2PPPP/R2QKBNR b
D50	Queen's Gambit Declined: Pseudo-Tarrasch Variation		rnbqkb1r/pp3ppp/4pn2/2pP2B1/3P4/2N5/PP2PPPP/R2QKBNR b
D50	Queen's Gambit Declined: Pseudo-Tarrasch Variation, Canal Variation		rnb1kb1r/pp3ppp/1q2pn2/2pP2B1/3P4/2N5/PP2PPPP/R2QKBNR w
D50	Queen's Gambit Declined: Pseudo-Tarrasch Variation, Primitive Pillsbury Variation		rnbqkb1r/pp3ppp/4pn2/3p2B1/2PQ4/2N2N2/PP2PPPP/R3KB1R b
D51	Queen's Gambit Declined: Alekhine Variation		r1bqkb1r/pp1n1ppp/2p1pn2/3p2B1/2PPP3/2N2N2/PP3PPP/R2QKB1R b
D51	Queen's Gambit Declined: Capablanca, Anti-Cambridge Springs Variation		r1bqkb1r/pp1n1ppp/2p1pn2/3p2B1/2PP4/P1N1P3/1P3PPP/R2QKBNR b
D51	Queen's Gambit Declined: Knight Defense, Alekhine Gambit		r1bqkb1r/pppn1pp1/4pn1p/8/2pP3B/2N2N2/PP2PPPP/R2QKB1R w
D51	Queen's Gambit Declined: Manhattan Variation		r1bqk2r/pppn1ppp/4pn2/3p2B1/1bPP4/2N1P3/PP3PPP/R2QKBNR w
D51	Queen's Gambit Declined: Modern, Knight Defense		r1bqkb1r/pp1n1ppp/2p1pn2/3p2B1/2PP4/2N1P3/PP3PPP/R2QKBNR w
D51	Queen's Gambit Declined: Modern, Knight Defense #2		r1bqkb1r/pppn1ppp/4pn2/3p2B1/2PP4/2N5/PP2PPPP/R2QKBNR w
D51	Queen's Gambit Declined: Modern, Knight Defense #3		r1bqkb1r/pppn1ppp/4pn2/3p2B1/2PP4/2N1P3/PP3PPP/R2QKBNR b
D51	Queen's Gambit Declined: Westphalian Variation		r1bqk2r/pp1n1ppp/4pn2/2pp2B1/1bPP4/2N1PN2/PP3PPP/R2QKB1R w
D52	Queen's Gambit Declined		r1bqkb1r/pp1n1ppp/2p1pn2/3p2B1/2PP4/2N1PN2/PP3PPP/R2QKB1R b
D52	Queen's Gambit Declined: Cambridge Springs Defense: 7.cxd5		r1b1kb1r/pp1n1ppp/2p1pn2/q2P2B1/3P4/2N1PN2/PP3PPP/R2QKB1R b
D52	Queen's Gambit Declined: Cambridge Springs Defense: Argentine Variation		r1b2rk1/pp1n1ppp/2p1pn2/q2p4/1bPP3B/2N1P3/PPQN1PPP/R3KB1R b
D52	Queen's Gambit Declined: Cambridge Springs Defense: Bogoljubov Variation		r1b1k2r/pp1n1ppp/2p1pn2/q2p2B1/1bPP4/2N1P3/PPQN1PPP/R3KB1R b
D52	Queen's Gambit Declined: Cambridge Springs Defense: Capablanca Variation		r1b1kb1r/pp1n1ppp/2p1pB2/q2p4/2PP4/2N1PN2/PP3PPP/R2QKB1R b
D52	Queen's Gambit Declined: Cambridge Springs Defense: Rubinstein Variation		r1b1kb1r/pp1n1ppp/2p1pn2/q5B1/2pP4/2N1P3/PP1N1PPP/R2QKB1R w
D52	Queen's Gambit Declined: Cambridge Springs Defense: Yugoslav Variation		r1b1kb1r/pp1n1ppp/2p1p3/q2n2B1/3P4/2N1PN2/PP3PPP/R2QKB1R w
D52	Queen's Gambit Declined: Cambridge Springs Variation		r1b1kb1r/pp1n1ppp/2p1pn2/q2p2B1/2PP4/2N1PN2/PP3PPP/R2QKB1R w
D53	Queen's Gambit Declined: 4.Bg5 Be7		rnbqk2r/ppp1bppp/4pn2/3p2B1/2PP4/2N5/PP2PPPP/R2QKBNR w
D53	Queen's Gambit Declined: 4.Bg5 Be7, 5.e3 O-O		rnbq1rk1/ppp1bppp/4pn2/3p2B1/2PP4/2N1P3/PP3PPP/R2QKBNR w
D53	Queen's Gambit Declined: Lasker Defense		rnbqk2r/ppp1bppp/4p3/3p2B1/2PPn3/2N1P3/PP3PPP/R2QKBNR w
D53	Queen's Gambit Declined: Miles Variation		rnbq1rk1/ppp1bppp/4pn2/3p2B1/2PP4/2N2N2/PPQ1PPPP/R3KB1R b
D53	Queen's Gambit Declined: Modern Variation, Heral Variation		rnbqk2r/ppp1bppp/4pB2/3p4/2PP4/2N5/PP2PPPP/R2QKBNR b
D55	Queen's Gambit Declined: Anti-Tartakower Variation		rnbq1rk1/ppp1bpp1/4pB1p/3p4/2PP4/2N1PN2/PP3PPP/R2QKB1R b
D55	Queen's Gambit Declined: Anti-Tartakower Variation, Petrosian Variation		r1bq1rk1/pp1n1pp1/2p1pb1p/8/2BP4/2N1PN2/PP3PPP/2RQ1RK1 b
D55	Queen's Gambit Declined: Modern Variation, Normal Line		rnbq1rk1/ppp1bppp/4pn2/3p2B1/2PP4/2N1PN2/PP3PPP/R2QKB1R b
D55	Queen's Gambit Declined: Neo-Orthodox Variation		rnbq1rk1/ppp1bpp1/4pn1p/3p2B1/2PP4/2N1PN2/PP3PPP/R2QKB1R w
D55	Queen's Gambit Declined: Neo-Orthodox Variation, Main Line		rnbq1rk1/ppp1bpp1/4pn1p/3p4/2PP3B/2N1PN2/PP3PPP/R2QKB1R b
D55	Queen's Gambit Declined: Pillsbury Attack		rn1q1rk1/pbp1bppp/1p3n2/3pN1B1/3P4/2NBP3/PP3PPP/R2QK2R b
D56	Queen's Gambit Declined: Lasker Defense		rnbq1rk1/ppp1bpp1/4p2p/3p4/2PPn2B/2N1PN2/PP3PPP/R2QKB1R w
D56	Queen's Gambit Declined: Lasker Defense, Teichmann Variation		rnb2rk1/ppp1qpp1/4p2p/3p4/2PPn3/2N1PN2/PPQ2PPP/R3KB1R b
D57	Queen's Gambit Declined: Lasker Defense, Bernstein Variation		8/2p3k1/p1r2rpp/1p1p4/3P1P2/2P1P1R1/P6P/2R4K w
D57	Queen's Gambit Declined: Lasker Defense, Bernstein Variation, Mar del Plata Gambit		6k1/p1pb2p1/7p/2PpP3/3P1QP1/8/q3r2P/2R1K2R w
D57	Queen's Gambit Declined: Lasker Defense, Main Line		rnb2rk1/ppp1qpp1/4p2p/3P4/3P4/2P1PN2/P4PPP/R2QKB1R b
D57	Queen's Gambit Declined: Tartakower Variation, Exchange Variation		rnbq1rk1/p1p1bpp1/1p3n1p/3p4/3P3B/2N1PN2/PP3PPP/R2QKB1R w
D58	Queen's Gambit Declined: Tartakower Defense		rnbq1rk1/p1p1bpp1/1p2pn1p/3p4/2PP3B/2N1PN2/PP3PPP/R2QKB1R w
D59	Queen's Gambit Declined: Tartakower Defense, Makogonov Exchange Variation		rnbq1rk1/p1p1bpp1/1p2p2p/3n4/3P3B/2N1PN2/PP3PPP/R2QKB1R w
D60	Queen's Gambit Declined: Orthodox Defense		r1bq1rk1/pppnbppp/4pn2/3p2B1/2PP4/2N1PN2/PP3PPP/R2QKB1R w
D60	Queen's Gambit Declined: Orthodox Defense, Botvinnik Variation		r1bq1rk1/pppnbppp/4pn2/3p2B1/2PP4/2NBPN2/PP3PPP/R2QK2R b
D60	Queen's Gambit Declined: Orthodox Defense, Rauzer Variation		r1bq1rk1/pppnbppp/4pn2/3p2B1/2PP4/1QN1PN2/PP3PPP/R3KB1R b
D61	Queen's Gambit Declined: Orthodox Defense, Rubinstein Variation		r1bq1rk1/pppnbppp/4pn2/3p2B1/2PP4/2N1PN2/PPQ2PPP/R3KB1R b
D62	Queen's Gambit Declined: Orthodox Defense, Rubinstein Variation, Flohr Line		r1bq1rk1/pp1nbppp/4pn2/2pP2B1/3P4/2N1PN2/PPQ2PPP/R3KB1R b
D67	Queen's Gambit Declined: Orthodox Defense, Main Line		r1b2rk1/pp1nqppp/2p1p3/3n4/2BP4/2N1PN2/PP3PPP/2RQ1RK1 b
D68	Queen's Gambit Declined: Orthodox Defense, Classical Variation		r1b2rk1/pp1nqppp/2p5/4p3/2BP4/2R1PN2/PP3PPP/3Q1RK1 w
D69	Queen's Gambit Declined: Orthodox Defense, Classical Variation		r1b2rk1/pp3ppp/2p5/4q3/2B5/2R1P3/PP3PPP/3Q1RK1 w
D70	Grünfeld Defense: Lutikov Variation		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP4/2N2P2/PP2P1PP/R1BQKBNR b
D70	Indian Game: Anti-Grünfeld, Alekhine Variation		rnbqkb1r/pppppp1p/5np1/8/2PP4/5P2/PP2P1PP/RNBQKBNR b
D70	Indian Game: Anti-Grünfeld, Alekhine Variation, Leko Gambit		rnbqkb1r/pppp1p1p/5np1/4p3/2PP4/5P2/PP2P1PP/RNBQKBNR w
D70	Neo-Grünfeld Defense		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP4/6P1/PP2PP1P/RNBQKBNR w
D70	Neo-Grünfeld Defense: Goglidze Attack		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP4/5P2/PP2P1PP/RNBQKBNR w
D70	Neo-Grünfeld Defense: Non- or Delayed Fianchetto		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP4/5N2/PP2PPPP/RNBQKB1R w
D71	Neo-Grünfeld Defense: Exchange Variation		rnbqk2r/ppp1ppbp/6p1/3n4/3P4/6P1/PP2PPBP/RNBQK1NR w
D72	Neo-Grünfeld Defense: Exchange Variation, with 6.e4		rnbqk2r/ppp1ppbp/1n4p1/8/3PP3/6P1/PP2NPBP/RNBQK2R b
D73	Neo-Grünfeld Defense, with 5.Nf3		rnbqk2r/ppp1ppbp/5np1/3p4/2PP4/5NP1/PP2PPBP/RNBQK2R b
D74	Neo-Grünfeld Defense: Delayed Exchange Variation		rnbq1rk1/ppp1ppbp/6p1/3n4/3P4/5NP1/PP2PPBP/RNBQ1RK1 b
D75	Neo-Grünfeld Defense: Delayed Exchange Variation		rnbq1rk1/pp2ppbp/6p1/2Pn4/8/5NP1/PP2PPBP/RNBQ1RK1 b
D75	Neo-Grünfeld Defense: Delayed Exchange Variation #2		rnbq1rk1/pp2ppbp/6p1/2pn4/3P4/2N2NP1/PP2PPBP/R1BQ1RK1 b
D76	Neo-Grünfeld Defense: Delayed Exchange Variation		rnbq1rk1/ppp1ppbp/1n4p1/8/3P4/5NP1/PP2PPBP/RNBQ1RK1 w
D77	Neo-Grünfeld Defense: Classical Variation		rnbq1rk1/ppp1ppbp/5np1/3p4/2PP4/5NP1/PP2PPBP/RNBQ1RK1 b
D78	Neo-Grünfeld Defense: Classical Variation, Modern Defense		rnbq1rk1/ppp1ppbp/5np1/8/2pP4/5NP1/PP2PPBP/RNBQ1RK1 w
D78	Neo-Grünfeld Defense: Classical Variation, Original Defense		rnbq1rk1/pp2ppbp/2p2np1/3p4/2PP4/5NP1/PP2PPBP/RNBQ1RK1 w
D78	Neo-Grünfeld Defense: Classical Variation, Polgar Variation		r1bq1rk1/ppp1ppbp/2n2np1/3p4/2PP4/5NP1/PP2PPBP/RNBQ1RK1 w
D79	Neo-Grünfeld Defense: Ultra-delayed Exchange Variation		rnbq1rk1/pp2ppbp/5np1/3p4/3P4/5NP1/PP2PPBP/RNBQ1RK1 w
D80	Grünfeld Defense		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP4/2N5/PP2PPPP/R1BQKBNR w
D80	Grünfeld Defense: Gibbon Gambit		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP2P1/2N5/PP2PP1P/R1BQKBNR b
D80	Grünfeld Defense: Lundin Variation		rnbqkb1r/pp2pp1p/6p1/2p3B1/2PPp3/8/PP1QPPPP/R3KBNR w
D80	Grünfeld Defense: Stockholm Variation		rnbqkb1r/ppp1pp1p/5np1/3p2B1/2PP4/2N5/PP2PPPP/R2QKBNR b
D80	Grünfeld Defense: Zaitsev Gambit		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP3P/2N5/PP2PPP1/R1BQKBNR b
D81	Grünfeld Defense: Russian Variation, Accelerated Variation		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP4/1QN5/PP2PPPP/R1B1KBNR b
D82	Grünfeld Defense: Brinckmann Attack		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP1B2/2N5/PP2PPPP/R2QKBNR b
D83	Grünfeld Defense: Brinckmann Attack, Grünfeld Gambit		rnbq1rk1/ppp1ppbp/5np1/3p4/2PP1B2/2N1P3/PP3PPP/R2QKBNR w
D84	Grünfeld Defense: Brinckmann Attack, Grünfeld Gambit Accepted		rnb2rk1/ppB1ppbp/6p1/3q4/3P4/4P3/PP3PPP/R2QKBNR b
D85	Grünfeld Defense: Exchange Variation		rnbqkb1r/ppp1pp1p/6p1/3n4/3P4/2N5/PP2PPPP/R1BQKBNR w
D85	Grünfeld Defense: Exchange Variation, Modern Exchange Variation		rnbqk2r/ppp1ppbp/6p1/8/3PP3/2P2N2/P4PPP/R1BQKB1R b
D85	Grünfeld Defense: Exchange Variation, Modern Exchange Variation, Kramnik's Line		rnbqk2r/pp2ppbp/6p1/2p5/3PP3/2P2N1P/P4PP1/R1BQKB1R b
D85	Grünfeld Defense: Exchange Variation, Nadanian Attack		rnbqkb1r/ppp1pp1p/6p1/3n4/N2P4/8/PP2PPPP/R1BQKBNR b
D86	Grünfeld Defense: Exchange Variation, Classical Variation		rnbqk2r/ppp1ppbp/6p1/8/2BPP3/2P5/P4PPP/R1BQK1NR b
D86	Grünfeld Defense: Exchange Variation, Simagin's Improved Variation		r1bq1rk1/ppp1ppbp/2n3p1/8/2BPP3/2P5/P3NPPP/R1BQK2R w
D86	Grünfeld Defense: Exchange, Simagin's Lesser Variation		rnbq1rk1/p1p1ppbp/1p4p1/8/2BPP3/2P5/P3NPPP/R1BQK2R w
D87	Grünfeld Defense: Exchange Variation, Seville Variation		r2q2k1/pp2prbp/6p1/n1p5/3PP1P1/2P1B3/P3N1PP/R2Q1RK1 b
D87	Grünfeld Defense: Exchange Variation, Spassky Variation		rnbq1rk1/pp2ppbp/6p1/2p5/2BPP3/2P5/P3NPPP/R1BQK2R w
D88	Grünfeld Defense: Exchange Variation, Spassky Variation		r1bq1rk1/pp2ppbp/2n3p1/8/2BPP3/4B3/P3NPPP/R2Q1RK1 b
D89	Grünfeld Defense: Exchange Variation, Sokolsky Variation		r2q1rk1/pp2ppbp/4b1p1/n2P4/4P3/3BBP2/P3N1PP/R2Q1RK1 b
D89	Grünfeld Defense: Exchange Variation, Spassky Variation		r2q1rk1/pp2ppbp/4b1p1/n7/3PP3/3BBP2/P3N1PP/R2Q1RK1 w
D90	Grünfeld Defense: Flohr Variation		rnbqk2r/ppp1ppbp/5np1/3p4/Q1PP4/2N2N2/PP2PPPP/R1B1KB1R b
D90	Grünfeld Defense: Three Knights Variation		rnbqk2r/ppp1ppbp/5np1/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R w
D90	Grünfeld Defense: Three Knights Variation #2		rnbqkb1r/ppp1pp1p/5np1/3p4/2PP4/2N2N2/PP2PPPP/R1BQKB1R b
D91	Grünfeld Defense: Three Knights Variation, Petrosian System		rnbqk2r/ppp1ppbp/5np1/3p2B1/2PP4/2N2N2/PP2PPPP/R2QKB1R b
D92	Grünfeld Defense: Three Knights Variation, Hungarian Attack		rnbqk2r/ppp1ppbp/5np1/3p4/2PP1B2/2N2N2/PP2PPPP/R2QKB1R b
D93	Grünfeld Defense: Three Knights Variation, Hungarian Variation		rnbq1rk1/ppp1ppbp/5np1/3p4/2PP1B2/2N1PN2/PP3PPP/R2QKB1R b
D94	Grünfeld Defense: Flohr Defense		rn1q1rk1/pp2ppbp/2p2np1/3p1b2/2PP4/2NBPN2/PP3PPP/R1BQ1RK1 w
D94	Grünfeld Defense: Makogonov Variation		rnbq1rk1/ppp1ppbp/5np1/3p4/1PPP4/2N1PN2/P4PPP/R1BQKB1R b
D94	Grünfeld Defense: Opocensky Variation		rnbq1rk1/ppp1ppbp/5np1/3p4/2PP4/2N1PN2/PP1B1PPP/R2QKB1R b
D94	Grünfeld Defense: Smyslov Defense		rn1q1rk1/pp2ppbp/2p2np1/3p4/2PP2b1/2NBPN2/PP3PPP/R1BQ1RK1 w
D94	Grünfeld Defense: Three Knights Variation, Burille Variation		rnbqk2r/ppp1ppbp/5np1/3p4/2PP4/2N1PN2/PP3PPP/R1BQKB1R b
D94	Grünfeld Defense: Three Knights Variation, Burille Variation, Reversed Tarrasch		r1bq1rk1/pp2ppbp/2n2np1/3p4/2PP4/2N2N2/PP2BPPP/R1BQ1RK1 w
D94	Grünfeld Defense: Three Knights Variation, Paris Variation		rnbq1rk1/ppp1ppbp/5np1/3p4/2PP4/2NBPN2/PP3PPP/R1BQK2R b
D95	Grünfeld Defense: Botvinnik Variation		rnbq1rk1/ppp2pbp/4pnp1/3p4/2PP4/1QN1PN2/PP3PPP/R1B1KB1R w
D95	Grünfeld Defense: Pachman Variation		r1bq1rk1/pppnppbp/5np1/6N1/2BP4/1QN1P3/PP3PPP/R1B1K2R b
D95	Grünfeld Defense: Three Knights Variation, Vienna Variation		rnbq1rk1/ppp1ppbp/5np1/3p4/2PP4/1QN1PN2/PP3PPP/R1B1KB1R b
D96	Grünfeld Defense: Russian Variation		rnbqk2r/ppp1ppbp/5np1/3p4/2PP4/1QN2N2/PP2PPPP/R1B1KB1R b
D97	Grünfeld Defense: Russian Variation, Byrne (Simagin) Variation		r1bq1rk1/ppp1ppbp/2n2np1/8/2QPP3/2N2N2/PP3PPP/R1B1KB1R w
D97	Grünfeld Defense: Russian Variation, Hungarian Variation		rnbq1rk1/1pp1ppbp/p4np1/8/2QPP3/2N2N2/PP3PPP/R1B1KB1R w
D97	Grünfeld Defense: Russian Variation, Levenfish Variation		rnbq1rk1/p1p1ppbp/1p3np1/8/2QPP3/2N2N2/PP3PPP/R1B1KB1R w
D97	Grünfeld Defense: Russian Variation, Prins Variation		r1bq1rk1/ppp1ppbp/n4np1/8/2QPP3/2N2N2/PP3PPP/R1B1KB1R w
D97	Grünfeld Defense: Russian Variation, Szabo (Boleslavsky)		rnbq1rk1/pp2ppbp/2p2np1/8/2QPP3/2N2N2/PP3PPP/R1B1KB1R w
D97	Grünfeld Defense: Russian Variation, with e4		rnbq1rk1/ppp1ppbp/5np1/8/2QPP3/2N2N2/PP3PPP/R1B1KB1R b
D98	Grünfeld Defense: Russian Variation, Smyslov Variation		rn1q1rk1/ppp1ppbp/5np1/8/2QPP1b1/2N2N2/PP3PPP/R1B1KB1R w
D99	Grünfeld Defense: Russian Variation, Smyslov Variation		1r4k1/1p4bp/2p3p1/4p3/Pp2P3/4Br1b/1PN4P/1K1R2R1 w
E00	Amar Opening		rnbqkbnr/pppppppp/8/8/8/7N/PPPPPPPP/RNBQKB1R b
E00	Catalan Opening		rnbqkb1r/pppp1ppp/4pn2/8/2PP4/6P1/PP2PP1P/RNBQKBNR b
E00	Catalan Opening: Hungarian Gambit		rnbqkb1r/pppp1ppp/8/4P3/2P3n1/6P1/PP2PP1P/RNBQKBNR w
E00	Indian Game: Devin Gambit		rnbqkb1r/pppp1ppp/4pn2/8/2PP2P1/8/PP2PP1P/RNBQKBNR b
E00	Indian Game: East Indian Defense		rnbqkb1r/pppp1ppp/4pn2/8/2PP4/8/PP2PPPP/RNBQKBNR w
E00	Indian Game: Seirawan Attack		rnbqkb1r/pppp1ppp/4pn2/6B1/2PP4/8/PP2PPPP/RN1QKBNR b
E00	Kangaroo Defense		rnbqk1nr/pppp1ppp/4p3/8/1bPP4/8/PP2PPPP/RNBQKBNR w
E00	Kangaroo Defense: Keres Defense, Transpositional Variation		rnbqk1nr/pppp1ppp/4p3/8/1bPP4/2N5/PP2PPPP/R1BQKBNR b
E01	Catalan Opening		rnbqkb1r/ppp2ppp/4pn2/3p4/2PP4/6P1/PP2PP1P/RNBQKBNR w
E01	Catalan Opening: Closed Variation		rnbqkb1r/ppp2ppp/4pn2/3p4/2PP4/6P1/PP2PPBP/RNBQK1NR b
E02	Catalan Opening: Open Defense		rnbqkb1r/ppp2ppp/4pn2/8/Q1pP4/6P1/PP2PPBP/RNB1K1NR b
E03	Catalan Opening: Open Defense		r1bqkb1r/pppn1ppp/4pn2/8/2QP4/6P1/PP2PPBP/RNB1K1NR b
E04	Catalan Opening: Open Defense		rnbqkb1r/ppp2ppp/4pn2/8/2pP4/5NP1/PP2PPBP/RNBQK2R b
E04	Catalan Opening: Open Defense, Modern Sharp Variation		r1bqk2r/ppp2ppp/2n1pn2/8/QbpP4/5NP1/PP2PPBP/RNB1K2R w
E04	Catalan Opening: Open Defense, Tarrasch Defense		r1bqkb1r/pp3ppp/2n1pn2/2pp4/2PP4/5NP1/PP2PPBP/RNBQK2R w
E05	Catalan Opening: Open Defense, Classical Line		rnbqk2r/ppp1bppp/4pn2/8/2pP4/5NP1/PP2PPBP/RNBQK2R w
E06	Catalan Opening: Closed Variation		rnbqk2r/ppp1bppp/4pn2/3p4/2PP4/5NP1/PP2PPBP/RNBQK2R b
E07	Catalan Opening: Closed Variation		r1bq1rk1/pppnbppp/4pn2/3p4/2PP4/5NP1/PP2PPBP/RNBQ1RK1 w
E07	Catalan Opening: Closed Variation, Botvinnik Variation		r1bq1rk1/pp1nbppp/2p1pn2/3p4/2PP4/2NQ1NP1/PP2PPBP/R1B2RK1 b
E08	Catalan Opening: Closed Variation		r1bq1rk1/pppnbppp/4pn2/3p4/2PP4/5NP1/PPQ1PPBP/RNB2RK1 b
E08	Catalan Opening: Closed, 7.Qc2 c6 8.b3		r1bq1rk1/pp1nbppp/2p1pn2/3p4/2PP4/1P3NP1/P1Q1PPBP/RNB2RK1 b
E08	Catalan Opening: Closed, Zagoryansky Variation		r1bq1rk1/p2nbppp/1pp1pn2/3p4/P1PP4/5NP1/1PQ1PPBP/RNBR2K1 b
E09	Catalan Opening: Closed Variation, Rabinovich Variation		r1bq1rk1/p2nbppp/2p1pn2/1p1p4/2PP4/5NP1/PPQNPPBP/R1B2RK1 w
E09	Catalan Opening: Closed Variation, Traditional Variation		r1bq1rk1/p2nbppp/1pp1pn2/3p4/2PP4/5NP1/PPQNPPBP/R1B2RK1 w
E09	Catalan Opening: Closed, Main Line		r1bq1rk1/pp1nbppp/2p1pn2/3p4/2PP4/5NP1/PPQNPPBP/R1B2RK1 b
E09	Catalan Opening: Closed, Sokolsky Variation		r2q1rk1/3nbppp/bpp1pn2/p2p4/2PP4/1P3NP1/PBQNPPBP/R4RK1 w
E10	Blumenfeld Countergambit		rnbqkb1r/p2p1ppp/4pn2/1ppP4/2P5/5N2/PP2PPPP/RNBQKB1R w
E10	Blumenfeld Countergambit #2		rnbqkb1r/pp1p1ppp/4pn2/2p5/2PP4/5N2/PP2PPPP/RNBQKB1R w
E10	Blumenfeld Countergambit Accepted		rnbqkb1r/p5pp/4pn2/1Ppp4/8/5N2/PP2PPPP/RNBQKB1R w
E10	Blumenfeld Countergambit: Dus-Khotimirsky Variation		rnbqkb1r/p2p1ppp/4pn2/1ppP2B1/2P5/5N2/PP2PPPP/RN1QKB1R b
E10	Blumenfeld Countergambit: Spielmann Variation		rnbqkb1r/p2p1pp1/5n1p/1ppP2B1/8/5N2/PP2PPPP/RN1QKB1R w
E10	Indian Game: 3.Qb3		rnbqkb1r/pppp1ppp/4pn2/8/2PP4/1Q6/PP2PPPP/RNB1KBNR b
E10	Indian Game: Anti-Nimzo-Indian		rnbqkb1r/pppp1ppp/4pn2/8/2PP4/5N2/PP2PPPP/RNBQKB1R b
E10	Indian Game: Doery Indian		rnbqkb1r/pppp1ppp/4p3/8/2PPn3/5N2/PP2PPPP/RNBQKB1R w
E10	Indian Game: Dzindzi-Indian Defense		rnbqkb1r/1ppp1ppp/p3pn2/8/2PP4/5N2/PP2PPPP/RNBQKB1R w
E11	Bogo-Indian Defense		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/5N2/PP2PPPP/RNBQKB1R w
E11	Bogo-Indian Defense: Exchange Variation		rnbqk2r/pppp1ppp/4pn2/8/2PP4/5N2/PP1bPPPP/RN1QKB1R w
E11	Bogo-Indian Defense: Grünfeld Variation		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/5N2/PP1NPPPP/R1BQKB1R b
E11	Bogo-Indian Defense: Haiti Variation		r1bqk2r/pppp1ppp/2n1pn2/8/1bPP4/5N2/PP1BPPPP/RN1QKB1R w
E11	Bogo-Indian Defense: New England Variation		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/8/PP1NPPPP/RNBQKB1R b
E11	Bogo-Indian Defense: Nimzowitsch Variation		rnb1k2r/ppppqppp/4pn2/8/1bPP4/5N2/PP1BPPPP/RN1QKB1R w
E11	Bogo-Indian Defense: Retreat Variation		rnbqk2r/ppppbppp/4pn2/8/2PP4/5N2/PP1BPPPP/RN1QKB1R w
E11	Bogo-Indian Defense: Vitolins Variation		rnbqk2r/pp1p1ppp/4pn2/2p5/1bPP4/5N2/PP1BPPPP/RN1QKB1R w
E11	Bogo-Indian Defense: Wade-Smyslov Variation		rnbqk2r/1ppp1ppp/4pn2/p7/1bPP4/5N2/PP1BPPPP/RN1QKB1R w
E11	Queen's Indian Defense: Petrosian Variation, Farago Defense		rn1qkb1r/pbpp1ppp/1p2pn2/8/2PP4/P4N2/1PQ1PPPP/RNB1KB1R w
E12	Queen's Indian Defense		rnbqkb1r/p1pp1ppp/1p2pn2/8/2PP4/5N2/PP2PPPP/RNBQKB1R w
E12	Queen's Indian Defense: Kasparov Variation		rnbqkb1r/p1pp1ppp/1p2pn2/8/2PP4/2N2N2/PP2PPPP/R1BQKB1R b
E12	Queen's Indian Defense: Kasparov Variation, Botvinnik Attack		rn1qkb1r/pbpp1p2/1p2p2p/6pn/2PP4/2N2NB1/PP2PPPP/R2QKB1R w
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Andersson Variation		rn1qkb1r/pbpp1ppp/1p2p3/8/2PPn3/P1N2N2/1P2PPPP/R1BQKB1R w
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Classical Variation		rn1qkb1r/pbp2ppp/1p3n2/3p4/3P4/P1N2N2/1P2PPPP/R1BQKB1R w
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Kasparov Attack		rn1qkb1r/pbp2ppp/1p2p3/3n4/3P4/P1N2N2/1PQ1PPPP/R1B1KB1R b
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Main Line		rn1qkb1r/pbp2ppp/1p2pn2/3p4/2PP4/P1N2N2/1P2PPPP/R1BQKB1R w
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Marco Defense		rn1qk2r/pbppbppp/1p2pn2/8/2PP4/P1N2N2/1P2PPPP/R1BQKB1R w
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Modern Variation		rn1qkb1r/pbp2ppp/1p2p3/3n4/3P4/P1N2N2/1P2PPPP/R1BQKB1R w
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Petrosian Attack		rn1qkb1r/pbp2ppp/1p2p3/3n4/3P4/P1N1PN2/1P3PPP/R1BQKB1R b
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Polovodin Gambit		rn1qkb1r/p1p2ppp/1p2p3/8/3Pb3/P1P2N2/5PPP/R1BQKB1R w
E12	Queen's Indian Defense: Kasparov-Petrosian Variation, Romanishin Attack		rn1qkb1r/pbp2ppp/1p2p3/3n4/3P4/P1N2N2/1P1BPPPP/R2QKB1R b
E12	Queen's Indian Defense: Miles Variation		rnbqkb1r/p1pp1ppp/1p2pn2/8/2PP1B2/5N2/PP2PPPP/RN1QKB1R b
E12	Queen's Indian Defense: Petrosian Variation		rnbqkb1r/p1pp1ppp/1p2pn2/8/2PP4/P4N2/1P2PPPP/RNBQKB1R b
E13	Queen's Indian Defense: Kasparov Variation		rn1qk2r/pbpp1pp1/1p2pn1p/8/1bPP3B/2N2N2/PP2PPPP/R2QKB1R w
E14	Queen's Indian Defense: Spassky System		rnbqkb1r/p1pp1ppp/1p2pn2/8/2PP4/4PN2/PP3PPP/RNBQKB1R b
E15	Queen's Indian Defense: Fianchetto Traditional		rn1qkb1r/pbpp1ppp/1p2pn2/8/2PP4/5NP1/PP2PP1P/RNBQKB1R w
E15	Queen's Indian Defense: Fianchetto Variation		rnbqkb1r/p1pp1ppp/1p2pn2/8/2PP4/5NP1/PP2PP1P/RNBQKB1R b
E15	Queen's Indian Defense: Fianchetto Variation, Check Variation		rn1qk2r/p1pp1ppp/bp2pn2/8/1bPP4/1P3NP1/P3PP1P/RNBQKB1R w
E15	Queen's Indian Defense: Fianchetto Variation, Check Variation, Intermezzo Line		rn1qk2r/p1ppbppp/bp2pn2/8/2PP4/1P3NP1/P2BPP1P/RN1QKB1R w
E15	Queen's Indian Defense: Fianchetto Variation, Check Variation, Modern Line		rn2k2r/p1ppqppp/bp2pn2/8/1bPP4/1P3NP1/P2BPP1P/RN1QKB1R w
E15	Queen's Indian Defense: Fianchetto Variation, Nimzowitsch Variation		rn1qkb1r/p1pp1ppp/bp2pn2/8/2PP4/5NP1/PP2PP1P/RNBQKB1R w
E15	Queen's Indian Defense: Fianchetto Variation, Nimzowitsch Variation, Nimzowitsch Attack		rn1qkb1r/p1pp1ppp/bp2pn2/8/Q1PP4/5NP1/PP2PP1P/RNB1KB1R b
E15	Queen's Indian Defense: Fianchetto Variation, Nimzowitsch Variation, Quiet Line		rn1qkb1r/p1pp1ppp/bp2pn2/8/2PP4/1P3NP1/P3PP1P/RNBQKB1R b
E15	Queen's Indian Defense: Fianchetto Variation, Nimzowitsch Variation, Timman's Line		rn1qkb1r/p1pp1ppp/bp2pn2/8/2PP4/1Q3NP1/PP2PP1P/RNB1KB1R b
E15	Queen's Indian Defense: Fianchetto Variation, Sämisch Variation		rn1qkb1r/pb1p1ppp/1p2pn2/2p5/2PP4/5NP1/PP2PPBP/RNBQK2R w
E15	Queen's Indian, Buerger Variation		rn1qkb1r/pb1p1ppp/1p3n2/2pp2N1/2P5/6P1/PP2PPBP/RNBQK2R b
E16	Queen's Indian Defense: Capablanca Variation		rn1qk2r/pbpp1ppp/1p2pn2/8/1bPP4/5NP1/PP2PPBP/RNBQK2R w
E16	Queen's Indian Defense: Fianchetto Variation, Rubinstein Variation		rn1qkb1r/pb1p1ppp/1p3n2/2pp4/2P4N/6P1/PP2PPBP/RNBQK2R b
E16	Queen's Indian Defense: Riumin Variation		rn1qk2r/pbppbppp/1p2pn2/8/2PP4/5NP1/PP1BPPBP/RN1QK2R w
E16	Queen's Indian Defense: Yates Variation		rn1qk2r/1bpp1ppp/1p2pn2/p7/1bPP4/5NP1/PP1BPPBP/RN1QK2R w
E17	Queen's Indian Defense: Anti-Queen's Indian System		rn1qk2r/pbppbppp/1p2pn2/8/2PP4/2N2NP1/PP2PPBP/R1BQK2R b
E17	Queen's Indian Defense: Classical Variation		rn1qk2r/pbppbppp/1p2pn2/8/2PP4/5NP1/PP2PPBP/RNBQ1RK1 b
E17	Queen's Indian Defense: Classical Variation, Polugaevsky Gambit		rn1q1rk1/pbppbppp/1p3n2/3p4/2P4N/6P1/PP2PPBP/RNBQ1RK1 b
E17	Queen's Indian Defense: Classical Variation, Taimanov Gambit		rn1q1rk1/pbppbppp/1p3n2/3p4/2PN4/6P1/PP2PPBP/RNBQ1RK1 b
E17	Queen's Indian Defense: Classical Variation, Tiviakov Defense		r2q1rk1/pbppbppp/np2pn2/8/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E17	Queen's Indian Defense: Classical Variation, Traditional Variation		rn1q1rk1/pbppbppp/1p2pn2/8/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 b
E17	Queen's Indian Defense: Euwe Variation		rn1q1rk1/pbppbppp/1p2pn2/8/2PP4/1P3NP1/P3PPBP/RNBQ1RK1 b
E17	Queen's Indian Defense: Fianchetto Variation, Kramnik Variation		rn1q1rk1/pbppbppp/1p2pn2/8/2PP4/5NP1/PP2PPBP/RNBQR1K1 b
E17	Queen's Indian Defense: Kasparov-Petrosian Variation		rn1qkb1r/pbpp1ppp/1p2pn2/8/2PP4/P1N2N2/1P2PPPP/R1BQKB1R b
E17	Queen's Indian Defense: Kasparov-Petrosian Variation, Hedgehog Variation		rn1qkb1r/pbpp1p1p/1p2pnp1/8/2PP4/P1N2N2/1P2PPPP/R1BQKB1R w
E17	Queen's Indian Defense: Opocensky Variation		rn1qk2r/pbppbppp/1p2p3/8/2PPn3/2N2NP1/PP1BPPBP/R2QK2R b
E17	Queen's Indian Defense: Traditional Variation		rn1qk2r/pbppbppp/1p2pn2/8/2PP4/5NP1/PP2PPBP/RNBQK2R w
E18	Queen's Indian Defense: Classical Variation, Traditional Variation, Nimzowitsch Line		rn1q1rk1/pbp1bppp/1p2pn2/3p4/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E19	Queen's Indian Defense: Classical Variation, Traditional Variation, Main Line		rn1q1rk1/pbppbppp/1p2p3/8/2PP4/2Q2NP1/PP2PPBP/R1B2RK1 b
E20	Nimzo-Indian Defense		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/2N5/PP2PPPP/R1BQKBNR w
E20	Nimzo-Indian Defense #2		rnbqkb1r/pppp1ppp/4pn2/8/2PP4/2N5/PP2PPPP/R1BQKBNR b
E20	Nimzo-Indian Defense: Kmoch Variation		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/2N2P2/PP2P1PP/R1BQKBNR b
E20	Nimzo-Indian Defense: Mikenas Attack		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/2NQ4/PP2PPPP/R1B1KBNR b
E20	Nimzo-Indian Defense: Ragozin Variation		rnbqk2r/ppp2ppp/4pn2/3p4/1bPP4/2N2N2/PP2PPPP/R1BQKB1R w
E20	Nimzo-Indian Defense: Romanishin Variation		rnbq1rk1/pp1p1ppp/4pn2/2p5/1bPP4/2N2NP1/PP2PPBP/R1BQK2R b
E20	Nimzo-Indian Defense: Romanishin Variation #2		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/2N3P1/PP2PP1P/R1BQKBNR b
E20	Nimzo-Indian Defense: Romanishin Variation, English Hybrid		rnbq1rk1/pp3ppp/4p3/3n4/1b1N4/2N3P1/PP2PPBP/R1BQK2R w
E21	Nimzo-Indian Defense: Three Knights Variation		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/2N2N2/PP2PPPP/R1BQKB1R b
E21	Nimzo-Indian Defense: Three Knights Variation, Duchamp Variation		rnbqk2r/p1pp1ppp/1p2pn2/8/1bPP4/2N2N2/PP2PPPP/R1BQKB1R w
E21	Nimzo-Indian Defense: Three Knights Variation, Duchamp Variation, Modern Line		rn1qk2r/pbpp1ppp/1p2pn2/6B1/1bPP4/2N5/PP1NPPPP/R2QKB1R b
E21	Nimzo-Indian Defense: Three Knights Variation, Korchnoi Variation		rnbqk2r/pp1p1ppp/4pn2/2pP4/1bP5/2N2N2/PP2PPPP/R1BQKB1R b
E21	Nimzo-Indian Defense: Three Knights Variation, Shocron Gambit		rnbqk2r/p2p1ppp/4pn2/1ppP4/1bP5/2N2N2/PP2PPPP/R1BQKB1R w
E21	Nimzo-Indian Defense: Three Knights, Euwe Variation		rnbqk2r/pp1p1ppp/4p3/2pP4/1bP1n3/2N2N2/PP2PPPP/R1BQKB1R w
E22	Nimzo-Indian Defense: Spielmann Variation		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/1QN5/PP2PPPP/R1B1KBNR b
E23	Nimzo-Indian Defense: Spielmann Variation, Karlsbad Variation		r1bqk2r/pp1p1ppp/2n1p3/2P5/1bP5/1QN5/PP1NPPPP/R3KB1R b
E23	Nimzo-Indian Defense: Spielmann Variation, Romanovsky Gambit		r1bqk2r/pp1p1ppp/2n1pn2/2P5/1bP5/1QN5/PP2PPPP/R1B1KBNR w
E24	Nimzo-Indian Defense: Sämisch Variation, Accelerated		rnbqk2r/pppp1ppp/4pn2/8/2PP4/P1P5/4PPPP/R1BQKBNR b
E24	Nimzo-Indian Defense: Sämisch, Botvinnik Variation		rnbq1rk1/pp3ppp/4p3/2pn4/3P4/P1P1PP2/6PP/R1BQKBNR w
E25	Nimzo-Indian Defense: Sämisch Variation		rnbqk2r/pp3ppp/4pn2/2pP4/3P4/P1P2P2/4P1PP/R1BQKBNR b
E25	Nimzo-Indian Defense: Sämisch Variation, Keres Variation		rnbqk2r/pp3ppp/4p3/2Pn4/8/P1P2P2/4P1PP/R1BQKBNR b
E25	Nimzo-Indian Defense: Sämisch, Romanovsky Variation		rnbqk2r/pp4pp/4p3/2Pn1p2/8/P1P2P2/4P1PP/R1BQKBNR w
E26	Nimzo-Indian Defense: Sämisch Variation		rnbqk2r/pp1p1ppp/4pn2/2p5/2PP4/P1P1P3/5PPP/R1BQKBNR b
E26	Nimzo-Indian Defense: Sämisch Variation, O'Kelly Variation		rnbqk2r/p2p1ppp/1p2pn2/2p5/2PP4/P1P1P3/5PPP/R1BQKBNR w
E27	Nimzo-Indian Defense: Sämisch Variation		rnbq1rk1/pppp1ppp/4pn2/8/2PP4/P1P5/4PPPP/R1BQKBNR w
E28	Nimzo-Indian Defense: Sämisch Variation		rnbq1rk1/pppp1ppp/4pn2/8/2PP4/P1P1P3/5PPP/R1BQKBNR b
E29	Nimzo-Indian Defense: Sämisch Variation		r1bq1rk1/pp1p1ppp/2n1pn2/2p5/2PP4/P1PBP3/5PPP/R1BQK1NR w
E29	Nimzo-Indian Defense: Sämisch Variation, Capablanca Variation		r1bqnrk1/p2p1ppp/1pn1p3/2p5/2PPP3/P1PB4/4NPPP/R1BQK2R w
E30	Nimzo-Indian Defense: Leningrad Variation		rnbqk2r/pppp1ppp/4pn2/6B1/1bPP4/2N5/PP2PPPP/R2QKBNR b
E30	Nimzo-Indian Defense: Leningrad Variation, Averbakh Gambit		rnbqk2r/p2p1pp1/4pn1p/1ppP4/1bP4B/2N5/PP2PPPP/R2QKBNR w
E31	Nimzo-Indian Defense: Leningrad Variation, Benoni Defense		rnbqk2r/pp3pp1/3ppn1p/2pP4/1bP4B/2N5/PP2PPPP/R2QKBNR w
E32	Nimzo-Indian Defense: Classical Variation		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/2N5/PPQ1PPPP/R1B1KBNR b
E32	Nimzo-Indian Defense: Classical Variation, Keres Defense		rnbq1rk1/p1pp1ppp/1p2pn2/8/2PP4/P1Q5/1P2PPPP/R1B1KBNR w
E32	Nimzo-Indian Defense: Classical Variation, Vitolins-Adorjan Gambit		rnbq1rk1/p1pp1ppp/4pn2/1p6/2PP4/P1Q5/1P2PPPP/R1B1KBNR w
E33	Nimzo-Indian Defense: Classical Variation, Milner-Barry Variation		r1bqk2r/ppp2ppp/2nppn2/8/1bPP4/2N2N2/PPQ1PPPP/R1B1KB1R w
E33	Nimzo-Indian Defense: Classical Variation, Zurich Variation		r1bqk2r/pppp1ppp/2n1pn2/8/1bPP4/2N5/PPQ1PPPP/R1B1KBNR w
E34	Nimzo-Indian Defense: Classical Variation, Noa Variation		rnbqk2r/ppp2ppp/4pn2/3p4/1bPP4/2N5/PPQ1PPPP/R1B1KBNR w
E35	Nimzo-Indian Defense: Classical, Noa Variation, 5.cxd5 exd5		rnbqk2r/ppp2ppp/5n2/3p4/1b1P4/2N5/PPQ1PPPP/R1B1KBNR w
E36	Nimzo-Indian Defense: Classical Variation, Noa Variation		rnbqk2r/ppp2ppp/4pn2/3p4/1bPP4/P1N5/1PQ1PPPP/R1B1KBNR b
E36	Nimzo-Indian Defense: Classical Variation, Noa Variation, Botvinnik Variation		r1bqk2r/ppp2ppp/2n1pn2/3p4/2PP4/P1Q5/1P2PPPP/R1B1KBNR w
E36	Nimzo-Indian Defense: Classical, Noa Variation, Main Line		rnbqk2r/ppp2ppp/4p3/3p4/2PPn3/P1Q5/1P2PPPP/R1B1KBNR w
E37	Nimzo-Indian Defense: Classical Variation, Noa Variation		rnbqk2r/ppp2ppp/4p3/3p4/2PPn3/P7/1PQ1PPPP/R1B1KBNR b
E37	Nimzo-Indian Defense: Classical Variation, Noa Variation, San Remo Variation		r1bqk2r/ppp2ppp/2n5/3pp3/2PPn3/P3P3/1PQ2PPP/R1B1KBNR w
E38	Nimzo-Indian Defense: Classical Variation, Berlin Variation		rnbqk2r/pp1p1ppp/4pn2/2p5/1bPP4/2N5/PPQ1PPPP/R1B1KBNR w
E39	Nimzo-Indian Defense: Classical Variation, Berlin Variation, Macieja System		rnbq1rk1/p2p1ppp/1p2pn2/2b5/2P2B2/P1N2N2/1PQ1PPPP/R3KB1R b
E39	Nimzo-Indian Defense: Classical Variation, Berlin Variation, Pirc Variation		rnbq1rk1/pp1p1ppp/4pn2/2P5/1bP5/2N5/PPQ1PPPP/R1B1KBNR w
E39	Nimzo-Indian Defense: Classical Variation, Berlin Variation, Steiner Variation		rnbqk2r/pp1p1ppp/4pn2/2P5/2P5/2Q5/PP2PPPP/R1B1KBNR b
E40	Nimzo-Indian Defense: Normal Line		rnbqk2r/pppp1ppp/4pn2/8/1bPP4/2N1P3/PP3PPP/R1BQKBNR b
E40	Nimzo-Indian Defense: Normal Variation, Taimanov Variation		r1bqk2r/pppp1ppp/2n1pn2/8/1bPP4/2N1P3/PP3PPP/R1BQKBNR w
E41	Nimzo-Indian Defense: Hübner Variation		rnbqk2r/pp1p1ppp/4pn2/2p5/1bPP4/2N1P3/PP3PPP/R1BQKBNR w
E41	Nimzo-Indian Defense: Hübner Variation, Main Line		r1bqk2r/pp3ppp/2nppn2/2p5/2PP4/2PBPN2/P4PPP/R1BQK2R w
E42	Nimzo-Indian Defense: Hübner Variation, Rubinstein Variation		rnbqk2r/pp1p1ppp/4pn2/2p5/1bPP4/2N1P3/PP2NPPP/R1BQKB1R b
E42	Nimzo-Indian Defense: Hübner Variation, Rubinstein Variation, Main Line		rnbq1rk1/pp1p1ppp/4pn2/8/1bPP4/P1N5/1P2NPPP/R1BQKB1R b
E42	Nimzo-Indian Defense: Hübner Variation, Rubinstein Variation, Sherbakov Attack		5rk1/3p1ppp/p1bN2r1/1pP5/1P6/P1q5/5RPP/R4QK1 w
E43	Nimzo-Indian Defense: St. Petersburg Variation		rnbqk2r/p1pp1ppp/1p2pn2/8/1bPP4/2N1P3/PP3PPP/R1BQKBNR w
E44	Nimzo-Indian Defense: Fischer Variation		rnbqk2r/p1pp1ppp/1p2pn2/8/1bPP4/2N1P3/PP2NPPP/R1BQKB1R b
E45	Nimzo-Indian Defense: Normal Variation, Bronstein (Byrne) Variation		rn1qk2r/p1pp1ppp/bp2pn2/8/1bPP4/2N1P3/PP2NPPP/R1BQKB1R w
E46	Nimzo-Indian Defense: Normal Variation		rnbq1rk1/pppp1ppp/4pn2/8/1bPP4/2N1P3/PP3PPP/R1BQKBNR w
E46	Nimzo-Indian Defense: Ragozin Defense		r1bq1rk1/ppp2ppp/2n1pn2/3p4/1bPP4/2NBPN2/PP3PPP/R1BQ1RK1 b
E46	Nimzo-Indian Defense: Reshevsky Variation		rnbq1rk1/pppp1ppp/4pn2/8/1bPP4/2N1P3/PP2NPPP/R1BQKB1R b
E46	Nimzo-Indian Defense: Simagin Variation		rnbq1rk1/ppp2ppp/3bpn2/3p4/2PP4/P1N1P3/1P2NPPP/R1BQKB1R w
E47	Nimzo-Indian Defense: Normal Variation, Bishop Attack		rnbq1rk1/pppp1ppp/4pn2/8/1bPP4/2NBP3/PP3PPP/R1BQK1NR b
E48	Nimzo-Indian Defense: Normal Variation, Bishop Attack, Classical Defense		rnbq1rk1/ppp2ppp/4pn2/3p4/1bPP4/2NBP3/PP3PPP/R1BQK1NR w
E49	Nimzo-Indian Defense: Normal Variation, Botvinnik System		rnbq1rk1/ppp2ppp/4pn2/3p4/2PP4/P1PBP3/5PPP/R1BQK1NR b
E50	Nimzo-Indian Defense: 4.e3 e8g8, 5.Nf3, without ...d5		rnbq1rk1/pppp1ppp/4pn2/8/1bPP4/2N1PN2/PP3PPP/R1BQKB1R b
E50	Nimzo-Indian Defense: Normal Variation, Hübner Deferred		rnbq1rk1/pp1p1ppp/4pn2/2p5/1bPP4/2N1PN2/PP3PPP/R1BQKB1R w
E51	Nimzo-Indian Defense: 4.e3, Ragozin Variation		r1bq1rk1/ppp2ppp/2n1pn2/8/1bBP4/2N1PN2/PP3PPP/R1BQ1RK1 b
E51	Nimzo-Indian Defense: Normal Variation, Ragozin Variation		rnbq1rk1/ppp2ppp/4pn2/3p4/1bPP4/2N1PN2/PP3PPP/R1BQKB1R w
E51	Nimzo-Indian Defense: Normal Variation, Sämisch Deferred		rnbq1rk1/ppp2ppp/4pn2/3p4/1bPP4/P1N1PN2/1P3PPP/R1BQKB1R b
E52	Nimzo-Indian Defense: Normal Variation, Schlechter Defense		rnbq1rk1/p1p2ppp/1p2pn2/3p4/1bPP4/2NBPN2/PP3PPP/R1BQK2R w
E53	Nimzo-Indian Defense: Normal Variation, Gligoric System		rnbq1rk1/pp3ppp/4pn2/2pp4/1bPP4/2NBPN2/PP3PPP/R1BQK2R w
E53	Nimzo-Indian Defense: Normal Variation, Gligoric System #2		r1bq1rk1/pp1n1ppp/4pn2/2pp4/1bPP4/2NBPN2/PP3PPP/R1BQ1RK1 w
E53	Nimzo-Indian Defense: Normal Variation, Gligoric System, Keres Variation		rnbq1rk1/p4ppp/1p2pn2/2pp4/1bPP4/2NBPN2/PP3PPP/R1BQ1RK1 w
E54	Nimzo-Indian Defense: Normal Variation, Gligoric System, Exchange at c4		rnbq1rk1/pp3ppp/4pn2/2p5/1bBP4/2N1PN2/PP3PPP/R1BQ1RK1 b
E54	Nimzo-Indian Defense: Normal Variation, Gligoric System, Smyslov Variation		rnb2rk1/pp2qppp/4pn2/2p5/1bBP4/2N1PN2/PP3PPP/R1BQ1RK1 w
E54	Nimzo-Indian Defense: Panov Attack, Main Line		rnbqk2r/pp3ppp/4pn2/3p4/1bPP4/2N2N2/PP3PPP/R1BQKB1R w
E55	Nimzo-Indian Defense: Normal Variation, Gligoric System, Bronstein Variation		r1bq1rk1/pp1n1ppp/4pn2/2p5/1bBP4/2N1PN2/PP3PPP/R1BQ1RK1 w
E56	Nimzo-Indian Defense: Normal Variation, Gligoric System, Bernstein Defense		r1bq1rk1/pp3ppp/2n1pn2/2pp4/1bPP4/2NBPN2/PP3PPP/R1BQ1RK1 w
E57	Nimzo-Indian Defense: 4.e3, Main Line, with 8...dxc4 and 9...cxd4		r1bq1rk1/pp3ppp/2n1pn2/8/1bBp4/P1N1PN2/1P3PPP/R1BQ1RK1 w
E57	Nimzo-Indian Defense: Normal Variation, Bernstein Defense, Exchange Line		r1bq1rk1/pp3ppp/2n1pn2/2pp4/2PP4/P1PBPN2/5PPP/R1BQ1RK1 b
E60	Grünfeld Defense: Counterthrust Variation		rnbqk2r/ppp1ppbp/5np1/3p4/2PP4/6P1/PP2PPBP/RNBQK1NR w
E60	Indian Game: Anti-Grünfeld, Adorjan Gambit		rnbqkb1r/p1pppp1p/5np1/1p1P4/2P5/8/PP2PPPP/RNBQKBNR w
E60	Indian Game: Anti-Grünfeld, Advance Variation		rnbqkb1r/pppppp1p/5np1/3P4/2P5/8/PP2PPPP/RNBQKBNR b
E60	King's Indian Defense: Fianchetto Variation, Immediate Fianchetto		rnbqkb1r/pppppp1p/5np1/8/2PP4/6P1/PP2PP1P/RNBQKBNR b
E60	King's Indian Defense: Normal Variation, King's Knight Variation		rnbqkb1r/pppppp1p/5np1/8/2PP4/5N2/PP2PPPP/RNBQKB1R b
E60	King's Indian Defense: Santasiere Variation		rnbqk2r/ppppppbp/5np1/8/1PPP4/5N2/P3PPPP/RNBQKB1R b
E60	Queen's Pawn, Mengarini Attack		rnbqkb1r/pppppp1p/5np1/8/2PP4/8/PPQ1PPPP/RNB1KBNR b
E61	Indian Game: West Indian Defense		rnbqkb1r/pppppp1p/5np1/8/2PP4/8/PP2PPPP/RNBQKBNR w
E61	King's Indian Defense		rnbqk2r/ppppppbp/5np1/8/2PP4/2N5/PP2PPPP/R1BQKBNR w
E61	King's Indian Defense: 3.Nc3		rnbqkb1r/pppppp1p/5np1/8/2PP4/2N5/PP2PPPP/R1BQKBNR b
E61	King's Indian Defense: Fianchetto Variation, Benjamin Defense		rnb2rk1/pp2ppbp/1qpp1np1/8/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E61	King's Indian Defense: Semi-Classical Variation		rnbq1rk1/ppp1ppbp/3p1np1/8/2PP4/2N1PN2/PP2BPPP/R1BQK2R b
E61	King's Indian Defense: Semi-Classical Variation, Benoni Variation		rnbq1rk1/pp2ppbp/2pp1np1/8/2PP4/2N1PN2/PP2BPPP/R1BQK2R w
E61	King's Indian Defense: Semi-Classical Variation, Exchange Variation		r1bq1rk1/pppn1pbp/5np1/4p3/2P5/2N1PN2/PP2BPPP/R1BQ1RK1 w
E61	King's Indian Defense: Semi-Classical Variation, Hollywood Variation		r1bq1rk1/ppp1ppbp/2np1np1/8/2PP4/2N1PN2/PP2BPPP/R1BQK2R w
E61	King's Indian Defense: Semi-Classical Variation, Queenside Storm Line		r1bq1rk1/pppn1pbp/3p1np1/4p3/1PPP4/2N1PN2/P3BPPP/R1BQ1RK1 b
E61	King's Indian Defense: Smyslov Variation		rnbqk2r/ppp1ppbp/3p1np1/6B1/2PP4/2N2N2/PP2PPPP/R2QKB1R b
E62	King's Indian Defense: Fianchetto Variation, Delayed Fianchetto		rnbqk2r/ppp1ppbp/3p1np1/8/2PP4/2N2NP1/PP2PP1P/R1BQKB1R b
E62	King's Indian Defense: Fianchetto Variation, Karlsbad Variation		r1bq1rk1/ppp1ppbp/2np1np1/8/2PP4/2N2NP1/PP2PPBP/R1BQK2R w
E62	King's Indian Defense: Fianchetto Variation, Kavalek Defense		rnb2rk1/pp2ppbp/2pp1np1/q7/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E62	King's Indian Defense: Fianchetto Variation, Larsen Defense		rn1q1rk1/pp2ppbp/2pp1np1/5b2/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E62	King's Indian Defense: Fianchetto Variation, Lesser Simagin (Spassky)		r2q1rk1/ppp1ppbp/2np1np1/5b2/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E62	King's Indian Defense: Fianchetto Variation, Simagin Variation		r2q1rk1/ppp1ppbp/2np1np1/8/2PP2b1/2N2NP1/PP2PPBP/R1BQ1RK1 w
E62	King's Indian Defense: Fianchetto Variation, Uhlmann-Szabo System		r1bq1rk1/ppp2pbp/2np1np1/4p3/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E63	King's Indian Defense: Fianchetto Variation, Panno Variation		r1bq1rk1/1pp1ppbp/p1np1np1/8/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E63	King's Indian Defense: Fianchetto Variation, Panno Variation, Blockade Line		3q2k1/8/Q2p2pb/1bpPp2p/4P1nP/5NP1/Pr4B1/R4NK1 w
E63	King's Indian Defense: Fianchetto Variation, Panno Variation, Donner Line		1rbq1rk1/4pp1p/p2p1npb/n1pP4/2P2P2/2N3P1/PBQNP1BP/R4RK1 b
E64	King's Indian Defense: Fianchetto Variation, Double Fianchetto Attack		r1bq1rk1/pppn1pbp/3p1np1/4p3/2PP4/1PN2NP1/P3PPBP/R1BQ1RK1 b
E64	King's Indian Defense: Fianchetto Variation, Hungarian Variation		r1bq1rk1/1ppnppbp/p2p1np1/8/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E64	King's Indian Defense: Fianchetto Variation, Pterodactyl Variation		rnb1k2r/pp1pppbp/1q3np1/2p5/2PP4/5NP1/PP1BPPBP/RN1QK2R w
E64	King's Indian Defense: Fianchetto Variation, Yugoslav System, without Nc3		rnbq1rk1/pp2ppbp/3p1np1/2p5/2PP4/5NP1/PP2PPBP/RNBQ1RK1 w
E64	King's Indian Defense: Fianchetto Variation, Yugoslav Variation, Rare Line		rnbq1rk1/pp2ppbp/3p1np1/2p5/2PP4/2N2NP1/PP2PPBP/R1BQK2R w
E65	King's Indian Defense: Fianchetto Variation, Yugoslav Variation		rnbq1rk1/pp2ppbp/3p1np1/2p5/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 b
E66	King's Indian Defense: Fianchetto Variation, Yugoslav Variation, Advance Line		r1bq1rk1/pp2ppbp/2np1np1/2pP4/2P5/2N2NP1/PP2PPBP/R1BQ1RK1 b
E66	King's Indian Defense: Fianchetto Variation, Yugoslav Variation, Exchange Line		r1bq1rk1/pp2ppbp/2n2np1/2p5/2P5/2N2NP1/PP2PPBP/R1BQ1RK1 w
E67	King's Indian Defense: Fianchetto Variation, Classical Fianchetto		r1bq1rk1/pppn1pbp/3p1np1/4p3/2PP4/2N2NP1/PP2PPBP/R1BQ1RK1 w
E67	King's Indian Defense: Fianchetto Variation, Debrecen Defense		r1bq1rk1/pppnppbp/3p1np1/8/2PP4/2N2NP1/PP2PPBP/R1BQK2R w
E68	King's Indian Defense: Fianchetto Variation, Long Variation		r1bqr1k1/1pp2pbp/3p1np1/p1n5/2PNP3/2N3PP/PP3PB1/R1BQR1K1 w
E68	King's Indian, Fianchetto, Classical Variation, 8.e4		r1bq1rk1/pppn1pbp/3p1np1/4p3/2PPP3/2N2NP1/PP3PBP/R1BQ1RK1 b
E69	King's Indian Defense: Fianchetto Variation, Classical Main Line		r1bq1rk1/pp1n1pbp/2pp1np1/4p3/2PPP3/2N2NPP/PP3PB1/R1BQ1RK1 b
E70	King's Indian Defense: Accelerated Averbakh Variation		rnbqk2r/ppp1ppbp/3p1np1/6B1/2PPP3/2N5/PP3PPP/R2QKBNR b
E70	King's Indian Defense: Kramer Variation		rnbqk2r/ppp1ppbp/3p1np1/8/2PPP3/2N5/PP2NPPP/R1BQKB1R b
E71	King's Indian Defense: Makogonov Variation		rnbqk2r/ppp1ppbp/3p1np1/8/2PPP3/2N4P/PP3PP1/R1BQKBNR b
E72	King's Indian Defense: Normal Variation, Deferred Fianchetto		rnbqk2r/ppp1ppbp/3p1np1/8/2PPP3/2N3P1/PP3P1P/R1BQKBNR b
E72	King's Indian Defense: Pomar System		rnbq1rk1/ppp2pbp/3p1np1/4p3/2PPP3/2N3P1/PP2NPBP/R1BQK2R b
E73	King's Indian Defense: Averbakh Variation		rnbq1rk1/ppp1ppbp/3p1np1/6B1/2PPP3/2N5/PP2BPPP/R2QK1NR b
E73	King's Indian Defense: Averbakh Variation, Flexible Defense		rnbq1rk1/ppp1ppb1/3p1npp/6B1/2PPP3/2N5/PP2BPPP/R2QK1NR w
E73	King's Indian Defense: Averbakh Variation, Geller Defense		r1bq1rk1/pppnppbp/3p1np1/6B1/2PPP3/2N5/PP2BPPP/R2QK1NR w
E73	King's Indian Defense: Averbakh Variation, Modern Defense		r1bq1rk1/ppp1ppbp/n2p1np1/6B1/2PPP3/2N5/PP2BPPP/R2QK1NR w
E73	King's Indian Defense: Averbakh Variation, Modern Defense, Burgess Line		r1bq1rk1/pp2ppbp/n1pp1np1/6B1/2PPP3/2N5/PP1QBPPP/R3K1NR w
E73	King's Indian Defense: Averbakh Variation, Nc6 Defense		r1bq1rk1/ppp1ppbp/2np1np1/6B1/2PPP3/2N5/PP2BPPP/R2QK1NR w
E73	King's Indian Defense: Averbakh Variation, Spanish Defense		rnbq1rk1/1pp1ppbp/p2p1np1/6B1/2PPP3/2N5/PP2BPPP/R2QK1NR w
E73	King's Indian Defense: Normal Variation, Standard Development		rnbqk2r/ppp1ppbp/3p1np1/8/2PPP3/2N5/PP2BPPP/R1BQK1NR b
E73	King's Indian Defense: Semi-Averbakh System		rnbq1rk1/ppp1ppbp/3p1np1/8/2PPP3/2N1B3/PP2BPPP/R2QK1NR b
E74	King's Indian Defense: Averbakh Variation, Benoni Defense		rnbq1rk1/pp2ppbp/3p1np1/2p3B1/2PPP3/2N5/PP2BPPP/R2QK1NR w
E74	King's Indian Defense: Averbakh Variation, Benoni Defense, Exchange Variation		rnbq1rk1/pp2ppbp/3p1np1/2P3B1/2P1P3/2N5/PP2BPPP/R2QK1NR b
E75	King's Indian Defense: Averbakh Variation, Benoni Defense, Advance Variation		rnbq1rk1/pp2ppbp/3p1np1/2pP2B1/2P1P3/2N5/PP2BPPP/R2QK1NR b
E75	King's Indian, Averbakh, Main Line		rnbq1rk1/pp3pbp/3ppnp1/2pP2B1/2P1P3/2N5/PP2BPPP/R2QK1NR w
E76	King's Indian Defense: Four Pawns Attack		rnbqk2r/ppp1ppbp/3p1np1/8/2PPPP2/2N5/PP4PP/R1BQKBNR b
E76	King's Indian Defense: Four Pawns Attack, Dynamic Attack		rnbq1rk1/pp2ppbp/3p1np1/2pP4/2P1PP2/2N2N2/PP4PP/R1BQKB1R b
E76	King's Indian Defense: Four Pawns Attack, Modern Defense		r1bqk2r/ppp1ppbp/n2p1np1/8/2PPPP2/2N5/PP4PP/R1BQKBNR w
E76	King's Indian Defense: Steiner Attack		rnbq1rk1/ppp1ppbp/3p1np1/6B1/2PPP3/2N2P2/PP4PP/R2QKBNR b
E77	King's Indian Defense: Four Pawns Attack		rnbq1rk1/ppp1ppbp/3p1np1/8/2PPPP2/2N5/PP2B1PP/R1BQK1NR b
E77	King's Indian Defense: Four Pawns Attack, Florentine Gambit		rnbq1rk1/pp3pbp/3p1np1/2ppP3/2P2P2/2N2N2/PP2B1PP/R1BQK2R b
E77	King's Indian Defense: Four Pawns Attack, Normal Attack		rnbq1rk1/pp3pbp/3ppnp1/2pP4/2P1PP2/2N2N2/PP2B1PP/R1BQK2R b
E78	King's Indian Defense: Four Pawns Attack, Fluid Attack		rnbq1rk1/pp2ppbp/3p1np1/2p5/2PPPP2/2N2N2/PP2B1PP/R1BQK2R b
E79	King's Indian Defense: Four Pawns Attack, Exchange Variation		r1bq1rk1/pp2ppbp/2np1np1/8/2PNPP2/2N1B3/PP2B1PP/R2QK2R b
E80	King's Indian Defense: Sämisch Variation		rnbqk2r/ppp1ppbp/3p1np1/8/2PPP3/2N2P2/PP4PP/R1BQKBNR b
E81	King's Indian Defense: Sämisch Variation, Bobotsov-Korchnoi-Petrosian Variation		rnbq1rk1/ppp1ppbp/3p1np1/8/2PPP3/2N2P2/PP2N1PP/R1BQKB1R b
E81	King's Indian Defense: Sämisch Variation, Byrne Defense		rnbq1rk1/1p2ppbp/p1pp1np1/8/2PPP3/2NBBP2/PP4PP/R2QK1NR w
E81	King's Indian Defense: Sämisch Variation, Normal Defense		rnbq1rk1/ppp1ppbp/3p1np1/8/2PPP3/2N2P2/PP4PP/R1BQKBNR w
E82	King's Indian Defense: Sämisch Variation, Double Fianchetto		rnbq1rk1/p1p1ppbp/1p1p1np1/8/2PPP3/2N1BP2/PP4PP/R2QKBNR w
E83	King's Indian Defense: Sämisch Variation, Panno Formation		r1bq1rk1/1pp1ppbp/p1np1np1/8/2PPP3/2N1BP2/PP2N1PP/R2QKB1R w
E83	King's Indian Defense: Sämisch Variation, Yates Defense		r1bq1rk1/ppp1ppbp/2np1np1/8/2PPP3/2N1BP2/PP4PP/R2QKBNR w
E83	King's Indian, Sämisch, Ruban Variation		1rbq1rk1/ppp1ppbp/2np1np1/8/2PPP3/2N1BP2/PP2N1PP/R2QKB1R w
E84	King's Indian, Sämisch, Panno Main Line		1rbq1rk1/1pp1ppbp/p1np1np1/8/2PPP3/2N1BP2/PP1QN1PP/R3KB1R w
E85	King's Indian Defense: Sämisch Variation, Orthodox Variation		rnbq1rk1/ppp2pbp/3p1np1/4p3/2PPP3/2N1BP2/PP4PP/R2QKBNR w
E86	King's Indian Defense: Sämisch Variation		rnbq1rk1/pp3pbp/2pp1np1/4p3/2PPP3/2N1BP2/PP2N1PP/R2QKB1R w
E87	King's Indian Defense: Sämisch Variation, Closed Variation		rnbq1rk1/ppp2pbp/3p1np1/3Pp3/2P1P3/2N1BP2/PP4PP/R2QKBNR b
E88	King's Indian Defense: Sämisch Variation, Closed Variation, 7...c6		rnbq1rk1/pp3pbp/2pp1np1/3Pp3/2P1P3/2N1BP2/PP4PP/R2QKBNR w
E89	King's Indian Defense: Sämisch Variation, Closed Variation, Main Line		rnbq1rk1/pp3pbp/3p1np1/3pp3/2P1P3/2N1BP2/PP2N1PP/R2QKB1R w
E90	King's Indian Defense: Larsen Variation		rnbq1rk1/ppp1ppbp/3p1np1/8/2PPP3/2N1BN2/PP3PPP/R2QKB1R b
E90	King's Indian Defense: Normal Variation, Rare Defenses		rnbqk2r/ppp1ppbp/3p1np1/8/2PPP3/2N2N2/PP3PPP/R1BQKB1R b
E90	King's Indian Defense: Zinnowitz Variation		rnbq1rk1/ppp1ppbp/3p1np1/6B1/2PPP3/2N2N2/PP3PPP/R2QKB1R b
E91	King's Indian Defense: Kazakh Variation		r1bq1rk1/ppp1ppbp/n2p1np1/8/2PPP3/2N2N2/PP2BPPP/R1BQK2R w
E91	King's Indian Defense: Orthodox Variation		rnbq1rk1/ppp1ppbp/3p1np1/8/2PPP3/2N2N2/PP2BPPP/R1BQK2R b
E92	King's Indian Defense: Exchange Variation		rnbq1rk1/ppp2pbp/3p1np1/4P3/2P1P3/2N2N2/PP2BPPP/R1BQK2R b
E92	King's Indian Defense: Orthodox Variation		rnbq1rk1/ppp2pbp/3p1np1/4p3/2PPP3/2N2N2/PP2BPPP/R1BQK2R w
E92	King's Indian Defense: Orthodox Variation, Gligoric-Taimanov System		rnbq1rk1/ppp2pbp/3p1np1/4p3/2PPP3/2N1BN2/PP2BPPP/R2QK2R b
E92	King's Indian Defense: Petrosian Variation		rnbq1rk1/ppp2pbp/3p1np1/3Pp3/2P1P3/2N2N2/PP2BPPP/R1BQK2R b
E92	King's Indian Defense: Petrosian Variation, Stein Defense		rnbq1rk1/1pp2pbp/3p1np1/p2Pp3/2P1P3/2N2N2/PP2BPPP/R1BQK2R w
E93	King's Indian Defense: Petrosian Variation, Keres Defense		r1bq1rk1/pppn1pb1/3p3p/3Pp1pn/2P1P2P/2N2NB1/PP2BPP1/R2QK2R b
E93	King's Indian Defense: Petrosian Variation, Normal Defense		r1bq1rk1/pppn1pbp/3p1np1/3Pp3/2P1P3/2N2N2/PP2BPPP/R1BQK2R w
E94	King's Indian Defense: Orthodox Variation		rnbq1rk1/ppp2pbp/3p1np1/4p3/2PPP3/2N2N2/PP2BPPP/R1BQ1RK1 b
E94	King's Indian Defense: Orthodox Variation, Donner Defense		rnbq1rk1/pp3pbp/2pp1np1/4p3/2PPP3/2N2N2/PP2BPPP/R1BQ1RK1 w
E94	King's Indian Defense: Orthodox Variation, Glek Defense		r1bq1rk1/ppp2pbp/n2p1np1/4p3/2PPP3/2N2N2/PP2BPPP/R1BQ1RK1 w
E94	King's Indian Defense: Orthodox Variation, Positional Defense		r1bq1rk1/pppn1pbp/3p1np1/4p3/2PPP3/2N2N2/PP2BPPP/R1BQ1RK1 w
E94	King's Indian Defense: Orthodox Variation, Positional Defense, Closed Line		r1bq1rk1/pppn1pbp/3p1np1/3Pp3/2P1P3/2N2N2/PP2BPPP/R1BQ1RK1 b
E94	King's Indian Defense: Orthodox Variation, Ukrainian Defense		rnbq1rk1/1pp2pbp/3p1np1/p3p3/2PPP3/2N2N2/PP2BPPP/R1BQ1RK1 w
E95	King's Indian, Orthodox, 7...Nbd7 8.Re1		r1bq1rk1/pppn1pbp/3p1np1/4p3/2PPP3/2N2N2/PP2BPPP/R1BQR1K1 b
E96	King's Indian Defense: Orthodox Variation, Positional Defense, Main Line		r1bq1rk1/1p1n1pbp/2pp1np1/p3p3/2PPP3/2N2N2/PP3PPP/R1BQRBK1 w
E97	King's Indian Defense: Orthodox Variation, Aronin-Taimanov Defense		r1bq1rk1/ppp2pbp/2np1np1/4p3/2PPP3/2N2N2/PP2BPPP/R1BQ1RK1 w
E97	King's Indian Defense: Orthodox Variation, Bayonet Attack		r1bq1rk1/ppp1npbp/3p1np1/3Pp3/1PP1P3/2N2N2/P3BPPP/R1BQ1RK1 b
E97	King's Indian Defense: Orthodox Variation, Bayonet Attack, Sokolov's Line		r1bq1rk1/ppp1npbp/3p2p1/3Pp2n/1PP1P3/2N2N2/P3BPPP/R1BQR1K1 b
E97	King's Indian Defense: Orthodox Variation, Bayonet Attack, Yepishin's Line		r1bq1rk1/ppp1npbp/3p2p1/3Pp2n/1PP1P3/2N2N2/P1Q1BPPP/R1B2RK1 b
E97	King's Indian Defense: Orthodox Variation, Korchnoi Attack		r1bq1rk1/ppp1npbp/3p1np1/3Pp3/2P1P3/2N2N2/PP1BBPPP/R2Q1RK1 b
E97	King's Indian Defense: Orthodox Variation, Modern System		r1bq1rk1/ppp1npbp/3p1np1/3Pp3/2P1P3/2N5/PP1NBPPP/R1BQ1RK1 b
E98	King's Indian Defense: Orthodox Variation, Classical System		r1bq1rk1/ppp1npbp/3p1np1/3Pp3/2P1P3/2N5/PP2BPPP/R1BQNRK1 b
E99	King's Indian Defense: Orthodox Variation, Classical System, Benko Attack		r1bq1rk1/pppnn1bp/3p2p1/3Ppp2/2P1P1P1/2N2P2/PP2B2P/R1BQNRK1 b
E99	King's Indian Defense: Orthodox Variation, Classical System, Kozul Gambit		r1bq1rk1/pppn2bp/3p2n1/2PPp1p1/4Pp2/2N2P2/PP2BBPP/2RQNRK1 b
E99	King's Indian Defense: Orthodox Variation, Classical System, Neo-Classical Line		r1bq1rk1/pppnnpbp/3p2p1/3Pp3/2P1P3/2N1B3/PP2BPPP/R2QNRK1 b
E99	King's Indian Defense: Orthodox Variation, Classical System, Traditional Line		r1bq1rk1/pppnn1bp/3p2p1/3Ppp2/2P1P3/2N2P2/PP2B1PP/R1BQNRK1 w
