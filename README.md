Ormulogun-ext
=============

External dependencies for [Ormulogun](https://gitlab.com/Artefact2/ormulogun).

Dependencies
============

* CMake
* LLVM, clang
* Emscripten

Credits
=======

See Ormulogun [`README.md`](https://gitlab.com/Artefact2/ormulogun/blob/staging/README.md#credits).
